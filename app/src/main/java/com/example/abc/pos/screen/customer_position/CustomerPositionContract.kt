package com.example.abc.pos.screen.customer_position

interface CustomerPositionContract {
    interface View{
        fun showPosition(positions : Int)
        fun returnOrder(position: Int)
    }
    interface Presenter{
        fun loadData(position : Int)
        fun returnOrder(position : Int)
    }
}