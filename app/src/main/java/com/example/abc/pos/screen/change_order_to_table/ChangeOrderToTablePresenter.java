package com.example.abc.pos.screen.change_order_to_table;

import android.os.AsyncTask;

import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Table;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class ChangeOrderToTablePresenter implements ChangeOrderToTableContract.Presenter {

    ChangeOrderToTableContract.View mView;


    public ChangeOrderToTablePresenter(ChangeOrderToTableContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void getEmptyTable(String username, String excludeTableName) {
        new GettingEmptyTableTask(username,excludeTableName).execute();
    }

    @Override
    public void goToCustomerPosition(String tableName) {
        new GetAllOrderTempListTask(tableName).execute();
    }

    private class GetAllOrderTempListTask extends AsyncTask<Void, Void, List<OrdersTemp>> {

        String tableName;

        public GetAllOrderTempListTask(String tableName) {
            this.tableName = tableName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<OrdersTemp> doInBackground(Void... voids) {

            JSONArray js = MySqlHandleUtils.selectStament("SELECT thutukhachtrenban FROM wapri.orders_temp where banso = " + "'" + tableName + "'");
            ArrayList<OrdersTemp> ordersTemps = new Gson().fromJson(js.toString(), new TypeToken<List<OrdersTemp>>() {
            }.getType());
            return ordersTemps;
        }

        @Override
        protected void onPostExecute(List<OrdersTemp> datas) {
            super.onPostExecute(datas);
            mView.dismissLoading();
            List<Integer> positionsHaveOrder = new ArrayList<>();
            for (OrdersTemp ob : datas) {
                if (!positionsHaveOrder.contains(Integer.valueOf(ob.getThutukhachtrenban()))) {
                    positionsHaveOrder.add(Integer.valueOf(ob.getThutukhachtrenban()));
                }
            }
            mView.goToCustomerPosition(positionsHaveOrder);
        }
    }

    private class GettingEmptyTableTask extends AsyncTask<Void, Void, ArrayList<Table>> {

        String username;
        String excludeTableName;

        GettingEmptyTableTask(String username, String excludeTableName){
            this.username = username;
            this.excludeTableName = excludeTableName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<Table> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT t.id, t.name, t.vitri, t.trangthai, t.soghengoi, t.username, t.tongtien, t.tabletype, t.status,giam," +
                    "t.tiengiam, t.tienthue19giam, t.tienthue7giam, t.taichohaymangve, t.thoigian, t.vitritronghayngoainha, t.orderfrom, t.from_tablet," +
                    "t.is_selling_on_tablet," +
                    "(select count(o.id) from wapri.orders_temp o where o.banso = t.name AND o.daprintchua = 'N' AND o.items_id NOT IN (1001,1005,10004) AND o.bophanphucvu <> 'Sonstiges0') as total_orders_not_printed, " +
                    "(select count(o.id) from wapri.orders_temp o where o.banso = t.name) as total_orders  FROM wapri.table t where t.trangthai = 'empty' or ( t.username = " + "'" + WapriManager.getUser().getUsername()  + "' AND t.name != " +"'" + excludeTableName + "')");

            ArrayList<Table> tables = new Gson().fromJson(js.toString(), new TypeToken<List<Table>>() {
            }.getType());
            for(Table table:tables){
                table.setTaichoHayMangVe();
            }
            return tables;
        }

        @Override
        protected void onPostExecute(ArrayList<Table> tables) {
            super.onPostExecute(tables);
            mView.dismissLoading();
            mView.showListTable(tables);
        }
    }
}
