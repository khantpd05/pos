package com.example.abc.pos.base;

import android.util.Log;

import com.example.abc.pos.R;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.model.Users;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.utils.Util;
import com.example.abc.pos.viewmodel.GeneralConfiguration;
import com.example.abc.pos.viewmodel.PaymentConfiguration;
import com.example.abc.pos.viewmodel.PrinterConfiguration;

import java.util.ArrayList;
import java.util.List;

public class WapriManager {
    /** CONSTANTS STRING **/
    public static String STORE_NAME = "";
    public static String MAC_ADDRESS = Util.getMacAddr();
    public static String DEVICE_NAME = "M"+MAC_ADDRESS.substring(MAC_ADDRESS.length()-5);
    public static final String TIEU_DE_DOT_DAT_ID = "1001";
    public static final String LAY_BAN_VA_THANH_TOAN = Util.getStringFromResouce(R.string.take_table_and_payment);
    public static final String DAT_MON_DUM =  Util.getStringFromResouce(R.string.order_for_table_top);
    public class Value {
        public static final int DEFAULT_LANGUAGE_ID = 0;
    }

    public class RequestCode {
        public static final int CHANGE_LANGUAGE = 10000;
    }

    /** Important variables of Wapri **/
    private static Users mUser;
    private static String time_zone;

    public static String getTime_zone() {
        return time_zone;
    }

    public static void setTime_zone(String time_zone) {
        WapriManager.time_zone = time_zone;
    }

    public static Users getUser() {
        return mUser;
    }

    public static void setUser(Users mUser) {
        WapriManager.mUser = mUser;
    }


    /** Enum **/
    public enum FeatureOrder {
        IN_BEP, GIA_TU_DO, GIAM, CHUYEN_SANG_BAN, DEM_VE, PHAN_AN, GHI_CHU_BEP, GIA_VI
    }

    public enum Taichohaymangve_ {

        TaiCho("TaiCho",Util.getStringFromResouce(R.string.stay_in)), MangVe("MangVe",Util.getStringFromResouce(R.string.bring_home)), GiaoHang("GiaoHang","Giao hàng"), Online("Online","Online"), Web("Web","Web");

        public String value;
        public String display;
        Taichohaymangve_(String value, String display) {
            this.value = value;
            this.display = display;
        }
    }

    public enum loai_mon_an{ // Assuming MyEnum is the name of the enum
        // enum values sample below
        KhaiVi("KhaiVi",Util.getStringFromResouce(R.string.appetizer)), Nuoc("Nuoc",Util.getStringFromResouce(R.string.water)), MonChinh("MonChinh",Util.getStringFromResouce(R.string.main_dish)), TrangMieng("TrangMieng",Util.getStringFromResouce(R.string.dessert));

        public String value;
        public String display;
        loai_mon_an(String value, String display) {
            this.value = value;
            this.display = display;
        }
    }

    public enum bophanphucvu{
        PhaChe("PhaChe","Pha chế"), PhaChe7("PhaChe7","Pha chế"), PhaChe19("PhaChe19",Util.getStringFromResouce(R.string.water_19)),Bep("Bep",Util.getStringFromResouce(R.string.kitchen)), Sushi("Sushi","Sushi"),
        Sonstiges0("Sonstiges0","Sonstiges");

        public String value;
        public String display;
        bophanphucvu(String value, String display) {
            this.value = value;
            this.display = display;
        }
    }

    public enum cachtra{
        TienMat("TienMat",Util.getStringFromResouce(R.string.Cash)), CreditCard("CreditCard",Util.getStringFromResouce(R.string.Credit_card)),None("CreditCard",Util.getStringFromResouce(R.string.Credit_card));
        public String value;
        public String display;
        cachtra(String value,String display) {
            this.value = value;
            this.display = display;
        }

    }

    public enum taichohaymangve{
        TaiCho, MangVe
    }

    public enum TrongNhaHayNgoaiNha{
        TrongNha, NgoaiNha
    }

    public enum KichCoMon{
        Lon("Lon",Util.getStringFromResouce(R.string.large)), Trung("Trung",Util.getStringFromResouce(R.string.medium)), Nho("Nho",Util.getStringFromResouce(R.string.small));
        public String value;
        public String display;
        KichCoMon(String value, String display) {
            this.value = value;
            this.display = display;
        }
    }

    public enum MoneyEuroType{
        CENT_1(1), CENT_2(2), CENT_5(5), CENT_10(10), CENT_20(20), CENT_50(50),
        EURO_1(100), EURO_2(200), EURO_5(500), EURO_10(1000), EURO_20(2000), EURO_50(5000), EURO_100(10000), EURO_200(20000);
        public long value;
        MoneyEuroType(int value){
            this.value = value;
        }
    }

    /** Standard of calculation **/
    public static double tinhTienThue19(double tongTienDaCoThue, int phanTramGiam){
        double thue = 1 - 100.0 / 119.0;
        double tienThue = tongTienDaCoThue * thue ;
        double result = tienThue - ((tienThue * phanTramGiam)/100);
        return result;
    }

    public static double tinhTienThue7(double tongTienDaCoThue, int phanTramGiam){
        double thue = 1 - 100.0 / 107.0;
        double tienThue= tongTienDaCoThue * thue ;
        double result = tienThue - ((tienThue * phanTramGiam)/100);
        return result;
    }

    public static double tinhTienThueGiam(double tienthue, int phanTramGiam){
        return ((tienthue*phanTramGiam)/100);
    }

    /* general setting */
    public static GeneralConfiguration configuration(){
        GeneralConfiguration configuration = SharedPrefs.getInstance().get(SharedPrefs.KEY_SETTING_GENERAL,GeneralConfiguration.class);
        if(configuration!=null){
            return configuration;
        }else{
            GeneralConfiguration config = new GeneralConfiguration();
            config.tuDongInBep.set(false);
            config.canhBaoThanhToan.set(false);
            config.huyThanhToan.set(false);
            config.tuDongKetNoi.set(false);
            config.showItemCode.set(true);
            config.otpLogin.set(false);

            SharedPrefs.getInstance().put(SharedPrefs.KEY_SETTING_GENERAL,config);
            return config;
        }
    }
    /* printer setting */
    public static PrinterConfiguration printerConfiguration(){
        PrinterConfiguration configuration = SharedPrefs.getInstance().get(SharedPrefs.KEY_SETTING_PRINTER,PrinterConfiguration.class);
        if(configuration!=null){
            return configuration;
        }else{
            PrinterConfiguration config = new PrinterConfiguration();
            config.charSet.set("2");
            config.maxChar.set("46");
            config.ip_bep.set("192.168.1.109");
            config.port_bep.set("9100");
            config.ip_nuoc.set("192.168.1.109");
            config.port_nuoc.set("9100");
            config.ip_sushi.set("192.168.1.109");
            config.port_sushi.set("9100");

            SharedPrefs.getInstance().put(SharedPrefs.KEY_SETTING_PRINTER,config);
            return config;
        }
    }

    /* payment setting */
    public static PaymentConfiguration paymentConfiguration(){
        PaymentConfiguration configuration = SharedPrefs.getInstance().get(SharedPrefs.KEY_SETTING_PAYMENT,PaymentConfiguration.class);
        if(configuration!=null){
            return configuration;
        }else{
            PaymentConfiguration config = new PaymentConfiguration();
            config.TIP.set(true);
            config.bewirtung.set(true);
            config.cash.set(true);
            config.credit_card.set(true);
            config.gutschein.set(true);
            config.restaurant_check.set(true);
            config.cancel.set(true);
            return config;
        }
    }

}
