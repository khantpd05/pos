package com.example.abc.pos.screen.tab_of_sale.menu;

import android.databinding.ObservableField;
import android.os.AsyncTask;
import android.util.Log;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.HappyHour_Detail;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.model.Table;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;


public class MenuTabPresenter implements MenuTabConstract.Presenter {

    MenuTabConstract.View mView;

    public MenuTabPresenter(MenuTabConstract.View view){
        mView = view;
        loadData();
    }

    @Override
    public void loadData() {
        if(SessionApp.menus_food!=null)
            mView.showListMenus(SessionApp.menus_food);

    }

    @Override
    public void getItems(String menu_id) {
        getItemsFromMenuListById(menu_id);
    }

    private void getItemsFromMenuListById(String menu_id){
        List<Items> food_list = new ArrayList<>();
        if(SessionApp.items_global != null && SessionApp.items_global.size() > 0){
            for(Items item : SessionApp.items_global){
                if(menu_id.equals(item.getNhomhang()+"")){
                    food_list.add(item);
                }
            }
            food_list.add(0,new Items());
            mView.showListItems(food_list);
        }
    }

//    private class MyTask extends AsyncTask<Void,Void,List<Menu>> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            mView.showLoading();
//        }
//
//        @Override
//        protected ArrayList<Menu> doInBackground(Void... voids) {
//            JSONArray js = MySqlHandleUtils.selectStament("select * from menu where id in (select nhomhang from items where bophanphucvu not like '%PhaChe%' group by nhomhang) ");
//            ArrayList<Menu> menus =  new Gson().fromJson(js.toString(), new TypeToken<List<Menu>>(){}.getType());
//            return menus;
//        }
//
//        @Override
//        protected void onPostExecute(List<Menu> menus) {
//            super.onPostExecute(menus);
//            mView.dismissLoading();
//            mView.showListMenus(menus);
//            SessionApp.menus_food = menus;
//        }
//    }
//
//
//
//    private class GettingItemsFromMenuTask extends AsyncTask<Void,Void,List<Items>> {
//        String menu_id;
//        public GettingItemsFromMenuTask(String menu_id){
//            this.menu_id = menu_id;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            mView.showLoading();
//        }
//
//        @Override
//        protected ArrayList<Items> doInBackground(Void... voids) {
//            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.items i" +
//                    " where " +menu_id+ " = i.nhomhang");
//            ArrayList<Items> items =  new Gson().fromJson(js.toString(), new TypeToken<List<Items>>(){}.getType());
//
//            for(Items ob: items){
//                ob.setGiaBan();
//            }
//
//            if(SessionApp.isHappyHour && !SessionApp.isAplyAllHappyHour){
//                if(SessionApp.happyHour_detail.size()>0){
//                    for(int i = 0; i < items.size();i++){
//                        for(int j = 0 ; j < SessionApp.happyHour_detail.size(); j++){
//                            if(items.get(i).getId().equals(SessionApp.happyHour_detail.get(j).getItems_id())){
////                                items.get(i).setGiale1(SessionApp.happyHour_detail.get(j).getGiahappyhour());
////                                items.get(i).setGiale2(SessionApp.happyHour_detail.get(j).getGiahappyhour2());
////                                items.get(i).setGiale3(SessionApp.happyHour_detail.get(j).getGiahappyhour3());
////                                items.get(i).setGialemangve1(SessionApp.happyHour_detail.get(j).getGiamangvehappyhour());
////                                items.get(i).setGialemangve2(SessionApp.happyHour_detail.get(j).getGiamangvehappyhour2());
////                                items.get(i).setGialemangve3(SessionApp.happyHour_detail.get(j).getGiamangvehappyhour3());
//                                items.get(i).setPhanTramGiam(SessionApp.happyHour.getPhantramgiam());
//                            }
//                        }
//                    }
//                }
//            }
//
//            return items;
//        }
//
//        @Override
//        protected void onPostExecute(List<Items> items) {
//            super.onPostExecute(items);
//            mView.dismissLoading();
//            Items firstOb = new Items();
//            firstOb.setTenhang(App.Companion.self().getString(R.string.Come_back));
//            items.add(0,firstOb);
//            mView.showListItems(items);
//        }
//    }


}
