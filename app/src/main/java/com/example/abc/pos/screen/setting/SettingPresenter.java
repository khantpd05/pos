package com.example.abc.pos.screen.setting;

import android.databinding.ObservableField;

import com.example.abc.pos.R;
import com.example.abc.pos.utils.Util;

public class SettingPresenter implements SettingConstract.Presenter{

    SettingConstract.View mView;

    public ObservableField<Boolean> isHavePermission = new ObservableField<>(false);
    public ObservableField<String> title = new ObservableField<>("");
    public ObservableField<String> language = new ObservableField<>("");
    public ObservableField<String> items = new ObservableField<>("");
    public ObservableField<String> theNextOrder = new ObservableField<>("");
    public ObservableField<String> deviceName = new ObservableField<>("");
    public ObservableField<String> generalSetting = new ObservableField<>("");
    public ObservableField<String> payment = new ObservableField<>("");
    public ObservableField<String> connection = new ObservableField<>("");
    public ObservableField<String> printer = new ObservableField<>("");
    public ObservableField<String> exit = new ObservableField<>("");

    public SettingPresenter(SettingConstract.View mView) {
        this.mView = mView;
        settingLanguage();
    }

    @Override
    public void goSettingConnection() {
        mView.goSettingConnection();
    }

    @Override
    public void goSettingMenu() {
        mView.goSettingMenu();
    }

    @Override
    public void goSettingNextOrder() {
        mView.goSettingNextOrder();
    }

    @Override
    public void goSettingNumberDevice() {
        mView.goSettingNumberDevice();
    }

    @Override
    public void goSettingGeneral() {
        mView.goSettingGeneral();
    }

    @Override
    public void goSettingLanguage() {
        mView.goSettingLanguage();
    }

    @Override
    public void goSettingPayment() {
        mView.goSettingPayment();
    }

    @Override
    public void goSettingPrinter() {
        mView.goSettingPrinter();
    }

    @Override
    public void goBack() {
        mView.goBack();
    }

    @Override
    public void showSettingLanguage() {
        mView.showSettingLanguage();
    }

    @Override
    public void settingLanguage() {
        title.set(Util.getStringFromResouce(R.string.Setting));
        language.set(Util.getStringFromResouce(R.string.Language));
        items.set(Util.getStringFromResouce(R.string.Items));
        theNextOrder.set(Util.getStringFromResouce(R.string.The_next_order));
        deviceName.set(Util.getStringFromResouce(R.string.device_name));
        generalSetting.set(Util.getStringFromResouce(R.string.general_settings));
        connection.set(Util.getStringFromResouce(R.string.Connection));
        payment.set(Util.getStringFromResouce(R.string.Payment));
        exit.set(Util.getStringFromResouce(R.string.Exit));
        printer.set(Util.getStringFromResouce(R.string.printer));
    }



}
