package com.example.abc.pos.screen.change_order_to_customer_position

interface CustomerPositionCurrentTableContract {
    interface View{
        fun showAlert(data: String)
        fun showPosition(positions : Int)
        fun returnOrder()
        fun goToCustomerPositionSeparate()
    }
    interface Presenter{
        fun loadData(position : Int)
        fun goToCustomerPositionSeparate()

    }
}