package com.example.abc.pos.screen.payment_detail;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.databinding.FragmentPaymentDetailBinding;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.screen.divide_bill.DivideBillFragment;
import com.example.abc.pos.screen.guschein_dialog.GuscheinFragment;
import com.example.abc.pos.screen.keyboard.KeyboardFragment;
import com.example.abc.pos.screen.payment.PaymentFragment;
import com.example.abc.pos.screen.restaurant_check_dialog.RestaurantCheckFragment;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class PaymentDetailFragment extends BaseFragment implements  PaymentDetailContract.View,
        KeyboardFragment.OnKeyboardPress,GuscheinFragment.OnGuscheinSelected, RestaurantCheckFragment.OnItemRestaurantCheckSelected {

    private String summer_price;
    private List<OrdersTemp> origOrderTemp_list;
    private List<OrdersTemp> divideOrderTemp_list;
    private String order_id;
    private PaymentDetailPresenter presenter;
    private Button btn_bewirtung;
    private OnPaymentFinish listener;
    private OnPaymentCancel cancel_listener;
    private Fragment keyboardFragment;
    private RestaurantCheckFragment restaurantCheckFragment;
    private GuscheinFragment guscheinFragment;
    public interface OnPaymentFinish{
        void onPaymentFinishListener(List<OrdersTemp> origOrderTemp_list);
    }

    public interface OnPaymentCancel{
        void onPaymentCancelListener();
    }

    public PaymentDetailFragment(){

    }

    public PaymentDetailFragment(String order_id,List<OrdersTemp> origOrderTemp_list,List<OrdersTemp> divideOrderTemp_list,String summer_price,OnPaymentFinish listener,OnPaymentCancel cancel_listener) {
        // Required empty public constructor
        this.order_id = order_id;
        this.origOrderTemp_list = origOrderTemp_list;
        this.divideOrderTemp_list = divideOrderTemp_list;
        this.summer_price = summer_price;
        this.listener = listener;
        this.cancel_listener = cancel_listener;
    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_payment_detail;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        FragmentPaymentDetailBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_payment_detail, container, false);
        view = binding.getRoot();

        if(savedInstanceState!=null){
            for(int i = 0; i < 1 ; i++){
                Util.popFragment(getActivity());
            }
        }else{
            btn_bewirtung = view.findViewById(R.id.btn_bewirtung);
            presenter = new PaymentDetailPresenter(this);
            presenter.loadData(order_id,origOrderTemp_list,divideOrderTemp_list,summer_price);
            binding.setPresenter(presenter);
        }


        return view;
    }

    @Override
    public Activity getRunUI() {
        return getActivity();
    }

    @Override
    public void notifyAdapter() {
        if(DivideBillFragment.orderDivideAdapter!=null)
        DivideBillFragment.orderDivideAdapter.notifyDataSetChanged();
        if(DivideBillFragment.orderOrigAdapter!=null)
        DivideBillFragment.orderOrigAdapter.notifyDataSetChanged();
    }

    @Override
    public void showToast(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(),data, Toast.LENGTH_SHORT);
    }

    @Override
    public void showKeyboard(String title,String hint) {
        keyboardFragment = new KeyboardFragment(this, title, hint,"00",SessionApp.KeyboardType.TIEN);
        ((KeyboardFragment) keyboardFragment).show(getFragmentManager(), null);
    }



    @Override
    public void showGuschein() {
        guscheinFragment = new GuscheinFragment(this);
        guscheinFragment.show(getFragmentManager(), null);
    }

    @Override
    public void showRestaurantCheck() {
        restaurantCheckFragment = new RestaurantCheckFragment(this);
        restaurantCheckFragment.show(getFragmentManager(), null);
    }

    @Override
    public void showAlert(String data) {
        Util.showCustomDialog(getContext(), data);
    }

    @Override
    public void returnDivide() {
        for (int i = 0 ; i < 2; i++){
            if(getActivity()!=null)
            Util.popFragment(getActivity());
        }
    }



    @Override
    public void returnTable(int lengthPop) {
        for (int i = 0 ; i < lengthPop; i++){
            if(getActivity()!=null)
            Util.popFragmentExcept(getActivity(),"TableFragment");
        }

    }

    @Override
    public void onKeyPressListener(String data) {
        presenter.updateSummerPrice(data);
    }

    @Override
    public void onGuscheinSelectedListener(String data) {
        presenter.updateGuschein(data);
    }
    Dialog dialog;
    @Override
    public void showCustomDialog(String content, View.OnClickListener listener) {
        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.alert_dialog);
        TextView tv_title, tv_content;
        tv_title = dialog.findViewById(R.id.tv_title);
        tv_content = dialog.findViewById(R.id.tv_content);

        tv_title.setText(App.Companion.self().getString(R.string.Warning));
        tv_content.setText(content);

        LinearLayout ll_wrap_btn = dialog.findViewById(R.id.ll_wrap_btn);
        ll_wrap_btn.setVisibility(View.VISIBLE);

        Button btn_ok = dialog.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(listener);
        Button btn_cancel = dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void dismissDialog() {
        dialog.dismiss();
    }

    @Override
    public void setBewirtungButton(boolean isChecked) {
        if(isChecked)
        btn_bewirtung.setBackground(getContext().getResources().getDrawable(R.drawable.bg_btn_xanhdam_vien));
        else btn_bewirtung.setBackground(getContext().getResources().getDrawable(R.drawable.bg_btn_xanhdam));
    }

    @Override
    public void clearDivideList() {
        listener.onPaymentFinishListener(presenter.getOrdertempList());
    }

    @Override
    public void cancelPayment() {

        cancel_listener.onPaymentCancelListener();
    }

    @Override
    public void onItemRestaurantCheckSelectedListener(String data) {
        presenter.updateRestaurantCheck(data);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("order_id",order_id);
    }

    @Override
    public void onPause() {
        super.onPause();
        if ((keyboardFragment) != null)
            ((KeyboardFragment) keyboardFragment).dismiss();
        if(guscheinFragment!=null)
            guscheinFragment.dismiss();
        if(restaurantCheckFragment!=null)
            restaurantCheckFragment.dismiss();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if ((keyboardFragment) != null)
            ((KeyboardFragment) keyboardFragment).dismiss();
        if(guscheinFragment!=null)
            guscheinFragment.dismiss();
        if(restaurantCheckFragment!=null)
            restaurantCheckFragment.dismiss();
    }

}
