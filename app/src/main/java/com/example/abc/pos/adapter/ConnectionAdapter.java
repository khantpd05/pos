package com.example.abc.pos.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Connection;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.utils.Constants;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.utils.Util;

import java.util.List;

public class ConnectionAdapter extends RecyclerView.Adapter<ConnectionAdapter.ViewHolder> {
    private RecyclerView rvList;
    private List<Connection> datas;
    private ItemClickListener mClickListener;
    private int selectedPosition = -1;
    private int selectedPosition2 = -1;
    private boolean isBackground = false;
    private boolean isboth = false;
    private Context context;

    public interface ItemClickListener {
        void onItemClick(Connection connection);

        void onItemLongClick(Connection connection, int position);
    }


    public ConnectionAdapter(RecyclerView rc, Context context, List<Connection> list, ItemClickListener clickListener) {
        rvList = rc;
        datas = list;
        this.context = context;
        mClickListener = clickListener;
    }

    public void setNewDatas(List<Connection> list) {
        datas = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.connection_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_ip.setText(datas.get(position).getIp());
        holder.tv_ip.setTag(position);
        holder.cb_isConnected.setTag(position);
        holder.img_status.setTag(position);

        if (position == selectedPosition2) {

            holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_ODD));
        } else {
            holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));
        }

        if(!isBackground){
            if(selectedPosition!= -1){
                if(position == selectedPosition){
                    holder.cb_isConnected.setChecked(true);
                }else{
                    holder.cb_isConnected.setChecked(false);
                    datas.get(position).setConnected(false);
                    SharedPrefs.getInstance().addConnectionSaveDraft(datas.get(position));
                }
            }else{
                if(datas.get(position).isConnected()){
                    MySqlHandleUtils.getInstance().disconnectToMySql();
                    new MyTask(position, holder.img_status).execute();
                    holder.cb_isConnected.setChecked(datas.get(position).isConnected());
                }

            }


            if (position == selectedPosition) {
                if (datas.get(position).isConnected()) {

                } else {
                    GradientDrawable backgroundGradient = (GradientDrawable) holder.img_status.getBackground();
                    backgroundGradient.setColor(Color.GRAY);
                }
            } else {
                GradientDrawable backgroundGradient = (GradientDrawable) holder.img_status.getBackground();
                backgroundGradient.setColor(Color.GRAY);
            }
        }


    }

    @Override
    public int getItemCount() {
        return datas.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView tv_ip;
        RadioButton cb_isConnected;
        ImageView img_status;
        LinearLayout ll_parent;

        ViewHolder(View itemView) {
            super(itemView);
            tv_ip = itemView.findViewById(R.id.tv_ip);
            cb_isConnected = itemView.findViewById(R.id.cb_isConnected);
            img_status = itemView.findViewById(R.id.img_status);
            ll_parent = itemView.findViewById(R.id.ll_parent);
            cb_isConnected.setOnClickListener(this);
            ll_parent.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            View containingView = rvList.findContainingItemView(view);
            int position = rvList.getChildAdapterPosition(containingView);


            if (position == RecyclerView.NO_POSITION) {
                return;
            }
            if (view.getId() == R.id.cb_isConnected) {
                MySqlHandleUtils.getInstance().disconnectToMySql();
                new MyTask(position, img_status).execute();
                itemCheckChanged(cb_isConnected);
                mClickListener.onItemClick(datas.get(position));
            } else if (view.getId() == R.id.ll_parent) {
                itemCheckChanged2(cb_isConnected);
                mClickListener.onItemClick(datas.get(position));
            }


//            if (mClickListener != null) {
//                mClickListener.onItemClick(view, img_status, datas.get(getAdapterPosition()));
//            }
        }

        @Override
        public boolean onLongClick(View view) {

            View containingView = rvList.findContainingItemView(view);
            int position = rvList.getChildAdapterPosition(containingView);

            mClickListener.onItemLongClick(datas.get(position), position);
            return false;
        }
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        selectedPosition2 = (Integer) v.getTag();
        isBackground = false;
        notifyDataSetChanged();
    }

    private void itemCheckChanged2(View v) {
        selectedPosition2 = (Integer) v.getTag();
        isBackground = true;
        notifyDataSetChanged();
    }

    private class MyTask extends AsyncTask<String, Void, String> {
        ImageView img;
        int position;

        public MyTask(int position, ImageView img) {
            this.img = img;
            this.position = position;
        }

        @Override
        protected String doInBackground(String... integers) {
            String status = MySqlHandleUtils.getInstance().connectToMySql(datas.get(position).getIp());

            return status;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(position<datas.size()){
                if (result.equals("ok")) {
                    datas.get(position).setConnected(true);
                    SharedPrefs.getInstance().addConnectionSaveDraft(datas.get(position));
                    GradientDrawable backgroundGradient = (GradientDrawable) img.getBackground();
                    backgroundGradient.setColor(Color.GREEN);
//                Toast.makeText(App.Companion.self(), Util.getStringFromResouce(R.string.Connect_successful), Toast.LENGTH_SHORT).show();
                    if(context!=null)
                        KToast.show((Activity) context, Util.getStringFromResouce(R.string.Connect_successful), Toast.LENGTH_SHORT);
                } else {
                    datas.get(position).setConnected(false);
                    GradientDrawable backgroundGradient = (GradientDrawable) img.getBackground();
                    backgroundGradient.setColor(Color.GRAY);
                    if(context!=null)
                        KToast.show((Activity) context, Util.getStringFromResouce(R.string.Connect_failed), Toast.LENGTH_SHORT);
                }
            }

        }
    }
}
