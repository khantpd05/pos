package com.example.abc.pos.viewmodel;

import android.databinding.ObservableField;

import com.example.abc.pos.model.Configuration;

import java.util.ArrayList;
import java.util.List;

public class GeneralConfiguration {
    public ObservableField<Boolean> tuDongInBep = new ObservableField<>(false);
    public ObservableField<List<Configuration>> gutscheins = new ObservableField<>();
    public ObservableField<List<Configuration>> restaurantCheck = new ObservableField<>();
    public ObservableField<Boolean> canhBaoThanhToan = new ObservableField<>(true);
    public ObservableField<Boolean> huyThanhToan = new ObservableField<>(true);
    public ObservableField<Boolean> tuDongKetNoi = new ObservableField<>(true);
    public ObservableField<Boolean> showItemCode = new ObservableField<>(true);
    public ObservableField<Boolean> otpLogin = new ObservableField<>(true);

    public GeneralConfiguration() {
    }


}
