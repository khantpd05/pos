package com.example.abc.pos.screen.payment_detail;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.example.abc.pos.model.Orders;
import com.example.abc.pos.model.OrdersTemp;

import java.util.List;

public interface PaymentDetailContract {
    interface View{
        Activity getRunUI();
        void notifyAdapter();
        void showToast(String data);
        void showKeyboard(String title,String hint);
        void showGuschein();
        void showLoading();
        void dismissLoading();
        void showRestaurantCheck();
        void showAlert(String data);
        void returnDivide();
        void returnTable(int lengthPop);
        void showCustomDialog(String content, android.view.View.OnClickListener listener);
        void dismissDialog();
        void setBewirtungButton(boolean isChecked);
        void clearDivideList();
        void cancelPayment();
    }
    interface Presenter{
        void loadData(String order_id,List<OrdersTemp> origOrderTem_id,List<OrdersTemp> divideOrderTemp_list,String summer_price);
        void createOrder(double giatien);
        void createOrderDetail(OrdersTemp ordersTemp, Orders orders);
        void paymentByCash();
        void paymentByCashNotBon();
        void paymentByCreditCard();
        void showGutschien();
        void showRestaurantCheck();
        void TIP();
        void restaurantCheck();
        void updateSummerPrice(String data);
        void updateGuschein(String data);
        void updateRestaurantCheck(String data);
        void exit();
        void checkBewirtung();
        List<OrdersTemp> getOrdertempList();
    }
}
