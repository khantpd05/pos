package com.example.abc.pos.model;

public class UsersTienbandauTemplate {

    private int id;
    private String username;
    private String pos_no;
    private Integer eur500;
    private Integer eur200;
    private Integer eur100;
    private Integer eur50;
    private Integer eur20;
    private Integer eur10;
    private Integer eur5;
    private Integer eur2;
    private Integer eur1;
    private Integer cent50;
    private Integer cent20;
    private Integer cent10;
    private Integer cent5;
    private Integer cent2;
    private Integer cent1;
    private int totalnumbers;
    private Double totalmoney;
    private Byte offset;


    public UsersTienbandauTemplate() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPos_no() {
        return pos_no;
    }

    public void setPos_no(String pos_no) {
        this.pos_no = pos_no;
    }

    public Integer getEur500() {
        return eur500;
    }

    public void setEur500(Integer eur500) {
        this.eur500 = eur500;
    }

    public Integer getEur200() {
        return eur200;
    }

    public void setEur200(Integer eur200) {
        this.eur200 = eur200;
    }

    public Integer getEur100() {
        return eur100;
    }

    public void setEur100(Integer eur100) {
        this.eur100 = eur100;
    }

    public Integer getEur50() {
        return eur50;
    }

    public void setEur50(Integer eur50) {
        this.eur50 = eur50;
    }

    public Integer getEur20() {
        return eur20;
    }

    public void setEur20(Integer eur20) {
        this.eur20 = eur20;
    }

    public Integer getEur10() {
        return eur10;
    }

    public void setEur10(Integer eur10) {
        this.eur10 = eur10;
    }

    public Integer getEur5() {
        return eur5;
    }

    public void setEur5(Integer eur5) {
        this.eur5 = eur5;
    }

    public Integer getEur2() {
        return eur2;
    }

    public void setEur2(Integer eur2) {
        this.eur2 = eur2;
    }

    public Integer getEur1() {
        return eur1;
    }

    public void setEur1(Integer eur1) {
        this.eur1 = eur1;
    }

    public Integer getCent50() {
        return cent50;
    }

    public void setCent50(Integer cent50) {
        this.cent50 = cent50;
    }

    public Integer getCent20() {
        return cent20;
    }

    public void setCent20(Integer cent20) {
        this.cent20 = cent20;
    }

    public Integer getCent10() {
        return cent10;
    }

    public void setCent10(Integer cent10) {
        this.cent10 = cent10;
    }

    public Integer getCent5() {
        return cent5;
    }

    public void setCent5(Integer cent5) {
        this.cent5 = cent5;
    }

    public Integer getCent2() {
        return cent2;
    }

    public void setCent2(Integer cent2) {
        this.cent2 = cent2;
    }

    public Integer getCent1() {
        return cent1;
    }

    public void setCent1(Integer cent1) {
        this.cent1 = cent1;
    }

    public int getTotalnumbers() {
        return totalnumbers;
    }

    public void setTotalnumbers(int totalnumbers) {
        this.totalnumbers = totalnumbers;
    }

    public Double getTotalmoney() {
        return totalmoney;
    }

    public void setTotalmoney(Double totalmoney) {
        this.totalmoney = totalmoney;
    }

    public Byte getOffset() {
        return offset;
    }

    public void setOffset(byte offset) {
        this.offset = offset;
    }
}
