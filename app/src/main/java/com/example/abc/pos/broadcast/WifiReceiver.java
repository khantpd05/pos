package com.example.abc.pos.broadcast;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Connection;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.utils.Util;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.List;

public class WifiReceiver  extends BroadcastReceiver {

    private final static String TAG = WifiReceiver.class.getSimpleName();
    private boolean wifi_isEnable = false;
    @Override
    public void onReceive(final Context context, final Intent intent) {
        int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);

        if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(intent.getAction())
                && WifiManager.WIFI_STATE_ENABLED == wifiState) {
            new Thread(new Runnable() {
                @Override
                public void run() {
//                    tryToConnect(context);
                    SessionApp.WIFI_STATE = 1;
                }
            }).start();
        }else{
            SessionApp.WIFI_STATE = 0;
            MySqlHandleUtils.getInstance().disconnectToMySql();
        }
    }

    boolean isConnected = false;
    void tryToConnect(Context context){
            try {
                int timeoutMs = 1500;
                Socket sock = new Socket();
                SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);
                sock.connect(sockaddr, timeoutMs);
                sock.close();
                SessionApp.WIFI_STATE = 1;
                connectToDb(context);
                isConnected = true;
            } catch (Exception e) {
                isConnected = false;
                while (!isConnected){
                    tryToConnect( context);
                }
            }
    }


    void connectToDb(Context context) {
        if(MySqlHandleUtils.getInstance().getConnection()==null){
            List<Connection> connectionList = SharedPrefs.getInstance().getIpListSharePreference();
            MySqlHandleUtils.getInstance().disconnectToMySql();
            if(connectionList!=null){
                for(Connection connection: connectionList){
                    if(connection.isConnected()){
                        new ConnectToDbTask(context,connection.getIp()).execute();
                        break;
                    }
                }
            }
        }

    }



    private class ConnectToDbTask extends AsyncTask<String, Void, String> {
        Context context;
        String ip;

        public ConnectToDbTask(Context context,String ip) {
            this.context = context;
            this.ip = ip;
        }

        @Override
        protected String doInBackground(String... strings) {
            MySqlHandleUtils.getInstance();
            return MySqlHandleUtils.getInstance().connectToMySql(ip);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equals("ok")) {
                if(context!=null)
                    KToast.show((Activity) context, Util.getStringFromResouce(R.string.Connect_successful), Toast.LENGTH_SHORT);
            } else {
                if(context!=null)
                    KToast.show((Activity) context, Util.getStringFromResouce(R.string.Connect_failed), Toast.LENGTH_SHORT);
            }
        }
    }
}