package com.example.abc.pos.screen.kich_co_mon_dialog;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.KichCoMonAdapter;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.base.WapriManager.KichCoMon;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.KichCoMonAn;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class KichCoMonDialogFragment extends DialogFragment implements KichCoMonAdapter.ItemClickListener {

    RecyclerView rc_typeSize;
    KichCoMonAdapter adapter;
    OnItemKichCoMonSelected listener;
    Items item;
    int soLuong;
    WapriManager.taichohaymangve taichohaymangve;
    public interface OnItemKichCoMonSelected{
        void onItemKichCoMonSelectedListener(Items item, int soLuong);
    }
    public KichCoMonDialogFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public KichCoMonDialogFragment(Items item,WapriManager.taichohaymangve taichohaymangve,int soLuong, OnItemKichCoMonSelected listener) {
        this.item = item;
        this.taichohaymangve = taichohaymangve;
        this.soLuong = soLuong;
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment.

        return inflater.inflate(R.layout.fragment_kich_co_mon_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rc_typeSize = view.findViewById(R.id.rc_typeSize);
        List<KichCoMonAn> datas = new ArrayList<>();
        if(SessionApp.global_table.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho){
            for(Items.GiaBanLe giaBanLe: item.getGiaBanLeList()){
                datas.add(new KichCoMonAn(giaBanLe.getKichCoMon().value,giaBanLe.getKichCoMon().display,giaBanLe.getGiaBanLe()));
            }
        }else{
            for(Items.GiaBanLe giaBanLe: item.getGiaBanLeMangVeList()){
                datas.add(new KichCoMonAn(giaBanLe.getKichCoMon().value,giaBanLe.getKichCoMon().display,giaBanLe.getGiaBanLe()));
            }
        }

        adapter = new KichCoMonAdapter(getContext(), datas,this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rc_typeSize.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                linearLayoutManager.getOrientation());
        rc_typeSize.addItemDecoration(dividerItemDecoration);
        rc_typeSize.setAdapter(adapter);
    }



    @Override
    public void onResume() {
        super.onResume();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        getDialog().getWindow().setLayout((6 * width)/7, RelativeLayout.LayoutParams.WRAP_CONTENT);

    }

    @Override
    public void onItemClick(int position, KichCoMonAn kichCoMon) {
        if(taichohaymangve == WapriManager.taichohaymangve.TaiCho){
            if(kichCoMon.getKichCoMon().equals(KichCoMon.Lon.value)){
                item.setKichcomonan("Lon");
                item.setGiale(item.getGiale1());
            }else if(kichCoMon.getKichCoMon().equals(KichCoMon.Trung.value)){
                item.setKichcomonan("Trung");
                item.setGiale(item.getGiale2());
            }else{
                item.setKichcomonan("Nho");
                item.setGiale(item.getGiale3());
            }
        }else{
            if(kichCoMon.getKichCoMon().equals(KichCoMon.Lon.value)){
                item.setKichcomonan("Lon");
                item.setGialemangve(item.getGialemangve1());
            }else if(kichCoMon.getKichCoMon().equals(KichCoMon.Trung.value)){
                item.setKichcomonan("Trung");
                item.setGialemangve(item.getGialemangve2());
            }else{
                item.setKichcomonan("Nho");
                item.setGialemangve(item.getGialemangve3());
            }
        }


        listener.onItemKichCoMonSelectedListener(item,soLuong);
        getDialog().dismiss();
    }
}
