package com.example.abc.pos.screen.separate_order_to_table;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.adapter.TablesAdapter;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.screen.change_order_to_customer_position.CustomerPositionCurrentTableFragment;
import com.example.abc.pos.screen.change_order_to_table.ChangeOrderToTableFragment;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SeparateOrderToTableFragment extends BaseFragment implements SeparateOrderToTableContract.View,
        TablesAdapter.ItemClickListener, View.OnClickListener, CustomerPositionCurrentTableFragment.OnSeparateOrderToTableSuccessful {

    private RecyclerView rc_table;

    private TablesAdapter table_adapter;

    private SeparateOrderToTablePresenter presenter;

    private TextView btn_okay, tv_title;

    private Table table;
    private Table table_new;

    private String username;
    private String currentTable;
    private String selectedTable;
    private int soghengoi;
    private List<String> orderIds;
    private WapriManager.taichohaymangve taiChoHayMangVe;

    private LinearLayout btn_exit;

    private OnSeparateOrderToTableSuccessful listener;


    public SeparateOrderToTableFragment() {
        // Required empty public constructor
    }



    public interface OnSeparateOrderToTableSuccessful {
        void onSeparateOrderToTableSuccessfulListener(List<OrdersTemp> ordersTempsWillChange);
    }

    @SuppressLint("ValidFragment")
    public SeparateOrderToTableFragment(String username, String currentTable, OnSeparateOrderToTableSuccessful listener) {
        // Required empty public constructor
        this.username = username;
        this.currentTable = currentTable;
        this.listener = listener;
    }

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_change_to_table;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_to_table, container, false);

        if(savedInstanceState!=null){
            Util.popFragment(getActivity());
        }else{
            rc_table = view.findViewById(R.id.rc_table);

            rc_table.setLayoutManager(new GridLayoutManager(getContext(),4));
            table_adapter = new TablesAdapter(getContext(),new ArrayList<Table>(),this);
            rc_table.setAdapter(table_adapter);

            tv_title = view.findViewById(R.id.tv_title);
            tv_title.setText(App.Companion.self().getString(R.string.The_table_want_to_move));

            btn_okay = view.findViewById(R.id.btn_okay);
            btn_okay.setOnClickListener(this);

            btn_exit = view.findViewById(R.id.btn_exit);
            btn_exit.setOnClickListener(this);

            presenter = new SeparateOrderToTablePresenter(this);
            presenter.getEmptyTable(username,currentTable);
        }



        return view;
    }


    @Override
    public void showListTable(List<Table> tables) {
        table_adapter.setNewDatas(tables);
    }

    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
        if(getActivity()!=null)
        Util.popFragment(getActivity());
    }

    @Override
    public void goToCustomerPositionOfGoTable() {
        ((BaseActivity) getActivity()).pushFragmentNotReplace(new CustomerPositionCurrentTableFragment(taiChoHayMangVe,this), true);
    }


    @Override
    public void onItemClick(View view, Table table) {
        taiChoHayMangVe = table.getTaichohaymangve();
        SessionApp.global_destination_table = table;
        SessionApp.TABLE_NAME_WILL_COME = table.getName();
        SessionApp.CUSTOMER_POSITIONS_SEPARATE_TABLE = table.getSoghengoi();
        btn_okay.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_okay:
                presenter.goToCustomerPositionOfGoTable();
                break;
            case R.id.btn_exit:
                Util.popFragment(getActivity());
                break;
        }
    }


    @Override
    public void onSeparateOrderToTableSuccessfulListener(@NotNull List<OrdersTemp> ordersTempListWillChange) {
        listener.onSeparateOrderToTableSuccessfulListener(ordersTempListWillChange);
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("key","temp");
    }
}
