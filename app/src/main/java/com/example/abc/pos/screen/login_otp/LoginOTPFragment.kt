package com.example.abc.pos.screen.login_otp


import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.TELEPHONY_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.example.abc.pos.App

import com.example.abc.pos.R
import com.example.abc.pos.base.BaseActivity
import com.example.abc.pos.base.BaseFragment
import com.example.abc.pos.databinding.FragmentLoginBinding
import com.example.abc.pos.screen.setting.SettingFragment
import com.example.abc.pos.screen.table.TableFragment
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.TextView
import com.example.abc.pos.base.SessionApp
import com.example.abc.pos.base.WapriManager
import com.example.abc.pos.databinding.FragmentLoginOtpBinding
import com.example.abc.pos.screen.keyboard.KeyboardFragment
import com.example.abc.pos.screen.login.LoginFragment
import com.example.abc.pos.screen.money_box.MoneyBoxFragment
import com.example.abc.pos.utils.*

class LoginOTPFragment : BaseFragment(), LoginOTPConstract.View, KeyboardFragment.OnKeyboardPress {


    private var REQUEST_READ_PHONE_STATE = 1
    private var REQUEST_WRITE_EXTERNAL_STORAGE = 1
    private var imei = ""

    var loginPresenter: LoginOTPPresenter? = null
    private val KEY_DEVICE_NAME = "device_named"

    override fun getFragmentLayoutId(): Int {
        return R.layout.fragment_login
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        setLanguage()
        getDeviceInfo()
        var mBinding: FragmentLoginOtpBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login_otp, container, false)
        var view = mBinding.root

        checkWifi()


        loginPresenter = LoginOTPPresenter(this)
        mBinding.presenter = loginPresenter
        getDeviceName()
        return view
    }

    override fun getDeviceId(): String {
        getDeviceInfo()
        return imei
    }


    fun getDeviceInfo() {

//        var manager = context?.getSystemService(Context.WIFI_SERVICE) as WifiManager
//        var info = manager.connectionInfo
//        var address = Util.getMacAddr()
//        imei = address
//        Log.d("////", imei)

        if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(activity!!,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        REQUEST_WRITE_EXTERNAL_STORAGE)
            } else {
                ActivityCompat.requestPermissions(activity!!,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        REQUEST_WRITE_EXTERNAL_STORAGE)
            }
        }

    }


    override fun showToast(data: String) {
        if (activity != null)
            KToast.show(activity!!,data, Toast.LENGTH_SHORT)
    }

    override fun goTables() {
        (activity as? BaseActivity)?.pushFragment(TableFragment(), true)
    }

    override fun goSetting(isHavePermission: Boolean) {
        (activity as? BaseActivity)?.pushFragment(SettingFragment(isHavePermission), true)
    }

    override fun goAny() {


    }

    override fun goMoneyBox() {
        (activity as BaseActivity).pushFragment(MoneyBoxFragment(), true)
    }

    override fun alertDialog(data: String?) {
        Util.showCustomDialog(context, data)
    }

    override fun closeApp() {
        var intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        startActivity(intent)
        val pid = android.os.Process.myPid()
        android.os.Process.killProcess(pid)
    }

    override fun checkWifi() {
        val connManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)

        if (mWifi.isConnected) {
            SessionApp.WIFI_STATE = 1
        } else {
            SessionApp.WIFI_STATE = 0
        }
    }

    private fun getDeviceName() {
        val device_name = SharedPrefs.getInstance().get(KEY_DEVICE_NAME, String::class.java)
        if (device_name != null && !device_name.isEmpty()) {
            WapriManager.DEVICE_NAME = device_name
        }
    }

    override fun onResume() {
        super.onResume()
        if (!WapriManager.configuration().otpLogin.get()!!) {
            (activity as BaseActivity).pushFragmentNotBackStack(LoginFragment(), Constants.TransitionType.NONE, true)
        }
        setLanguage()
    }

    private fun setLanguage() {
        LanguageUtils.loadLocale()
    }

    override fun logout() {
        Util.showCustomDialogConfirm(context, App.self().getString(R.string.are_u_s_u_w_t_logout), object : Util.ConfirmDialogInterface {
            override fun onPositiveClickListener() {
                loginPresenter?.logout()
            }

            override fun onNegativeClickListener() {

            }
        })
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_READ_PHONE_STATE -> {

                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    override fun showOTPCode(otp: String) {
        Util.showCustomDialog(context, Util.getStringFromResouce(R.string.your_otp) + ": " + otp)
    }

    override fun showKeyboard() {

        var keyboardFragment: Fragment = KeyboardFragment(this, "Password", "****", "00", SessionApp.KeyboardType.PASSWORD)
        (keyboardFragment as KeyboardFragment).show(fragmentManager!!, "")
    }

    override fun onKeyPressListener(data: String?) {
        loginPresenter?.goSetting(data)
    }
}
