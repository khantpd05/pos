package com.example.abc.pos.screen.chuc_nang_order;

import android.util.Log;

import com.example.abc.pos.screen.keyboard.KeyboardConstract;
import com.example.abc.pos.viewmodel.Keyboard;

public class ChucNangOrderPresenter implements ChucNangOrderConstract.Presenter {

    public ChucNangOrderConstract.View mView;


    public ChucNangOrderPresenter(ChucNangOrderConstract.View mView) {
        this.mView = mView;
    }

    @Override
    public void doFeatureClick(int key) {
        mView.doFeatureClick(key);
    }
}
