package com.example.abc.pos.screen.tab_of_sale.drink;

import android.os.AsyncTask;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.screen.tab_of_sale.menu.MenuTabPresenter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class DrinkTabPresenter implements DrinkTabConstract.Presenter {

    DrinkTabConstract.View mView;

    public DrinkTabPresenter(DrinkTabConstract.View view){
        mView = view;
        loadData();
    }

    @Override
    public void loadData() {
        if(SessionApp.menus_drink!=null) mView.showListMenus(SessionApp.menus_drink);
    }

    @Override
    public void getItems(String menu_id) {
        getItemsFromMenuListById(menu_id);
    }


    private void getItemsFromMenuListById(String menu_id){
        List<Items> drink_list = new ArrayList<>();
        if(SessionApp.items_global != null && SessionApp.items_global.size() > 0){
            for(Items item : SessionApp.items_global){
                if(menu_id.equals(item.getNhomhang()+"")){
                    drink_list.add(item);
                }
            }
            drink_list.add(0,new Items());
            mView.showListItems(drink_list);
        }
    }

//    private class MyTask extends AsyncTask<Void,Void,List<Menu>> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            mView.showLoading();
//        }
//
//        @Override
//        protected ArrayList<Menu> doInBackground(Void... voids) {
//            JSONArray js = MySqlHandleUtils.selectStament("select * from menu where id in (select nhomhang from items where bophanphucvu like '%PhaChe%' group by nhomhang)");
//            ArrayList<Menu> menus =  new Gson().fromJson(js.toString(), new TypeToken<List<Menu>>(){}.getType());
//            return menus;
//        }
//
//        @Override
//        protected void onPostExecute(List<Menu> menus) {
//            super.onPostExecute(menus);
//            mView.dismissLoading();
//            mView.showListMenus(menus);
//            SessionApp.menus_drink = menus;
//        }
//    }
//
//    private class GettingItemsFromMenuTask extends AsyncTask<Void,Void,List<Items>> {
//        String menu_id;
//        public GettingItemsFromMenuTask(String menu_id){
//            this.menu_id = menu_id;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            mView.showLoading();
//        }
//
//        @Override
//        protected ArrayList<Items> doInBackground(Void... voids) {
//            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.items i" +
//                    " where "+menu_id+ " = i.nhomhang");
//            ArrayList<Items> items =  new Gson().fromJson(js.toString(), new TypeToken<List<Items>>(){}.getType());
//            for(Items ob: items){
//                ob.setGiaBan();
//            }
//            return items;
//        }
//
//        @Override
//        protected void onPostExecute(List<Items> items) {
//            super.onPostExecute(items);
//            mView.dismissLoading();
//            Items firstOb = new Items();
//            firstOb.setTenhang(App.Companion.self().getString(R.string.Come_back));
//            items.add(0,firstOb);
//            mView.showListItems(items);
//        }
//    }
}
