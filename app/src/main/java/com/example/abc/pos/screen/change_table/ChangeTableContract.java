package com.example.abc.pos.screen.change_table;

import com.example.abc.pos.model.Table;

import java.util.List;

public interface ChangeTableContract {
    interface View{
         void showListTable(List<Table> tables);
        void showLoading();
        void dismissLoading();
        void exit();
    }

    interface Presenter{
        void getUsingTable();
    }
}
