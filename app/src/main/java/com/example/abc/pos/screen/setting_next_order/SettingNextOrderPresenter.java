package com.example.abc.pos.screen.setting_next_order;

import android.os.AsyncTask;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Tieudedotdat;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class SettingNextOrderPresenter implements SettingNextOrderContract.Presenter{

    SettingNextOrderContract.View mView;

    public SettingNextOrderPresenter(SettingNextOrderContract.View mView) {
        this.mView = mView;
        loadData();
    }

    @Override
    public void loadData() {
        new SelectTask().execute();
    }

    @Override
    public void deleteRow(int id) {
        new DeleteOrderTask(id).execute();
    }

    @Override
    public void updateOrderTask(Tieudedotdat ob) {
        new updateOrderTask(ob).execute();
    }

    @Override
    public void insertOrderTask(String name) {
        if(name.trim().isEmpty()) mView.showAlert(Util.getStringFromResouce(R.string.please_enter_full_information));
        else new insertOrderTask(name).execute();
    }

    private class SelectTask extends AsyncTask<Void,Void,List<Tieudedotdat>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<Tieudedotdat> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.tieudedotdat");
            ArrayList<Tieudedotdat> tieudedotdats =  new Gson().fromJson(js.toString(), new TypeToken<List<Tieudedotdat>>(){}.getType());
            return tieudedotdats;
        }

        @Override
        protected void onPostExecute(List<Tieudedotdat> tieudedotdats) {
            super.onPostExecute(tieudedotdats);
            mView.dismissLoading();
            mView.showNextOrderList(tieudedotdats);

        }
    }

    private class DeleteOrderTask extends AsyncTask<Void,Void,Integer> {
        int id;
        public DeleteOrderTask(int id) {
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result =  MySqlHandleUtils.deleteRecordFromTieuDeDotDat(id);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            mView.dismissLoading();
            if(result == 1){
                mView.showAlert(Util.getStringFromResouce(R.string.delete_successfully));
                loadData();
                mView.hideEditStuff();
            }else{
                mView.showAlert(Util.getStringFromResouce(R.string.delete_failed));
            }
        }
    }

    private class updateOrderTask extends AsyncTask<Void, Void, Integer> {

        Tieudedotdat item;

        public updateOrderTask(Tieudedotdat item) {
            this.item = item;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result = MySqlHandleUtils.updateNextOrderItem(item);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                mView.showAlert(App.Companion.self().getString(R.string.Update_successful));
                loadData();
            } else {
                mView.showAlert(App.Companion.self().getString(R.string.There_was_an_error_updating_the_data));
            }
        }
    }

    private class insertOrderTask extends AsyncTask<Void, Void, Integer> {
        String name;
        public insertOrderTask(String name) {
            this.name = name;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result = MySqlHandleUtils.insertRecordIntoTieuDeDotDat(name);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                mView.showAlert(App.Companion.self().getString(R.string.Update_successful));
                loadData();
            } else {
                mView.showAlert(App.Companion.self().getString(R.string.There_was_an_error_updating_the_data));
            }
        }
    }
}
