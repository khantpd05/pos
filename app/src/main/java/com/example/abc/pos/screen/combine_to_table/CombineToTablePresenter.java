package com.example.abc.pos.screen.combine_to_table;

import android.os.AsyncTask;
import android.util.Log;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.ExtraItem;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.screen.change_to_table.ChangeToTablePresenter;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class CombineToTablePresenter implements CombineToTableContract.Presenter {

    CombineToTableContract.View mView;
    private List<OrdersTemp> ordersTempList;
    private Table table_new;
    private List<Table> tableOldList;

    public CombineToTablePresenter(List<Table> tableList, CombineToTableContract.View mView) {
        this.mView = mView;
        ordersTempList = new ArrayList<>();
        this.tableOldList = tableList;
        getEmptyTable();
        getAllOrderFromOldTable();
    }

    @Override
    public void getEmptyTable() {
        new GettingEmptyTableTask().execute();
    }

    boolean isNull = false;

    @Override
    public void doChangeTable(Table table_new) {
        new ExecuteAllDbTask().execute();
    }


    private void updateNewTable(Table table_new) {
        double summer = 0;

        for (Table table : tableOldList) {
            if (table.getTongtien() != null) {
                if (!table.getName().equals(table_new.getName()))
                    summer += table.getTongtien();
            } else {
                isNull = true;
                break;
            }
        }


        table_new.setTrangthai("using");
        table_new.setUsername(WapriManager.getUser().getUsername());
        table_new.setGiam(table_new.getGiam());

        double tienGiam = table_new.getTiengiam() + summer * table_new.getGiam() / 100;

        summer = summer - ((table_new.getGiam() * summer) / 100);
        table_new.setTongtien(table_new.getTongtien() + summer);


        table_new.setTiengiam(tienGiam);
        table_new.setTienthue19giam(WapriManager.tinhTienThueGiam(0, table_new.getGiam()));
        table_new.setTienthue7giam(WapriManager.tinhTienThueGiam(0, table_new.getGiam()));
        table_new.setThoigian(Util.getDate("dt"));

        new UpdateNewTableToDbTask(table_new).execute();
    }

    private void updateOldTables(List<Table> tableOldList) {

        for (Table table : tableOldList) {
            if (!table_new.getName().equals(table.getName())) {
                table.setTrangthai("empty");
                table.setUsername(null);
                table.setThoigian(null);
                table.setTongtien(0.0);
                table.setGiam(0);
                table.setTiengiam(0.0);
                table.setTienthue19giam(0.0);
                table.setTienthue7giam(0.0);
                new UpdateOldTableToDbTask(table).execute();
            }
        }
    }


    private void updateOrderOfOlderTableToNewTable() {
        for (OrdersTemp ob : ordersTempList) {
//            if (table_new.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho){
//                ob.setTaichohaymangve(WapriManager.Taichohaymangve_.TaiCho);
//            }else{
//                ob.setTaichohaymangve(WapriManager.Taichohaymangve_.MangVe);
//            }
            ob.initExtraItemList();
            ob.setGiatien(tinhGiaTien(ob));

            if ((table_new.getTaichohaymangve() != WapriManager.taichohaymangve.MangVe && ob.getTaichohaymangve() == WapriManager.Taichohaymangve_.TaiCho)
                    || ob.getBophanphucvu().equals("PhaChe19")) {
                ob.setThue(19);
                ob.setTienthue(tinhTienThue(ob));
            } else {
                ob.setThue(7);
                ob.setTienthue(tinhTienThue(ob));
            }
            new UpdateChangeOrderToNewTableDbTask(ob).execute();
        }
    }

    private double tinhGiaTien(OrdersTemp orderTemp) {
        // count all include spice price
        double totalSpicePrice = 0;
        for (ExtraItem extraItem : orderTemp.getExtraItemList()) {
            if (extraItem.getFlag() == WapriManager.FeatureOrder.GIA_VI || extraItem.getFlag() == WapriManager.FeatureOrder.PHAN_AN) {
                totalSpicePrice += extraItem.getValue();
            }
        }
        double tongtien = (orderTemp.getSoluong() * orderTemp.getorig_price_no_extra()) + (totalSpicePrice * orderTemp.getSoluong());
        return tongtien;
    }

    private double tinhTienThue(OrdersTemp ordersTemp) {
        if (ordersTemp.getBophanphucvu().equals("Sonstiges0"))
            return 0.0;
        else if ((table_new.getTaichohaymangve() != WapriManager.taichohaymangve.MangVe && ordersTemp.getTaichohaymangve() == WapriManager.Taichohaymangve_.TaiCho) || ordersTemp.getBophanphucvu().equals("PhaChe19"))
            return WapriManager.tinhTienThue19(ordersTemp.getGiatien(), ordersTemp.getPhantramgiam());
        else
            return WapriManager.tinhTienThue7(ordersTemp.getGiatien(), ordersTemp.getPhantramgiam());
    }

    @Override
    public void setTableNew(Table tableNew) {
        this.table_new = tableNew;
        for (Table tb : tableOldList) {
            if (tb.getTaichohaymangve() == WapriManager.taichohaymangve.MangVe && table_new.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho) {
                for (OrdersTemp ob : ordersTempList) {
                    if (ob.getBanso().equals(tb.getName()))
                        ob.setTaichohaymangve(WapriManager.Taichohaymangve_.TaiCho);
                }
            }
            if (table_new.getTaichohaymangve() == WapriManager.taichohaymangve.MangVe) {
                for (OrdersTemp ob : ordersTempList) {
                    if (ob.getBanso().equals(tb.getName()))
                        ob.setTaichohaymangve(WapriManager.Taichohaymangve_.MangVe);
                }

            }
        }
    }


    private void getAllOrderFromOldTable() {
        new GetAllOrderTempListTask().execute();
    }


    private class ExecuteAllDbTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            if (isNull) {

                return false;
            } else {

                updateNewTable(table_new);
                updateOrderOfOlderTableToNewTable();
            }
            return true;
        }


        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                mView.dismissLoading();
                mView.goBack();
            } else {
                mView.showAlert("Một số bàn bị thiếu tổng tiền");
            }

        }
    }


    private class UpdateNewTableToDbTask extends AsyncTask<Void, Void, Integer> {

        Table table_new;

        public UpdateNewTableToDbTask(Table table_new) {
            this.table_new = table_new;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            if (MySqlHandleUtils.updateTableChangeNew(table_new) != 0) return 1;
            else return 0;

        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                updateOldTables(tableOldList);
            } else {
                mView.showAlert(Util.getStringFromResouce(R.string.update_failed));
            }
        }
    }

    private class UpdateOldTableToDbTask extends AsyncTask<Void, Void, Integer> {

        Table table;

        public UpdateOldTableToDbTask(Table table_old) {
            this.table = table_old;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            if (MySqlHandleUtils.updateTableChangeOld(table) != 0) return 1;
            else return 0;

        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (result == 1) {
            } else {
                mView.showAlert(Util.getStringFromResouce(R.string.update_failed));
            }
        }
    }


    private class UpdateChangeOrderToNewTableDbTask extends AsyncTask<Void, Void, Integer> {
        OrdersTemp ordersTemp;

        public UpdateChangeOrderToNewTableDbTask(OrdersTemp ordersTemp) {
            this.ordersTemp = ordersTemp;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result = MySqlHandleUtils.updateChangeOrderTableToNewTable(ordersTemp, table_new.getName());
            return result;
        }
    }


    private class GetAllOrderTempListTask extends AsyncTask<Void, Void, List<OrdersTemp>> {

        public GetAllOrderTempListTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<OrdersTemp> doInBackground(Void... voids) {
            ArrayList<OrdersTemp> ordersTempsNew = new ArrayList<>();
            for (Table table : tableOldList) {
                JSONArray js = MySqlHandleUtils.selectStament("SELECT *  FROM wapri.orders_temp where banso = " + "'" + table.getName() + "'");
                ArrayList<OrdersTemp> ordersTemps = new Gson().fromJson(js.toString(), new TypeToken<List<OrdersTemp>>() {
                }.getType());
                ordersTempsNew.addAll(ordersTemps);
            }


            return ordersTempsNew;
        }

        @Override
        protected void onPostExecute(List<OrdersTemp> datas) {
            super.onPostExecute(datas);
            mView.dismissLoading();
            ordersTempList = datas;

        }
    }


    private class GettingEmptyTableTask extends AsyncTask<Void, Void, ArrayList<Table>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<Table> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT t.id, t.name, t.vitri, t.trangthai, t.soghengoi, t.username, t.tongtien, t.tabletype, t.status,giam," +
                    "t.tiengiam, t.tienthue19giam, t.tienthue7giam, t.taichohaymangve, t.thoigian, t.vitritronghayngoainha, t.orderfrom, t.from_tablet," +
                    "t.is_selling_on_tablet," +
                    "(select count(o.id) from wapri.orders_temp o where o.banso = t.name AND o.daprintchua = 'N' AND o.items_id NOT IN (1001,1005,10004) AND o.bophanphucvu <> 'Sonstiges0') as total_orders_not_printed, " +
                    "(select count(o.id) from wapri.orders_temp o where o.banso = t.name) as total_orders  FROM wapri.table t where t.trangthai = 'empty' or t.username = " + "'" + WapriManager.getUser().getUsername() + "'");

            ArrayList<Table> tables = new Gson().fromJson(js.toString(), new TypeToken<List<Table>>() {
            }.getType());
            for(Table table:tables){
                table.setTaichoHayMangVe();
            }
            return tables;
        }

        @Override
        protected void onPostExecute(ArrayList<Table> tables) {
            super.onPostExecute(tables);
            mView.dismissLoading();
            mView.showListTable(tables);
        }
    }
}
