package com.example.abc.pos.screen.tab_of_sale.menu;

import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;

import java.util.List;

public interface MenuTabConstract {
    interface View{
        void showListMenus(List<Menu> menus);
        void showListItems(List<Items> items);
        void showLoading();
        void dismissLoading();

    }
    interface Presenter{
        void loadData();
        void getItems(String menu_id);
    }
}
