package com.example.abc.pos.screen.language;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.KichCoMonAdapter;
import com.example.abc.pos.adapter.LanguageAdapter;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.base.WapriManager.KichCoMon;
import com.example.abc.pos.model.ItemClickListener;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.KichCoMonAn;
import com.example.abc.pos.model.Language;
import com.example.abc.pos.utils.LanguageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class LanguageDialogFragment extends DialogFragment {

    RecyclerView rc_language;

    private LanguageAdapter adapter;

    private OnLanguageSelected listener;

    public interface OnLanguageSelected {
        void onLanguageSelectedListener();
    }

    public LanguageDialogFragment() {
        // Required empty public constructor
    }

    public void setListener(OnLanguageSelected listener){
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment.

        return inflater.inflate(R.layout.fragment_language_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rc_language = view.findViewById(R.id.rc_language);
        adapter = new LanguageAdapter(LanguageUtils.getLanguageData());
        adapter.setListener(new ItemClickListener<Language>() {
            @Override
            public void onClickItem(int position, Language language) {
                if (!language.getCountryCode().equals(LanguageUtils.getCurrentLanguage().getCountryCode())) {
                    onChangeLanguageSuccessfully(language);
                }
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rc_language.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                linearLayoutManager.getOrientation());
        rc_language.addItemDecoration(dividerItemDecoration);
        rc_language.setAdapter(adapter);
    }


    @Override
    public void onResume() {
        super.onResume();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        getDialog().getWindow().setLayout((6 * width) / 7, RelativeLayout.LayoutParams.WRAP_CONTENT);

    }

    private void onChangeLanguageSuccessfully(final Language language) {
        adapter.setCurrentLanguage(language);
        LanguageUtils.changeLanguage(language);
        getDialog().dismiss();
        listener.onLanguageSelectedListener();
    }
}
