package com.example.abc.pos.viewmodel;

import android.databinding.ObservableField;

import com.example.abc.pos.model.Configuration;

import java.util.List;

public class PaymentConfiguration {
    public ObservableField<Boolean> TIP = new ObservableField<>(true);
    public ObservableField<Boolean> restaurant_check = new ObservableField<>(true);
    public ObservableField<Boolean> gutschein = new ObservableField<>(true);
    public ObservableField<Boolean> cash = new ObservableField<>(true);
    public ObservableField<Boolean> credit_card = new ObservableField<>(true);
    public ObservableField<Boolean> bewirtung = new ObservableField<>(true);
    public ObservableField<Boolean> cancel = new ObservableField<>(true);

    public PaymentConfiguration() {
    }


}
