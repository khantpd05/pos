package com.example.abc.pos.model;

public interface ItemClickListener<T> {
    void onClickItem(int position, T item);
}
