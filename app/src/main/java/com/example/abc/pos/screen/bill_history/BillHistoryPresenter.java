package com.example.abc.pos.screen.bill_history;

import android.os.AsyncTask;
import android.util.Log;

import com.example.abc.pos.R;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Orders;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class BillHistoryPresenter implements BillHistoryContract.Presenter {
    private BillHistoryFragment mView;
    private List<Orders> ordersList;
    private Orders orders;
    public BillHistoryPresenter(BillHistoryFragment mView) {
        this.mView = mView;
    }

    @Override
    public void loadBillHistoryList() {
        new SelectOrdersTask().execute();
    }

    @Override
    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    @Override
    public void updateInLaiHoaDon(String data) {
        orders.setLoaiIn(data);
        orders.setInlaihoadonfromtablet("Y");
        orders.setDaprintchua("N");
        new UpdateOrderToDbTask(orders,0).execute();
    }

    @Override
    public void updateDoiCachTraTien(String data, int type) {
        orders.setLichsudoicachthanhtoantien(data);
        if(orders.getLoaiIn().equals("None")){
            orders.setLoaiIn("BON");
        }
        if(type == 1)
            orders.setCachtra(WapriManager.cachtra.TienMat);
        else
            orders.setCachtra(WapriManager.cachtra.CreditCard);
        orders.setLadonhangcodoicachthanhtoantien("Y");
        orders.setInlaihoadonfromtablet("Y");
        orders.setDaprintchua("N");
        new UpdateOrderToDbTask(orders,1).execute();
        new UpdateCachTraOderDetailTask(orders.getId(),orders.getCachtra().value).execute();
    }


    private class SelectOrdersTask extends AsyncTask<Void,Void,List<Orders>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<Orders> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.orders_"+Util.currrentYear()+" where nhanvien = '"+
                    WapriManager.getUser().getUsername()+"'"+" AND  ngaymua = "+"'"+Util.getDate("d")+"'");
            ArrayList<Orders> menus =  new Gson().fromJson(js.toString(), new TypeToken<List<Orders>>(){}.getType());
            return menus;
        }

        @Override
        protected void onPostExecute(List<Orders> bills) {
            super.onPostExecute(bills);
            mView.dismissLoading();
            mView.showBillHistory(bills);
            ordersList = bills;
        }
    }


    private class UpdateOrderToDbTask extends AsyncTask<Void, Void, Integer> {

        Orders orders;
        int type;
        public UpdateOrderToDbTask(Orders orders, int type) {
            this.orders = orders;
            if(type == 0 ){
                this.orders.setInlaihoadonfromtablet("Y");
            }
            this.type = type;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;
            result = MySqlHandleUtils.updateOrderTask(orders);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                if(type==0){
                    mView.showDialog(Util.getStringFromResouce(R.string.please_take_the_invoice_again));
                }else{
                    mView.showAlert(Util.getStringFromResouce(R.string.successful));
                }
                new SelectOrdersTask().execute();
            } else {
                mView.showAlert(Util.getStringFromResouce(R.string.update_failed));
            }
        }
    }


    private class UpdateCachTraOderDetailTask extends AsyncTask<Void, Void, Integer> {

        String cachTra;
        String orders_id;
        public UpdateCachTraOderDetailTask(String orders_id, String cachTra) {
            this.cachTra = cachTra;
            this.orders_id = orders_id;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;
            result = MySqlHandleUtils.updateCachTraOrderDetailTask(cachTra,orders_id);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

        }
    }
}
