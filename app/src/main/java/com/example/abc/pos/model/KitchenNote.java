package com.example.abc.pos.model;

public class KitchenNote {
    private String name;

    public KitchenNote(String name) {
        this.name = name;
    }


    public KitchenNote() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
