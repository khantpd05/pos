package com.example.abc.pos.screen.separate_order_to_table;

import com.example.abc.pos.model.Table;

import java.util.List;

public interface SeparateOrderToTableContract {
    interface View{
         void showListTable(List<Table> tables);
        void showLoading();
        void dismissLoading();
        void showAlert(String data);
        void goToCustomerPositionOfGoTable();
    }

    interface Presenter{
        void getEmptyTable(String username, String excludeTableName);
        void goToCustomerPositionOfGoTable();
    }
}
