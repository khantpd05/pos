package com.example.abc.pos.binding;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.databinding.InverseBindingListener;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.screen.keyboard.KeyboardPresenter;
import com.example.abc.pos.screen.keyboard_code.KeyboardCodePresenter;

public class KeyboardCodeBinding {
    @BindingAdapter("onCodeClick")
    public static void onClick(final TextView button, final KeyboardCodePresenter presenter) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(button.getTag()!=null){
                    if(button.getTag().toString().equals("DEL")){
                        if( presenter.mViewModel.input.get().length()>0){
                            String inputText = presenter.mViewModel.input.get();
                            presenter
                                    .mViewModel
                                    .input.
                                    set(inputText.substring(0, inputText.length() - 1));
                        }
                    }else if(button.getTag().toString().equals("x")){
                        presenter.mViewModel.input.set("");
                        presenter.number = 2;
                    }

                }

                 if (button.getText().equals("ENTER")) {
                    presenter.getDataInput();
                } else if (button.getText().equals("Aa")) {
                    if (!presenter.isSwitchKeyboard.get()) presenter.isSwitchKeyboard.set(true);
                    else presenter.isSwitchKeyboard.set(false);
                } else if (button.getText().equals("*")) {
                    String inputText = presenter.mViewModel.input.get();

                    if (inputText.length() > 0) {
                        if (!inputText.contains("*")) {
                            presenter.mViewModel.input.set(presenter.mViewModel.input.get() + "*");
                        }
                    }
                } else if (button.getText().equals("+")) {
                    String inputText = presenter.mViewModel.input.get();
                    if (inputText.length() > 0) {
                        if (inputText.contains("*")) {
                            String xSeparate = inputText;
                            int beforeX = xSeparate.indexOf("*");
                            String beforeXText = "";
                            String afterXText = "";
                            if (beforeX != -1) {

                                    beforeXText = xSeparate.substring(0, beforeX) + "*";
                                    afterXText = xSeparate.substring(beforeX + 1);
                                    if(afterXText.length()<1){
                                        afterXText = afterXText+"1";
                                    }

                            }else{

                            }
                            presenter.number = Integer.parseInt(afterXText);
                            presenter.mViewModel.input.set(beforeXText + (++presenter.number));
                        } else {
                            presenter.number = 2;
                            presenter.mViewModel.input.set(presenter.mViewModel.input.get() + "*" + (presenter.number++));
                        }
                    }
                } else if (button.getText().equals("-")) {
                    String inputText = presenter.mViewModel.input.get();
                    if (inputText.length() > 0) {
                        if (inputText.contains("*")) {
                            String xSeparate = inputText;
                            int iend = xSeparate.indexOf("*");
                            String subString = "";

                            int beforeX = xSeparate.indexOf("*");
                            String afterXText = "";

                            if (iend != -1) {
                                subString = xSeparate.substring(0, iend);
                            }

                            if (beforeX != -1) {
                                afterXText = xSeparate.substring(beforeX + 1);
                                if(afterXText.length()<1){
                                    afterXText = afterXText+"1";
                                }
                            }

                            presenter.number = Integer.parseInt(afterXText);

                            if (presenter.number > 2) {
                                presenter.mViewModel.input.set(subString + "*" + (--presenter.number));
                            } else {
                                presenter.mViewModel.input.set(subString);
                            }
                        } else {
//                            presenter.mViewModel.input.set(presenter.mViewModel.input.get() + "x" + (--presenter.number));
                        }
                    }
                } else {
                    String inputText = presenter.mViewModel.input.get();

                    if (inputText.length() > 0) {
                        if (inputText.contains("*")) {
                            String regexStr ="-?\\d+(\\.\\d+)?";
                            if (button.getText().toString().matches(regexStr)) {
                                presenter.mViewModel.input.set(presenter.mViewModel.input.get() + button.getText().toString());
                            }
                        } else {
                            presenter.mViewModel.input.set(presenter.mViewModel.input.get() + button.getText().toString());
                        }
                    } else {
                        presenter.mViewModel.input.set(presenter.mViewModel.input.get() + button.getText().toString());
                    }
                }
            }
        });
    }


    @BindingAdapter(value = {"setTextFind", "setTextFindAttrChanged"}, requireAll = false)
    public static void setTextFindEdittextText(EditText editText, String value, final InverseBindingListener newTextAttrChanged) {
        editText.setText(value);


        editText.setSelection(editText.getText().length());
    }

    @InverseBindingAdapter(attribute = "setTextFind", event = "setTextFindAttrChanged")
    public static String getTextFindEdittextText(EditText editText) {
        return editText.getText().toString();
    }
}
