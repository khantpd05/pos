package com.example.abc.pos.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.abc.pos.App;
import com.example.abc.pos.model.Connection;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SharedPrefs {
    private static final String PREFS_NAME_WAPRI = "Wapri";
    private static final String CONNECTION_SAVED = "Connection_Saved";
    public static final String MONEY_BOX = "moneybox_position";
    public static final String LANGUAGE = "langauge";
    public static final String KEY_SETTING_GENERAL = "setting_general";
    public static final String KEY_SETTING_PRINTER = "printer_general";
    public static final String KEY_SETTING_PAYMENT = "payment_general";
    public static final String KEY_DEVICE_NAME = "device_named";

    public static final String KEY_TABLE = "table_";
    private static SharedPrefs mInstance;
    private SharedPreferences mSharedPreferences;
    private App mContext;

    private SharedPrefs() {
        mContext = App.Companion.self();
        mSharedPreferences = mContext.getSharedPreferences(PREFS_NAME_WAPRI, Context.MODE_PRIVATE);
    }

    public static SharedPrefs getInstance() {
        if (mInstance == null) {
            mInstance = new SharedPrefs();
        }
        return mInstance;
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key, Class<T> anonymousClass) {
        if (anonymousClass == String.class) {
            return (T) mSharedPreferences.getString(key, "");
        } else if (anonymousClass == Boolean.class) {
            return (T) Boolean.valueOf(mSharedPreferences.getBoolean(key, false));
        } else if (anonymousClass == Float.class) {
            return (T) Float.valueOf(mSharedPreferences.getFloat(key, 0));
        } else if (anonymousClass == Integer.class) {
            return (T) Integer.valueOf(mSharedPreferences.getInt(key, 0));
        } else if (anonymousClass == Long.class) {
            return (T) Long.valueOf(mSharedPreferences.getLong(key, 0));
        } else if (anonymousClass == List.class) {
            Type type = new TypeToken<List<T>>() {
            }.getType();
            return (T) mContext
                    .getGSon().fromJson(mSharedPreferences.getString(key, ""), type);
        } else {
            return (T) mContext
                    .getGSon()
                    .fromJson(mSharedPreferences.getString(key, ""), anonymousClass);
        }
    }

    public <T> void put(String key, T data) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        if (data instanceof String) {
            editor.putString(key, (String) data);
        } else if (data instanceof Boolean) {
            editor.putBoolean(key, (Boolean) data);
        } else if (data instanceof Float) {
            editor.putFloat(key, (Float) data);
        } else if (data instanceof Integer) {
            editor.putInt(key, (Integer) data);
        } else if (data instanceof Long) {
            editor.putLong(key, (Long) data);
        } else {
            editor.putString(key, mContext.getGSon().toJson(data));
        }
        editor.apply();
    }

    public void clear() {
        mSharedPreferences.edit().clear().apply();
    }

    public void remove(String key) {
        try{
            mSharedPreferences.edit().remove(key).commit();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void addConnectionSaveDraft(Connection connection) {
        List<Connection> favorites = getIpListSharePreference();
        boolean b = false;
        int tempI = 0;
        if (favorites == null) {
            favorites = new ArrayList<>();
        } else {
            for (int i = 0; i < favorites.size(); i++) {
                if (connection.id == favorites.get(i).id) {
                    b = true;
                    tempI = i;
                    break;
                }
            }
        }
        if (!b) {
            favorites.add(connection);
        } else {
            favorites.set(tempI, connection);
        }
        saveFIHistoryFavorites(favorites);
    }

    public void saveFIHistoryFavorites(List<Connection> favorites) {
        String jsonFavorites = mContext.getGSon().toJson(favorites);
        put(CONNECTION_SAVED, jsonFavorites);
    }

    public ArrayList<Connection> getIpListSharePreference() {
        List<Connection> favorites;
        if (mSharedPreferences.contains(CONNECTION_SAVED)) {
            String jsonFavorites = mSharedPreferences.getString(CONNECTION_SAVED, null);
            Gson gson = new Gson();
            Connection[] favoriteItems = gson.fromJson(jsonFavorites,
                    Connection[].class);

            if (favoriteItems != null)
                favorites = Arrays.asList(favoriteItems);
            else {
                favorites = new ArrayList<>();
            }
            favorites = new ArrayList<>(favorites);
        } else
            return null;
        return (ArrayList<Connection>) favorites;
    }

    public void removeSavedConectionSaved(Connection connection) {
        ArrayList<Connection> favorites = getIpListSharePreference();
        if (favorites != null) {
            for (int i = 0; i < favorites.size(); i++) {
                if (connection.id == favorites.get(i).id) {
                    favorites.remove(i);
                    break;
                }
            }
        }
        saveFIHistoryFavorites(favorites);
    }
}
