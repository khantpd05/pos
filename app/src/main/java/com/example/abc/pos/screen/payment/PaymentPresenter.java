package com.example.abc.pos.screen.payment;

import android.databinding.ObservableField;
import android.os.AsyncTask;

import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.broadcast.RestartService;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.utils.Util;

import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class PaymentPresenter implements PaymentContract.Presenter {

    private PaymentContract.View mView;
    private List<OrdersTemp> ordersTempList;

    public ObservableField<String> title = new ObservableField<>(WapriManager.STORE_NAME);
    public ObservableField<String> rechnung_value = new ObservableField<>();
    public ObservableField<String> tischnr_value = new ObservableField<>();
    public ObservableField<String> bediener_value = new ObservableField<>();
    public ObservableField<String> time_value = new ObservableField<>();
    public ObservableField<String> tax_7 = new ObservableField<>();
    public ObservableField<String> tax_19 = new ObservableField<>();
    public ObservableField<String> summer = new ObservableField<>();


    public PaymentPresenter(PaymentContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void loadData(List<OrdersTemp> ordersTempList,double summer_price,String banso) {
        this.ordersTempList = ordersTempList;
        new SelectSoHoaDon().execute();
        tischnr_value.set(banso);
        bediener_value.set(WapriManager.getUser().getUsername());
        time_value.set(Util.getDate("dt"));
        summer.set(String.format("%,.2f",summer_price)+""+"");
        getTax();
        mView.showListOrder(ordersTempList);
        SessionApp.global_table.setis_selling_on_tablet("Y");
        new UpdateTableToDbTask(SessionApp.global_table).execute();
    }

    @Override
    public void goToPaymentDetail() {
        mView.goToPaymentDetail(rechnung_value.get(),summer.get());
    }

    private void getTax(){
        double thue7 = 0;
        double thue19 = 0;
        if(ordersTempList!=null && ordersTempList.size()>0){
            for(OrdersTemp ordersTemp: ordersTempList){
                if(!ordersTemp.getDanhdaubixoa().equals("Y")){
                    if(ordersTemp.getThue() == 7){
                        thue7 += ordersTemp.getTienthue();
                    }else{
                        thue19 += ordersTemp.getTienthue();
                    }
                }
            }
        }

        if(SessionApp.global_table!=null){
            tax_7.set(String.format("%,.2f",thue7 - ((thue7*SessionApp.global_table.getGiam())/100))+"");
            tax_19.set(String.format("%,.2f",thue19 - ((thue19*SessionApp.global_table.getGiam())/100))+"");
        }
    }

    private class SelectSoHoaDon extends AsyncTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            Integer soHoaDon = null;
            try{
                 soHoaDon = Integer.parseInt(MySqlHandleUtils.selectMaSoHoaDon());
            }catch (NumberFormatException ex){
                soHoaDon = 0000;

            }


            return soHoaDon;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            mView.dismissLoading();
            Integer soHoaDon = (Integer) o;
            String idGenera = WapriManager.DEVICE_NAME+"-"+Util.getDate("date_code")+ "-" +String.format("%04d",  (++soHoaDon))+"";
            rechnung_value.set(idGenera);
        }
    }

    class UpdateTableToDbTask extends AsyncTask<Void, Void, Integer> {

        Table table;

        public UpdateTableToDbTask(Table table) {
            this.table = table;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            if (table != null) {
                if (MySqlHandleUtils.updateTableChangeNew(table) != 0)
                    return 1;
                else return 0;
            }
            return 1;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {

            } else {

            }
        }
    }

}
