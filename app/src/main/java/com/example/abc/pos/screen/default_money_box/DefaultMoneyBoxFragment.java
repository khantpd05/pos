package com.example.abc.pos.screen.default_money_box;


import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.databinding.FragmentDefaultMoneyBoxBinding;
import com.example.abc.pos.model.UsersTienbandauTemplate;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.utils.Util;

/**
 * A simple {@link Fragment} subclass.
 */
public class DefaultMoneyBoxFragment extends Fragment implements DefaultMoneyBoxContact.View{

    DefaultMoneyBoxPresenter presenter;
    private OnBoxClick listener;
    private OnReturn returnListener;
    private LinearLayout ll_box1,ll_box2,ll_box3,ll_box4;
    public interface OnBoxClick{
        void onBoxClickListener(UsersTienbandauTemplate usersTienbandauTemplate);
    }

    public interface OnReturn{
        void onReturnListener();
    }


    public DefaultMoneyBoxFragment() {
        // Required empty public constructor
    }

    public void setData(OnBoxClick listener, OnReturn onReturn){
        this.listener = listener;
        this.returnListener = onReturn;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentDefaultMoneyBoxBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_default_money_box, container, false);
        presenter = new DefaultMoneyBoxPresenter(this);
        binding.setPresenter(presenter);
        View view = binding.getRoot();
        ll_box1 = view.findViewById(R.id.ll_box_1);
        ll_box2 = view.findViewById(R.id.ll_box_2);
        ll_box3 = view.findViewById(R.id.ll_box_3);
        ll_box4 = view.findViewById(R.id.ll_box_4);
        Integer box_pos = SharedPrefs.getInstance().get(SharedPrefs.MONEY_BOX,Integer.class);
        if(box_pos!=null && box_pos != 0){
            if(box_pos == 1){
                ll_box1.setBackgroundColor(Color.parseColor("#3a8e4c"));
            }else if(box_pos == 2){
                ll_box2.setBackgroundColor(Color.parseColor("#3a8e4c"));
            }else if (box_pos == 3){
                ll_box3.setBackgroundColor(Color.parseColor("#3a8e4c"));
            }else{
                ll_box4.setBackgroundColor(Color.parseColor("#3a8e4c"));
            }
        }else{
            ll_box1.setBackgroundColor(Color.parseColor("#3a8e4c"));
        }

        return view;
    }

    @Override
    public void goBack() {
        if(presenter.isSetting.get()) SessionApp.isSetting = true;
        else SessionApp.isSetting = false;
        returnListener.onReturnListener();
        Util.popFragment(getActivity());
    }

    @Override
    public void boxClick(UsersTienbandauTemplate usersTienbandauTemplate) {
        listener.onBoxClickListener(usersTienbandauTemplate);
        goBack();
    }
}
