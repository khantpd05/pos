package com.example.abc.pos.screen.spice;


import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.adapter.SpiceAdapter;
import com.example.abc.pos.adapter.SpiceItemAdapter;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.model.Connection;
import com.example.abc.pos.model.ItemsMontuychon;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Spice;
import com.example.abc.pos.screen.keyboard.KeyboardFragment;
import com.example.abc.pos.screen.page_spice_kitchennote_dialog.FragmentPageSpiceKitchenDialog;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SpiceFragment extends Fragment implements SpiceContract.View, SpiceAdapter.ItemClickListener,KeyboardFragment.OnKeyboardPress,  SpiceItemAdapter.ItemClickListener, FragmentPageSpiceKitchenDialog.OnPageItemClick {



    public interface OnUpdateSpiceList{
        void onUpdateSpiceListListener(List<Spice> spiceList);
    }

    private List<Spice> spiceList;

    private OnUpdateSpiceList listener;

    private EditText edt_name;

    private TextView tv_page;

    private Button btn_add;

    private LinearLayout ll_exit;

    private RecyclerView rc_spice;

    private RecyclerView rc_spice_item;

    private SpiceAdapter adapter;

    private SpiceItemAdapter adapter_spice_item;

    private SpiceContract.Presenter presenter;

    private int selectedPosition;

    private Spice mSpice;

    private ItemsMontuychon item;

    private SpiceFragment this_;

    public SpiceFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public SpiceFragment(OnUpdateSpiceList listener,List<Spice> datas ){
        this.listener = listener;
        this.spiceList = datas;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_spice, container, false);
        this_ = this;

        rc_spice = view.findViewById(R.id.rc_spice);
        adapter = new SpiceAdapter(rc_spice,getContext(),new ArrayList<Spice>(),this);
        rc_spice.setAdapter(adapter);
        rc_spice.setLayoutManager(new LinearLayoutManager(getContext()));
        rc_spice.addItemDecoration(new DividerItemDecoration(rc_spice.getContext(), DividerItemDecoration.VERTICAL));

        rc_spice_item = view.findViewById(R.id.rc_spice_item);
        adapter_spice_item = new SpiceItemAdapter(getContext(),new ArrayList<ItemsMontuychon>(),this);
        rc_spice_item.setAdapter(adapter_spice_item);
        rc_spice_item.setLayoutManager(new GridLayoutManager(getContext(),12, GridLayoutManager.HORIZONTAL, false));

        edt_name = view.findViewById(R.id.edt_name);
        btn_add = view.findViewById(R.id.btn_add);

        ll_exit = view.findViewById(R.id.ll_exit);
        ll_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.popFragment(getActivity());
            }
        });
        tv_page = view.findViewById(R.id.tv_page);
        tv_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentPageSpiceKitchenDialog editNameDialogFragment = new FragmentPageSpiceKitchenDialog(this_);
                editNameDialogFragment.show(fm, null);

            }
        });

        presenter = new SpicePresenter(this);
        presenter.loadData(spiceList);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(item!=null){
                    presenter.addNewItem(item,edt_name.getText().toString());
                }else{
                    item = new ItemsMontuychon();
                    item.setGiatc1(0.0);
                    presenter.addNewItem(item,edt_name.getText().toString());
                }


            }
        });

        return view;
    }


    @Override
    public void onItemClick(int position, Spice spice) {
        selectedPosition = position;
        mSpice = spice;
        presenter.showKeyboard(App.Companion.self().getString(R.string.Price));
    }

    @Override
    public void onItemLongClick(final int position, Spice spice) {
        Util.showCustomDialogConfirm(getActivity(), Util.getStringFromResouce(R.string.are_u_w_to_delete), new Util.ConfirmDialogInterface() {
            @Override
            public void onPositiveClickListener() {
                presenter.deletePriceItem(position);
            }
            @Override
            public void onNegativeClickListener() {

            }
        });
    }


    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }

    @Override
    public void showListSpice(List<Spice> datas) {
        adapter.setNewDatas(datas);
    }

    @Override
    public void showListSpiceItem(List<ItemsMontuychon> datas) {
        adapter_spice_item.setNewDatas(datas);
    }

    @Override
    public void getSpiceList(List<Spice> datas) {
        listener.onUpdateSpiceListListener(datas);
    }

    @Override
    public void showKeyboard(String title) {
        Fragment fragment = new KeyboardFragment(this, title ,mSpice.getPrice()+"","00",SessionApp.KeyboardType.TIEN);
        ((KeyboardFragment) fragment).show(getFragmentManager(), null);
    }

    @Override
    public void setEditTextEmpty() {
        edt_name.setText("");
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void doClickItem(final int pos) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                if(rc_spice.findViewHolderForAdapterPosition(pos)!=null)
                rc_spice.findViewHolderForAdapterPosition(pos).itemView.performClick();

            }
        },1);

    }

    @Override
    public void resetAdapter() {
        adapter.resetSelected();
    }

    @Override
    public void onKeyPressListener(String data) {
        if(data!=null && !data.isEmpty()){
            presenter.updatePriceItem(selectedPosition,data);
        }else{
            presenter.updatePriceItem(selectedPosition,"0.0");
        }
        adapter.resetSelected();
    }

    @Override
    public void onPageItemClickListener(String data) {
        tv_page.setText(data);
        presenter.getAllPageSpiceItem(data);
    }


    @Override
    public void onItemClick(int position, ItemsMontuychon data) {
        item = data;
        if(!edt_name.getText().toString().isEmpty() ){
            if(data.getTextdisplay()!=null){
                edt_name.setText(edt_name.getText().toString()+", "+data.getTextdisplay());
            }

        }else{
            edt_name.setText(data.getTextdisplay());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.getSpiceList();
    }
}
