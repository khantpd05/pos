package com.example.abc.pos.screen.bill_history;

import com.example.abc.pos.model.Orders;

import java.util.List;

public interface BillHistoryContract {
    interface View{
        void showBillHistory(List<Orders> ordersList);
        void showAlert(String data);
        void showDialog(String data);
        void showLoading();
        void dismissLoading();
        void showInLaiHoaDon(List<String> datas);
        void showDoiCachTraTien(List<String> datas);
    }

    interface Presenter{
        void loadBillHistoryList();
        void setOrders(Orders orders);
        void updateInLaiHoaDon(String data);
        void updateDoiCachTraTien(String data, int type);
    }
}
