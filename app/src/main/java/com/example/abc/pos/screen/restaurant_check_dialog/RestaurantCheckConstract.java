package com.example.abc.pos.screen.restaurant_check_dialog;

import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.model.Guschein;
import com.example.abc.pos.model.RestaurantCheck;

import java.util.List;

public interface RestaurantCheckConstract {
    interface View{
        void showRestaurantCheckList(List<RestaurantCheck> guscheinList);
        void getDataInput(String data);
        void showKeyboard(String title,String hint,String key11,SessionApp.KeyboardType keyboardType);
    }

    interface Presenter{
        void getDataInput();
        void showKeyboard();
    }
}
