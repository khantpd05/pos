
package com.example.abc.pos.base;

import android.app.Activity;
import android.content.Intent;

import com.example.abc.pos.R;


public class ActivityNavigationController {

    public static void showHomeActivity(Activity activity,Class zclass) {
        activity.startActivity(new Intent(activity,zclass));
    }

    public static void showLoginActivity(Activity activity, Class zclass) {
        activity.startActivity(new Intent(activity, zclass));
        activity.overridePendingTransition(R.anim.slide_in_bottom, R.anim.stand_still);
    }
}
