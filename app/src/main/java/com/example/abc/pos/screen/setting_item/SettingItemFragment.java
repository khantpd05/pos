package com.example.abc.pos.screen.setting_item;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.ItemsEditAdapter;
import com.example.abc.pos.adapter.MenuEditAdapter;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.screen.create_items_item.CreateItemsItemFragment;
import com.example.abc.pos.screen.create_menu_item.CreateMenuItemFragment;
import com.example.abc.pos.screen.setting_menu.SettingMenuPresenter;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingItemFragment extends BaseFragment implements SettingItemConstract.View , ItemsEditAdapter.ItemClickListener, View.OnClickListener {
    private ItemsEditAdapter adapter;
    private RecyclerView rc_item;
    private SettingItemPresenter presenter;
    private LinearLayout ll_extra_feature, ll_exit;
    private TextView btn_edit, btn_remove;
    private String menu_id;
    private String loai_mon_an;
    private static final String KEY= "menu_id";
    private static final String KEY_2= "loai_mon_an";
    private Button btn_add;
    private Items item;
    private int positionClicked;
    public SettingItemFragment() {
        // Required empty public constructor
    }

    public static SettingItemFragment newInstance(String param1,String param2) {
        SettingItemFragment fragment = new SettingItemFragment();
        Bundle args = new Bundle();
        args.putString(KEY, param1);
        args.putString(KEY_2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            menu_id = getArguments().getString(KEY);
            loai_mon_an = getArguments().getString(KEY_2);
        }
    }

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_setting_item;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting_item, container, false);

        rc_item = view.findViewById(R.id.rc_item);

        rc_item.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new ItemsEditAdapter(getContext(),new ArrayList<Items>(),this);

        rc_item.setAdapter(adapter);

        btn_add = view.findViewById(R.id.btn_add);
        btn_add.setOnClickListener(this);

        ll_extra_feature = view.findViewById(R.id.ll_extra_feature);
        ll_exit = view.findViewById(R.id.ll_exit);

        btn_edit = view.findViewById(R.id.btn_edit);
        btn_add = view.findViewById(R.id.btn_add);
        btn_remove = view.findViewById(R.id.btn_remove);

        btn_edit.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        btn_remove.setOnClickListener(this);
        ll_exit.setOnClickListener(this);


        presenter = new SettingItemPresenter(this);
        presenter.loadData(menu_id);

        return view;
    }

    @Override
    public void showListItems(List<Items> items) {
        adapter.setNewDatas(items);
    }

    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }

    @Override
    public void onItemClick(int position, Items item) {
        ll_extra_feature.setVisibility(View.VISIBLE);
        positionClicked = position;
        this.item = item;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_add:
                ((BaseActivity) getActivity()).pushFragment(new CreateItemsItemFragment(Integer.valueOf(menu_id),loai_mon_an), true);
                break;

            case R.id.btn_edit:
                Fragment fm = CreateItemsItemFragment.newInstance(item);
                ((BaseActivity) getActivity()).pushFragment(fm, true);
                break;

            case R.id.btn_remove: {
                Util.showCustomDialogConfirm(getActivity(), Util.getStringFromResouce(R.string.are_u_w_to_delete), new Util.ConfirmDialogInterface() {
                    @Override
                    public void onPositiveClickListener() {
                        presenter.deleteRow(item.getId());
                        adapter.removeItem(positionClicked);
                    }
                    @Override
                    public void onNegativeClickListener() {

                    }
                });

                break;
            }
            case R.id.ll_exit:
                Util.popFragment(getActivity());
                break;
        }
    }
}
