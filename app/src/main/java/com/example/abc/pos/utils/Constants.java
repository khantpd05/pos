package com.example.abc.pos.utils;


import com.example.abc.pos.R;

public class Constants {

    public static final String ENCRYPT_KEY = "18d38d09f1fb4d0862bb6a4aa67aa90c";
    public static final String IP_PRINTER = "192.168.18.100";
    public static final int TRANSITION_DURATION = 300;
    public static final int CAMERA_REQUEST_CODE = 1000;
    public static final int IMAGE_PICKER_REQUEST_CODE = 1001;

    public static final int MON_TU_DO = 1;
    public static final int GIAM_GIA_DON_HANG = 2;
    public static final int CHIA_HOA_DON = 3;
    public static final int TACH_BAN = 4;
    public static final int DOT_DAT_KE_TIEP = 5;
    public static final int THOAT = 6;

    /** color constants **/
    public class ColorConst{
        public static final int BSD_BLACK = R.font.barlowsemicondensed_black;
        public static final int BSD_BLACK_ITALIC = R.font.barlowsemicondensed_blackitalic;
        public static final int BSD_BOLD = R.font.barlowsemicondensed_bold;
        public static final int BSD_BOLD_ITALIC = R.font.barlowsemicondensed_bolditalic;
        public static final int BSD_EXTRA_BOLD = R.font.barlowsemicondensed_extrabold;
        public static final int BSD_EXTRA_BOLD_ITALIC = R.font.barlowsemicondensed_extrabolditalic;
        public static final int BSD_EXTRA_LIGHT = R.font.barlowsemicondensed_extralight;
        public static final int BSD_EXTRA_LIGHT_ITALIC = R.font.barlowsemicondensed_extralightitalic;
        public static final int BSD_ITALIC = R.font.barlowsemicondensed_italic;
        public static final int BSD_LIGHT = R.font.barlowsemicondensed_light;
        public static final int BSD_LIGHT_ITALIC = R.font.barlowsemicondensed_lightitalic;
        public static final int BSD_MEDIUM = R.font.barlowsemicondensed_medium;
        public static final int BSD_MEDIUM_ITALIC = R.font.barlowsemicondensed_mediumitalic;
        public static final int BSD_REGULAR = R.font.barlowsemicondensed_regular;
        public static final int BSD_SEMIBOLD = R.font.barlowsemicondensed_semibold;
        public static final int BSD_SEMIBOLD_ITALIC = R.font.barlowsemicondensed_semibolditalic;
        public static final int BSD_THIN = R.font.barlowsemicondensed_thin;
        public static final int BSD_THIN_ITALIC = R.font.barlowsemicondensed_thinitalic;

        public static final String TEXT_ORDER_PRINTED = "#7CA40D";
        public static final String TEXT_ORDER_NOT_PRINTED = "#C9CBCC";

        public static final String BG_ORDER_CHOSE = "#000000";
        public static final String BG_ORDER_CHOSE_GREEN = "#059139";
        public static final String BG_ORDER_NOT_CHOSE = "#323948";

        public static final String BG_ORDER_EVEN = "#282F3F";
        public static final String BG_ORDER_ODD = "#201D2E";
    }



    public class Value {
        public static final int DEFAULT_LANGUAGE_ID = 2;
    }

    public enum TransitionType {
        NONE(R.anim.stand_still, R.anim.stand_still, R.anim.stand_still, R.anim.stand_still),
        SLIDE_IN_RIGHT_TO_LEFT(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right),
        SLIDE_IN_LEFT_TO_RIGHT(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left),
        SLIDE_IN_BOTTOM(R.anim.slide_in_bottom, R.anim.stand_still, R.anim.stand_still, R.anim.slide_out_bottom),
        CROSS_FADE(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);

        public int enter;
        public int exit;
        public int popEnter;
        public int popExit;

        TransitionType(int enter, int exit, int popEnter, int popExit) {
            this.enter = enter;
            this.exit = exit;
            this.popEnter = popEnter;
            this.popExit = popExit;
        }
    }

    public enum LoadDataState {
        NONE,
        NEW,
        MORE
    }
}
