package com.example.abc.pos.binding;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abc.pos.screen.login.LoginPresenter;

public class LoginBinding {
    @BindingAdapter("onClick")
    public static void onClick(final View button, final LoginPresenter presenter){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(button instanceof TextView){

                    TextView tv = (TextView) button;

                        if(presenter.getPassword().get().length()>0){
                            presenter.isNotEmpty().set(true);
                        }else{
//                            presenter.isNotEmpty().set(false);
                        }
                        presenter.getPassword().set(presenter.getPassword().get()+tv.getText().toString());
                        presenter.isNotEmpty().set(true);

                }else if(button instanceof ImageView){
                    presenter.getPassword().set("");
//                    presenter.isNotEmpty().set(false);
                }


            }
        });
    }
}
