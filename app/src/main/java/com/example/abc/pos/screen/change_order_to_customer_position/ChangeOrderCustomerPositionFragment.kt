package com.example.abc.pos.screen.change_order_to_customer_position


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.example.abc.pos.R
import com.example.abc.pos.base.BaseActivity
import com.example.abc.pos.base.BaseFragment
import com.example.abc.pos.base.WapriManager
import com.example.abc.pos.model.OrdersTemp
import com.example.abc.pos.utils.KToast
import com.example.abc.pos.utils.Util


@SuppressLint("ValidFragment")
class ChangeOrderCustomerPositionFragment @SuppressLint("ValidFragment") constructor
(var ordersTempsWillChange: MutableList<OrdersTemp>,var positionsHaveOrder: List<Int>, var positions: Int,var table_name: String,var taichohaymangve: WapriManager.taichohaymangve,var listener:OnPositionClick,var listener_change_success:OnChangeOrderToTableSuccessful) : BaseFragment(), ChangeOrderCustomerPositionContract.View {


    override fun showAlert(data: String) {
        if (activity != null)
            KToast.show(activity!!, data, Toast.LENGTH_SHORT)
    }

    interface OnPositionClick {
        fun onCustomerPositionClickListener()
    }

    interface OnChangeOrderToTableSuccessful {
        fun onChangeOrderToTableSuccessfulListener(ordersTempListWillChange: MutableList<OrdersTemp>)
    }


    var ll_top: LinearLayout? = null
    var ll_bot: LinearLayout? = null
    var ll_exit: LinearLayout? = null
    var ll_prepared_table: LinearLayout? = null
    var presenter: ChangeOrderCustomerPositionContract.Presenter? = null
    lateinit var onChangeOrderCustomerPositionFragment: OnChangeOrderToTableSuccessful
    lateinit var main_table: TextView


    override fun getFragmentLayoutId(): Int {
        return R.layout.fragment_customer_position
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_customer_position, container, false)

        main_table = view.findViewById(R.id.main_table)
        ll_top = view.findViewById(R.id.ll_top)
        ll_bot = view.findViewById(R.id.ll_bot)
        ll_prepared_table = view.findViewById(R.id.ll_prepared_table)

        ll_exit = view.findViewById(R.id.ll_exit)

        ll_exit?.setOnClickListener{
            Util.popFragment(activity)
        }
        presenter = ChangeOrderCustomerPositionPresenter(this)
        presenter?.loadData(positions,taichohaymangve)


        main_table.setOnClickListener {
            presenter?.updateChangeOrderToTable(ordersTempsWillChange,1,table_name)
        }

        return view
    }


    override fun showPosition(positions: Int) {
        var halfSize = positions / 2


        if (positionsHaveOrder.size > 0) {
            main_table.setBackground(ContextCompat.getDrawable(context!!, R.drawable.bg_radius_all_position))
        } else {
            main_table.setBackground(ContextCompat.getDrawable(context!!, R.drawable.bg_radius_customer_position))
        }

        for (i in 0 until halfSize) {
            var k = 0
            for (j in 0 until positionsHaveOrder.size) {
                if ((i + 1) == positionsHaveOrder[j]) {
                    k = positionsHaveOrder[j]
                    break
                }
            }
            if (k != 0) {
                createNewButton(ll_top, i, R.style.TextCustomerPositionGreen)
            } else {
                createNewButton(ll_top, i, R.style.TextCustomerPosition)
            }
        }


        for (i in halfSize until positions) {
            var k = 0
            for (j in 0 until positionsHaveOrder.size) {
                if (i + 1 == positionsHaveOrder[j]) {
                    k = positionsHaveOrder[j]
                }
            }
            if (k != 0) {
                createNewButton(ll_bot, i, R.style.TextCustomerPositionGreen)
            } else {
                createNewButton(ll_bot, i, R.style.TextCustomerPosition)
            }
        }

        for (i in positions..positions + 4) {
            var k = 0
            for (j in 0 until positionsHaveOrder.size) {
                if (i + 1 == positionsHaveOrder[j]) {
                    k = positionsHaveOrder[j]
                } else {
                }
            }
            if (k != 0) {
                createNewButton(ll_prepared_table, i, R.style.TextCustomerPositionGreen)
            } else {
                createNewButton(ll_prepared_table, i, R.style.TextCustomerPosition)
            }
        }
    }


    private fun createNewButton(parent: LinearLayout?, i: Int, theme: Int) {
        var button = Button(ContextThemeWrapper(context, theme), null, 0)
        button.id = i + 1
        button.text = (i + 1).toString()
        val scale = context!!.resources.displayMetrics.density
        val pixels = (40 * scale + 0.5f).toInt()
        button.layoutParams = LinearLayout.LayoutParams(pixels, pixels)
        setMargins(button, 8, 8, 8, 8)
        parent?.addView(button)

        button.setOnClickListener {
            presenter?.updateChangeOrderToTable(ordersTempsWillChange,button.text.toString().toInt(),table_name)
        }
    }

    private fun setMargins(view: View, left: Int, top: Int, right: Int, bottom: Int) {
        if (view.layoutParams is ViewGroup.MarginLayoutParams) {
            val p = view.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }

    override fun returnOrder() {
        listener.onCustomerPositionClickListener()
        if(activity!=null)
        (activity as BaseActivity ).popFragment()
    }

    override fun showLoading() {
        super.showLoading()
    }

    override fun dismissLoading() {
        super.dismissLoading()
    }

    override fun onChangeSuccessful() {
        listener_change_success.onChangeOrderToTableSuccessfulListener(ordersTempsWillChange)
    }
}
