package com.example.abc.pos.screen.money_box;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.abc.pos.R;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.databinding.FragmentMoneyBoxBinding;
import com.example.abc.pos.model.UsersTienbandauTemplate;
import com.example.abc.pos.screen.default_money_box.DefaultMoneyBoxFragment;
import com.example.abc.pos.screen.keyboard.KeyboardFragment;
import com.example.abc.pos.screen.kitchen_note.KitchenNoteFragment;
import com.example.abc.pos.screen.table.TableFragment;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoneyBoxFragment extends BaseFragment implements MoneyBoxConstract.View, KeyboardFragment.OnKeyboardPress, DefaultMoneyBoxFragment.OnBoxClick, DefaultMoneyBoxFragment.OnReturn {

    private MoneyBoxPresenter presenter;
    public MoneyBoxFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_money_box;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentMoneyBoxBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_money_box, container, false);

        presenter = new MoneyBoxPresenter(this);
        binding.setPresenter(presenter);
        return binding.getRoot();
    }

    @Override
    public void showToast(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }

    @Override
    public void showKeyboard(String title, String hint) {
        Fragment fragment = new KeyboardFragment(this, title, hint,"00",SessionApp.KeyboardType.SO_LUONG);
        ((KeyboardFragment) fragment).show(getFragmentManager(), null);
    }

    @Override
    public void goToDefaultMoneyBox() {
        DefaultMoneyBoxFragment defaultMoneyBoxFragment = new DefaultMoneyBoxFragment();
        defaultMoneyBoxFragment.setData(this,this);
        ((BaseActivity) getActivity()).pushFragmentNotReplace(defaultMoneyBoxFragment, true);
    }

    @Override
    public void goBack() {
        Util.popFragment(getActivity());
    }

    @Override
    public void goToTable() {
//        Util.popFragment(getActivity());
        TableFragment tableFragment = new TableFragment();
        if(((BaseActivity) getActivity())!=null){
            ((BaseActivity) getActivity()).pushFragment(tableFragment, true);
        }


    }


    @Override
    public void onKeyPressListener(String data) {
        presenter.setTongTien(data);
    }

    @Override
    public void onBoxClickListener(UsersTienbandauTemplate usersTienbandauTemplate) {
        presenter.setUsersTienbandauTemplate(usersTienbandauTemplate);
    }

    @Override
    public void onReturnListener() {
        if(SessionApp.isSetting){
            presenter.enterOrSave.set(Util.getStringFromResouce(R.string.Save));
        }else{
            presenter.enterOrSave.set("Enter");
        }
    }
}
