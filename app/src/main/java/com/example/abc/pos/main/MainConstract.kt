package com.example.abc.pos.main

interface MainConstract{
    interface View{
       fun showLoginPage()
       fun showLoginOTPPage()
    }

    interface Presenter{

    }
}