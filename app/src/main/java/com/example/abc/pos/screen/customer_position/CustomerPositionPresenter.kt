package com.example.abc.pos.screen.customer_position

import android.os.AsyncTask
import com.example.abc.pos.database.MySqlHandleUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class CustomerPositionPresenter(var view: CustomerPositionContract.View): CustomerPositionContract.Presenter{
    override fun returnOrder(position : Int) {
        view.returnOrder(position)
    }


    override fun loadData(positions: Int) {
        view.showPosition(positions)
    }


}