package com.example.abc.pos.model;

import java.util.List;

public class PendingPaymentSync {
    private String userName = "";
    private List<OrdersTemp> divideOrdersTempList;
    private List<OrdersTemp> origOrdersTempList;
    private Table table;
    private double summerDivide;
    private String banso;
    private String from_DivideOrder_or_Order;
    public static final String KEY = "payment_sync";
    public List<OrdersTemp> getDivideOrdersTempList() {
        return divideOrdersTempList;
    }

    public void setDivideOrdersTempList(List<OrdersTemp> divideOrdersTempList) {
        this.divideOrdersTempList = divideOrdersTempList;
    }

    public List<OrdersTemp> getOrigOrdersTempList() {
        return origOrdersTempList;
    }

    public void setOrigOrdersTempList(List<OrdersTemp> origOrdersTempList) {
        this.origOrdersTempList = origOrdersTempList;
    }

    public double getSummerDivide() {
        return summerDivide;
    }

    public void setSummerDivide(double summerDivide) {
        this.summerDivide = summerDivide;
    }

    public String getBanso() {
        return banso;
    }

    public void setBanso(String banso) {
        this.banso = banso;
    }


    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public String getFrom_DivideOrder_or_Order() {
        return from_DivideOrder_or_Order;
    }

    public void setFrom_DivideOrder_or_Order(String from_DivideOrder_or_Order) {
        this.from_DivideOrder_or_Order = from_DivideOrder_or_Order;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
