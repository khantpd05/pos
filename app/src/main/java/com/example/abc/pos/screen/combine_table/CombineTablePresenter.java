package com.example.abc.pos.screen.combine_table;

import android.os.AsyncTask;

import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Table;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class CombineTablePresenter implements CombineTableContract.Presenter {

    CombineTableContract.View mView;
    private List<Table> tableList;

    public CombineTablePresenter(CombineTableContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void getUsingTable() {
        new GettingTableTask().execute();
    }

    @Override
    public void goToCombineToTable(List<Integer> positionsSelected) {
        List<Table> tables = new ArrayList<>();
        if(positionsSelected!=null && !positionsSelected.isEmpty()){
            for(int i = 0; i < positionsSelected.size(); i++){
                tables.add(tableList.get(positionsSelected.get(Integer.valueOf(i))));
            }
        }

        mView.goToCombineToTable(tables);
    }


    private class GettingTableTask extends AsyncTask<Void,Void,ArrayList<Table>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<Table> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT t.id, t.name, t.vitri, t.trangthai, t.soghengoi, t.username, t.tongtien, t.tabletype, t.status,giam," +
                    "t.tiengiam, t.tienthue19giam, t.tienthue7giam, t.taichohaymangve, t.thoigian, t.vitritronghayngoainha, t.orderfrom, t.from_tablet," +
                    "t.is_selling_on_tablet," +
                    "(select count(o.id) from wapri.orders_temp o where o.banso = t.name AND o.daprintchua = 'N' AND o.items_id NOT IN (1001,1005,10004) AND o.bophanphucvu <> 'Sonstiges0') as total_orders_not_printed, " +
                    "(select count(o.id) from wapri.orders_temp o where o.banso = t.name) as total_orders  FROM wapri.table t where (select count(o.id) from wapri.orders_temp o where o.banso = t.name) > 0 and t.username = "+"'"+WapriManager.getUser().getUsername() +"'");

            ArrayList<Table> tables =  new Gson().fromJson(js.toString(), new TypeToken<List<Table>>(){}.getType());
            for(Table table:tables){
                table.setTaichoHayMangVe();
            }
            return tables;
        }

        @Override
        protected void onPostExecute(ArrayList<Table> tables) {
            super.onPostExecute(tables);
            mView.dismissLoading();
            tableList = tables;
            mView.showListTable(tables);

        }
    }
}
