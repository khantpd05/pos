package com.example.abc.pos.screen.free_dish;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.MyEdittext;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.screen.order.OrderFragment;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class FreeDishFragment extends BaseFragment implements FreeDishConstract.View, View.OnClickListener {
    private Spinner spinner;
    private FreeDishConstract.Presenter presenter;
    private TextView btn_bep, btn_nuoc, btn_sushi, btn_add;
    private EditText edt_food_name;
    private MyEdittext  edt_price;
    private String loai_mon_an;
    private String bophanphucvu = "Bep";
    private LinearLayout ll_exit;
    private OnAddFreeDish listener;

    public interface OnAddFreeDish{
        void onAddFreeDishListener(OrdersTemp ordersTemp);
    }

    public FreeDishFragment(){

    }

    public FreeDishFragment(OnAddFreeDish listener) {
        // Required empty public constructor
        this.listener = listener;
    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_free_dish;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_free_dish, container, false);
        if(listener==null){
            listener = OrderFragment.getOrderFragmentListener();
        }
        spinner = view.findViewById(R.id.spn_free_dish);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextSize(22);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edt_food_name = view.findViewById(R.id.edt_food_name);
        edt_price = view.findViewById(R.id.edt_price);
        ll_exit = view.findViewById(R.id.ll_exit);
        ll_exit.setOnClickListener(this);

        btn_add = view.findViewById(R.id.btn_add);
        btn_bep = view.findViewById(R.id.btn_bep);
        btn_nuoc = view.findViewById(R.id.btn_nuoc);
        btn_sushi = view.findViewById(R.id.btn_sushi);
        btn_bep.setOnClickListener(this);
        btn_nuoc.setOnClickListener(this);
        btn_sushi.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        presenter = new FreeDishPresenter(this);
        return view;
    }


    @Override
    public void showSpinner(List<String> datas) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_food_type_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
                if(i == 0){
                    loai_mon_an = WapriManager.loai_mon_an.KhaiVi.value;
                }else if(i == 1){
                    loai_mon_an = WapriManager.loai_mon_an.Nuoc.value;
                }else if(i == 2){
                    loai_mon_an = WapriManager.loai_mon_an.MonChinh.value;
                }else if(i == 3){
                    loai_mon_an = WapriManager.loai_mon_an.TrangMieng.value;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void renderButtonText(List<WapriManager.bophanphucvu> datas) {
        btn_bep.setText(datas.get(0).display);
        btn_nuoc.setText(datas.get(1).display);
        btn_sushi.setText(datas.get(2).display);
        btn_bep.setBackgroundColor(Color.parseColor("#43AF91"));
    }

    @Override
    public void returnOrder() {

        ((BaseActivity) getActivity()).popFragment();
    }

    @Override
    public void addFreeDishOrder(OrdersTemp ordersTemp) {
        listener.onAddFreeDishListener(ordersTemp);
        returnOrder();
    }

    int positionSelected = -1;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_bep:
                setBackgroundSelectedButton(btn_bep);
                setBackgroundNotSelectedButton(btn_nuoc);
                setBackgroundNotSelectedButton(btn_sushi);
                bophanphucvu = WapriManager.bophanphucvu.Bep.value;
                break;
            case R.id.btn_nuoc:
                setBackgroundSelectedButton(btn_nuoc);
                setBackgroundNotSelectedButton(btn_bep);
                setBackgroundNotSelectedButton(btn_sushi);
                bophanphucvu = WapriManager.bophanphucvu.PhaChe19.value;
                break;
            case R.id.btn_sushi:
                setBackgroundSelectedButton(btn_sushi);
                setBackgroundNotSelectedButton(btn_bep);
                setBackgroundNotSelectedButton(btn_nuoc);
                bophanphucvu = WapriManager.bophanphucvu.Sushi.value;
                break;
            case R.id.btn_add:
                if(edt_food_name.getText().toString().trim().isEmpty()||edt_price.getText().toString().trim().isEmpty()){
                    if(getActivity()!=null)
                        KToast.show(getActivity(), Util.getStringFromResouce(R.string.please_enter_full_information_of_item), Toast.LENGTH_SHORT);
                }else{
                    presenter.addOrder(edt_food_name.getText().toString(),edt_price.getCurrencyDouble(),loai_mon_an,bophanphucvu);
                }
                break;
            case R.id.ll_exit:
                Util.popFragment(getActivity());
                break;
        }
    }

    private void setBackgroundSelectedButton(TextView btn) {
        btn.setBackgroundColor(Color.parseColor("#43AF91"));
    }

    private void setBackgroundNotSelectedButton(TextView btn) {
        btn.setBackgroundColor(Color.parseColor("#1b61be"));
    }
}
