package com.example.abc.pos.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.service.RealtimeService;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RestartService extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
//        Intent i  = new Intent(context,RealtimeService.class);
////        writeToFile("restarting...");
//        i.putExtra("RestartService","RestartService");
//
//        context.startService(i);
        SessionApp.global_table.setIs_selling_on_POS("N");
        SessionApp.global_table.setis_selling_on_tablet("N");
        new UpdateTableToDbTask(SessionApp.global_table).execute();
    }



 class UpdateTableToDbTask extends AsyncTask<Void, Void, Integer> {

    Table table;

    public UpdateTableToDbTask(Table table) {
        this.table = table;
    }

    @Override
    protected Integer doInBackground(Void... voids) {
        if (table != null) {
            if (MySqlHandleUtils.updateTableChangeNew(table) != 0)
                return 1;
            else return 0;
        }
        return 1;
    }

    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
        if (result == 1) {

        } else {

        }
    }
}
//    private void writeToFile(String currentStacktrace) {
//        try {
//
//            //Gets the Android external storage directory & Create new folder Crash_Reports
//            File dir = new File(Environment.getExternalStorageDirectory(),
//                    "Wapri_crash_report");
//            if (!dir.exists()) {
//                dir.mkdirs();
//            }
//
//            SimpleDateFormat dateFormat = new SimpleDateFormat(
//                    "yyyy_MM_dd_HH_mm_ss");
//            Date date = new Date();
//            String filename = dateFormat.format(date) + ".STACKTRACE";
//
//            // Write the file into the folder
//            File reportFile = new File(dir, filename);
//            FileWriter fileWriter = new FileWriter(reportFile);
//            fileWriter.append(currentStacktrace);
//            fileWriter.flush();
//            fileWriter.close();
//        } catch (Exception e) {
//            Log.e("ExceptionHandler", e.getMessage());
//        }
//    }
}