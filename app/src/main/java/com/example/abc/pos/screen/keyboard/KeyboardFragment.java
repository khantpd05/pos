package com.example.abc.pos.screen.keyboard;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.databinding.FragmentKeyboardBinding;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.screen.order.OrderFragment;
import com.example.abc.pos.utils.HideNavigationBarComponent;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Locale;

import faranjit.currency.edittext.CurrencyEditText;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class KeyboardFragment extends BottomSheetDialogFragment implements KeyboardConstract.View {

    public interface OnKeyboardPress{
        void onKeyPressListener(String data);
    }
    private static Dialog dialog;
    OnKeyboardPress mListener;
    String hint;
    String title;
    String key11;
    KeyboardPresenter presenter;
    SessionApp.KeyboardType keyboardType;

    public KeyboardFragment(){

    }

    public KeyboardFragment(OnKeyboardPress mListener,String title, String hint, String key11, SessionApp.KeyboardType keyboardType) {
        // Required empty public constructor
        this.mListener = mListener;
        this.title = title;
        this.hint = hint;
        this.key11 = key11;
        this.keyboardType = keyboardType;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        FragmentKeyboardBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_keyboard,container,false);
        View view = binding.getRoot();

        CurrencyEditText edt_money = view.findViewById(R.id.edt_money);
        EditText edt_percent = view.findViewById(R.id.edt_percent);

        ((EditText) view.findViewById(R.id.edt_money)).setRawInputType(InputType.TYPE_CLASS_TEXT);
        ((EditText) view.findViewById(R.id.edt_money)).setTextIsSelectable(true);

        ((EditText) view.findViewById(R.id.edt_percent)).setRawInputType(InputType.TYPE_CLASS_TEXT);
        ((EditText) view.findViewById(R.id.edt_percent)).setTextIsSelectable(true);

        edt_money.setLocale(new Locale("de", "DE"));


        presenter = new KeyboardPresenter(this);
        if(keyboardType == SessionApp.KeyboardType.TIEN){
            edt_percent.setVisibility(View.GONE);
            edt_money.setVisibility(View.VISIBLE);
            presenter.mViewModel.hint.set(String.format("%,.2f",Double.valueOf(hint)));
        }else if(keyboardType == SessionApp.KeyboardType.PASSWORD){
            edt_percent.setTransformationMethod(PasswordTransformationMethod.getInstance());
            edt_percent.setVisibility(View.VISIBLE);
            edt_money.setVisibility(View.GONE);
            presenter.mViewModel.hint.set(hint);
        }else{
            edt_percent.setVisibility(View.VISIBLE);
            edt_money.setVisibility(View.GONE);
            presenter.mViewModel.hint.set(hint);
        }

        presenter.mViewModel.title.set(title);
        presenter.mViewModel.key11.set(key11);
        presenter.mViewModel.keyboardType.set(keyboardType);
        binding.setPresenter(presenter);

        return view;
    }



    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();
        if (dialog != null) {
            DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
            int height = displayMetrics.heightPixels;
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = (int) (height*0.60);
        }
        final View view = getView();
        view.post(new Runnable() {
            @Override
            public void run() {
                View parent = (View) view.getParent();
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
                CoordinatorLayout.Behavior behavior = params.getBehavior();
                BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) behavior;
                bottomSheetBehavior.setPeekHeight(view.getMeasuredHeight());
            }
        });
    }

    @Override
    public void getDataInput(String data) {
        if(keyboardType == SessionApp.KeyboardType.TIEN){
            if(!data.equals("")){
                mListener.onKeyPressListener(String.valueOf(Double.parseDouble(data)/100));
            }else{
                mListener.onKeyPressListener(hint);
            }
        }
        else {
            if(!data.equals("")) mListener.onKeyPressListener(data);
            else mListener.onKeyPressListener(hint);
        }
        dialog.dismiss();
    }

    public static void dismissKeyboard(){
        dialog.dismiss();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
