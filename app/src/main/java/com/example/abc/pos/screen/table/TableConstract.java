package com.example.abc.pos.screen.table;

import android.content.Context;

import com.example.abc.pos.model.Table;
import com.example.abc.pos.model.TableType;

import java.util.ArrayList;
import java.util.List;

public interface TableConstract {
    interface View{
        void showToast(String data);
        void showListTable(List<Table> tables);
        void showListTableType(List<TableType> tables,boolean isAll);
        void showLayBan(List<String> datas);
        void goToOrder(Table table,String data);
        void showLoading();
        void dismissLoading();
        void showCustomDialog(String text);
        void goToPayment();
        void hideKeyBoard();
        void exit();
    }
    interface Presenter{
        void loadData();
        void loadAllTablesHaveCustomer();
        void getAllTable();
        void goToOrder(Table table);
        void showPendingPayment();
        void stopRepeatingTask();
        void startRepeatingTask();
    }
}
