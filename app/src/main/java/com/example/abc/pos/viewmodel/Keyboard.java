package com.example.abc.pos.viewmodel;

import android.databinding.ObservableField;

import com.example.abc.pos.base.SessionApp;

public class Keyboard {
    public ObservableField<SessionApp.KeyboardType> keyboardType = new ObservableField<>();
    public ObservableField<String> title = new ObservableField<>("");
    public ObservableField<String> input = new ObservableField<>("");
    public ObservableField<String> hint = new ObservableField<>("");
    public ObservableField<String> key1 = new ObservableField<>("");
    public ObservableField<String> key2 = new ObservableField<>("");
    public ObservableField<String> key3 = new ObservableField<>("");
    public ObservableField<String> key4 = new ObservableField<>("");
    public ObservableField<String> key5 = new ObservableField<>("");
    public ObservableField<String> key6 = new ObservableField<>("");
    public ObservableField<String> key7 = new ObservableField<>("");
    public ObservableField<String> key8 = new ObservableField<>("");
    public ObservableField<String> key9 = new ObservableField<>("");
    public ObservableField<String> key10 = new ObservableField<>("");
    public ObservableField<String> key11 = new ObservableField<>("");
}
