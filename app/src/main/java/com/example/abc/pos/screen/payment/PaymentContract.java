package com.example.abc.pos.screen.payment;

import com.example.abc.pos.model.OrdersTemp;

import java.util.List;

public interface PaymentContract {
    interface View{
        void showLoading();
        void dismissLoading();
        void showListOrder(List<OrdersTemp> ordersTempList);
        void goToPaymentDetail(String orderId,String summer_price);
    }
    interface Presenter{
        void loadData(List<OrdersTemp> ordersTempList,double summer_price,String banso);
        void goToPaymentDetail();
    }
}
