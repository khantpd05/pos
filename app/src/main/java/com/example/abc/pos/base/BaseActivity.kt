package com.example.abc.pos.base

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.example.abc.pos.R
import com.example.abc.pos.screen.login.LoginFragment
import com.example.abc.pos.utils.Constants
import com.example.abc.pos.utils.Util
import java.util.*
import kotlin.collections.ArrayList

open class BaseActivity : AppCompatActivity() {


    companion object {
        var fragmentNames = Stack<String>()
    }
    protected var fragmentNamesArr = ArrayList<String>()
    protected var blockAllTouchEvent = false
    var fragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        if(savedInstanceState!=null){
            fragmentNamesArr = savedInstanceState.get("fragmentNames") as ArrayList<String>
            fragmentNames.addAll(fragmentNamesArr)
        }
    }

    fun pushFragment(fragment: Fragment, addToBackStack: Boolean) {
        pushFragment(fragment, Constants.TransitionType.SLIDE_IN_RIGHT_TO_LEFT, addToBackStack)
        this.fragment = fragment
    }

    fun pushFragment(fragment: Fragment, transitionType: Constants.TransitionType,
                               addToBackStack: Boolean) {
        //        blockAllTouchEventOnAnimation();

        val fragmentName = fragment.javaClass.simpleName

        supportFragmentManager.beginTransaction()
                .setCustomAnimations(transitionType.enter, transitionType.exit,
                        transitionType.popEnter, transitionType.popExit)
                .replace(R.id.fragment_container, fragment, fragmentName)
                .addToBackStack(fragmentName)
                .commitAllowingStateLoss()
        if (addToBackStack) {
            fragmentNames.push(fragmentName)
        }
    }

    fun pushFragmentNotBackStack(fragment: Fragment?, transitionType: Constants.TransitionType,
                                 addToBackStack: Boolean) {
        //        blockAllTouchEventOnAnimation();

        val fragmentName = fragment?.javaClass?.simpleName + System.currentTimeMillis()

        supportFragmentManager.beginTransaction()
                .setCustomAnimations(transitionType.enter, transitionType.exit,
                        transitionType.popEnter, transitionType.popExit)
                .replace(R.id.fragment_container, fragment, fragmentName)
                .commitAllowingStateLoss()
    }

    fun pushFragmentNotReplace(fragment: Fragment, addToBackStack: Boolean) {
        pushFragmentNotReplace(fragment, Constants.TransitionType.SLIDE_IN_RIGHT_TO_LEFT, addToBackStack)
        this.fragment = fragment
    }

    fun pushFragmentNotReplace(fragment: Fragment, transitionType: Constants.TransitionType,
                     addToBackStack: Boolean) {
        //        blockAllTouchEventOnAnimation();

        val fragmentName = fragment.javaClass.simpleName

        supportFragmentManager.beginTransaction()
                .setCustomAnimations(transitionType.enter, transitionType.exit,
                        transitionType.popEnter, transitionType.popExit)
                .add(R.id.fragment_container, fragment, fragmentName)
                .addToBackStack(fragmentName)
                .commitAllowingStateLoss()
        if (addToBackStack) {
            fragmentNames.push(fragmentName)
        }
    }


    fun popFragment() {
        blockAllTouchEventOnAnimation()
        var tag_temp= ""
        try{

            if (!fragmentNames.empty()) {
                val tag = fragmentNames.pop()
                tag_temp = tag
                supportFragmentManager.popBackStack(tag,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }else if(!SessionApp.table_fragment_name.isEmpty()){
                supportFragmentManager.popBackStack(SessionApp.table_fragment_name,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }
        }catch (ex : IllegalStateException){
            if(tag_temp!=null && tag_temp!="")
            SessionApp.table_fragment_name = tag_temp
        }
    }

    fun popFragment(exceptFrag:String) {
        blockAllTouchEventOnAnimation()
        if (!fragmentNames.empty() && !fragmentNames.peek().contains(exceptFrag)) {
            val tag = fragmentNames.pop()
            supportFragmentManager.popBackStack(tag,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }


    fun popBackStackImmediate() {
        blockAllTouchEventOnAnimation()
            supportFragmentManager.popBackStackImmediate()


    }

    fun blockAllTouchEventOnAnimation() {
        Util.delayExecute(300)
                .doOnSubscribe({ blockAllTouchEvent = true })
                .doOnNext({ aLong -> blockAllTouchEvent = false })
                .subscribe(SubscriberImpl())
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        fragmentNamesArr =  ArrayList()
        fragmentNamesArr.addAll(fragmentNames)
        outState?.putStringArrayList("fragmentNames", fragmentNamesArr)
    }
}

