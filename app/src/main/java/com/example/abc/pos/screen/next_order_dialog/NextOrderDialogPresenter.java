package com.example.abc.pos.screen.next_order_dialog;

import android.databinding.ObservableField;
import android.os.AsyncTask;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.model.Guschein;
import com.example.abc.pos.model.Tieudedotdat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class NextOrderDialogPresenter implements NextOrderDialogContract.Presenter {

    private NextOrderDialogContract.View mView;
    private List<Tieudedotdat> tieudedotdatList;
    public  ObservableField<String> tieuDeDotDat = new ObservableField<>("");

    public NextOrderDialogPresenter(NextOrderDialogContract.View mView) {
        this.mView = mView;
        new SelectTieuDeDotDatTask().execute();
    }


    @Override
    public void getDataInput(Tieudedotdat tieudedotdat) {
        tieuDeDotDat.set(tieudedotdat.getName());
    }

    @Override
    public void enterPress() {
        if(tieuDeDotDat.get().trim().isEmpty()){
            mView.showToast(App.Companion.self().getString(R.string.please_select_the_next_order_title));
        }else{
            mView.enterPress(tieuDeDotDat.get());
        }

    }

    @Override
    public void clearInput() {
        tieuDeDotDat.set("");
    }


    private class SelectTieuDeDotDatTask extends AsyncTask<Void,Void,List<Tieudedotdat>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Tieudedotdat> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.tieudedotdat");
            ArrayList<Tieudedotdat> tieudedotdats =  new Gson().fromJson(js.toString(), new TypeToken<List<Tieudedotdat>>(){}.getType());
            return tieudedotdats;
        }

        @Override
        protected void onPostExecute(List<Tieudedotdat> tieudedotdats) {
            super.onPostExecute(tieudedotdats);
            tieudedotdatList = tieudedotdats;
            mView.showTieuDeDotDat(tieudedotdats);
        }
    }
}
