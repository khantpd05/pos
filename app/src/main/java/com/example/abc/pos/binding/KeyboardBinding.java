package com.example.abc.pos.binding;

import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.databinding.InverseBindingListener;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.screen.keyboard.KeyboardPresenter;

public class KeyboardBinding {
    @BindingAdapter("onClick")
    public static void onClick(final TextView button, final KeyboardPresenter presenter) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(button.getTag()!=null){
                    if(button.getTag().toString().equals("del")){
                        if( presenter.mViewModel.input.get().length()>0){
                            String inputText = presenter.mViewModel.input.get();
                            presenter
                                    .mViewModel
                                    .input.
                                    set(inputText.substring(0, inputText.length() - 1));
                        }
                    }else if(button.getTag().toString().equals("x")){
                        presenter.mViewModel.input.set("");
                    }

                }

                 if (button.getText().equals("ENTER")) {
                    presenter.getDataInput();
                } else {
//                    if(presenter.mViewModel.input.get().length()>0){
//                        presenter.isNotEmpty().set(true);
//                    }else{
//                        presenter.isNotEmpty().set(false);
//                    }




                    if (presenter.mViewModel.keyboardType.get() == SessionApp.KeyboardType.PHAN_TRAM) {
                        try {
                            if(button.getText().equals("00")){
                                presenter.mViewModel.input.set(presenter.mViewModel.input.get() +"0");
                                if(Integer.valueOf(presenter.mViewModel.input.get())>=10){
                                    presenter.mViewModel.input.set("100");
                                }
                            }
                            if (presenter.mViewModel.input.get() != null && !presenter.mViewModel.input.get().isEmpty()) {
                                int phantram = Integer.parseInt(presenter.mViewModel.input.get());
                                if (phantram > 10) {
                                    presenter.mViewModel.input.set("100");
                                } else {
                                    presenter.mViewModel.input.set(presenter.mViewModel.input.get() + button.getText().toString());
                                }
                            } else {
                                presenter.mViewModel.input.set(presenter.mViewModel.input.get() + button.getText().toString());
                            }

                        } catch (NumberFormatException ex) {
                        }
                    } else {
                        presenter.mViewModel.input.set(presenter.mViewModel.input.get() + button.getText().toString());
                    }
                }

            }
        });
    }


    @BindingAdapter(value = {"setText", "setTextAttrChanged"}, requireAll = false)
    public static void setTextEdittextText(EditText editText, String value, final InverseBindingListener newTextAttrChanged) {
        if (SessionApp.keyboardType == SessionApp.KeyboardType.TIEN) {
            editText.setGravity(Gravity.RIGHT);
        }
        try{
            editText.setText(value);
        }catch (NumberFormatException ex){

        }



//        editText.setSelection(editText.getText().length());
    }

    @InverseBindingAdapter(attribute = "setText", event = "setTextAttrChanged")
    public static String getTextEdittextText(EditText editText) {
        return editText.getText().toString();
    }
}
