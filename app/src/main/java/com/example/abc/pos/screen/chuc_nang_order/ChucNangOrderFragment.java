package com.example.abc.pos.screen.chuc_nang_order;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.databinding.FragmentChucNangOrderBinding;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class ChucNangOrderFragment extends BottomSheetDialogFragment implements ChucNangOrderConstract.View {

    OnChucNangPress mListener;


    public interface OnChucNangPress{
        void onChucNangPressListener(int key);
    }
    Dialog dialog;
    SessionApp.KeyboardType keyboardType;
    ChucNangOrderPresenter presenter;
    public ChucNangOrderFragment(){

    }

    public ChucNangOrderFragment(OnChucNangPress listener ){
        mListener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        FragmentChucNangOrderBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_chuc_nang_order,container,false);
        View view = binding.getRoot();
        presenter = new ChucNangOrderPresenter(this);

        binding.setPresenter(presenter);
        return view;
    }

    @Override
    public void doFeatureClick(int key) {
        mListener.onChucNangPressListener(key);
        dialog.dismiss();
    }


    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();

        if (dialog != null) {
            DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
            int height = displayMetrics.heightPixels;
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = (int) (height*0.60);
        }
        final View view = getView();
        view.post(new Runnable() {
            @Override
            public void run() {
                View parent = (View) view.getParent();
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
                CoordinatorLayout.Behavior behavior = params.getBehavior();
                BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) behavior;
                bottomSheetBehavior.setPeekHeight(view.getMeasuredHeight());
            }
        });
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
