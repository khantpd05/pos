package com.example.abc.pos.screen.payment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.ExpandOrderSaleAdapter;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.databinding.FragmentPaymentBinding;
import com.example.abc.pos.model.PendingPaymentSync;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.screen.payment_detail.PaymentDetailFragment;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.utils.Util;

import java.util.ArrayList;
import java.util.List;

import static com.example.abc.pos.base.SessionApp.OrderOrPayment.PAYMENT;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class PaymentFragment extends BaseFragment implements PaymentContract.View, PaymentDetailFragment.OnPaymentFinish, PaymentDetailFragment.OnPaymentCancel, ExpandOrderSaleAdapter.OnItemClickGroup {

    private ExpandableListView orderTempExpandList;
    private ExpandOrderSaleAdapter orderSaleAdapter;
    private PaymentPresenter presenter;
    private List<OrdersTemp> origOrdersTempList = new ArrayList<>();
    private List<OrdersTemp> divideOrdersTempList = new ArrayList<>();
    private List<OrdersTemp> generalOrderTempList = new ArrayList<>();
    private String banso;
    private double summerDivide;

    private OnPaymentFinish listener;
    private OnPaymentCancel cancel_listener;
    private PendingPaymentSync paymentSync;

    @Override
    public void onItemClickGroupListener(List<Integer> positions, OrdersTemp ordersTemp) {

    }

    @Override
    public void onItemHeaderListener(OrdersTemp ordersTemp) {

    }

    @Override
    public void onItemSelectedListener(int positionSelected, OrdersTemp ordersTemp) {

    }



    public interface OnPaymentFinish {
        void onPaymentFinishListener(List<OrdersTemp> origOrderTemp_list);
    }

    public interface OnPaymentCancel {
        void onPaymentCancelListener();
    }

    public PaymentFragment() {
        paymentSync = SharedPrefs.getInstance().get(PendingPaymentSync.KEY, PendingPaymentSync.class);
        if (paymentSync != null) {
            this.divideOrdersTempList = paymentSync.getDivideOrdersTempList();
            this.origOrdersTempList = paymentSync.getOrigOrdersTempList();
            this.banso = paymentSync.getBanso();
            SessionApp.TABLE_NAME = this.banso;
            this.summerDivide = paymentSync.getSummerDivide();
            SessionApp.global_table = paymentSync.getTable();
            SessionApp.from_DivideOrder_or_Order = paymentSync.getFrom_DivideOrder_or_Order();
        }
    }

    public PaymentFragment(List<OrdersTemp> divideOrdersTempList, List<OrdersTemp> origOrdersTempList, double summerDivide, String banso, OnPaymentFinish listener, OnPaymentCancel cancel_listener) {
        // Required empty public constructor
        this.divideOrdersTempList = divideOrdersTempList;
        this.origOrdersTempList = origOrdersTempList;
        this.banso = banso;
        this.summerDivide = summerDivide;
        this.listener = listener;
        this.cancel_listener = cancel_listener;

        if(!WapriManager.configuration().huyThanhToan.get()) {
            PendingPaymentSync paymentSync = new PendingPaymentSync();
            paymentSync.setUserName(SessionApp.global_table.getUsername());
            paymentSync.setBanso(banso);
            paymentSync.setSummerDivide(summerDivide);
            paymentSync.setDivideOrdersTempList(divideOrdersTempList);
            paymentSync.setOrigOrdersTempList(origOrdersTempList);
            paymentSync.setTable(SessionApp.global_table);
            paymentSync.setFrom_DivideOrder_or_Order(SessionApp.from_DivideOrder_or_Order);
            SharedPrefs.getInstance().put(PendingPaymentSync.KEY, paymentSync);
        }
    }

    public PaymentFragment(List<OrdersTemp> origOrdersTempList, double summerDivide, String banso, OnPaymentFinish listener, OnPaymentCancel cancel_listener) {
        // Required empty public constructor
        this.origOrdersTempList = origOrdersTempList;
//        List<OrdersTemp> deleteList = new ArrayList<>();
//        for(OrdersTemp ordersTemp: this.origOrdersTempList){
//            if(ordersTemp.getLydohuyxoa()!=null && !ordersTemp.getLydohuyxoa().isEmpty()){
//                deleteList.add(ordersTemp);
//            }
//        }
//        if(deleteList.size()>0){
//            for(OrdersTemp ordersTemp: deleteList){
//                this.origOrdersTempList.remove(ordersTemp);
//            }
//        }
        this.banso = banso;
        this.summerDivide = summerDivide;
        this.listener = listener;
        this.cancel_listener = cancel_listener;

        if(!WapriManager.configuration().huyThanhToan.get()){
            PendingPaymentSync paymentSync = new PendingPaymentSync();
            paymentSync.setUserName(WapriManager.getUser().getUsername());
            paymentSync.setBanso(banso);
            paymentSync.setSummerDivide(summerDivide);
            paymentSync.setOrigOrdersTempList(origOrdersTempList);
            paymentSync.setTable(SessionApp.global_table);
            paymentSync.setFrom_DivideOrder_or_Order(SessionApp.from_DivideOrder_or_Order);
            SharedPrefs.getInstance().put(PendingPaymentSync.KEY, paymentSync);
        }

    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_payment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        FragmentPaymentBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment, container, false);
        View view = binding.getRoot();
        if (paymentSync != null) {
            Util.showCustomDialog(getContext(), Util.getStringFromResouce(R.string.previous_bill_not_payment_previous_bill));
        }
        if (savedInstanceState != null) {
            Util.popFragment(getActivity());
        } else {
            orderTempExpandList = view.findViewById(R.id.orderTempList);
            orderSaleAdapter = new ExpandOrderSaleAdapter(getContext(), new ArrayList<OrdersTemp>(), PAYMENT, this);
            orderTempExpandList.setAdapter(orderSaleAdapter);
            presenter = new PaymentPresenter(this);

            if (divideOrdersTempList != null && divideOrdersTempList.size() >0){
                generalOrderTempList = divideOrdersTempList;
            }
            else{
                generalOrderTempList = origOrdersTempList;
            }

            double tongTien = Util.tinhTongThanhTien(generalOrderTempList);
            SessionApp.global_table.setTiengiam((tongTien * SessionApp.global_table.getGiam()) / 100);
            presenter.loadData(generalOrderTempList, summerDivide, banso);
            binding.setPresenter(presenter);
        }


        return view;
    }



    @Override
    public void showListOrder(List<OrdersTemp> ordersTempList) {
        orderSaleAdapter.setNewDatas(ordersTempList);
    }

    @Override
    public void goToPaymentDetail(String orderId, String summer_price) {

        ((BaseActivity) getActivity()).pushFragmentNotReplace(new PaymentDetailFragment(orderId, origOrdersTempList, divideOrdersTempList, summer_price, this, this), true);
    }

    @Override
    public void onPaymentFinishListener(List<OrdersTemp> origOrderTemp_list) {
        if (listener != null) listener.onPaymentFinishListener(origOrderTemp_list);
    }

    @Override
    public void onPaymentCancelListener() {
        if (cancel_listener != null) cancel_listener.onPaymentCancelListener();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("banso", banso);
    }
}
