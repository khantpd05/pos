package com.example.abc.pos.screen.login_otp

import android.content.DialogInterface
import android.databinding.ObservableField
import android.os.AsyncTask
import android.os.Handler
import android.util.Log
import com.example.abc.pos.App
import com.example.abc.pos.R
import com.example.abc.pos.base.SessionApp
import com.example.abc.pos.base.SessionApp.admin_password
import com.example.abc.pos.base.WapriManager
import com.example.abc.pos.database.MySqlHandleUtils
import com.example.abc.pos.model.*
import com.example.abc.pos.utils.SharedPrefs
import com.example.abc.pos.utils.Util
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class LoginOTPPresenter(mView: LoginOTPConstract.View) : LoginOTPConstract.Presenter {


    var mView: LoginOTPConstract.View = mView
    var password = ObservableField<String>("")
    var token = ObservableField<String>("")
    var isLogged = ObservableField<Boolean>(false)
    var font: Int = R.font.barlowsemicondensed_extrabold

    lateinit var mobile_Login: MobileLogin

    var loginMethod: ObservableField<String> = ObservableField("")
    var device_name: ObservableField<String> = ObservableField("")

    private var imei = ""
    private var STATE_LOGIN = 0

    private val NOT_LOGGED = 0
    private val STILL_LOGGED = 1
    private val EXPIRED = 2

    private val TALBE = 1
    private val SETTING = 2

    private var table_or_setting = 0

    private var mHandler: Handler? = null
    private val mInterval = 2000

    private var onlyOne = 1

    private var deviceName = ""

    var runnableLoginTask = object : Runnable {
        override fun run() {
            try {
                if (WapriManager.configuration().otpLogin.get()!!) {
                    if (onlyOne > 1)
                        getLoginStateTask(deviceName).execute()
                }
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler?.postDelayed(this, mInterval.toLong())
            }
        }
    }

    init {
        setNameDevice()
        if (WapriManager.configuration().otpLogin.get()!!) {
            loginMethod.set(Util.getStringFromResouce(R.string.otp_login))
        } else {
            loginMethod.set(Util.getStringFromResouce(R.string.Login))
        }

        if (SessionApp.WIFI_STATE == 0) {
            mView.showToast(App.self()!!.getString(R.string.Please_turn_on_wifi))
        } else {
            connectToDb()
        }
    }


    private fun listenAuthorizationLoginOTP() {
        mHandler = Handler()
        runnableLoginTask.run()
    }

    override fun setNameDevice() {
        val data = SharedPrefs.getInstance().get(SharedPrefs.KEY_DEVICE_NAME, String::class.java)
        deviceName = if (data != null && !data.trim().isEmpty()) {
            data
        } else {
            ""
        }
        device_name.set(deviceName)
        SessionApp.IMEI = deviceName
    }

    override fun setImei(imei: String) {
        this.imei = imei
        SessionApp.IMEI = imei
        device_name.set(imei)
//        getLoginStateTask(imei).execute()
    }

    override fun connectToDb() {
        var connectionList = SharedPrefs.getInstance().ipListSharePreference
        MySqlHandleUtils.getInstance().disconnectToMySql()
        if (connectionList != null) {
            for (item in connectionList) {
                if (item.isConnected) {
                    ConnectToDbTask(item.ip).execute()
                    break
                }
            }
        }
    }

    override fun requireLogin() {
        if (!deviceName.isEmpty())
            getDeviceIdTask(deviceName).execute()
        else
            mView.showToast(Util.getStringFromResouce(R.string.please_setup_your_device_name))
    }


    override fun getTextButtonClick() {

    }

    override fun goMoneyBox() {
        mView.goMoneyBox()
    }

    override fun goAny() {
        mView.goAny()
    }

    override fun login() {

    }

    override fun requireSetting() {
        mView.showKeyboard()
    }


    override fun goSetting(data: String?) {

        if (data == "11032002") {
            admin_password = password.get()
            mView.goSetting(true)
            mHandler?.removeCallbacks(runnableLoginTask)
        } else {
            mView.goSetting(false)
//                mView.showToast(Util.getStringFromResouce(R.string.Wrong_password))
        }

    }

    override fun goTables() {
        if (WapriManager.configuration().otpLogin.get()!!) {
            if (SessionApp.WIFI_STATE == 0) {
                mView.showToast(App.self().getString(R.string.Please_turn_on_wifi))
                MySqlHandleUtils.getInstance().disconnectToMySql()
            } else {
                table_or_setting = TALBE
                requireLogin()
            }

        }
    }

    override fun logout() {
        mobile_Login.token = null
        mobile_Login.is_accepted = "N"
        mobile_Login.user_accepted = null
        mobile_Login.time_accepted = null
        mobile_Login.time_timeout = null
        mobile_Login.date_created = null
        UpdateMobileLoginTask(mobile_Login, true).execute()
    }

    override fun doLogout() {
        mView.logout()
    }

    private inner class ConnectToDbTask(var ip: String) : AsyncTask<String, Void, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
            mView.showLoading()
        }

        override fun doInBackground(vararg integers: String?): String {
            MySqlHandleUtils.getInstance()
            return MySqlHandleUtils.getInstance().connectToMySql(ip)
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            mView.dismissLoading()
            if (result.equals("ok")) {
                SelectRestaurantName().execute()
                requireLogin()
                listenAuthorizationLoginOTP()
                mView.showToast(App.self().getString(R.string.Connect_successful))
            } else {
                mView.showToast(App.self().getString(R.string.Connect_failed))
            }
        }
    }

    override fun closeApp() {
        mView.closeApp()
    }


    private inner class getLoginStateTask(var deviceId: String) : AsyncTask<Void, Void, List<MobileLogin>>() {


        override fun doInBackground(vararg p0: Void): List<MobileLogin> {

            var js = MySqlHandleUtils.selectStament("select * from wapri.mobilelogin where mobile_name = " + "'" + deviceId + "'")

            var mobileLogin: ArrayList<MobileLogin> = Gson().fromJson<ArrayList<MobileLogin>>(js.toString(), object : TypeToken<List<MobileLogin>>() {}.type)

            return mobileLogin
        }

        override fun onPostExecute(result: List<MobileLogin>) {
            super.onPostExecute(result)
            if (result != null && result.size > 0) {
                var mobileLogin = result[0]

                if (mobileLogin.token != null && mobileLogin.time_timeout != null
                        && Util.StringToDate(mobileLogin.time_timeout).after(Util.StringToDate(Util.getDate("dt"))) && mobileLogin.user_accepted != null) {
                    isLogged.set(true)
                    GoToTableByLoginOTPTask(mobileLogin.user_accepted).execute()
                    mHandler?.removeCallbacks(runnableLoginTask)
//                    mobile_Login = mobileLogin
//                    token.set(mobileLogin.token.toString())
                } else {
                    isLogged.set(false)
                }
            } else {

            }
//            isExistDevice = result != null && result.size > 0
        }
    }


    private inner class getDeviceIdTask(var deviceId: String) : AsyncTask<Void, Void, List<MobileLogin>>() {

        override fun onPreExecute() {
            super.onPreExecute()
            mView.showLoading()
        }

        override fun doInBackground(vararg p0: Void): List<MobileLogin> {

            var js = MySqlHandleUtils.selectStament("select * from wapri.mobilelogin where mobile_name = " + "'" + deviceId + "'")

            var mobileLogin: ArrayList<MobileLogin> = Gson().fromJson<ArrayList<MobileLogin>>(js.toString(), object : TypeToken<List<MobileLogin>>() {}.type)

            return mobileLogin
        }

        override fun onPostExecute(result: List<MobileLogin>) {
            super.onPostExecute(result)
            if (result != null && result.size > 0) {
                var mobileLogin = result[0]
                mobile_Login = mobileLogin
                if (onlyOne == 1) {
                    logout()
                }

                // hết timout login lại
                if (mobile_Login.token != null && mobile_Login.time_timeout != null && Util.StringToDate(mobile_Login.time_timeout).before(Util.StringToDate(Util.getDate("dt")))) {
                    STATE_LOGIN = EXPIRED
                    var token_ = Random().nextInt(10000).toString()
                    mobile_Login.token = token_
                    token.set(token_)
                    mobile_Login.date_created = Util.getDate("dt")
                    UpdateMobileLoginTask(mobile_Login, false).execute()

                }
                // đã login vẫn còn timeout
                else if (mobile_Login.token != null && mobile_Login.time_timeout != null
                        && Util.StringToDate(mobile_Login.time_timeout).after(Util.StringToDate(Util.getDate("dt"))) && mobile_Login.user_accepted != null) {
                    STATE_LOGIN = STILL_LOGGED
                    isLogged.set(true)
//                    token.set(mobile_Login.token_not_md5)
                    admin_password = ""


//                    GoToTableByLoginOTPTask(mobile_Login.user_accepted).execute()

//                    else GoToSettingByLoginOTPTask(mobileLogin.user_accepted).execute()
                }
                // chưa yêu cầu login
                else {
                    var token_ = Random().nextInt(10000).toString()
                    mobile_Login.token = token_
                    token.set(token_)
                    mobile_Login.date_created = Util.getDate("dt")
                    UpdateMobileLoginTask(mobile_Login, false).execute()
                }
            } else {
                STATE_LOGIN = NOT_LOGGED

                var mobileLogin = MobileLogin()
                mobileLogin.mobile_name = deviceName
                var token_ = Random().nextInt(10000).toString()
                mobileLogin.token = token_
                token.set(token_)
                mobileLogin.is_accepted = "N"
                mobileLogin.date_created = Util.getDate("dt")
                InsertMobileLoginTask(mobileLogin).execute()
            }
            mView.dismissLoading()
//            isExistDevice = result != null && result.size > 0
        }
    }


    private inner class InsertMobileLoginTask(var mobileLogin: MobileLogin) : AsyncTask<Void, Void, Int>() {

        override fun doInBackground(vararg p0: Void): Int {

            var js = MySqlHandleUtils.insertToMobileLogin(mobileLogin)
            return js
        }
    }

    private inner class UpdateMobileLoginTask(var mobileLogin: MobileLogin, var isLogout: Boolean) : AsyncTask<Void, Void, Int>() {

        override fun doInBackground(vararg p0: Void): Int {

            var js = MySqlHandleUtils.update_moibleLogin(mobileLogin)
            return js
        }

        override fun onPostExecute(result: Int) {
            super.onPostExecute(result)
            if (result == 1) {
                if (isLogout) {
                    onlyOne = 2
                    isLogged.set(false)
//                    mView.showToast(Util.getStringFromResouce(R.string.logout_successful))
                }

            }
        }
    }


    private inner class GoToSettingByLoginOTPTask(var user_name: String) : AsyncTask<Void, Void, List<Users>>() {

        override fun doInBackground(vararg p0: Void): List<Users> {

            var js = MySqlHandleUtils.selectStament("select * from wapri.users where username = " + "'" + user_name + "'")

            var users: ArrayList<Users> = Gson().fromJson<ArrayList<Users>>(js.toString(), object : TypeToken<List<Users>>() {}.type)

            return users
        }

        override fun onPostExecute(result: List<Users>) {
            super.onPostExecute(result)

            if (result != null && result.size > 0) {
                if (result[0].coQuyenCauHinhThietBi == "Y") {
                    mView.goSetting(true)
                }
            } else {
//                if(password.get()=="11032002"){
//                    mView.goSetting(true)
//                }else{
//                    mView.goSetting(false)
//                }
            }
        }
    }


    private inner class GoToTableByLoginOTPTask(var user_name: String) : AsyncTask<Void, Void, List<Users>>() {

        override fun doInBackground(vararg p0: Void): List<Users> {

            var js = MySqlHandleUtils.selectStament("select * from wapri.users where username = " + "'" + user_name + "'")

            var users: ArrayList<Users> = Gson().fromJson<ArrayList<Users>>(js.toString(), object : TypeToken<List<Users>>() {}.type)

            return users
        }

        override fun onPostExecute(result: List<Users>) {
            super.onPostExecute(result)

            if (result != null && result.size > 0) {
                mView.showToast(Util.getStringFromResouce(R.string.login_successful))
                WapriManager.setUser(result[0])
                getTimeZoneTask().execute()
                if (result[0].tienbandau <= 0.0) {
                    mView.goMoneyBox()
                } else {
                    mView.goTables()
                }
                ExecuteGetAllItems().execute()

            } else {
                mView.showToast(App.self().getString(R.string.Wrong_password))
            }
        }
    }


    private inner class SettingTask(var password_: String) : AsyncTask<Void, Void, List<Users>>() {

        override fun doInBackground(vararg p0: Void): List<Users> {

            var js = MySqlHandleUtils.selectStament("select * from wapri.users where password = " + "'" + password_ + "'")

            var users: ArrayList<Users> = Gson().fromJson<ArrayList<Users>>(js.toString(), object : TypeToken<List<Users>>() {}.type)

            return users
        }

        override fun onPostExecute(result: List<Users>) {
            super.onPostExecute(result)
            admin_password = ""
            if (result != null && result.size > 0) {
                if (result[0].coQuyenCauHinhThietBi == "Y") {
                    mView.goSetting(true)
                }
            } else {
//                if(password.get()=="11032002"){
//                    mView.goSetting(true)
//                }else{
//                    mView.goSetting(false)
//                }
            }
        }
    }


    private inner class getTimeZoneTask : AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg p0: Void): String {
            var js = MySqlHandleUtils.selectTimeZoneMySQL()
            return js
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            WapriManager.setTime_zone(result)
        }
    }

    private inner class SelectRestaurantName : AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg p0: Void): String {
            WapriManager.STORE_NAME = MySqlHandleUtils.selectThongtincuahang()
            return ""
        }
    }


    private inner class ExecuteGetAllItems : AsyncTask<Void, Void, Void>() {
        override fun onPreExecute() {
            super.onPreExecute()
            mView.showLoading()
        }

        override fun doInBackground(vararg voids: Void): Void? {
            GettingMenusFoodItem().execute()
            GettingMenusDrinkItem().execute()
            GettingItemsFromMenuTask().execute()
            GettingItems_PopupFromMenuTask().execute()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            mView.dismissLoading()
        }

    }

    private inner class GettingMenusFoodItem : AsyncTask<Void, Void, List<Menu>>() {


        override fun doInBackground(vararg voids: Void): ArrayList<Menu>? {
            val js = MySqlHandleUtils.selectStament("select * from menu where id in (select nhomhang from items where bophanphucvu not like '%PhaChe%' group by nhomhang) ")
            return Gson().fromJson<ArrayList<Menu>>(js.toString(), object : TypeToken<List<Menu>>() {

            }.type)
        }

        override fun onPostExecute(menus: List<Menu>) {
            super.onPostExecute(menus)
            SessionApp.menus_food = menus
        }
    }

    private inner class GettingMenusDrinkItem : AsyncTask<Void, Void, List<Menu>>() {


        override fun doInBackground(vararg voids: Void): ArrayList<Menu>? {
            val js = MySqlHandleUtils.selectStament("select * from menu where id in (select nhomhang from items where bophanphucvu like '%PhaChe%' group by nhomhang)")
            return Gson().fromJson<ArrayList<Menu>>(js.toString(), object : TypeToken<List<Menu>>() {

            }.type)
        }

        override fun onPostExecute(menus: List<Menu>) {
            super.onPostExecute(menus)
            SessionApp.menus_drink = menus
        }
    }

    private inner class GettingItemsFromMenuTask : AsyncTask<Void, Void, MutableList<Items>>() {


        override fun doInBackground(vararg voids: Void): MutableList<Items> {
            val js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.items i")
            val items = Gson().fromJson<ArrayList<Items>>(js.toString(), object : TypeToken<List<Items>>() {

            }.type)

            for (ob in items) {
                ob.setGiaBan()
            }
            return items
        }

        override fun onPostExecute(items: MutableList<Items>?) {
            super.onPostExecute(items)
//            val firstOb = Items()
//            firstOb.tenhang = App.self().getString(R.string.Come_back)
//            items?.add(0, firstOb)
            SessionApp.items_global = items
        }
    }


    private inner class GettingItems_PopupFromMenuTask : AsyncTask<Void, Void, MutableList<ItemsPopup>>() {
        override fun doInBackground(vararg voids: Void): MutableList<ItemsPopup> {
            val js = MySqlHandleUtils.selectStament("select * from wapri.items_popup")
            val items = Gson().fromJson<ArrayList<ItemsPopup>>(js.toString(), object : TypeToken<List<ItemsPopup>>() {

            }.type)


            return items
        }

        override fun onPostExecute(items: MutableList<ItemsPopup>?) {
            super.onPostExecute(items)
            SessionApp.items_popup = items
        }
    }
}