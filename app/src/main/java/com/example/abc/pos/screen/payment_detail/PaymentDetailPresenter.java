package com.example.abc.pos.screen.payment_detail;

import android.databinding.ObservableField;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Orders;
import com.example.abc.pos.model.OrdersDetail;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.PendingPaymentSync;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.screen.order.OrderPresenter;
import com.example.abc.pos.screen.payment.PaymentContract;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.utils.Util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class PaymentDetailPresenter implements PaymentDetailContract.Presenter {

    private PaymentDetailContract.View mView;

    public ObservableField<String> title = new ObservableField<>(App.Companion.self().getString(R.string.Payment));
    public ObservableField<String> summer_price = new ObservableField<>();
    public ObservableField<String> TIP = new ObservableField<>("0.00");
    public ObservableField<String> guschein = new ObservableField<>("0.00");
    public ObservableField<String> restaurantCheck = new ObservableField<>("0.00");
    public ObservableField<Boolean> isBewirtung = new ObservableField<>(false);
    private double summer_orig_price;
    private double tienkhachdua;
    private double tienthoi;
    private List<OrdersTemp> origOrderTem_list;
    private List<OrdersTemp> divideOrderTemp_list;
    private String order_id;
    private WapriManager.cachtra cachTra;
    private String payType;
    private double oldTipPrice = 0;

    public PaymentDetailPresenter(PaymentDetailContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void loadData(String order_id, List<OrdersTemp> origOrderTem_list, List<OrdersTemp> divideOrderTemp_list, String summer_price) {
        this.order_id = order_id;
        this.origOrderTem_list = origOrderTem_list;
        this.divideOrderTemp_list = divideOrderTemp_list;
        this.summer_price.set(summer_price.replaceAll("[,.]", ""));
        this.summer_price.set(this.summer_price.get().substring(0, this.summer_price.get().length() - 2) + "." + this.summer_price.get().substring(this.summer_price.get().length() - 2));
        summer_orig_price = convertToDouble(this.summer_price.get());
    }

    @Override
    public void createOrder(double giatien) {
        Orders order = new Orders();
        order.setId(order_id);
        order.setNgaymua(Util.getDate("d"));
        order.setThoigian(Util.getDate("dt"));

        Random ran = new Random();
        String idLinkToDetailGenera = WapriManager.DEVICE_NAME + "x" + Util.getDate("") + "" + ran.nextInt(10000);
        order.setLinktodetails(idLinkToDetailGenera);

        order.setNhanvien(WapriManager.getUser().getUsername());
        order.setBanso(SessionApp.TABLE_NAME);
        order.setGiatien(giatien);
        order.setInclude_total_tienmat_hoac_the(giatien - (convertToDouble(guschein.get()) + convertToDouble(restaurantCheck.get())));
        order.setInclude_total_coupon_value(convertToDouble(guschein.get()));
        order.setInclude_total_restaurantcheck_value(convertToDouble(restaurantCheck.get()));
        order.setCachtra(cachTra);
        order.setGiam(SessionApp.global_table.getGiam());
        order.setTiengiam(SessionApp.global_table.getTiengiam());
        order.setTienthue7giam(SessionApp.global_table.getTienthue7giam());
        order.setTienthue19giam(SessionApp.global_table.getTienthue19giam());
        order.setDathanhtoantienvoinhanvienchua("N");
        order.setDatongketcuoingaychua("N");
        order.setDelete("N");
        order.setLadonhangtralai("N");
        if (SessionApp.global_table.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho) {
            order.setTaichohaymangve("TaiCho");
        } else {
            order.setTaichohaymangve("MangVe");
        }
        if (payType.equals("credit")) {
            order.setTienthoilai(0);
            order.setTienkhachdua(tienkhachdua + (convertToDouble(guschein.get()) + convertToDouble(restaurantCheck.get())));
        } else {
            order.setTienthoilai(tienthoi);
            order.setTienkhachdua(tienkhachdua + (convertToDouble(guschein.get()) + convertToDouble(restaurantCheck.get())));
        }

        order.setOrderfrom("local");
        order.setDainxacnhanlaixe("N");
        order.setLadonhangcomathangbixoa("N");
        order.setLadonhangcodoicachthanhtoantien("N");
        order.setfrom_tablet("Y");
        order.setDaprintchua("N");
        order.setInlaihoadonfromtablet("N");

        if (payType.equals("bon")) {
            order.setLoaiIn("None");
        } else {
            if (isBewirtung.get()) {
                order.setLoaiIn("BON_BS");
            } else {
                order.setLoaiIn("BON");
            }
        }

        updateOrderTempTable(order);
//        new InsertOrderToDbTask(order).execute();
    }

    @Override
    public void createOrderDetail(OrdersTemp ordersTemp, Orders orders) {
        OrdersDetail ordersDetail = new OrdersDetail();
        Random ran = new Random();
        String idGenera = WapriManager.DEVICE_NAME + "x" + Util.getDate("") + "" + ran.nextInt(10000);
        if (ordersTemp.getitems_id().equals("10004")) {
            ordersDetail.setitems_id("10004");
            ordersDetail.setTenhang("Trinkgeld");
            ordersDetail.setThue(0);
            ordersDetail.setTienthue(0.0);
            ordersDetail.setPhantramgiam(0);
            ordersDetail.setTiengiam(0.0);
            ordersDetail.setDaprintchua("Y");
        } else {
            ordersDetail.setitems_id(ordersTemp.getitems_id());
            if (ordersTemp.getBophanphucvu().equals("Sonstiges0"))
                ordersDetail.setThue(0);
            else ordersDetail.setThue(ordersTemp.getThue());

            ordersDetail.setTienthue(ordersTemp.getTienthue());
            ordersDetail.setPhantramgiam(ordersTemp.getPhantramgiam());
            ordersDetail.setTiengiam(ordersTemp.getTiengiam());


            if (ordersTemp.getDaprintchua().equals("Y")) {
                ordersDetail.setDaprintchua("Y");
            } else {
                ordersDetail.setDaprintchua("N");
            }
        }
        ordersDetail.setId(idGenera);
        ordersDetail.setTenhang(ordersTemp.getTenhang());
        ordersDetail.setLinktodetails(orders.getLinktodetails());
        ordersDetail.setorders_id(orders.getId());
        ordersDetail.setNgaymua(orders.getNgaymua());
        ordersDetail.setBanso(ordersTemp.getBanso());


        if (ordersTemp.getBonname() != null && !ordersTemp.getBonname().isEmpty())
            ordersDetail.setBonname(ordersTemp.getBonname());
        else
            ordersDetail.setBonname(ordersTemp.getTenhang());

        if (ordersTemp.getKichcomonan() != null && !ordersTemp.getKichcomonan().isEmpty())
            ordersDetail.setKichcomonan(ordersTemp.getKichcomonan());
        else
            ordersDetail.setKichcomonan("");


        ordersDetail.setSoluong(ordersTemp.getSoluong());

        ordersDetail.setorig_price_from_item(ordersTemp.getorig_price_from_item());
        ordersDetail.setorig_price_no_extra(ordersTemp.getorig_price_no_extra());
        ordersDetail.setGiatien(ordersTemp.getGiatien() / ordersDetail.getSoluong());
        ordersDetail.setThanhtien(ordersTemp.getThanhtien());
        ordersDetail.setUsername(WapriManager.getUser().getUsername());
        ordersDetail.setLoatdat(ordersTemp.getLoatdat());
        ordersDetail.setThoigian(ordersTemp.getThoigian());
        ordersDetail.setTaichohaymangve(ordersTemp.getTaichohaymangve().value);
        ordersDetail.setDanhdaubixoa(ordersTemp.getDanhdaubixoa());
        ordersDetail.setDathanhtoantienvoinhanvienchua("N");
        ordersDetail.setQuaythanhtoanso(WapriManager.DEVICE_NAME);
        ordersDetail.setCachtra(cachTra);

        ordersDetail.setgiavi_text_(ordersTemp.getgiavi_text());
        ordersDetail.setgiavi_price_(ordersTemp.getgiavi_price());

        ordersDetail.setgiavi_text_2(ordersTemp.getgiavi_text_2());
        ordersDetail.setgiavi_price_2(ordersTemp.getgiavi_price_2());

        ordersDetail.setgiavi_text_3(ordersTemp.getgiavi_text_3());
        ordersDetail.setgiavi_price_3(ordersTemp.getgiavi_price_3());

        ordersDetail.setgiavi_text_4(ordersTemp.getgiavi_text_4());
        ordersDetail.setgiavi_price_4(ordersTemp.getgiavi_price_4());

        ordersDetail.setgiavi_text_5(ordersTemp.getgiavi_text_5());
        ordersDetail.setgiavi_price_5(ordersTemp.getgiavi_price_5());

        ordersDetail.setgiavi_text_6(ordersTemp.getgiavi_text_6());
        ordersDetail.setgiavi_price_6(ordersTemp.getgiavi_price_6());

        ordersDetail.setgiavi_text_7(ordersTemp.getgiavi_text_7());
        ordersDetail.setgiavi_price_7(ordersTemp.getgiavi_price_7());

        ordersDetail.setgiavi_text_8(ordersTemp.getgiavi_text_8());
        ordersDetail.setgiavi_price_8(ordersTemp.getgiavi_price_8());

        ordersDetail.setgiavi_text_9(ordersTemp.getgiavi_text_9());
        ordersDetail.setgiavi_price_9(ordersTemp.getgiavi_price_9());

        ordersDetail.setgiavi_text_10(ordersTemp.getgiavi_text_10());
        ordersDetail.setgiavi_price_10(ordersTemp.getgiavi_price_10());

        ordersDetail.setPhanangomco(ordersTemp.getPhanangomco());
        ordersDetail.setphanangomco_soluong(ordersTemp.getphanangomco_soluong());
        ordersDetail.setPhanangomco2(ordersTemp.getPhanangomco2());
        ordersDetail.setphanangomco2_soluong(ordersTemp.getphanangomco2_soluong());

        ordersDetail.setPhanangomco3(ordersTemp.getPhanangomco3());
        ordersDetail.setphanangomco3_soluong(ordersTemp.getphanangomco3_soluong());

        ordersDetail.setPhanangomco4(ordersTemp.getPhanangomco4());
        ordersDetail.setphanangomco4_soluong(ordersTemp.getphanangomco4_soluong());

        ordersDetail.setPhanangomco5(ordersTemp.getPhanangomco5());
        ordersDetail.setphanangomco5_soluong(ordersTemp.getphanangomco5_soluong());

        ordersDetail.setPhanangomco6(ordersTemp.getPhanangomco6());
        ordersDetail.setphanangomco6_soluong(ordersTemp.getphanangomco6_soluong());

        ordersDetail.setPhanangomco7(ordersTemp.getPhanangomco7());
        ordersDetail.setphanangomco7_soluong(ordersTemp.getphanangomco7_soluong());

        ordersDetail.setPhanangomco8(ordersTemp.getPhanangomco8());
        ordersDetail.setphanangomco8_soluong(ordersTemp.getphanangomco8_soluong());

        ordersDetail.setPhanangomco9(ordersTemp.getPhanangomco9());
        ordersDetail.setphanangomco9_soluong(ordersTemp.getphanangomco9_soluong());


        ordersDetail.setphanangomco_value(ordersTemp.getphanangomco_value());
        ordersDetail.setPhanangomco2Value(ordersTemp.getPhanangomco2Value());
        ordersDetail.setPhanangomco3Value(ordersTemp.getPhanangomco3Value());
        ordersDetail.setPhanangomco4Value(ordersTemp.getPhanangomco4Value());
        ordersDetail.setPhanangomco5Value(ordersTemp.getPhanangomco5Value());
        ordersDetail.setPhanangomco6Value(ordersTemp.getPhanangomco6Value());
        ordersDetail.setPhanangomco7Value(ordersTemp.getPhanangomco7Value());
        ordersDetail.setPhanangomco8Value(ordersTemp.getPhanangomco8Value());
        ordersDetail.setPhanangomco9Value(ordersTemp.getPhanangomco9Value());


        ordersDetail.setGhichu(ordersTemp.getGhichu());
        ordersDetail.setGhichu2(ordersTemp.getGhichu2());


        ordersDetail.setBophanphucvu(ordersTemp.getBophanphucvu());

        ordersDetail.setDatongketcuoingaychua("N");
        ordersDetail.setOrderfrom("local");
        ordersDetail.setis_tiengiamtructiep("N");
        ordersDetail.setLydohuyxoa(ordersTemp.getLydohuyxoa());
        ordersDetail.setloai_mon_an(ordersTemp.getloai_mon_an());

        if (ordersTemp.getDanhdaubixoa().equals("Y") && ordersTemp.getDaprintxoachua().equals("N")) {
            ordersDetail.setDaprintxoachua("N");
        } else {
            ordersDetail.setDaprintxoachua("Y");
        }


        ordersDetail.setKichcomonanngonngu(ordersTemp.getKichcomonanngonngu());
        if (ordersTemp.getitems_id().equals("10004") || ordersTemp.getitems_id().equals("1005"))
            ordersDetail.setPhantramgiamtrentoandonhang(0);
        else
            ordersDetail.setPhantramgiamtrentoandonhang(SessionApp.global_table.getGiam());
        new InsertOrderDetailToDbTask(ordersDetail).execute();
    }

    private void addNewOrderTemp(double tienTip) {
        if (tienTip > 0) {
            Random ran = new Random();
            Date date = new Date();
            String idGenera = WapriManager.DEVICE_NAME + "x" + date.getTime() + "" + ran.nextInt(10000);
            OrdersTemp ordersTemp = new OrdersTemp();
            ordersTemp.setId(idGenera);
            ordersTemp.setBanso(SessionApp.global_table.getName());
            ordersTemp.setitems_id("10004");
            ordersTemp.setNgaymua(Util.getDate("d"));
            ordersTemp.setTenhang("Trinkgeld");
            ordersTemp.setBonname("Trinkgeld");
            ordersTemp.setSoluong(1);
            ordersTemp.setThanhtien(tienTip);
            ordersTemp.setorig_price_from_item(tienTip);
            ordersTemp.setorig_price_no_extra(tienTip);
            ordersTemp.setGiatien(tienTip);

            ordersTemp.setUsername(WapriManager.getUser().getUsername());
            ordersTemp.setThoigian(Util.getDate("dt"));
            ordersTemp.setBophanphucvu("Bep");

            if (SessionApp.global_table.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho) {
                ordersTemp.setTaichohaymangve(WapriManager.Taichohaymangve_.TaiCho);

            } else {
                ordersTemp.setTaichohaymangve(WapriManager.Taichohaymangve_.MangVe);
            }
            ordersTemp.setDaprintxoachua("N");
            ordersTemp.setDaprintchua("Y");
            ordersTemp.setInlaimonfromtablet("N");
            ordersTemp.setloai_mon_an("MonChinh");
            if (SessionApp.from_DivideOrder_or_Order.equals("order")) {
                if (origOrderTem_list != null && !origOrderTem_list.isEmpty()) {
                    if (!origOrderTem_list.get(origOrderTem_list.size() - 1).getitems_id().equals("10004")) {
                        origOrderTem_list.add(ordersTemp);
                    } else {
                        origOrderTem_list.set(origOrderTem_list.size() - 1, ordersTemp);
                    }
                }
            } else {
                if (divideOrderTemp_list != null && !divideOrderTemp_list.isEmpty()) {
                    if (!divideOrderTemp_list.get(divideOrderTemp_list.size() - 1).getitems_id().equals("10004")) {
                        divideOrderTemp_list.add(ordersTemp);
                    } else {
                        divideOrderTemp_list.set(divideOrderTemp_list.size() - 1, ordersTemp);
                    }
                }
            }
        }

    }

    @Override
    public void paymentByCash() {
        payType = "cash";
        cachTra = WapriManager.cachtra.TienMat;
        if (convertToDouble(summer_price.get()) > 0) {
            mView.showKeyboard(App.Companion.self().getString(R.string.Money_pay), summer_price.get());
        } else {
            tienthoi = convertToDouble(summer_price.get()) * -1;
            confirmPayMent(App.Companion.self().getString(R.string.Exchange));
        }
    }

    @Override
    public void paymentByCashNotBon() {
        payType = "bon";
        cachTra = WapriManager.cachtra.TienMat;
        if (convertToDouble(summer_price.get()) > 0) {
            mView.showKeyboard(App.Companion.self().getString(R.string.Money_pay), summer_price.get());
        } else {
            tienthoi = convertToDouble(summer_price.get()) * -1;
            confirmPayMent(App.Companion.self().getString(R.string.Exchange));
        }
    }

    @Override
    public void paymentByCreditCard() {
        payType = "credit";
        cachTra = WapriManager.cachtra.CreditCard;
        if (convertToDouble(summer_price.get()) > 0) {
            mView.showKeyboard(App.Companion.self().getString(R.string.Money_pay), summer_price.get());
        } else {
            tienthoi = convertToDouble(summer_price.get()) * -1;
            confirmPayMent("Tip: ");
        }
    }

    @Override
    public void showGutschien() {
        mView.showGuschein();
    }

    @Override
    public void showRestaurantCheck() {
        mView.showRestaurantCheck();
    }

    @Override
    public void TIP() {
        payType = "TIP";
        mView.showKeyboard("TIP", "0");
    }

    @Override
    public void restaurantCheck() {

    }

    @Override
    public void updateSummerPrice(String data) {
        if (data.equals("")) {
            data = summer_price.get();
        }
        tienkhachdua = convertToDouble(data);
        if (payType.equals("TIP")) {
            TIP.set(String.format("%.2f", convertToDouble(data)).replace(",", "."));
            double summer = (convertToDouble(summer_price.get()) - oldTipPrice) + convertToDouble(data);
            summer_price.set((String.format("%.2f", summer)).replace(",", "."));
            oldTipPrice = convertToDouble(data);

            addNewOrderTemp(convertToDouble(data));


        } else {
            if (convertToDouble(data) < convertToDouble(summer_price.get())) {
                mView.showAlert(Util.getStringFromResouce(R.string.money_pay_must_be));
            } else {
                tienthoi = tienkhachdua - convertToDouble(summer_price.get());
                final double summer = convertToDouble(summer_price.get()) - convertToDouble(data);
//                summer_price.set((String.format("%.2f", summer)).replace(",", "."));
                if (payType.equals("credit")) {
                    if (tienthoi > 0) {
                        confirmPayMent("TIP: ");
                    } else {
                        double giatien = summer_orig_price + Double.parseDouble(TIP.get());
                        createOrder(giatien);
                    }
                } else {
                    if (tienthoi > 0) {
                        confirmPayMent(App.Companion.self().getString(R.string.Exchange));
                    } else {
                        double giatien = summer_orig_price + Double.parseDouble(TIP.get());
                        createOrder(giatien);
                    }
                }
            }
        }
    }

    @Override
    public void updateGuschein(String data) {
        guschein.set((String.format("%.2f", (convertToDouble(guschein.get()) + convertToDouble(data)))));
        summer_price.set((String.format("%.2f", (convertToDouble(summer_price.get()) - convertToDouble(data)))).replace(",", "."));
    }

    @Override
    public void updateRestaurantCheck(String data) {
        restaurantCheck.set((String.format("%.2f", (convertToDouble(restaurantCheck.get()) + convertToDouble(data)))));
        summer_price.set((String.format("%.2f", (convertToDouble(summer_price.get()) - convertToDouble(data)))).replace(",", "."));
    }

    @Override
    public void exit() {
        if (SessionApp.from_DivideOrder_or_Order.equals("order")) {
            if (origOrderTem_list != null && !origOrderTem_list.isEmpty()) {
                if (origOrderTem_list.get(origOrderTem_list.size() - 1).getitems_id().equals("10004")) {
                    origOrderTem_list.remove(origOrderTem_list.size() - 1);
                }
            }
        } else {
            if (divideOrderTemp_list != null && !divideOrderTemp_list.isEmpty()) {
                if (divideOrderTemp_list.get(divideOrderTemp_list.size() - 1).getitems_id().equals("10004")) {
                    divideOrderTemp_list.remove(divideOrderTemp_list.size() - 1);
                }
            }
        }
        mView.cancelPayment();
        mView.returnDivide();
    }

    @Override
    public void checkBewirtung() {
        if (isBewirtung.get()) {
            isBewirtung.set(false);
        } else {
            isBewirtung.set(true);
        }
        mView.setBewirtungButton(isBewirtung.get());

    }

    @Override
    public List<OrdersTemp> getOrdertempList() {
        return origOrderTem_list;
    }

    private double convertToDouble(String data) {
        try {

            return Double.parseDouble(data.replaceAll(",", "."));
        } catch (Exception ex) {
            mView.showToast(ex.getMessage());
            return 0.0;
        }
    }

    private void confirmPayMent(String message) {
        mView.showCustomDialog(message + String.format("%.2f", tienthoi).replace(",", "."), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_ok:
                        mView.dismissDialog();
                        if (payType.equals("credit")) {
                            double giatien = summer_orig_price + Double.parseDouble(TIP.get()) + tienthoi;


                            if (SessionApp.from_DivideOrder_or_Order.equals("order")) {
                                if (origOrderTem_list != null && !origOrderTem_list.isEmpty()) {
                                    OrdersTemp ordersTemp = origOrderTem_list.get(origOrderTem_list.size() - 1);
                                    if (ordersTemp.getitems_id().equals("10004")) {
                                        ordersTemp.setThanhtien(ordersTemp.getThanhtien() + tienthoi);
                                        ordersTemp.setorig_price_from_item(ordersTemp.getorig_price_from_item() + tienthoi);
                                        ordersTemp.setorig_price_no_extra(ordersTemp.getorig_price_no_extra() + tienthoi);
                                        ordersTemp.setGiatien(ordersTemp.getGiatien() + tienthoi);
                                    } else {
                                        addNewOrderTemp(tienthoi);
                                    }
                                }
                            } else {
                                if (divideOrderTemp_list != null && !divideOrderTemp_list.isEmpty()) {
                                    OrdersTemp ordersTemp = divideOrderTemp_list.get(divideOrderTemp_list.size() - 1);
                                    if (ordersTemp.getitems_id().equals("10004")) {
                                        ordersTemp.setThanhtien(ordersTemp.getThanhtien() + tienthoi);
                                        ordersTemp.setorig_price_from_item(ordersTemp.getorig_price_from_item() + tienthoi);
                                        ordersTemp.setorig_price_no_extra(ordersTemp.getorig_price_no_extra() + tienthoi);
                                        ordersTemp.setGiatien(ordersTemp.getGiatien() + tienthoi);
                                    } else {
                                        addNewOrderTemp(tienthoi);
                                    }
                                }
                            }
                            createOrder(giatien);
                        } else {
                            double giatien = summer_orig_price + Double.parseDouble(TIP.get());
                            createOrder(giatien);
                        }
                        break;
                }
            }
        });
    }

    private void updateOrderTempTable(Orders order) {
        new ExcuseAllDbTask(order).execute();
    }


    private class ExcuseAllDbTask extends AsyncTask<Void, Void, Integer> {
        Orders order;

        public ExcuseAllDbTask(Orders order) {
            this.order = order;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result = -1;
            if (SessionApp.from_DivideOrder_or_Order.equals("order") || SessionApp.from_DivideOrder_or_Order.equals("pending")) {
                result = 1;
                for (int i = 0; i < origOrderTem_list.size(); i++) {
//                    int soLuong = 0;
//                    double tongTien = 0;
//                    double giaTien = 0;
//                    int tienThue = 0;
//                    /*tìm món cùng tên, cùng mã, cùng giá */
//                    for (int j = i; j < origOrderTem_list.size(); j++) {
//                        if (i != j) {
//                            if ((origOrderTem_list.get(i).getitems_id().equals(origOrderTem_list.get(j).getitems_id()) &&
//                                    origOrderTem_list.get(i).getTenhang().equals(origOrderTem_list.get(j).getTenhang())) &&
//                                    origOrderTem_list.get(i).getThanhtien() == origOrderTem_list.get(j).getThanhtien() &&
//                                    (origOrderTem_list.get(i).isInsertedToDb() && !origOrderTem_list.get(j).isInsertedToDb())) {
//                                soLuong += origOrderTem_list.get(j).getSoluong();
//                                tongTien += origOrderTem_list.get(j).getThanhtien();
//                                tienThue += origOrderTem_list.get(j).getTienthue();
//                                giaTien += origOrderTem_list.get(j).getGiatien();
//                                if(!origOrderTem_list.get(j).isInsertedToDb()){
//                                        new InsertOrderTempToDbTask(origOrderTem_list.get(j)).execute();
//                                }
//                                new DeleteOrderTempToDbTask(origOrderTem_list.get(j)).execute();
//                                origOrderTem_list.remove(origOrderTem_list.get(j));
//                            }
//                        }
//                    }
//                    origOrderTem_list.get(i).setSoluong(origOrderTem_list.get(i).getSoluong() + soLuong);
//                    origOrderTem_list.get(i).setThanhtien(origOrderTem_list.get(i).getThanhtien() + tongTien);
//                    origOrderTem_list.get(i).setTienthue(origOrderTem_list.get(i).getTienthue() + tienThue);
//                    origOrderTem_list.get(i).setGiatien(origOrderTem_list.get(i).getGiatien() + giaTien);
//                    Log.d("////",origOrderTem_list.get(i).getSoluong()+"x"+origOrderTem_list.get(i).getitems_id() + " "+origOrderTem_list.get(i).getThanhtien() );

                    if (origOrderTem_list.get(i).getitems_id().equals("10004") && origOrderTem_list.get(i).getorig_price_no_extra() <= 0) {

                    } else {
                        createOrderDetail(origOrderTem_list.get(i), order);
                    }
                    new DeleteOrderTempToDbTask(origOrderTem_list.get(i)).execute();
                }
            } else if (SessionApp.from_DivideOrder_or_Order.equals("divide_order")) {
                // update lai orig list
                final List<OrdersTemp> ordersTemps_will_delete = new ArrayList<>();
                try {
                    for (int i = 0; i < origOrderTem_list.size(); i++) {
                        if (origOrderTem_list.get(i).getSoluong() < 1) {
                            new DeleteOrderTempToDbTask(origOrderTem_list.get(i)).execute();
                            ordersTemps_will_delete.add(origOrderTem_list.get(i));
                        } else {
                            new UpdateOrderTempToDbTask(origOrderTem_list.get(i)).execute();
                        }
                    }
                    if (ordersTemps_will_delete.size() > 0) {
                        mView.getRunUI().runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                for (OrdersTemp ordersTemp : ordersTemps_will_delete) {
                                    origOrderTem_list.remove(ordersTemp);
                                }
                                mView.notifyAdapter();
                            }
                        });

                    }
                } catch (Exception ex) {
                }


//                for (OrdersTemp ordersTemp : origOrderTem_list) {
//                   Log.d("/////",ordersTemp.getBonname()+" "+ ordersTemp.getSoluong());
//                }

                // insert order duoc chia de thanh toan
                for (OrdersTemp ordersTemp : divideOrderTemp_list) {
                    createOrderDetail(ordersTemp, order);
                }
                if (origOrderTem_list.size() < 1) {
                    return 1;
                } else {
                    for (OrdersTemp ordersTemp : origOrderTem_list) {
                        if (ordersTemp.getSoluong() < 1) {
                            result = 1;
                        } else {
                            result = 0;
                            break;
                        }
                    }
                }

            }


            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            mView.dismissLoading();

            if (result == 1) {
                if (SessionApp.from_DivideOrder_or_Order.equals("order")) {
                    // neu vi tri khach hang  = 0 (tat ca) thi quay ve man hinh table.
                    new InsertOrderToDbTask(order).execute();
                    if (SessionApp.CUSTOMER_POSITION == 0) {
                        new UpdateTableStateToDbTask(3).execute();
                    } else {
                        if (SessionApp.SIZE_OF_POSITION_ORDER == SessionApp.SIZE_OF_ALL_ORDER) {
                            new UpdateTableStateToDbTask(3).execute();
                        } else {
//                            new UpdateTableStateToDbTask(2).execute();
                            mView.returnTable(2);
                            SessionApp.is_updated_table = false;
                            mView.clearDivideList();
//                            mView.returnDivide();
                        }
                    }
                } else if (SessionApp.from_DivideOrder_or_Order.equals("divide_order")) {
                    new InsertOrderToDbTask(order).execute();
                    new UpdateTableStateToDbTask(4).execute();
                } else if (SessionApp.from_DivideOrder_or_Order.equals("pending")) {
                    new InsertOrderToDbTask(order).execute();
                    new UpdateTableStateToDbTask(2).execute();
                }

            } else {
                new InsertOrderToDbTask(order).execute();
                PendingPaymentSync paymentSync = SharedPrefs.getInstance().get(PendingPaymentSync.KEY, PendingPaymentSync.class);
                if (paymentSync != null) {
                    paymentSync.getTable().setTongtien(paymentSync.getTable().getTongtien() - paymentSync.getSummerDivide());
                    new UpdateTableDivideStateToDbTask(paymentSync.getTable()).execute();
                }
                mView.clearDivideList();
                mView.returnDivide();
                mView.notifyAdapter();
            }
            SharedPrefs.getInstance().remove(PendingPaymentSync.KEY);
            SharedPrefs.getInstance().remove(SharedPrefs.KEY_TABLE + SessionApp.global_table.getName());
        }
    }


//    private class InsertOrderTempToDbTask extends AsyncTask<Void, Void, Integer> {
//
//        OrdersTemp ordersTemp;
//
//        public InsertOrderTempToDbTask(OrdersTemp ordersTemp) {
//            this.ordersTemp = ordersTemp;
//        }
//
//        @Override
//        protected Integer doInBackground(Void... voids) {
//            int result;
//
//            result = MySqlHandleUtils.insertOrderItem(ordersTemp);
//            return result;
//        }
//
//        @Override
//        protected void onPostExecute(Integer integer) {
//            super.onPostExecute(integer);
//            new DeleteOrderTempToDbTask(ordersTemp).execute();
//        }
//    }


    private class InsertOrderToDbTask extends AsyncTask<Void, Void, Integer> {

        Orders order;

        public InsertOrderToDbTask(Orders order) {
            this.order = order;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            result = MySqlHandleUtils.insertOrder2018Item(order);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (result == 1) {
//                updateOrderTempTable(order);
            } else {
//                mView.showAlert("Có lỗi trong quá trình thêm dữ liệu");
            }
        }
    }

    private class UpdateOrderTempToDbTask extends AsyncTask<Void, Void, Integer> {

        OrdersTemp ordersTemp;

        public UpdateOrderTempToDbTask(OrdersTemp ordersTemp) {
            this.ordersTemp = ordersTemp;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            result = MySqlHandleUtils.updateAllOrderTempByAmount(ordersTemp);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
        }
    }


    private class DeleteOrderTempToDbTask extends AsyncTask<Void, Void, Integer> {

        OrdersTemp ordersTemp;

        public DeleteOrderTempToDbTask(OrdersTemp ordersTemp) {
            this.ordersTemp = ordersTemp;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            result = MySqlHandleUtils.deleteOrderTempItem(ordersTemp);

            return result;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

        }
    }

    private class InsertOrderDetailToDbTask extends AsyncTask<Void, Void, Integer> {

        OrdersDetail ordersDetail;

        public InsertOrderDetailToDbTask(OrdersDetail ordersDetail) {
            this.ordersDetail = ordersDetail;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result = 0;
            if (!ordersDetail.getitems_id().equals("1001"))
                result = MySqlHandleUtils.insertOrderDetail2018Item(ordersDetail);


            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {

            } else {
//                mView.showAlert("Có lỗi trong quá trình thêm dữ liệu");
            }
        }
    }

    private class UpdateTableStateToDbTask extends AsyncTask<Void, Void, Void> {
        int lengthPop;

        public UpdateTableStateToDbTask(int lengthPop) {
            this.lengthPop = lengthPop;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            SessionApp.global_table.setTrangthai("empty");
            SessionApp.global_table.setTongtien(null);
            SessionApp.global_table.setUsername(null);
            SessionApp.global_table.setThoigian(null);
            SessionApp.global_table.setis_selling_on_tablet("N");
            SessionApp.global_table.setIs_selling_on_POS("N");
            SessionApp.global_table.setTiengiam(0.0);
            SessionApp.global_table.setGiam(0);
            if (lengthPop == 2)
                SessionApp.is_updated_table = false;
            else SessionApp.is_updated_table = true;
            MySqlHandleUtils.updateTableChangeNew(SessionApp.global_table);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            origOrderTem_list.clear();
            mView.returnTable(lengthPop);
        }
    }

    private class UpdateTableDivideStateToDbTask extends AsyncTask<Void, Void, Void> {
        Table table;

        public UpdateTableDivideStateToDbTask(Table table) {
            this.table = table;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            SessionApp.global_table.setTrangthai("using");
            SessionApp.global_table.setTongtien(table.getTongtien());
            SessionApp.global_table.setUsername(table.getUsername());
            SessionApp.global_table.setThoigian(Util.getDate("dt"));
            SessionApp.global_table.setis_selling_on_tablet("N");
            SessionApp.global_table.setIs_selling_on_POS("N");
            SessionApp.global_table.setTiengiam(0.0);
            SessionApp.is_updated_table = false;
            MySqlHandleUtils.updateTableChangeNew(SessionApp.global_table);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }
}
