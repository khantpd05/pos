package com.example.abc.pos.screen.setting_payment;

import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.screen.setting_general.SettingGeneralPresenter;

import java.util.List;

public interface SettingPaymentContract {
    interface View{
        void showLoading();
        void dismissLoading();
        void showAlert(String data);

        void exit();
    }

    interface Presenter{
        void loadData();
        void updateNewConfig();
        void exit();
    }

}
