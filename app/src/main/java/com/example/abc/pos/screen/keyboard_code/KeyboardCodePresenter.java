package com.example.abc.pos.screen.keyboard_code;

import android.databinding.ObservableField;

import com.example.abc.pos.viewmodel.Keyboard;
import com.example.abc.pos.viewmodel.KeyboardCode;

public class KeyboardCodePresenter implements KeyboardCodeConstract.Presenter {
    public KeyboardCode mViewModel;

    public KeyboardCodeConstract.View mView;

    public ObservableField<Boolean> isSwitchKeyboard = new ObservableField<>(false);
    public int number = 2;
    public KeyboardCodePresenter(KeyboardCodeConstract.View mView) {
        this.mView = mView;
        mViewModel = new KeyboardCode();
    }

    @Override
    public void getDataInput() {
        mView.getDataInput(mViewModel.input.get());
        mViewModel.input.set("");
    }
}
