package com.example.abc.pos.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.ParseException;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Tieudedotdat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.math.BigInteger;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by ducth on 12/23/15.
 */
public class Util {


    public static HashMap<String, String> getEnumBoPhanPhucVu() {
        HashMap<String, String> hashMap = new HashMap();
        hashMap.put("Bep", Util.getStringFromResouce(R.string.kitchen));
        hashMap.put("PhaChe19", Util.getStringFromResouce(R.string.water_19));
        hashMap.put("Sushi", "Sushi");
        return hashMap;
    }

    public static HashMap<String, String> getEnumFoodType() {
        HashMap<String, String> hashMap = new HashMap();
        hashMap.put("KhaiVi", Util.getStringFromResouce(R.string.appetizer));
        hashMap.put("Nuoc", Util.getStringFromResouce(R.string.water));
        hashMap.put("MonChinh", Util.getStringFromResouce(R.string.main_dish));
        hashMap.put("TrangMieng", Util.getStringFromResouce(R.string.dessert));
        return hashMap;
    }

    public static HashMap<String, String> getEnumResponsibleType() {
        HashMap<String, String> hashMap = new HashMap();
        hashMap.put("PhaChe", "Pha chế");
        hashMap.put("Bep", "Bếp");
        hashMap.put("Sushi", "SuShi");
        hashMap.put("Sonstiges0", "Khác");
        return hashMap;
    }

    public static Object getKeyFromValue(Map hm, Object value) {
        for (Object o : hm.keySet()) {
            if (hm.get(o).equals(value)) {
                return o;
            }
        }
        return "";
    }

    public static String getValueFromKey(Map hm, Object key) {
        return (String) hm.get(key);
    }

    public static void selectSpinnerItemByValue(Spinner spnr, Object value) {
        for (int position = 0; position < spnr.getCount(); position++) {
            if (spnr.getItemAtPosition(position).equals(value)) {
                spnr.setSelection(position);
                return;
            }
        }
    }

    public static String dateToString(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date);
    }

    public static Date StringToDate(String data) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(data);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return date;
    }



    public static String getDate(String type) {
        String now = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new java.util.Date().getTime());
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
        Date date = new Date();
        try {
            if (type.equals("dt")) {
                SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String time24 = outFormat.format(inFormat.parse(now));
                return time24;
            } else if (type.equals("d")) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                return formatter.format(date);
            } else if (type.equals("date_code")) {
                SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
                return formatter.format(date);
            } else {
                return date.getTime() + "";
            }
        } catch (java.text.ParseException e) {
            e.printStackTrace();
            return date.getTime() + "";
        }
    }




    public static String getMinuteFromTwoTime(Date date1, Date date2) {
        int min;
        long difference;
        difference = date1.getTime() - date2.getTime();
        min = Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(difference)).intValue();
        return min + "";
    }

    public static int hourHH = 0;
    public static long miliHH = 0;
//    public static String getTimeFromTwoTime(Date startDate, Date endDate){
//        int hours;
//        int min;
//        int sec;
//        long difference ;
//        difference = endDate.getTime() - startDate.getTime();
//        hours = Long.valueOf(TimeUnit.MILLISECONDS.toHours(difference)).intValue();
//        min = Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(difference)).intValue();
//        hours = (hours < 0 ? -hours : hours);
//
//        hourHH = hours;
//        minHH = min;
//        String min_ = min+"";
//        if(min<10){
//            min_ = "0"+min;
//        }
//
//        return hours+":"+min_;
//    }

    public static String getDayHourMinuteByMinute(Date startDate, Date endDate) {
        long difference;
        difference = endDate.getTime() - startDate.getTime();
        long min = Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(difference)).intValue();
        miliHH = difference;
        String date_time;
        long hour;
        long minute = min;

        hour = minute / 60;
        minute = minute % 60;
        hour = hour % 24;

        String hour_;
        String min_;

        hour_ = hour + "";
        min_ = minute + "";

        if (minute < 10) {
            min_ = "0" + minute;
        }
        if (hour < 10) {
            hour_ = "0" + hour;
        }

//        if(day > 0){
//            date_time = day +" " +Util.getStringFromResouce(R.string.day)+" "+hour_+":"+min_;
//        }else {
        date_time = hour_ + ":" + min_;
//        }

        return date_time;
    }

    public static int getDayOfWeek(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.DAY_OF_WEEK);
    }


    public static String getHour(String originalString) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(originalString);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("H:mm").format(date);
    }

    public static String currrentYear() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        return sdf.format(new Date());
    }

    public static String currentSecond() {
        SimpleDateFormat sdf = new SimpleDateFormat("ss");
        return sdf.format(new Date());
    }

    public static String getYear(String originalString) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(originalString);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("yyyy").format(date);
    }

    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return "02:00:00:00:00:00";
    }

    public static String sha256(String plainPassword) {
        String hashedPass = "";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(plainPassword.getBytes(), 0,
                    plainPassword.length());
            hashedPass = new BigInteger(1, messageDigest.digest()).toString(16);
            if (hashedPass.length() < 32) {
                hashedPass = "" + hashedPass;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashedPass;
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static Observable<Long> delayExecute(int delayTimeInMillisecond) {
        return rx.Observable
                .create(new Observable.OnSubscribe<Long>() {
                    @Override
                    public void call(Subscriber<? super Long> subscriber) {
                        subscriber.onNext(0L);
                        subscriber.onCompleted();
                    }
                })
                .delay(delayTimeInMillisecond, TimeUnit.MILLISECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static boolean isNullOrEmpty(String text) {
        return text == null || text.length() == 0;
    }


    public static void hideKeyboard(Context context, View view) {
        view.clearFocus();
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String safelyGetTextFromEditText(EditText editText) {
        return editText.getText() != null ? editText.getText().toString() : "";
    }

    public static Point getScreenSize(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        return new Point(metrics.widthPixels, metrics.heightPixels);
    }

    public static String makeGoogleDocsLink(String link) {
        return "http://docs.google.com/gview?embedded=true&url=" + link;
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int getNavigationBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen",
                "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat);
        df_input.setTimeZone(TimeZone.getTimeZone("UTC"));

        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat);

        try {
            try {
                parsed = df_input.parse(inputDate);
            } catch (java.text.ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {

        }

        return outputDate;
    }

    public static Date formatStringToDate(String dtStart) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(dtStart);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static double tinhTongThanhTien(List<OrdersTemp> ordersTempList) {
        double tongtien = 0;
        for (OrdersTemp ordersTemp : ordersTempList) {
            tongtien += ordersTemp.getThanhtien();
        }
        return tongtien;
    }

    public static void alertPromp(Context context, String title, String content
            , DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(content)
                .setPositiveButton(android.R.string.yes, positiveListener)
                .setNegativeButton(android.R.string.no, negativeListener)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private static Dialog dialog;

    public static void showCustomDialog(Context context, String content) {

        dialog = new Dialog(context);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_dialog);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }
//
        TextView tv_title, tv_content;
        tv_title = dialog.findViewById(R.id.tv_title);
        tv_content = dialog.findViewById(R.id.tv_content);

        tv_title.setText(Util.getStringFromResouce(R.string.Warning));
        tv_content.setText(content);

        dialog.show();
    }

    public interface ConfirmDialogInterface {
        void onPositiveClickListener();

        void onNegativeClickListener();
    }

    public static void showCustomDialogConfirm(Context context, String content, final ConfirmDialogInterface listener) {

        dialog = new Dialog(context);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirm_dialog);
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        dialog.getWindow().setLayout((width * 1), RelativeLayout.LayoutParams.WRAP_CONTENT);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
            dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        }
//
        TextView tv_title, tv_content, btn_ok, btn_cancel;
        tv_title = dialog.findViewById(R.id.tv_title);
        tv_content = dialog.findViewById(R.id.tv_content);
        btn_ok = dialog.findViewById(R.id.btn_ok);
        btn_cancel = dialog.findViewById(R.id.btn_cancel);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPositiveClickListener();
                dialog.dismiss();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onNegativeClickListener();
                dialog.dismiss();
            }
        });

        tv_title.setText(Util.getStringFromResouce(R.string.Warning));
        tv_content.setText(content);

        dialog.show();
    }

    public static void dismissCustomDialog() {
        if (dialog != null) dialog.dismiss();
    }

    public static <T> T getDataSaveInstanceStateFromGson(Bundle savedInstanceState, String key) {
        Gson gson = new Gson();
        T data = gson.fromJson(savedInstanceState.getString(key), new TypeToken<T>() {
        }.getType());
        return data;
    }

    public static <T> void setDataSaveInstanceFromGson(Bundle outState, T data, String key) {
        Type listType = new TypeToken<T>() {
        }.getType();
        Gson gson = new Gson();
        String json = gson.toJson(data, listType);
        outState.putString(key, json);
    }

    public static String getStringFromResouce(int R) {
        return App.Companion.self().getString(R);
    }

    public static void slideUp(View view) {
        view.animate().translationY(0);
    }

    public static void slideDown(View view) {
        view.animate().translationY(view.getHeight() + 500);
    }


    public static void popFragment(Activity activity) {
        if (activity != null)
            ((BaseActivity) activity).popFragment();
    }

    public static void popFragmentByName(Activity activity, String fragName) {
        if (activity != null)
            ((BaseActivity) activity).popFragment(fragName);
    }

    public static void popFragmentExcept(Activity activity, String excFrag) {
        if (activity != null)
            ((BaseActivity) activity).popFragment(excFrag);
    }

//    public static int validateInput(String...data) {
//        if(data.length> 0){
//            for(int i = 0 ; i < data.length; i++){
//                if(Util.isNullOrEmpty(data[i])){
//                    return R.string.please_fill_all_input;
//                }
//            }
//            return -1;
//        }
//        return  R.string.please_fill_all_input;
//    }

    //    public static String getLangCode(){
//        return String.valueOf(PrefUtil.get(PrefUtil.LANGUAGE, Language.class).getCode());
//    }
//    public static final String CODE = getLangCode();
    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
