package com.example.abc.pos.screen.items_popup;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.ItemPopupAdapter;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.model.ItemsPopup;
import com.example.abc.pos.screen.chuc_nang_order.ChucNangOrderPresenter;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ItemsPopupDialogFragment extends BottomSheetDialogFragment implements ItemPopupAdapter.ItemClickListener {

    private OnItemPopupClick mListener;

    private ItemPopupAdapter adapter;

    private RecyclerView rc_item_popup;

    private List<ItemsPopup> itemsPopupList;

    int positionOrder;

    public interface OnItemPopupClick {
        void onItemPopupClickListener(int positionOrder,ItemsPopup itemsPopup);
    }

    Dialog dialog;

    public ItemsPopupDialogFragment() {

    }

    @SuppressLint("ValidFragment")
    public ItemsPopupDialogFragment(List<ItemsPopup> itemsPopupList, int positionOrder ,OnItemPopupClick listener) {
        this.itemsPopupList = itemsPopupList;
        this.positionOrder = positionOrder;
        this.mListener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_items_popup_dialog, container, false);
        rc_item_popup = view.findViewById(R.id.rc_item_popup);
        adapter = new ItemPopupAdapter(getContext(),itemsPopupList,this);
        rc_item_popup.setLayoutManager(new GridLayoutManager(getContext(),4,GridLayoutManager.VERTICAL, false));
        rc_item_popup.setAdapter(adapter);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();

        if (dialog != null) {
            DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
            int height = displayMetrics.heightPixels;
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = (int) (height * 0.60);
        }
        final View view = getView();
        view.post(new Runnable() {
            @Override
            public void run() {
                View parent = (View) view.getParent();
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
                CoordinatorLayout.Behavior behavior = params.getBehavior();
                BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) behavior;
                bottomSheetBehavior.setPeekHeight(view.getMeasuredHeight());
            }
        });
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onItemClick(int position, ItemsPopup data) {
        mListener.onItemPopupClickListener(positionOrder,data);
    }

}
