package com.example.abc.pos.screen.combine_table;

import com.example.abc.pos.model.Table;

import java.util.List;

public interface CombineTableContract {
    interface View{
         void showListTable(List<Table> tables);
        void showLoading();
        void dismissLoading();
        void goToCombineToTable(List<Table> tableName);
    }

    interface Presenter{
        void getUsingTable();
        void goToCombineToTable(List<Integer> positionsSelected);
    }
}
