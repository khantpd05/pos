package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.model.Spice;

import java.util.List;

public class LayBanAdapter extends RecyclerView.Adapter<LayBanAdapter.ViewHolder> {
    private List<String> list;
    private ItemClickListener mClickListener;
    private int selectedPosition = -1;

    public LayBanAdapter( Context context, List<String> list, ItemClickListener clickListener) {
        this.list = list;
        mClickListener = clickListener;
    }

    public void setNewDatas(List<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lay_ban, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tv_name.setText(list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tv_name;


        ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (position == RecyclerView.NO_POSITION) {
                return;
            }

            if (mClickListener != null) {
                mClickListener.onItemClick(position, list.get(position));
            }
        }
    }




    public interface ItemClickListener {
        void onItemClick(int position, String data);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
