package com.example.abc.pos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.v7.graphics.drawable.DrawableWrapper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.Table;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TablesAdapter extends RecyclerView.Adapter<TablesAdapter.ViewHolder> {
    private List<Table> mTables;
    private ItemClickListener mClickListener;
    private int selectedPosition = -1;
    private Context context;


    public TablesAdapter(Context context, List<Table> tables, ItemClickListener mClickListener) {
        mTables = tables;
        this.context = context;
        this.mClickListener = mClickListener;
    }

    public void setNewDatas(List<Table> tables) {
        mTables = tables;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_item, parent, false);

        return new ViewHolder(view);
    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.tv_name.setText(mTables.get(position).getName());
        holder.ll_wrap.setTag(position);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);

        if (mTables.get(position).getVitritronghayngoainha().equals(WapriManager.TrongNhaHayNgoaiNha.NgoaiNha.name())) {
            params.gravity = Gravity.END;
            params.rightMargin = 8;
            holder.ll_wrap_userinfo.setGravity(Gravity.START);
            holder.ll_wrap_userinfo.setLayoutParams(params);
        } else {
            params.gravity = Gravity.CENTER;
            params.rightMargin = 0;
            holder.ll_wrap_userinfo.setLayoutParams(params);
        }

        if (position == selectedPosition) {
            GradientDrawable gd = new GradientDrawable();
            gd.setShape(GradientDrawable.RECTANGLE);
            gd.setStroke(5, Color.RED);
            gd.setCornerRadius(10);
            holder.ll_wrap2.setBackground(gd);
        } else {
            GradientDrawable gd = new GradientDrawable();
            gd.setShape(GradientDrawable.RECTANGLE);
            gd.setColor(Color.WHITE);
            gd.setStroke(0, Color.WHITE);
            gd.setCornerRadius(0);
            holder.ll_wrap2.setBackground(gd);
        }

        if (mTables.get(position).getis_selling_on_tablet().equals("Y") || mTables.get(position).getIs_selling_on_POS().equals("Y")) {
            holder.tv_user_name.setVisibility(View.VISIBLE);
            holder.tv_time.setVisibility(View.VISIBLE);
            holder.tv_status.setVisibility(View.VISIBLE);

            if (mTables.get(position).getVitritronghayngoainha().equals(WapriManager.TrongNhaHayNgoaiNha.TrongNha.name()))
                holder.ll_wrap.setBackground(context.getResources().getDrawable(R.drawable.bg_xanh_dam));
            else
                holder.ll_wrap.setBackground(context.getResources().getDrawable(R.drawable.bg_xe_xanh_dam));

            holder.tv_user_name.setText(mTables.get(position).getUsername());

            String pattern = "yyyy-MM-dd hh:mm:ss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            try {
                if (mTables.get(position).getThoigian() != null && !mTables.get(position).getThoigian().isEmpty()) {
                    Date data = simpleDateFormat.parse(mTables.get(position).getThoigian());
                    String time = new SimpleDateFormat("HH:mm").format(data);

                    holder.tv_time.setText(time);
                } else {
                    holder.tv_time.setVisibility(View.INVISIBLE);
                }


            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (mTables.get(position).getTotal_orders_not_printed() > 0) {
            holder.tv_user_name.setVisibility(View.VISIBLE);
            holder.tv_time.setVisibility(View.VISIBLE);
            holder.tv_status.setVisibility(View.GONE);
            if (mTables.get(position).getVitritronghayngoainha().equals(WapriManager.TrongNhaHayNgoaiNha.TrongNha.name())) {
                holder.ll_wrap.setBackgroundResource(R.drawable.taicho_animate);
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.ll_wrap.getBackground();
                frameAnimation.start();
            } else {
                holder.ll_wrap.setBackgroundResource(R.drawable.mangve_animate);
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.ll_wrap.getBackground();
                frameAnimation.start();
            }

            holder.tv_user_name.setText(mTables.get(position).getUsername());

            String pattern = "yyyy-MM-dd hh:mm:ss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            try {
                if (mTables.get(position).getThoigian() != null && !mTables.get(position).getThoigian().isEmpty()) {
                    Date data = simpleDateFormat.parse(mTables.get(position).getThoigian());
                    String time = new SimpleDateFormat("HH:mm").format(data);

                    holder.tv_time.setText(time);
                } else {
                    holder.tv_time.setVisibility(View.INVISIBLE);
                }


            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (mTables.get(position).getTotal_orders() > 0) {
            holder.tv_user_name.setVisibility(View.VISIBLE);
            holder.tv_time.setVisibility(View.VISIBLE);
            holder.tv_status.setVisibility(View.GONE);
            if (mTables.get(position).getVitritronghayngoainha().equals(WapriManager.TrongNhaHayNgoaiNha.TrongNha.name()))
                holder.ll_wrap.setBackground(context.getResources().getDrawable(R.drawable.bg_xanh_la));
            else
                holder.ll_wrap.setBackground(context.getResources().getDrawable(R.drawable.bg_xe_cam));

            holder.tv_user_name.setText(mTables.get(position).getUsername());

            String pattern = "yyyy-MM-dd hh:mm:ss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            try {
                if (mTables.get(position).getThoigian() != null && !mTables.get(position).getThoigian().isEmpty()) {
                    Date data = simpleDateFormat.parse(mTables.get(position).getThoigian());
                    String time = new SimpleDateFormat("HH:mm").format(data);

                    holder.tv_time.setText(time);
                } else {
                    holder.tv_time.setVisibility(View.INVISIBLE);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {

            if (mTables.get(position).getVitritronghayngoainha().equals(WapriManager.TrongNhaHayNgoaiNha.TrongNha.name()))
                holder.ll_wrap.setBackground(context.getResources().getDrawable(R.drawable.bg_xanh_dam));
            else
                holder.ll_wrap.setBackground(context.getResources().getDrawable(R.drawable.bg_xe_xanh_dam));

            holder.tv_user_name.setVisibility(View.INVISIBLE);
            holder.tv_time.setVisibility(View.INVISIBLE);
            holder.tv_status.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mTables.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_name, tv_status, tv_user_name, tv_time;
        LinearLayout ll_wrap, ll_wrap2, ll_wrap_userinfo;

        ViewHolder(View itemView) {
            super(itemView);

            tv_name = itemView.findViewById(R.id.info_text);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_user_name = itemView.findViewById(R.id.tv_user_name);
            tv_time = itemView.findViewById(R.id.tv_time);
            ll_wrap = itemView.findViewById(R.id.ll_wrap);
            ll_wrap2 = itemView.findViewById(R.id.ll_wrap2);
            ll_wrap_userinfo = itemView.findViewById(R.id.ll_wrap_userinfo);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                if (getAdapterPosition() >= 0)
                    mClickListener.onItemClick(view, mTables.get(getAdapterPosition()));

            itemCheckChanged(ll_wrap);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    public interface ItemClickListener {
        void onItemClick(View view, Table table);
    }


}
