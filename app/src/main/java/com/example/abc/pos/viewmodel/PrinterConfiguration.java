package com.example.abc.pos.viewmodel;

import android.databinding.ObservableField;

import com.example.abc.pos.model.Configuration;

import java.util.List;

public class PrinterConfiguration {
    public ObservableField<String> charSet = new ObservableField<>("2");
    public ObservableField<String> maxChar = new ObservableField<>("46");
    public ObservableField<String> ip_bep = new ObservableField<>("192.168.1.109");
    public ObservableField<String> port_bep = new ObservableField<>("9100");
    public ObservableField<String> ip_nuoc = new ObservableField<>("192.168.1.109");
    public ObservableField<String> port_nuoc = new ObservableField<>("9100");
    public ObservableField<String> ip_sushi = new ObservableField<>("192.168.1.109");
    public ObservableField<String> port_sushi = new ObservableField<>("9100");

    public PrinterConfiguration() {
    }


}
