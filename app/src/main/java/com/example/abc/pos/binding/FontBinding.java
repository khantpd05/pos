package com.example.abc.pos.binding;

import android.databinding.BindingAdapter;
import android.support.v4.content.res.ResourcesCompat;
import android.widget.TextView;


public class FontBinding {
    @BindingAdapter({"bind:font"})
    public static void setFont(TextView textView, int fontName) {
        textView.setTypeface(ResourcesCompat.getFont(textView.getContext(), fontName));
    }
}
