package com.example.abc.pos.screen.keyboard_code;

public interface KeyboardCodeConstract {
    interface View{
        void getDataInput(String data);

    }

    interface Presenter{
        void getDataInput();

    }
}
