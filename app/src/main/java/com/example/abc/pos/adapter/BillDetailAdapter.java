package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.ExtraItem;
import com.example.abc.pos.model.Orders;
import com.example.abc.pos.model.OrdersDetail;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BillDetailAdapter  extends BaseExpandableListAdapter {


    private Context context;

    private List<OrdersDetail> ordersDetailList;


    private List<Integer> positionsSelected = new ArrayList<>();

    private SessionApp.OrderOrPayment orderOrPayment;


    public BillDetailAdapter(Context context, List<OrdersDetail> ordersDetailList) {

        this.context = context;
        this.ordersDetailList = ordersDetailList;
    }


    public void setNewDatas(List<OrdersDetail> ordersDetailList) {
        this.ordersDetailList = ordersDetailList;
        notifyDataSetChanged();
    }

    public void addNewItem(OrdersDetail ordersTemp) {
        this.ordersDetailList.add(ordersTemp);
        notifyDataSetChanged();
    }

    @Override
    public ExtraItem getChild(int listPosition, int expandedListPosition) {
        return this.ordersDetailList.get(listPosition).getExtraItemList()
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {


        ViewHolderChild holder;
        if (convertView == null) {
            holder = new ViewHolderChild();

            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item_extra_item_bill, null);

            holder.tv_item_extra_name = convertView.findViewById(R.id.tv_item_extra_name);
            holder.tv_price = convertView.findViewById(R.id.tv_price);
            holder.tv_thanhtien = convertView.findViewById(R.id.tv_thanhtien);
            holder.ll_wrap_child = convertView.findViewById(R.id.ll_wrap_child);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderChild) convertView.getTag();
            holder.tv_item_extra_name.setText("");
            holder.tv_price.setText("");
            holder.tv_thanhtien.setText("");
        }
        holder.tv_item_extra_name.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
        holder.tv_price.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));

        final String name = getChild(listPosition, expandedListPosition).getName();
        final double value = getChild(listPosition, expandedListPosition).getValue();
        final WapriManager.FeatureOrder flag = getChild(listPosition, expandedListPosition).getFlag();


        holder.tv_item_extra_name.setText(name);




        if (flag == WapriManager.FeatureOrder.DEM_VE || flag == WapriManager.FeatureOrder.GHI_CHU_BEP ) {
            holder.tv_price.setVisibility(View.INVISIBLE);

        } else {
            holder.tv_price.setVisibility(View.VISIBLE);
        }

        if (orderOrPayment == SessionApp.OrderOrPayment.PAYMENT) {
            if (flag == WapriManager.FeatureOrder.DEM_VE || flag == WapriManager.FeatureOrder.GIAM  || flag == WapriManager.FeatureOrder.GHI_CHU_BEP || flag == WapriManager.FeatureOrder.GIA_TU_DO) {
                holder.tv_thanhtien.setVisibility(View.INVISIBLE);
            } else {
                holder.tv_thanhtien.setVisibility(View.VISIBLE);
            }
            holder.tv_item_extra_name.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
            holder.tv_price.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
        } else {
            holder.tv_thanhtien.setVisibility(View.INVISIBLE);
        }

        if (flag == WapriManager.FeatureOrder.GIAM) {
            holder.tv_price.setText(String.format("%.2f", value) + "");
            holder.tv_thanhtien.setText(String.format("%.2f", value) + "");
        }

        if (flag == WapriManager.FeatureOrder.GIA_VI || flag == WapriManager.FeatureOrder.DEM_VE || flag == WapriManager.FeatureOrder.GIAM  ||
                flag == WapriManager.FeatureOrder.GHI_CHU_BEP || flag == WapriManager.FeatureOrder.GIA_TU_DO || flag == WapriManager.FeatureOrder.PHAN_AN ) {
            holder.tv_thanhtien.setVisibility(View.INVISIBLE);
        } else {
            holder.tv_thanhtien.setVisibility(View.VISIBLE);
        }

        if (flag == WapriManager.FeatureOrder.GIA_VI || flag == WapriManager.FeatureOrder.GIA_TU_DO || flag == WapriManager.FeatureOrder.PHAN_AN) {
            holder.tv_price.setText(String.format("%.2f", value) + "");
            holder.tv_thanhtien.setText(String.format("%.2f", value) + "");
        }


        return convertView;
    }

    private class ViewHolderChild {
        public TextView tv_item_extra_name, tv_price, tv_thanhtien;
        public LinearLayout ll_wrap_child;

    }


    @Override
    public int getChildrenCount(int listPosition) {
        return ordersDetailList.get(listPosition).getExtraItemList() != null && ordersDetailList.get(listPosition).getExtraItemList()
                .size() > 0 ? ordersDetailList.get(listPosition).getExtraItemList()
                .size() : 0;
    }

    @Override
    public OrdersDetail getGroup(int listPosition) {
        return this.ordersDetailList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.ordersDetailList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }


    private class ViewHolderGroup{
        TextView tv_number, tv_food_name, tv_price, tv_summer;
        LinearLayout ll_wrap_group,ll_wrap_text;



        public ViewHolderGroup(View view) {
            tv_number = view.findViewById(R.id.tv_number);
            tv_food_name = view.findViewById(R.id.tv_food_name);
            tv_price = view.findViewById(R.id.tv_price);
            tv_summer = view.findViewById(R.id.tv_summer);
            ll_wrap_group = view.findViewById(R.id.ll_wrap_group);
            ll_wrap_text = view.findViewById(R.id.ll_wrap_text);
            ll_wrap_text = view.findViewById(R.id.ll_wrap_text);

        }

    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(listPosition);
        ViewHolderGroup holder;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_bill_detail, null);
            holder = new ViewHolderGroup(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderGroup) convertView.getTag();
            holder.tv_number.setText("");
            holder.tv_food_name.setText("");
            holder.tv_price.setText("");
            holder.tv_summer.setText("");
        }
        /** set font family **/
        holder.tv_number.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_BOLD));
        holder.tv_food_name.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_BOLD));
        holder.tv_price.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_BOLD));
        holder.tv_summer.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_BOLD));

        holder.tv_number.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
        holder.tv_food_name.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
        holder.tv_price.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
        holder.tv_summer.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));

        holder.tv_number.setText(getGroup(listPosition).getSoluong()+"");
        holder.tv_food_name.setText(getGroup(listPosition).getTenhang());
        holder.tv_price.setText(String.format("%.2f",getGroup(listPosition).getorig_price_no_extra()));
        holder.tv_summer.setText(String.format("%.2f",getGroup(listPosition).getThanhtien()));

        String danhdaubixoa = getGroup(listPosition).getDanhdaubixoa();


        holder.ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));

        if(danhdaubixoa!=null){
            if (danhdaubixoa.equals("Y")) {
                holder.ll_wrap_text.setBackground(context.getResources().getDrawable(R.drawable.strike_through));
            } else {
                holder.ll_wrap_text.setBackground(null);
            }
        }

        return convertView;
    }

    private class ItemSelected {
        public boolean isSelected = false;
    }

    public void resetSelectedList() {
        positionsSelected.clear();
        positionsSelected = new ArrayList<>();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}

