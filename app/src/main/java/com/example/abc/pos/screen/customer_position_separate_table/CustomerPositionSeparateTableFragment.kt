package com.example.abc.pos.screen.change_order_to_customer_position


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.abc.pos.App

import com.example.abc.pos.R
import com.example.abc.pos.base.BaseActivity
import com.example.abc.pos.base.SessionApp
import com.example.abc.pos.base.WapriManager
import com.example.abc.pos.model.OrdersTemp
import com.example.abc.pos.utils.KToast
import com.example.abc.pos.utils.Util


@SuppressLint("ValidFragment")
class CustomerPositionSeparateTableFragment (): Fragment(), CustomerPositionSeparateTableContract.View {



    var positions: Int = SessionApp.CUSTOMER_POSITIONS_SEPARATE_TABLE
    var positionsHaveOrder: List<Int> = SessionApp.POSITIONS_HAVE_ORDER_SEPARATE_TABLE
    lateinit var listener: OnPositionClick
    lateinit var taichohaymangve: WapriManager.taichohaymangve
    lateinit var listener_change_success: OnSeparateOrderToTableSuccessful
    constructor( listener:OnPositionClick,  taichohaymangve: WapriManager.taichohaymangve,  listener_change_success: OnSeparateOrderToTableSuccessful):this(){
        this.listener = listener
        this.taichohaymangve = taichohaymangve
        this.listener_change_success = listener_change_success
    }

    override fun showAlert(data: String) {
        if (activity != null)
            KToast.show(activity!!, data, Toast.LENGTH_SHORT)
    }

     interface OnPositionClick {
        fun onCustomerPositionClickListener()
    }

    interface OnSeparateOrderToTableSuccessful {
        fun onSeparateOrderToTableSuccessfulListener(ordersTempListWillChange: MutableList<OrdersTemp>)
    }

    lateinit var tv_title: TextView
    var ll_top: LinearLayout? = null
    var ll_bot: LinearLayout? = null
    var ll_exit: LinearLayout? = null
    var ll_prepared_table: LinearLayout? = null
    var presenter: CustomerPositionSeparateTableContract.Presenter? = null

    lateinit var main_table: TextView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_customer_position, container, false)

        if(savedInstanceState!=null){
                Util.popFragment(activity)
        }else{
            tv_title = view.findViewById(R.id.tv_title)
            tv_title.text = App.self().getText(R.string.the_sitting_position_will_move_come)

            main_table = view.findViewById(R.id.main_table)
            ll_top = view.findViewById(R.id.ll_top)
            ll_bot = view.findViewById(R.id.ll_bot)
            ll_exit = view.findViewById(R.id.ll_exit)
            ll_exit?.setOnClickListener{
                Util.popFragment(activity)
            }
            ll_prepared_table = view.findViewById(R.id.ll_prepared_table)

            presenter = CustomerPositionSeparateTablePresenter(this)
            presenter?.loadData(positions,taichohaymangve)


            main_table.setOnClickListener {
                presenter?.updateSeparateToTable(1)
            }
        }



        return view
    }


    override fun showPosition(positions: Int) {
        var halfSize = positions / 2


        if (positionsHaveOrder.size > 0) {
            main_table.setBackground(ContextCompat.getDrawable(context!!, R.drawable.bg_radius_all_position))
        } else {
            main_table.setBackground(ContextCompat.getDrawable(context!!, R.drawable.bg_radius_customer_position))
        }

        for (i in 0 until halfSize) {
            var k = 0
            for (j in 0 until positionsHaveOrder.size) {
                if ((i + 1) == positionsHaveOrder[j]) {
                    k = positionsHaveOrder[j]
                    break
                }
            }
            if (k != 0) {
                createNewButton(ll_top, i, R.style.TextCustomerPositionGreen)
            } else {
                createNewButton(ll_top, i, R.style.TextCustomerPosition)
            }
        }


        for (i in halfSize until positions) {
            var k = 0
            for (j in 0 until positionsHaveOrder.size) {
                if (i + 1 == positionsHaveOrder[j]) {
                    k = positionsHaveOrder[j]
                }
            }
            if (k != 0) {
                createNewButton(ll_bot, i, R.style.TextCustomerPositionGreen)
            } else {
                createNewButton(ll_bot, i, R.style.TextCustomerPosition)
            }
        }

        for (i in positions..positions + 4) {
            var k = 0
            for (j in 0 until positionsHaveOrder.size) {
                if (i + 1 == positionsHaveOrder[j]) {
                    k = positionsHaveOrder[j]
                } else {
                }
            }
            if (k != 0) {
                createNewButton(ll_prepared_table, i, R.style.TextCustomerPositionGreen)
            } else {
                createNewButton(ll_prepared_table, i, R.style.TextCustomerPosition)
            }
        }
    }


    private fun createNewButton(parent: LinearLayout?, i: Int, theme: Int) {
        var button = Button(ContextThemeWrapper(context, theme), null, 0)
        button.id = i + 1
        button.text = (i + 1).toString()
        val scale = context!!.resources.displayMetrics.density
        var valueTextSize = resources.getDimension(R.dimen.text_20)
        val pixels = (valueTextSize * scale + 0.5f).toInt()
        button.layoutParams = LinearLayout.LayoutParams(pixels, pixels)
        setMargins(button, 8, 8, 8, 8)
        parent?.addView(button)

        button.setOnClickListener {
            presenter?.updateSeparateToTable(button.text.toString().toInt())
        }
    }

    private fun setMargins(view: View, left: Int, top: Int, right: Int, bottom: Int) {
        if (view.layoutParams is ViewGroup.MarginLayoutParams) {
            val p = view.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }

    override fun returnOrder() {
       for(i in 0..2){
           if(activity!=null)
               (activity as BaseActivity ).popFragment()
       }

    }


    override fun dismissLoading() {

    }

    override fun showLoading() {

    }

    override fun onChangeSuccessful(ordersTempsWillChange: MutableList<OrdersTemp>) {
        listener_change_success.onSeparateOrderToTableSuccessfulListener(ordersTempsWillChange)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("key", "temp")
    }
}
