package com.example.abc.pos.screen.tab_of_sale.drink;

import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;

import java.util.List;

public interface DrinkTabConstract {
    interface View{
        void showListMenus(List<Menu> menus);
        void showListItems(List<Items> items);
        void showLoading();
        void dismissLoading();
    }
    interface Presenter{
        void loadData();
        void getItems(String menu_id);
    }
}
