package com.example.abc.pos.screen.setting_gutschein_restaurant;

import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.viewmodel.GeneralConfiguration;

import java.util.List;

public interface SettingGutscheinRestaurantContract {
    interface View {
        void showLoading();

        void dismissLoading();

        void showAlert(String data);

        int getTypeOfList();

        GeneralConfiguration getConfig();

        void exit();
    }

    interface Presenter {
        void getData(List<Configuration> datas);

        void updateValues();

        void exit();
    }
}
