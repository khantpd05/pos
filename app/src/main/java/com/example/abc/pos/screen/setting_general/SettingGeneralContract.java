package com.example.abc.pos.screen.setting_general;

import com.example.abc.pos.model.Configuration;

import java.util.List;

public interface SettingGeneralContract {
    interface View{
        void showLoading();
        void dismissLoading();
        void showAlert(String data);
        void goGutscheinSetting(List<Configuration> datas);
        void goRestaurantSetting(List<Configuration> datas);
        void exit();
    }

    interface Presenter{
        void loadData();
        void getListValue(@SettingGeneralPresenter.OperationsDef int type);
        void goGutscheinSetting();
        void goRestaurantSetting();
        void updateNewConfig();
        void exit();
    }

}
