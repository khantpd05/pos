package com.example.abc.pos.screen.change_to_table;

import android.os.AsyncTask;
import android.util.Log;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.ExtraItem;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.model.Orders;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.screen.change_table.ChangeTableContract;
import com.example.abc.pos.screen.payment_detail.PaymentDetailPresenter;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class ChangeToTablePresenter implements ChangeToTableContract.Presenter {

    ChangeToTableContract.View mView;
    private List<OrdersTemp> ordersTempList;
    private Table table_old, table_new;

    public ChangeToTablePresenter(Table table, ChangeToTableContract.View mView) {
        this.mView = mView;
        ordersTempList = new ArrayList<>();
        this.table_old = table;

        getEmptyTable();
        getAllOrderFromOldTable();
    }

    @Override
    public void getEmptyTable() {
        new GettingEmptyTableTask().execute();
    }

    @Override
    public void doChangeTable(Table table, Table table_new) {
        table_new.setTrangthai(table.getTrangthai());
        table_new.setUsername(table.getUsername());
        table_new.setThoigian(table.getThoigian());
        table_new.setTongtien(table.getTongtien());

        table.setTrangthai("empty");
        table.setUsername(null);
        table.setThoigian(null);
        table.setTongtien(null);
        table.setGiam(0);
        table.setTiengiam(0.0);
        table.setTienthue19giam(0.0);
        table.setTienthue7giam(0.0);

        new ExecuteAllDbTask(table).execute();
    }

    @Override
    public void setTableNew(Table tableNew) {
        this.table_new = tableNew;
    }


    private void getAllOrderFromOldTable() {
        new GetAllOrderTempListTask(table_old.getName()).execute();
    }


    private double tinhTienThue(OrdersTemp ordersTemp) {
        if (ordersTemp.getBophanphucvu().equals("Sonstiges0"))
            return 0.0;
        else if ((table_new.getTaichohaymangve() != WapriManager.taichohaymangve.MangVe && ordersTemp.getTaichohaymangve() == WapriManager.Taichohaymangve_.TaiCho) || ordersTemp.getBophanphucvu().equals("PhaChe19"))
            return WapriManager.tinhTienThue19(ordersTemp.getGiatien(), ordersTemp.getPhantramgiam());
        else
            return WapriManager.tinhTienThue7(ordersTemp.getGiatien(), ordersTemp.getPhantramgiam());
    }

    private class ExecuteAllDbTask extends AsyncTask<Void, Void, Void> {
        Table table;

        public ExecuteAllDbTask(Table table) {
            this.table = table;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            new UpdateToDbTask(table, table_new).execute();

            for (OrdersTemp ob : ordersTempList) {

//                if (table_new.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho){
//                    ob.setTaichohaymangve(WapriManager.Taichohaymangve_.TaiCho);
//                }else{
//                    ob.setTaichohaymangve(WapriManager.Taichohaymangve_.MangVe);
//                }
//                if (table_new.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho || ob.getBophanphucvu().equals("PhaChe19")) {
//                    ob.setThue(19);
//
//                    ob.setTienthue(tinhTienThue(ob));
//                }else{
//                    ob.setThue(7);
//                    ob.setTienthue(tinhTienThue(ob));
//                }
                ob.initExtraItemList();
                ob.setGiatien(tinhGiaTien(ob));
                if(table.getTaichohaymangve() == WapriManager.taichohaymangve.MangVe && table_new.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho){
                    ob.setTaichohaymangve(WapriManager.Taichohaymangve_.TaiCho);
                }
                if (table_new.getTaichohaymangve() == WapriManager.taichohaymangve.MangVe) {
                    ob.setTaichohaymangve(WapriManager.Taichohaymangve_.MangVe);
                }
                if ((table_new.getTaichohaymangve() != WapriManager.taichohaymangve.MangVe && ob.getTaichohaymangve() == WapriManager.Taichohaymangve_.TaiCho)
                        || ob.getBophanphucvu().equals("PhaChe19")) {
                    ob.setThue(19);
                    ob.setTienthue(tinhTienThue(ob));
                } else {
                    ob.setThue(7);
                    ob.setTienthue(tinhTienThue(ob));
                }

                new UpdateChangeOrderToNewTableDbTask(ob).execute();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mView.dismissLoading();
            mView.goBack();
        }
    }


    private class UpdateToDbTask extends AsyncTask<Void, Void, Integer> {

        Table table, table_new;

        public UpdateToDbTask(Table table, Table table_new) {
            this.table = table;
            this.table_new = table_new;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            if (MySqlHandleUtils.updateTableChangeOld(table) != 0 && MySqlHandleUtils.updateTableChangeNew(table_new) != 0)
                return 1;
            else return 0;

        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
            } else {
                mView.showAlert(Util.getStringFromResouce(R.string.update_failed));
            }
        }
    }


    private class UpdateChangeOrderToNewTableDbTask extends AsyncTask<Void, Void, Integer> {

        OrdersTemp ordersTemp;

        public UpdateChangeOrderToNewTableDbTask(OrdersTemp ordersTemp) {
            this.ordersTemp = ordersTemp;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result = MySqlHandleUtils.updateChangeOrderTableToNewTable(ordersTemp, table_new.getName());
            return result;

        }

    }


    private class GetAllOrderTempListTask extends AsyncTask<Void, Void, List<OrdersTemp>> {

        String tableName;

        public GetAllOrderTempListTask(String tableName) {
            this.tableName = tableName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<OrdersTemp> doInBackground(Void... voids) {

            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.orders_temp where banso = " + "'" + tableName + "'");
            ArrayList<OrdersTemp> ordersTemps = new Gson().fromJson(js.toString(), new TypeToken<List<OrdersTemp>>() {
            }.getType());
            return ordersTemps;
        }

        @Override
        protected void onPostExecute(List<OrdersTemp> datas) {
            super.onPostExecute(datas);
            mView.dismissLoading();
            ordersTempList = datas;
        }
    }


    private class GettingEmptyTableTask extends AsyncTask<Void, Void, ArrayList<Table>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<Table> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.table where trangthai = 'empty'");
            ArrayList<Table> tables = new Gson().fromJson(js.toString(), new TypeToken<List<Table>>() {
            }.getType());
            for(Table table:tables){
                table.setTaichoHayMangVe();
            }
            return tables;
        }

        @Override
        protected void onPostExecute(ArrayList<Table> tables) {
            super.onPostExecute(tables);
            mView.dismissLoading();
            mView.showListTable(tables);
        }
    }

    private double tinhGiaTien(OrdersTemp orderTemp) {
        // count all include spice price
        double totalSpicePrice = 0;
        for (ExtraItem extraItem : orderTemp.getExtraItemList()) {
            if (extraItem.getFlag() == WapriManager.FeatureOrder.GIA_VI || extraItem.getFlag() == WapriManager.FeatureOrder.PHAN_AN) {
                totalSpicePrice += extraItem.getValue();
            }
        }
        double tongtien = (orderTemp.getSoluong() * orderTemp.getorig_price_no_extra()) + (totalSpicePrice * orderTemp.getSoluong());
        return tongtien;
    }
}
