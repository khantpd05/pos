package com.example.abc.pos.screen.setting_payment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abc.pos.R;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.databinding.FragmentSettingPaymentBinding;
import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.screen.setting_general.SettingGeneralContract;
import com.example.abc.pos.screen.setting_general.SettingGeneralPresenter;
import com.example.abc.pos.screen.setting_gutschein_restaurant.SettingGutscheinRestaurantFragment;
import com.example.abc.pos.utils.Util;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingPaymentFragment extends BaseFragment implements SettingPaymentContract.View{

    SettingPaymentPresenter presenter;
    public SettingPaymentFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_setting_payment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentSettingPaymentBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_setting_payment, container, false);

        presenter = new SettingPaymentPresenter(this);

        binding.setPresenter(presenter);

        return binding.getRoot();
    }



    @Override
    public void showAlert(String data) {

    }

    @Override
    public void exit() {
        Util.popFragment(getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.updateNewConfig();
    }
}
