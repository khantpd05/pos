package com.example.abc.pos.screen.create_items_item;

import android.os.AsyncTask;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.utils.Util;

import java.util.ArrayList;

public class CreateItemsItemPresenter implements CreateItemsItemConstract.Presenter {

    private CreateItemsItemConstract.View mView;
    private String loai_mon_an;

    public CreateItemsItemPresenter(CreateItemsItemConstract.View mView, String loai_mon_an) {
        this.mView = mView;
        this.loai_mon_an = loai_mon_an;
    }

    @Override
    public void loadData() {
        ArrayList<String> arr = new ArrayList<>();
        for (String value : Util.getEnumBoPhanPhucVu().values()) {
            arr.add(value);
        }
        mView.showSpinnerData(arr);
    }


    @Override
    public void addNewItem(String item_id, int nhom_hang, String name, double price, String color, String loai) {
        if (item_id.trim().isEmpty() || name.trim().isEmpty()) {
           mView.showAlert(Util.getStringFromResouce(R.string.please_enter_full_information));
        } else if (item_id.equals("1003") || item_id.equals("1001") || item_id.equals("10004") || item_id.equals("1005")) {
            mView.showAlert(App.Companion.self().getString(R.string.code) + " " + item_id + " " + App.Companion.self().getString(R.string.already_exist) + "!");
        } else {
            new insertToDbTask(item_id, nhom_hang, name, price, color, loai).execute();
        }

    }

    @Override
    public void updateItem(String old_id, Items item) {
        new updateItemTask(old_id, item).execute();
    }

    private class insertToDbTask extends AsyncTask<Void, Void, Integer> {
        String item_id;
        int nhom_hang;
        String color;
        String name;
        double price;
        String bo_phan_phuc_vu;

        public insertToDbTask(String item_id, int nhom_hang, String name, double price, String color, String bo_phan_phuc_vu) {
            this.item_id = item_id;
            this.nhom_hang = nhom_hang;
            this.name = name;
            this.color = color;
            this.price = price;
            this.bo_phan_phuc_vu = bo_phan_phuc_vu;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            Items item = new Items();
            item.setId(item_id);
            item.setNhomhang(nhom_hang);
            item.setTenhang(name);
            item.setBonname(name);
            item.setGiale1(price);
            item.setGialemangve1(price);
            item.setColor(color);
            item.setBophanphucvu(bo_phan_phuc_vu);
            item.setLoaiMonAn(loai_mon_an);
            result = MySqlHandleUtils.insertRecordIntoItem(item);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                mView.showAlert(App.Companion.self().getString(R.string.add_successful));
            } else if (result == 0) {
                mView.showAlert(App.Companion.self().getString(R.string.add_failed));
            } else if (result == 2) {
                mView.showAlert(App.Companion.self().getString(R.string.duplicate_code));
            }
        }
    }

    private class updateItemTask extends AsyncTask<Void, Void, Integer> {
        String old_id;
        Items item;

        public updateItemTask(String old_id, Items item) {
            this.old_id = old_id;
            this.item = item;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result = MySqlHandleUtils.updateRecordIntoItem(old_id, item);

            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                mView.showAlert(App.Companion.self().getString(R.string.update_successful));
                mView.goBack();
            } else if(result == 2) {
                mView.showAlert(App.Companion.self().getString(R.string.the_code_of_item_is_duplicate_p_c_again));
                item.setId(old_id);
            } else {
                mView.showAlert(App.Companion.self().getString(R.string.update_failed));
            }
        }
    }
}
