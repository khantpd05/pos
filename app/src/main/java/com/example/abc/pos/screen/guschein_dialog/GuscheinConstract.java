package com.example.abc.pos.screen.guschein_dialog;

import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.model.Guschein;

import java.util.List;

public interface GuscheinConstract {
    interface View{
        void showGuscheinList(List<Guschein> guscheinList);
        void getDataInput(String data);
        void showKeyboard(String title,String hint,String key11,SessionApp.KeyboardType keyboardType);
    }

    interface Presenter{
        void getDataInput();
        void showKeyboard();
    }
}
