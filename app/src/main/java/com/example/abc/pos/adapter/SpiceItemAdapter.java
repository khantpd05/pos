package com.example.abc.pos.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.ItemsMontuychon;
import com.example.abc.pos.model.Table;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SpiceItemAdapter extends RecyclerView.Adapter<SpiceItemAdapter.ViewHolder> {
    private List<ItemsMontuychon> mDatas;
    private ItemClickListener mClickListener;
    private Context context;


    public SpiceItemAdapter(Context context, List<ItemsMontuychon> datas, ItemClickListener mClickListener) {
        mDatas = datas;
        this.context = context;
        this.mClickListener = mClickListener;
    }

    public void setNewDatas(List<ItemsMontuychon> datas) {
        mDatas = datas;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spice_item, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        ItemsMontuychon item = mDatas.get(position);
        holder.tv_name.setText(item.getTextdisplay());

        if(item.getColor() != null && !item.getColor().isEmpty()){
            holder.tv_name.setBackgroundColor(Integer.parseInt(item.getColor()));
        }else{
            holder.tv_name.setBackgroundColor(Color.parseColor("#8ad4e7"));
        }

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int devicewidth = displaymetrics.widthPixels / 6;
        holder.ll_wrap.getLayoutParams().width = devicewidth;
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_name;
        LinearLayout ll_wrap;

        ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_spice_name);
            ll_wrap = itemView.findViewById(R.id.ll_wrap);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(getAdapterPosition(), mDatas.get(getAdapterPosition()));

        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }


    public interface ItemClickListener {
        void onItemClick(int position, ItemsMontuychon data);
    }


}
