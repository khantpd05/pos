package com.example.abc.pos.screen.setting_printer;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.abc.pos.databinding.FragmentSettingPrinterBinding;

import com.example.abc.pos.R;
import com.example.abc.pos.screen.setting_item.SettingItemPresenter;
import com.example.abc.pos.utils.Util;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingPrinterFragment extends Fragment implements SettingPrinterContract.View{

    SettingPrinterPresenter presenter;

    public SettingPrinterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentSettingPrinterBinding fragmentSettingPrinterBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_setting_printer,container,false);

        presenter = new SettingPrinterPresenter(this);

        fragmentSettingPrinterBinding.setPresenter(presenter);



        return fragmentSettingPrinterBinding.getRoot();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.updateNewConfig();
    }

    @Override
    public void exit() {
        Util.popFragment(getActivity());
    }
}
