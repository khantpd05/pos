package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.model.Items;

import java.util.List;

public class DrawableTableAdapter extends RecyclerView.Adapter<DrawableTableAdapter.ViewHolder> {
    private ItemClickListener mClickListener;
    private Context mContext;
    int img[];
    String title[];

    public DrawableTableAdapter(Context context, int img[], String title[], ItemClickListener clickListener){
        mContext = context;
        this.img = img;
        this.title = title;
        mClickListener = clickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_drawable_table,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_title.setText(title[position]);
        holder.img_ic.setImageResource(img[position]);

    }

    @Override
    public int getItemCount() {
        return img.length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_title;
        ImageView img_ic;

        ViewHolder(View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            img_ic = itemView.findViewById(R.id.img_ic);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(getAdapterPosition());
        }
    }

    public interface ItemClickListener {
        void onItemClick(int position);
    }
}
