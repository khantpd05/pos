package com.example.abc.pos.screen.combine_table;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.TableCombinesAdapter;
import com.example.abc.pos.adapter.TablesAdapter;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.screen.change_table.ChangeTablePresenter;
import com.example.abc.pos.screen.change_to_table.ChangeToTableFragment;
import com.example.abc.pos.screen.combine_to_table.CombineToTableFragment;
import com.example.abc.pos.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CombineTableFragment extends BaseFragment implements CombineTableContract.View,View.OnClickListener, TableCombinesAdapter.ItemClickListener {

    private RecyclerView rc_table;

    private TableCombinesAdapter table_adapter;

    private CombineTablePresenter presenter;

    private LinearLayout btn_change_table;

    private Table table;

    private List<Integer> positionSelectedList;

    public CombineTableFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_combine_table;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_combine_table, container, false);

        rc_table = view.findViewById(R.id.rc_table);

        rc_table.setLayoutManager(new GridLayoutManager(getContext(),4));
        table_adapter = new TableCombinesAdapter(getContext(),new ArrayList<Table>(),this);
        rc_table.setAdapter(table_adapter);

        btn_change_table = view.findViewById(R.id.btn_change_table);
        btn_change_table.setOnClickListener(this);

        LinearLayout btn_exit = view.findViewById(R.id.btn_exit);
        btn_exit.setOnClickListener(this);

        presenter = new CombineTablePresenter(this);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                presenter.getUsingTable();
            }
        }, 500);



        return view;
    }


    @Override
    public void showListTable(List<Table> tables) {
        table_adapter.setNewDatas(tables);
    }

    @Override
    public void goToCombineToTable(List<Table> tableList) {
        ((BaseActivity) getActivity()).pushFragmentNotReplace(new CombineToTableFragment(tableList), true);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_change_table:
                presenter.goToCombineToTable(positionSelectedList);
            break;
            case R.id.btn_exit:
                Util.popFragment(getActivity());
                break;
        }
    }



    @Override
    public void onItemClickGroupListener(List<Integer> positions) {
        if(positions.size()>1) btn_change_table.setVisibility(View.VISIBLE);
        else btn_change_table.setVisibility(View.GONE);
        positionSelectedList = positions;
        if(positions!=null && !positions.isEmpty()){

        }
    }
}
