
package com.example.abc.pos.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.example.abc.pos.R;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;


public abstract class BaseFragment extends Fragment {

    public boolean shouldPerformExitTransition = true;
    protected boolean blockBackButtonPressed = false;

    protected abstract int getFragmentLayoutId();

    protected int getBackgroundImage() {
        return 0;
    }

    private ProgressDialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(getFragmentLayoutId(), container, false);

        return view;
    }


    protected boolean shouldPerformExitTransition() {
        return shouldPerformExitTransition;
    }

    @Override
    public final Animation onCreateAnimation(int transit, final boolean enter,
                                             int nextAnim) {
        if (nextAnim != 0) {
            if (!enter && !shouldPerformExitTransition()) {
                return null;
            }

            Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);

            anim.setAnimationListener(new Animation.AnimationListener() {

                public void onAnimationStart(Animation animation) {
                    blockBackButtonPressed = true;
                }

                public void onAnimationEnd(Animation animation) {
                    if (enter) {
                        blockBackButtonPressed = false;
                        onFragmentTransitionEnd();
                    }
                }

                public void onAnimationRepeat(Animation animation) {
                }

            });

            return anim;
        }

        if (enter) {
            blockBackButtonPressed = false;
            onFragmentTransitionEnd();
        }
        return null;
    }



    protected boolean checkFragmentAvailable(Fragment fragment) {
        return fragment != null && !fragment.isDetached() && fragment.getView() != null;
    }


    protected void onFragmentTransitionEnd() {
    }

    public void showMessage(int msgId, boolean success) {
        //AlertUtil.showTopSnackbar(getView(), msgId, success);
    }

    public void showMessage(String message, boolean success) {
       // AlertUtil.showTopSnackbar(getView(), message, success);
        KToast.show((Activity) getContext(), message, Toast.LENGTH_LONG);
    }


    public void showLoading() {
        if(dialog==null){
            dialog = new ProgressDialog(getContext());
        }else{
            dialog.setMessage( Util.getStringFromResouce(R.string.please_wait_for_a_while));
            dialog.show();
        }


    }

    public void dismissLoading() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
