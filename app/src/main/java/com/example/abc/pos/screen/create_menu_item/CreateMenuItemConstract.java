package com.example.abc.pos.screen.create_menu_item;

import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;

import java.util.List;

public interface CreateMenuItemConstract {
    interface View{
        void showSpinnerData(List<String> datas);
        void showLoading();
        void dismissLoading();
        void showAlert(String data);
        void initUpdateField();
        void goBack();
    }
    interface Presenter{
        void loadData();
        void addNewMenu(String name, int thutu, String color, String loai);
        void updateMenu(Menu menu);
    }
}
