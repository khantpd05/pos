package com.example.abc.pos.screen.found_items;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.FoundItemsAdapter;
import com.example.abc.pos.adapter.KichCoMonAdapter;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.base.WapriManager.KichCoMon;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.KichCoMonAn;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoundItemsDialogFragment extends DialogFragment implements FoundItemsAdapter.ItemClickListener {

    RecyclerView rc_foundItems;
    EditText edt_search;
    FoundItemsAdapter adapter;
    OnItemFoundSelected listener;
    List<Items> datas;
    int soLuong;

    public interface OnItemFoundSelected{
        void onItemFoundSelectedListener(Items item, int soLuong);
    }
    public FoundItemsDialogFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FoundItemsDialogFragment(List<Items> datas,int soLuong,OnItemFoundSelected listener) {
        this.datas = datas;
        this.soLuong = soLuong;
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment.

        return inflater.inflate(R.layout.fragment_found_items_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rc_foundItems = view.findViewById(R.id.rc_foundItems);


        adapter = new FoundItemsAdapter(getContext(), datas,this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rc_foundItems.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                linearLayoutManager.getOrientation());
        rc_foundItems.addItemDecoration(dividerItemDecoration);
        rc_foundItems.setAdapter(adapter);

        edt_search = view.findViewById(R.id.edt_search);
        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });
    }



    @Override
    public void onResume() {
        super.onResume();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        getDialog().getWindow().setLayout((width * 1),RelativeLayout.LayoutParams.WRAP_CONTENT);

    }

    @Override
    public void onItemClick(int position, Items data)  {
        listener.onItemFoundSelectedListener(data,soLuong);
        getDialog().dismiss();
    }

    private void filter(String text) {
        ArrayList<Items> filteredList = new ArrayList<>();

        for (Items item : datas) {
            if (item.getTenhang().toLowerCase().contains(text.toLowerCase()) || item.getId().toLowerCase().contains(text.toLowerCase()) ) {
                filteredList.add(item);
            }
        }

        adapter.filterList(filteredList);
    }
}
