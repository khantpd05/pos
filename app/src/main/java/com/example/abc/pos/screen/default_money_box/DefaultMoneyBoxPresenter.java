package com.example.abc.pos.screen.default_money_box;

import android.databinding.ObservableField;
import android.os.AsyncTask;

import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.UsersTienbandauTemplate;
import com.example.abc.pos.utils.SharedPrefs;

import java.util.List;

public class DefaultMoneyBoxPresenter implements DefaultMoneyBoxContact.Presenter{

    private DefaultMoneyBoxContact.View mView;
    private List<UsersTienbandauTemplate> usersTienbandauTemplate;
    public ObservableField<String> value_box1 = new ObservableField<>(String.format("%.2f",0.0));
    public ObservableField<String> value_box2 = new ObservableField<>(String.format("%.2f",0.0));
    public ObservableField<String> value_box3 = new ObservableField<>(String.format("%.2f",0.0));
    public ObservableField<String> value_box4 = new ObservableField<>(String.format("%.2f",0.0));
    public ObservableField<Boolean> isSetting = new ObservableField<>(SessionApp.isSetting);
    public DefaultMoneyBoxPresenter(DefaultMoneyBoxContact.View mView) {
        this.mView = mView;
        new SelectUserTienBanDauToDbTask().execute();
    }

    private void initValue(List<UsersTienbandauTemplate> usersTienbandauTemplate){
        this.usersTienbandauTemplate = usersTienbandauTemplate;
        for(UsersTienbandauTemplate ob : usersTienbandauTemplate){
            if(ob.getOffset() == 1) value_box1.set(String.format("%.2f",ob.getTotalmoney()));
            else if(ob.getOffset() == 2) value_box2.set(String.format("%.2f",ob.getTotalmoney()));
            else if(ob.getOffset() == 3) value_box3.set(String.format("%.2f",ob.getTotalmoney()));
            else if(ob.getOffset() == 4) value_box4.set(String.format("%.2f",ob.getTotalmoney()));
        }
    }

    @Override
    public void goBack() {
        mView.goBack();
    }

    @Override
    public void boxClick(int box_position) {
        SharedPrefs.getInstance().put(SharedPrefs.MONEY_BOX,box_position);
        UsersTienbandauTemplate tienbandauTemplate = new UsersTienbandauTemplate();
        if(usersTienbandauTemplate!=null && usersTienbandauTemplate.size()>0){
            for(UsersTienbandauTemplate ob : usersTienbandauTemplate){
                if(ob.getOffset() == box_position){
                    tienbandauTemplate = ob;
                    break;
                }
            }
        }

        tienbandauTemplate.setOffset((byte)box_position);
        mView.boxClick(tienbandauTemplate);
    }

    private class SelectUserTienBanDauToDbTask extends AsyncTask<Void, Void, List<UsersTienbandauTemplate>> {
        @Override
        protected List<UsersTienbandauTemplate> doInBackground(Void... voids) {
            List<UsersTienbandauTemplate> datas;

            datas = MySqlHandleUtils.select_users_tienbandau_template();
            return datas;
        }

        @Override
        protected void onPostExecute(List<UsersTienbandauTemplate> result) {
            super.onPostExecute(result);
            if(result.size()>0){
                initValue(result);
            }
        }
    }
}
