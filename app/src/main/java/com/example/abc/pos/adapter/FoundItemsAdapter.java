package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.KichCoMonAn;

import java.util.ArrayList;
import java.util.List;

public class FoundItemsAdapter extends RecyclerView.Adapter<FoundItemsAdapter.ViewHolder> {
    private List<Items> list;
    private ItemClickListener mClickListener;
    private Context context;
    private int selectedPosition = -1;

    public FoundItemsAdapter(Context context, List<Items> list, ItemClickListener clickListener) {
        this.list = list;
        this.context = context;
        mClickListener = clickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_found_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tv_id.setText(list.get(position).getId()+" -");
        holder.tv_name.setText(list.get(position).getTenhang());
        if(list.get(position).getColor() != null){
            holder.tv_color.setBackgroundColor(Integer.parseInt(list.get(position).getColor()));
        }else{
            holder.tv_color.setBackgroundColor(Color.RED);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tv_name,tv_color,tv_id;
        ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_color = itemView.findViewById(R.id.tv_color);
            tv_id = itemView.findViewById(R.id.tv_id);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (position == RecyclerView.NO_POSITION) {
                return;
            }


            if (mClickListener != null) {
                mClickListener.onItemClick(position, list.get(position));
            }
        }

    }

    public interface ItemClickListener {
        void onItemClick(int position, Items data);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void filterList(ArrayList<Items> filteredList) {
        list = filteredList;
        notifyDataSetChanged();
    }
}
