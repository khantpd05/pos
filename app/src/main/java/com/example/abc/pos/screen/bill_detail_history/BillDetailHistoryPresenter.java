package com.example.abc.pos.screen.bill_detail_history;

import android.os.AsyncTask;

import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Orders;
import com.example.abc.pos.model.OrdersDetail;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class BillDetailHistoryPresenter implements BillDetailHistoryContract.Presenter {
    private BillDetailHistoryFragment mView;
    private List<OrdersDetail> orderDetailsList;
    private Orders order;
    public BillDetailHistoryPresenter(BillDetailHistoryFragment mView,Orders order) {
        this.mView = mView;
        this.order = order;
    }



    @Override
    public void loadBillHistoryDetailList(String orderId) {
            new SelectOrdersDetailTask(orderId).execute();
    }

    @Override
    public void showBillInfo() {
        mView.showBillInfo(order.getBanso(),order.getId(),Util.getHour(order.getThoigian()),String.format("%.2f",order.getGiatien()));
    }


    private class SelectOrdersDetailTask extends AsyncTask<Void,Void,List<OrdersDetail>> {

        String orderId;

        public SelectOrdersDetailTask(String orderId) {
            this.orderId = orderId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<OrdersDetail> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.orders_detail_"+Util.currrentYear()+" where orders_id = '"+ orderId+"'");
            ArrayList<OrdersDetail> ordersDetails =  new Gson().fromJson(js.toString(), new TypeToken<List<OrdersDetail>>(){}.getType());

            if(ordersDetails.size()>0){
                for (OrdersDetail ordersDetail : ordersDetails) {
                    if(!ordersDetail.getitems_id().equals("10004")){
                        ordersDetail.initExtraItemList();
                    }
//                new UpdateOrderToDbTask(ordersTemp).execute();
                }
            }
            return ordersDetails;
        }

        @Override
        protected void onPostExecute(List<OrdersDetail> billDetails) {
            super.onPostExecute(billDetails);

            mView.dismissLoading();
            mView.showBillDetailHistory(billDetails);
            orderDetailsList = billDetails;
        }
    }
}
