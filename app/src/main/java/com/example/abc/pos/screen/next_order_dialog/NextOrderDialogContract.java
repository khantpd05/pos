package com.example.abc.pos.screen.next_order_dialog;

import com.example.abc.pos.model.Guschein;
import com.example.abc.pos.model.Tieudedotdat;

import java.util.List;

public interface NextOrderDialogContract {
    interface View{
        void showTieuDeDotDat(List<Tieudedotdat> tieudedotdats);
        void getDataInput(String data);
        void enterPress(String data);
        void showToast(String data);
    }

    interface Presenter{
        void getDataInput(Tieudedotdat tieudedotdat);
        void enterPress();
        void clearInput();
    }
}
