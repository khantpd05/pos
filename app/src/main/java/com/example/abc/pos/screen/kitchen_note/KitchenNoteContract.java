package com.example.abc.pos.screen.kitchen_note;

import com.example.abc.pos.model.ItemsMontuychon;
import com.example.abc.pos.model.KitchenNote;
import com.example.abc.pos.model.Spice;

import java.util.List;

public interface KitchenNoteContract {
    interface View{
        void showAlert(String data);
        void showListKitchenNote(List<KitchenNote> datas);
        void showListKitchenNoteItem(List<ItemsMontuychon> datas);
        void getKitchenNoteList(List<KitchenNote> datas);
        void setEditTextEmpty();
        void showLoading();
        void dismissLoading();
        void doClickItem(int pos);
    }

    interface Presenter{
        void addNewItem(String data);
        void loadData(List<KitchenNote> datas);
        void getAllPageSpiceItem(String id);
        void getKitchenNoteList();
        void deletePriceItem(int positionSelected);
    }
}
