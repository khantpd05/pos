package com.example.abc.pos.screen.setting_menu;


import android.content.DialogInterface;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.adapter.MenuEditAdapter;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.screen.create_menu_item.CreateMenuItemFragment;
import com.example.abc.pos.screen.setting_item.SettingItemFragment;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingMenuFragment extends BaseFragment implements SettingMenuConstract.View,
        MenuEditAdapter.ItemClickListener, View.OnClickListener {

    private MenuEditAdapter adapter;
    private RecyclerView rc_menu;
    private SettingMenuPresenter presenter;
    private LinearLayout ll_extra_feature, ll_exit;
    private TextView btn_edit, btn_detail, btn_remove;
    private Button btn_add;
    private String menu_item;
    private Menu menu;
    private List<Menu> menus;
    private int positionClicked;
    private String loai_mon_an;
    public SettingMenuFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_setting_menu;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting_menu, container, false);
        rc_menu = view.findViewById(R.id.rc_menu);


        rc_menu.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MenuEditAdapter(getContext(), new ArrayList<Menu>(), this);
        rc_menu.setAdapter(adapter);

        ll_extra_feature = view.findViewById(R.id.ll_extra_feature);
        ll_exit = view.findViewById(R.id.ll_exit);

        btn_edit = view.findViewById(R.id.btn_edit);
        btn_detail = view.findViewById(R.id.btn_detail);
        btn_add = view.findViewById(R.id.btn_add);
        btn_remove = view.findViewById(R.id.btn_remove);

        btn_edit.setOnClickListener(this);
        btn_detail.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        btn_remove.setOnClickListener(this);
        ll_exit.setOnClickListener(this);


        presenter = new SettingMenuPresenter(this);

        return view;
    }


    @Override
    public void showListMenu(List<Menu> menus) {
        this.menus = menus;
        Collections.sort(menus);
        adapter.setNewDatas(menus);
    }

    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }

    @Override
    public void onItemClick(int position, Menu menu) {
        ll_extra_feature.setVisibility(View.VISIBLE);
        positionClicked = position;
        menu_item = String.valueOf(menu.getId());
        this.menu = menu;
        loai_mon_an = menu.getLoaiMonAn();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_detail:
                Fragment fragment = SettingItemFragment.newInstance(menu_item,loai_mon_an);
                ((BaseActivity) getActivity()).pushFragment(fragment, true);
                break;

            case R.id.btn_edit:
                Fragment fm = CreateMenuItemFragment.newInstance(menu);
                ((BaseActivity) getActivity()).pushFragment(fm, true);
                break;

            case R.id.btn_add:
                ((BaseActivity) getActivity()).pushFragment(new CreateMenuItemFragment(), true);
                break;

            case R.id.btn_remove: {
                Util.showCustomDialogConfirm(getActivity(), Util.getStringFromResouce(R.string.are_u_w_to_delete), new Util.ConfirmDialogInterface() {
                    @Override
                    public void onPositiveClickListener() {
                        presenter.deleteRow(menu.getId());
                        menus.remove(positionClicked);
                        adapter.setNewDatas(menus);
                    }
                    @Override
                    public void onNegativeClickListener() {

                    }
                });

                break;
            }

            case R.id.ll_exit:
                Util.popFragment(getActivity());
                break;
        }
    }
}
