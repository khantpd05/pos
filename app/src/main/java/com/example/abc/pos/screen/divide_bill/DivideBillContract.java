package com.example.abc.pos.screen.divide_bill;

import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.model.OrdersTemp;

import java.util.List;

public interface DivideBillContract {
    interface View {
        void loadOrigOrderList(List<OrdersTemp> orderList);

        void loadDivideOrderList(List<OrdersTemp> orderList);

        void goToPayment(List<OrdersTemp> origOrderList, List<OrdersTemp> divideOrderList);

        void showKeyboard(String title, String hint, String key11, SessionApp.KeyboardType keyboardType);
    }

    interface Presenter {
        void loadData(List<OrdersTemp> orderList, double tienGiamBan);

        void addItemToDivideOrderList(OrdersTemp ordersTemp, int position);

        void addItemToOrigOrderList(OrdersTemp ordersTemp, int position);

        double getSummerPrice();

        void goToPayment();

        void showKeyboard(int soluong);

        void addItemToDivideOrderListByInput(String data);

        List<OrdersTemp> getDivideOrdertempList();

        void setDivideOrdertempList( List<OrdersTemp> ordersTempDivideList);
    }
}
