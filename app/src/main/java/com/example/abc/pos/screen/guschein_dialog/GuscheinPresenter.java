package com.example.abc.pos.screen.guschein_dialog;

import android.os.AsyncTask;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.model.Guschein;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class GuscheinPresenter implements GuscheinConstract.Presenter {

    private GuscheinConstract.View mView;
    private List<Guschein> guscheinList;

    public GuscheinPresenter(GuscheinConstract.View mView) {
        this.mView = mView;
        new SelectTask().execute();
    }

    @Override
    public void getDataInput() {

    }

    @Override
    public void showKeyboard() {
        mView.showKeyboard("Gutschein","0.00","00",SessionApp.KeyboardType.TIEN);
    }


    private class SelectTask extends AsyncTask<Void,Void,List<Configuration>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Configuration> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.configuration where keyid like '%GouponValue%'");
            ArrayList<Configuration> config =  new Gson().fromJson(js.toString(), new TypeToken<List<Configuration>>(){}.getType());
            return config;
        }

        @Override
        protected void onPostExecute(List<Configuration> config) {
            super.onPostExecute(config);
            guscheinList = new ArrayList<>();
            guscheinList.add(0,new Guschein(Util.getStringFromResouce(R.string.Free_price)));
            for(Configuration ob: config){
                if(ob.getKeyid().equals("GouponValue1")){
                    guscheinList.add(new Guschein(ob.getValue()));
                } else if(ob.getKeyid().equals("GouponValue2")){
                    guscheinList.add(new Guschein(ob.getValue()));
                } else if(ob.getKeyid().equals("GouponValue3")){
                    guscheinList.add(new Guschein(ob.getValue()));
                } else if(ob.getKeyid().equals("GouponValue4")){
                    guscheinList.add(new Guschein(ob.getValue()));
                } else if(ob.getKeyid().equals("GouponValue5")){
                    guscheinList.add(new Guschein(ob.getValue()));
                }

            }
            mView.showGuscheinList(guscheinList);
        }
    }
}
