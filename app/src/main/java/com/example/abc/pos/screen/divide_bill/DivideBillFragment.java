package com.example.abc.pos.screen.divide_bill;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.adapter.ExpandListDivideOrderAdapter;
import com.example.abc.pos.adapter.ExpandListOrigOrderPaymentAdapter;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.screen.keyboard.KeyboardFragment;
import com.example.abc.pos.screen.order.OrderFragment;
import com.example.abc.pos.screen.payment.PaymentFragment;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class DivideBillFragment extends Fragment implements DivideBillContract.View, ExpandListDivideOrderAdapter.OnItemClickGroup, ExpandListOrigOrderPaymentAdapter.OnItemClickGroup, View.OnClickListener, KeyboardFragment.OnKeyboardPress, PaymentFragment.OnPaymentFinish,PaymentFragment.OnPaymentCancel {

    private List<OrdersTemp> ordersTempList;
    private List<OrdersTemp> ordersTempListRestore;
    private double tienBanGiam;
    private ExpandableListView orderTempOrigExpandList;
    public static ExpandListOrigOrderPaymentAdapter orderOrigAdapter;
    private ExpandableListView orderTempDivideExpandList;
    public static ExpandListDivideOrderAdapter orderDivideAdapter;

    private DivideBillContract.Presenter presenter;

    private LinearLayout btn_payment;
    private LinearLayout ll_exit;

    private OnPaymentFinishFromDivide listener_finish;

    @Override
    public void onPaymentCancelListener() {
        if(orderDivideAdapter!=null){
            orderDivideAdapter.notifyDataSetChanged();
        }
    }

    public interface OnPaymentFinishFromDivide{
        void onPaymentFinishFromDivideListener();
    }

    public interface OnCancelDivideBill{
        void onCancelDivideBill(List<OrdersTemp> ordersTempList);
    }

    public OnCancelDivideBill listener;

    public DivideBillFragment(){

    }


   public static DivideBillFragment newInstance(List<OrdersTemp> ordersTempList,List<OrdersTemp> ordersTempListRestore,double tienBanGiam) {

        Bundle args = new Bundle();

       Type listType = new TypeToken<List<OrdersTemp>>() {
       }.getType();
       Gson gson = new Gson();

       String ordertempListJson = gson.toJson(ordersTempList, listType);
       String ordersTempListRestoreJson = gson.toJson(ordersTempListRestore, listType);
       args.putString("ordertempList", ordertempListJson);
       args.putString("ordersTempListRestore", ordersTempListRestoreJson);
       args.putDouble("tienBanGiam", tienBanGiam);

        DivideBillFragment fragment = new DivideBillFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_divide, container, false);

        if(getArguments()!=null){
            Gson gson = new Gson();
            ordersTempList = gson.fromJson(getArguments().getString("ordertempList"), new TypeToken<List<OrdersTemp>>() {
            }.getType());

            ordersTempListRestore = gson.fromJson(getArguments().getString("ordersTempListRestore"), new TypeToken<List<OrdersTemp>>() {
            }.getType());

            tienBanGiam = getArguments().getDouble("tienBanGiam");
        }


        orderTempOrigExpandList = view.findViewById(R.id.rc_orgin);
        orderOrigAdapter = new ExpandListOrigOrderPaymentAdapter(getContext(), new ArrayList<OrdersTemp>(), this);
        orderTempOrigExpandList.setAdapter(orderOrigAdapter);

        orderTempDivideExpandList = view.findViewById(R.id.rc_divide);
        orderDivideAdapter = new ExpandListDivideOrderAdapter(getContext(), new ArrayList<OrdersTemp>(), this);
        orderTempDivideExpandList.setAdapter(orderDivideAdapter);

        btn_payment = view.findViewById(R.id.btn_payment);
        btn_payment.setOnClickListener(this);
        ll_exit = view.findViewById(R.id.ll_exit);
        ll_exit.setOnClickListener(this);

        presenter = new DivideBillPresenter(this);
        presenter.loadData(ordersTempList,tienBanGiam);

        this.listener = OrderFragment.getOrderFragmentListener();
        this.listener_finish = OrderFragment.getOrderFragmentListener();

        return view;
    }

    @Override
    public void loadOrigOrderList(List<OrdersTemp> orderList) {
        orderOrigAdapter.setNewDatas(orderList);
    }

    @Override
    public void loadDivideOrderList(List<OrdersTemp> orderList) {
        orderDivideAdapter.setNewDatas(orderList);
    }

    @Override
    public void goToPayment(List<OrdersTemp> origOrderList,List<OrdersTemp> divideOrderList) {
        if(divideOrderList!=null && divideOrderList.size()>0){
            ((BaseActivity) getActivity()).pushFragmentNotReplace(new PaymentFragment(divideOrderList,ordersTempList, presenter.getSummerPrice(), SessionApp.TABLE_NAME,this,this), true);
        }else{
            Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.no_orders_yet));
        }
    }

    @Override
    public void showKeyboard(String title, String hint, String key11, SessionApp.KeyboardType keyboardType) {
        Fragment fragment = new KeyboardFragment(this, title, hint, key11, keyboardType);
        ((KeyboardFragment) fragment).show(getFragmentManager(), null);
    }

    @Override
    public void onDivideItemClickGroupListener(OrdersTemp item, int position) {
        presenter.addItemToOrigOrderList(item,position);
    }

    @Override
    public void onOrigItemClickGroupListener(OrdersTemp item, int position) {
        presenter.addItemToDivideOrderList(item,position);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_payment:
                presenter.goToPayment();
                break;

            case R.id.ll_exit:
                listener.onCancelDivideBill(ordersTempListRestore);
                Util.popFragment(getActivity());


                break;
        }
    }


    @Override
    public void onKeyPressListener(String data) {
         presenter.addItemToDivideOrderListByInput(data);
    }

    @Override
    public void onPaymentFinishListener(List<OrdersTemp> origOrderTemp_list) {
        /* khi thanh toán thành công thì sẽ update lại restore list nếu không sẽ dính với orig list */
        ordersTempListRestore.clear();
        ordersTempListRestore = new ArrayList<>();
        for (OrdersTemp ob : origOrderTemp_list) {
            OrdersTemp object = new OrdersTemp();
            try {
                object = (OrdersTemp) ob.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            ordersTempListRestore.add(object);
        }
//                for (OrdersTemp ordersTemp : origOrderTemp_list) {
//                   Log.d("/////",ordersTemp.getBonname()+" "+ ordersTemp.getSoluong());
//                }
        loadOrigOrderList(origOrderTemp_list);
        presenter.setDivideOrdertempList(new ArrayList<OrdersTemp>());
        loadDivideOrderList(presenter.getDivideOrdertempList());
        listener_finish.onPaymentFinishFromDivideListener();
    }
}
