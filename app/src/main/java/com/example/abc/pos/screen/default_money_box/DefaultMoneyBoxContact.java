package com.example.abc.pos.screen.default_money_box;

import com.example.abc.pos.model.UsersTienbandauTemplate;

public interface DefaultMoneyBoxContact {
    interface View {
        void goBack();
        void boxClick(UsersTienbandauTemplate usersTienbandauTemplate);
    }

    interface Presenter {
        void goBack();
        void boxClick(int box_position);
    }
}
