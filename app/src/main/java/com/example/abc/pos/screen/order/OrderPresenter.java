package com.example.abc.pos.screen.order;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.util.Log;
import android.util.Printer;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.ExtraItem;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.ItemsPopup;
import com.example.abc.pos.model.KitchenNote;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Spice;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.utils.PrinterUtils;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class OrderPresenter implements OrderConstract.Presenter {

    private OrderConstract.View mView;
    private List<OrdersTemp> ordersTempList = new ArrayList<>();
    private List<OrdersTemp> ordersTempListAll = new ArrayList<>();
    private List<OrdersTemp> ordersTempsWillDelete  = new ArrayList<>();
    private List<OrdersTemp> ordersTempsWillUpdateCancelReason = new ArrayList<>();
    private List<OrdersTemp> ordersTempsRecoveryList;
    private String currentFuture = "";
    private int currentPostion = 0;
    private Table table;

    private class dotDat {
        int position;
    }

    public OrderPresenter(OrderConstract.View view) {
        mView = view;

    }


    @Override
    public void loadData(Table table) {
        this.table = table;
        SessionApp.TABLE_NAME = table.getName();
        getNewOrderTempList(0);

        SessionApp.CUSTOMER_POSITION = currentPostion;
        mView.setHienThiBanGiam();
        ordersTempsWillDelete = new ArrayList<>();
    }

    @Override
    public void getNewOrderTempList(int positionCustomer) {
        new GetOrderTempListByCustomerPositionTask(positionCustomer).execute();
    }

    @Override
    public void getOffOrderTempList(int positionCustomer) {
        currentPostion = positionCustomer;
        List<OrdersTemp> ordersTemps = new ArrayList<>();
        if (positionCustomer != 0) {
            for (OrdersTemp ob : ordersTempListAll) {
                if (ob.getThutukhachtrenban() == positionCustomer) {
                    ordersTemps.add(ob);
                }
            }
            ordersTempList = ordersTemps;
        } else {
            ordersTempList = ordersTempListAll;
        }

        mView.showOrderTempList(ordersTempList);
    }

    @Override
    public List<OrdersTemp> getOrderTempList() {
        return ordersTempListAll;
    }

    @Override
    public List<OrdersTemp> getOrderTempRecoveryList() {
        return ordersTempsRecoveryList;
    }

    @Override
    public List<OrdersTemp> getOrderTempListByPosition() {
        return ordersTempList;
    }

    @Override
    public void setTable(Table table) {
        this.table = table;
    }

    @Override
    public void setOrderTempRecoveryList(List<OrdersTemp> orderTempList) {
        ordersTempsRecoveryList = new ArrayList<>();
        for (OrdersTemp ob : orderTempList) {
            OrdersTemp object = new OrdersTemp();
            try {
                object = (OrdersTemp) ob.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            ordersTempsRecoveryList.add(object);
        }

    }

    @Override
    public void setOrderTempList(List<OrdersTemp> orderTempList) {
        ordersTempListAll = orderTempList;
    }


    @Override
    public void goToCustomerPosition() {
//        new GetAllOrderTempListTask().execute();
        mView.goToCustomerPosition(ordersTempListAll);
    }


    @Override
    public void addNewOrder(Items item, int soLuong) {
        Random ran = new Random();
        Date date = new Date();
        String idGenera = WapriManager.DEVICE_NAME + "x" + date.getTime() + "" + ran.nextInt(10000);
        OrdersTemp ordersTemp = new OrdersTemp();
        ordersTemp.setId(idGenera);
        ordersTemp.setBanso(table.getName());
        ordersTemp.setitems_id(item.getId());
        ordersTemp.setNgaymua(Util.getDate("d"));
        ordersTemp.setTenhang(item.getTenhang());
        if (item.getBonname() != null && !item.getBonname().isEmpty())
            ordersTemp.setBonname(item.getBonname());
        else
            ordersTemp.setBonname(item.getTenhang());
        if (soLuong < 1) {
            ordersTemp.setSoluong(1);
        } else {
            ordersTemp.setSoluong(soLuong);
        }

        ordersTemp.setUsername(WapriManager.getUser().getUsername());
        ordersTemp.setThoigian(Util.getDate("dt"));
        ordersTemp.setBophanphucvu(item.getBophanphucvu());
        ordersTemp.setThutukhachtrenban(getViTriNgoi());
        ordersTemp.setKichcomonan(item.getKichcomonan());

        if (table.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho) {
            ordersTemp.setTaichohaymangve(WapriManager.Taichohaymangve_.TaiCho);
            ordersTemp.setorig_price_from_item(item.getGiale());
            ordersTemp.setorig_price_no_extra(item.getGiale());
            ordersTemp.setGiatien(tinhGiaTien(ordersTemp));
            if (SessionApp.isHappyHour && SessionApp.isAplyAllHappyHour && ordersTemp.getorig_price_no_extra() >= 0 && !ordersTemp.getitems_id().equals("1001"))
                ordersTemp.setPhantramgiam(SessionApp.happyHour.getPhantramgiam());
            else if (SessionApp.isHappyHour && ordersTemp.getorig_price_no_extra() >= 0)
                ordersTemp.setPhantramgiam(item.getPhanTramGiam());
            ordersTemp.setThanhtien(tinhThanhTien(ordersTemp));
        } else {
            ordersTemp.setTaichohaymangve(WapriManager.Taichohaymangve_.MangVe);
            ordersTemp.setorig_price_from_item(item.getGialemangve());
            ordersTemp.setorig_price_no_extra(item.getGialemangve());
            ordersTemp.setGiatien(tinhGiaTien(ordersTemp));
            if (SessionApp.isHappyHour && SessionApp.isAplyAllHappyHour)
                ordersTemp.setPhantramgiam(SessionApp.happyHour.getPhantramgiam());
            else if (SessionApp.isHappyHour)
                ordersTemp.setPhantramgiam(item.getPhanTramGiam());
            ordersTemp.setThanhtien(tinhThanhTien(ordersTemp));
        }

        ordersTemp.setTiengiam(((tinhTienChuaGiam(ordersTemp) * ordersTemp.getPhantramgiam()) / 100));

        if (table.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho || item.getBophanphucvu().equals("PhaChe19")) {
            ordersTemp.setThue(19);
            ordersTemp.setTienthue(tinhTienThue(ordersTemp));
        } else {
            ordersTemp.setThue(7);
            ordersTemp.setTienthue(tinhTienThue(ordersTemp));
        }

        if (item.getBophanphucvu().equals("Sonstiges0")) {
            ordersTemp.setThue(0);
            ordersTemp.setTienthue(tinhTienThue(ordersTemp));
        }

        ordersTemp.setPhanangomco(item.getPhanangomco());
        ordersTemp.setphanangomco_soluong(item.getphanangomco_soluong());
        ordersTemp.setPhanangomco2(item.getPhanangomco2());
        ordersTemp.setphanangomco2_soluong(item.getPhanangomco2Soluong());

        ordersTemp.setPhanangomco3(item.getPhanangomco3());
        ordersTemp.setphanangomco3_soluong(item.getPhanangomco3Soluong());

        ordersTemp.setPhanangomco4(item.getPhanangomco4());
        ordersTemp.setphanangomco4_soluong(item.getPhanangomco4Soluong());

        ordersTemp.setPhanangomco5(item.getPhanangomco5());
        ordersTemp.setphanangomco5_soluong(item.getPhanangomco5Soluong());

        ordersTemp.setPhanangomco6(item.getPhanangomco6());
        ordersTemp.setphanangomco6_soluong(item.getPhanangomco6Soluong());

        ordersTemp.setPhanangomco7(item.getPhanangomco7());
        ordersTemp.setphanangomco7_soluong(item.getPhanangomco7Soluong());

        ordersTemp.setPhanangomco8(item.getPhanangomco8());
        ordersTemp.setphanangomco8_soluong(item.getPhanangomco8Soluong());

        ordersTemp.setPhanangomco9(item.getPhanangomco9());
        ordersTemp.setphanangomco9_soluong(item.getPhanangomco9Soluong());


//        ordersTemp.setThanhtien((ordersTemp.getorig_price_no_extra() - tinhPhanTram(ordersTemp)));


        ordersTemp.setDaprintxoachua("N");
        ordersTemp.setDaprintchua("N");
        ordersTemp.setInlaimonfromtablet("N");
        ordersTemp.setloai_mon_an(item.getLoaiMonAn());
        ordersTemp.initExtraItemList();
        ordersTempListAll.add(ordersTemp);
        if (currentPostion != 0) {
            ordersTempList.add(ordersTemp);
        } else {
            ordersTempList = ordersTempListAll;
        }
        mView.showOrderTempList(ordersTempList);
        mView.setMarkSignList();
    }

    @Override
    public void addOrder(Items item, int soLuong) {
        if (table.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho) {
            if (item.getGiaBanLeList().size() > 1) {
                mView.showKichCoMonDialog(item, soLuong);
            } else {
                item.setGiale(item.getGiale1());
                doAddOrderTemp(item, soLuong);
            }
        } else {
            if (item.getGiaBanLeMangVeList().size() > 1) {
                mView.showKichCoMonDialog(item, soLuong);
            } else {
                item.setGialemangve(item.getGialemangve1());
                doAddOrderTemp(item, soLuong);
            }
        }
    }


    boolean is_new = true;
    CountDownTimer countDownTimer = new CountDownTimer(3000, 1000) {
        public void onTick(long millisUntilFinished) {

        }

        public void onFinish() {
            is_new = true;
        }
    };

    @Override
    public void doAddOrderTemp(Items item, int soLuong) {


        // Add new Order
        if (ordersTempList != null && ordersTempList.size() > 0 && !ordersTempList.get(ordersTempList.size() - 1).getitems_id().equals(item.getId())) {
            addNewOrder(item, soLuong);
            is_new = false;
            countDownTimer.cancel();
            countDownTimer.start();

        } else {
            if (is_new) {
                addNewOrder(item, soLuong);
                is_new = false;
                countDownTimer.cancel();
                countDownTimer.start();
            } else {

                countDownTimer.cancel();
                countDownTimer.start();

                boolean isEqual = false;
                int equalPosition = -1;

                List<dotDat> dotDatList = new ArrayList<>();
                for (int i = 0; i < ordersTempList.size(); i++) {
                    if (ordersTempList.get(i).getitems_id().equals("1001")) {
                        dotDat ob = new dotDat();
                        ob.position = i;
                        dotDatList.add(ob);
                    }
                }
                if (ordersTempList.size() > 0) {
                    if (ordersTempList.get(ordersTempList.size() - 1).getitems_id().equals("1001")) {
                        addNewOrder(item, soLuong);
                    } else {
                        if (dotDatList.size() > 0) {
                            for (int i = dotDatList.get(dotDatList.size() - 1).position; i < ordersTempList.size(); i++) {
                                if (i > dotDatList.get(dotDatList.size() - 1).position && ordersTempList.get(i).getitems_id().equals(item.getId())) {
                                    isEqual = true;
                                    equalPosition = i;
                                }
                            }
                            if (isEqual) {
                                OrdersTemp ordersTemp = ordersTempList.get(equalPosition);
                                if (ordersTemp.getInlaimonfromtablet().equals("Y") || ordersTemp.getDaprintchua().equals("Y") || !ordersTemp.getKichcomonan().equals(item.getKichcomonan())) {
                                    addNewOrder(item, soLuong);
                                } else {
                                    int soluong = ordersTemp.getSoluong() + soLuong;
                                    if (soLuong < 1) {
                                        ordersTemp.setSoluong(++soluong);
                                        ordersTemp.setThanhtien(tinhThanhTien(ordersTemp));
                                        ordersTemp.setGiatien(tinhGiaTien(ordersTemp));
                                        ordersTemp.setTienthue(tinhTienThue(ordersTemp));
                                        ordersTemp.setTiengiam(((tinhTienChuaGiam(ordersTemp) * ordersTemp.getPhantramgiam()) / 100));
                                    } else {
                                        ordersTemp.setSoluong(soluong);
                                        ordersTemp.setThanhtien(tinhThanhTien(ordersTemp));
                                        ordersTemp.setGiatien(tinhGiaTien(ordersTemp));
                                        ordersTemp.setTienthue(tinhTienThue(ordersTemp));
                                        ordersTemp.setTiengiam(((tinhTienChuaGiam(ordersTemp) * ordersTemp.getPhantramgiam()) / 100));
                                    }
                                }

                                mView.showOrderTempList(ordersTempList);
                            } else {
                                addNewOrder(item, soLuong);
                            }
                        } else {
                            for (int i = 0; i < ordersTempList.size(); i++) {
                                if (ordersTempList.get(i).getitems_id().equals(item.getId()) && ordersTempList.get(i).getKichcomonan().equals(item.getKichcomonan())) {
                                    isEqual = true;
                                    equalPosition = i;
                                }
                            }

                            if (isEqual) {
                                OrdersTemp ordersTemp = ordersTempList.get(equalPosition);
                                if (ordersTemp.getInlaimonfromtablet().equals("Y") || ordersTemp.getDaprintchua().equals("Y") || !ordersTemp.getKichcomonan().equals(item.getKichcomonan())) {
                                    addNewOrder(item, soLuong);
                                } else {
                                    int soluong = ordersTemp.getSoluong() + soLuong;
                                    if (soLuong < 1) {
                                        ordersTemp.setSoluong(++soluong);
                                        ordersTemp.setThanhtien(tinhThanhTien(ordersTemp));
                                        ordersTemp.setGiatien(tinhGiaTien(ordersTemp));
                                        ordersTemp.setTienthue(tinhTienThue(ordersTemp));
                                        ordersTemp.setTiengiam(((tinhTienChuaGiam(ordersTemp) * ordersTemp.getPhantramgiam()) / 100));
                                    } else {
                                        ordersTemp.setSoluong(soluong);
                                        ordersTemp.setThanhtien(tinhThanhTien(ordersTemp));
                                        ordersTemp.setGiatien(tinhGiaTien(ordersTemp));
                                        ordersTemp.setTienthue(tinhTienThue(ordersTemp));
                                        ordersTemp.setTiengiam(((tinhTienChuaGiam(ordersTemp) * ordersTemp.getPhantramgiam()) / 100));
                                    }
                                }
                                mView.showOrderTempList(ordersTempList);
                            } else {
                                addNewOrder(item, soLuong);
                            }
                        }
                    }
                } else {
                    addNewOrder(item, soLuong);
                }
                mView.showOrderTempList(ordersTempList);
            }
        }

        /**Check whether Item contains items_popup
         *  if contain show popup order to pick item optional
         * **/

        List<ItemsPopup> itemsPopupList = new ArrayList<>();
        boolean isMatch = false;
        for (ItemsPopup items_popup : SessionApp.items_popup) {
            if (items_popup.getItems_id().equals(item.getId())) {
                itemsPopupList.add(items_popup);
                isMatch = true;
            }
        }
        if (isMatch) {
            mView.showItemsPopup(ordersTempList.size() - 1, itemsPopupList);
        }
    }

    @Override
    public void addOrderFromFreeDish(OrdersTemp ordersTemp) {

        if (ordersTemp.getTaichohaymangve() == WapriManager.Taichohaymangve_.TaiCho || ordersTemp.getBophanphucvu().equals("PhaChe19")) {
            ordersTemp.setThue(19);
            ordersTemp.setTienthue(tinhTienThue(ordersTemp));
        } else {
            ordersTemp.setThue(7);
            ordersTemp.setTienthue(tinhTienThue(ordersTemp));
        }

        ordersTempListAll.add(ordersTemp);
        if (currentPostion != 0) {
            ordersTempList.add(ordersTemp);
        } else {
            ordersTempList = ordersTempListAll;
        }
        mView.showOrderTempList(ordersTempList);
    }

    @Override
    public void updateHeaderOrder(String orderId, String name) {
        new UpdateOrderTieuDeToDbTask(orderId, name).execute();
    }


    @Override
    public void updateGoHome(List<Integer> positionsSelected) {
        for (int i = 0; i < positionsSelected.size(); i++) {
            OrdersTemp orderItem = ordersTempList.get(positionsSelected.get(i));
            if (orderItem.getTaichohaymangve() == WapriManager.Taichohaymangve_.TaiCho) {
                orderItem.setTaichohaymangve(WapriManager.Taichohaymangve_.MangVe);
                if (!orderItem.getBophanphucvu().equals("PhaChe19")) {
                    orderItem.setThue(7);
                    orderItem.setTienthue(tinhTienThue(orderItem));
                }
            } else {
                orderItem.setTaichohaymangve(WapriManager.Taichohaymangve_.TaiCho);
                orderItem.setThue(19);
                orderItem.setTienthue(tinhTienThue(orderItem));
            }
            if (orderItem.getBophanphucvu().equals("Sonstiges0")) {
                orderItem.setThue(0);
                orderItem.setTienthue(tinhTienThue(orderItem));
            }
            orderItem.initExtraItemList();
        }
        mView.resetSelectedList();
        mView.showOrderTempList(ordersTempList);
    }

    @Override
    public void updateInBep(List<Integer> positionsSelected) {
        if (positionsSelected != null && positionsSelected.size() > 0) {

            if (WapriManager.configuration().tuDongInBep.get()) {
                List<OrdersTemp> ordersTemps = new ArrayList<>();
                for (int i = 0; i < positionsSelected.size(); i++) {
                    OrdersTemp orderItem = ordersTempList.get(positionsSelected.get(i));
                    currentFuture = "InBep";
                    if (orderItem.getInlaimonfromtablet().equals("N") && orderItem.getDaprintchua().equals("N")) {
                        orderItem.setInsertedToDb(true);
                        ordersTemps.add(orderItem);
                    }
                    orderItem.initExtraItemList();
                }

                final List<OrdersTemp> bepList = new ArrayList<>();
                final List<OrdersTemp> nuocList = new ArrayList<>();
                final List<OrdersTemp> sushiList = new ArrayList<>();

                for (OrdersTemp ordersTemp : ordersTemps) {
                    if (!ordersTemp.getDaprintchua().equals("Y")) {
                        if (ordersTemp.getBophanphucvu().equals("Sushi")) {
                            sushiList.add(ordersTemp);
                        } else if (ordersTemp.getBophanphucvu().contains("PhaChe")) {
                            nuocList.add(ordersTemp);
                        } else {
                            bepList.add(ordersTemp);
                        }
                    }
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (bepList.size() > 0) PrinterUtils.Print(bepList, "Bep");
                        if (nuocList.size() > 0) PrinterUtils.Print(nuocList, "Nuoc");
                        if (sushiList.size() > 0) PrinterUtils.Print(sushiList, "Sushi");
                    }
                }).start();


                new InsertInBepOfflineToDbTask(ordersTemps).execute();
            } else {

                for (int i = 0; i < positionsSelected.size(); i++) {
                    OrdersTemp orderItem = ordersTempList.get(positionsSelected.get(i));
                    currentFuture = "InBep";

                    if (orderItem.getInlaimonfromtablet().equals("N") && orderItem.getDaprintchua().equals("N")) {
                        orderItem.setInlaimonfromtablet("Y");
                        orderItem.setDaprintchua("Y");
                        orderItem.setInsertedToDb(true);
                        new InsertOrderToDbTask(orderItem).execute();
                    }
                    orderItem.initExtraItemList();
                }
            }
            mView.resetSelectedList();
            mView.showOrderTempList(ordersTempList);
        }

    }

    @Override
    public void showChucNangOrder() {
        mView.showChucNangOrder();
    }

    @Override
    public void showKeyboard(String title, String hint, String key11, SessionApp.KeyboardType
            keyboardType) {
        mView.showKeyboard(title, hint, key11, keyboardType);
    }

    @Override
    public void showKeyboardCode() {
        mView.showKeyboardCode("200x2");
    }

    @Override
    public void showNextOrderDialog() {
        mView.showNextOrderDialog();
    }

    @Override
    public void updateFreePrice(int positionSelected, String data) {
        OrdersTemp orderItem = ordersTempList.get(positionSelected);
        orderItem.setorig_price_no_extra(Double.valueOf(data));
        orderItem.setThanhtien(tinhThanhTien(orderItem));
        orderItem.setGiatien(tinhGiaTien(orderItem));
        orderItem.setTienthue(tinhTienThue(orderItem));
        orderItem.setTiengiam((tinhTienChuaGiam(orderItem) * orderItem.getPhantramgiam()) / 100);
        orderItem.initExtraItemList();
        mView.resetSelectedList();
        mView.showOrderTempList(ordersTempList);
    }

    @Override
    public void updateGiamGiaMonAn(List<Integer> positionsSelected, String data) {
        for (int i = 0; i < positionsSelected.size(); i++) {
            OrdersTemp orderItem = ordersTempList.get(positionsSelected.get(i));
            int phantramgiam = Integer.parseInt(data);
            orderItem.setPhantramgiam(phantramgiam);
            orderItem.setTiengiam((tinhTienChuaGiam(orderItem) * orderItem.getPhantramgiam()) / 100);
            orderItem.setTienthue(tinhTienThue(orderItem));
            orderItem.setThanhtien(tinhThanhTien(orderItem));
            orderItem.initExtraItemList();
        }
        mView.resetSelectedList();
        mView.showOrderTempList(ordersTempList);
    }

    @Override
    public void updateGiamGiaTatCa(String data) {

    }

    @Override
    public void updateGiaVi() {

    }

    @Override
    public void goToGiaVi(int position) {
        OrdersTemp ordersTemp = ordersTempList.get(position);
        List<Spice> datas = new ArrayList<>();
        if (ordersTemp.getgiavi_text() != null && !ordersTemp.getgiavi_text().isEmpty()) {
            Spice spice = new Spice(ordersTemp.getgiavi_text(), ordersTemp.getgiavi_price());
            datas.add(spice);
        }

        if (ordersTemp.getgiavi_text_2() != null && !ordersTemp.getgiavi_text_2().isEmpty()) {
            Spice spice = new Spice(ordersTemp.getgiavi_text_2(), ordersTemp.getgiavi_price_2());
            datas.add(spice);
        }

        if (ordersTemp.getgiavi_text_3() != null && !ordersTemp.getgiavi_text_3().isEmpty()) {
            Spice spice = new Spice(ordersTemp.getgiavi_text_3(), ordersTemp.getgiavi_price_3());
            datas.add(spice);
        }

        if (ordersTemp.getgiavi_text_4() != null && !ordersTemp.getgiavi_text_4().isEmpty()) {
            Spice spice = new Spice(ordersTemp.getgiavi_text_4(), ordersTemp.getgiavi_price_4());
            datas.add(spice);
        }

        if (ordersTemp.getgiavi_text_5() != null && !ordersTemp.getgiavi_text_5().isEmpty()) {
            Spice spice = new Spice(ordersTemp.getgiavi_text_5(), ordersTemp.getgiavi_price_5());
            datas.add(spice);
        }

        if (ordersTemp.getgiavi_text_6() != null && !ordersTemp.getgiavi_text_6().isEmpty()) {
            Spice spice = new Spice(ordersTemp.getgiavi_text_6(), ordersTemp.getgiavi_price_6());
            datas.add(spice);
        }

        if (ordersTemp.getgiavi_text_7() != null && !ordersTemp.getgiavi_text_7().isEmpty()) {
            Spice spice = new Spice(ordersTemp.getgiavi_text_7(), ordersTemp.getgiavi_price_7());
            datas.add(spice);
        }

        if (ordersTemp.getgiavi_text_8() != null && !ordersTemp.getgiavi_text_8().isEmpty()) {
            Spice spice = new Spice(ordersTemp.getgiavi_text_8(), ordersTemp.getgiavi_price_8());
            datas.add(spice);
        }

        if (ordersTemp.getgiavi_text_9() != null && !ordersTemp.getgiavi_text_9().isEmpty()) {
            Spice spice = new Spice(ordersTemp.getgiavi_text_9(), ordersTemp.getgiavi_price_9());
            datas.add(spice);
        }

        if (ordersTemp.getgiavi_text_10() != null && !ordersTemp.getgiavi_text_10().isEmpty()) {
            Spice spice = new Spice(ordersTemp.getgiavi_text_10(), ordersTemp.getgiavi_price_10());
            datas.add(spice);
        }
        mView.goToGiaVi(datas);
    }

    @Override
    public void goToKitchenNote(List<Integer> positions) {
        if (positions.size() > 1) {
            List<KitchenNote> datas = new ArrayList<>();
            mView.goToKitchenNote(datas);
        } else {
            OrdersTemp ordersTemp = ordersTempList.get(positions.get(0));
            List<KitchenNote> datas = new ArrayList<>();
            if (ordersTemp.getGhichu() != null && !ordersTemp.getGhichu().isEmpty()) {
                KitchenNote kitchenNote = new KitchenNote(ordersTemp.getGhichu());
                datas.add(kitchenNote);
            }

            if (ordersTemp.getGhichu2() != null && !ordersTemp.getGhichu2().isEmpty()) {
                KitchenNote kitchenNote = new KitchenNote(ordersTemp.getGhichu2());
                datas.add(kitchenNote);
            }

            mView.goToKitchenNote(datas);
        }

    }


    @Override
    public void updateSpiceList(int position, List<Spice> datas) {
        OrdersTemp orderItem = ordersTempList.get(position);
        if (datas != null) {

            if (datas.size() < 1) {
                orderItem.setgiavi_text(null);
                orderItem.setgiavi_price_(0);

                orderItem.setgiavi_text_2(null);
                orderItem.setgiavi_price_2(0);

                orderItem.setgiavi_text_3(null);
                orderItem.setgiavi_price_3(0);

                orderItem.setgiavi_text_4(null);
                orderItem.setgiavi_price_4(0);

                orderItem.setgiavi_text_5(null);
                orderItem.setgiavi_price_5(0);

                orderItem.setgiavi_text_6(null);
                orderItem.setgiavi_price_6(0);

                orderItem.setgiavi_text_7(null);
                orderItem.setgiavi_price_7(0);

                orderItem.setgiavi_text_8(null);
                orderItem.setgiavi_price_8(0);

                orderItem.setgiavi_text_9(null);
                orderItem.setgiavi_price_9(0);
            } else {
                if (datas.size() > 0) {
                    orderItem.setgiavi_text(datas.get(0).getName());
                    orderItem.setgiavi_price_(datas.get(0).getPrice());
                } else {
                    orderItem.setgiavi_text(null);
                    orderItem.setgiavi_price_(0);
                }

                if (datas.size() > 1) {
                    orderItem.setgiavi_text_2(datas.get(1).getName());
                    orderItem.setgiavi_price_2(datas.get(1).getPrice());
                } else {
                    orderItem.setgiavi_text_2(null);
                    orderItem.setgiavi_price_2(0);
                }

                if (datas.size() > 2) {
                    orderItem.setgiavi_text_3(datas.get(2).getName());
                    orderItem.setgiavi_price_3(datas.get(2).getPrice());
                } else {
                    orderItem.setgiavi_text_3(null);
                    orderItem.setgiavi_price_3(0);
                }

                if (datas.size() > 3) {
                    orderItem.setgiavi_text_4(datas.get(3).getName());
                    orderItem.setgiavi_price_4(datas.get(3).getPrice());
                } else {
                    orderItem.setgiavi_text_4(null);
                    orderItem.setgiavi_price_4(0);
                }

                if (datas.size() > 4) {
                    orderItem.setgiavi_text_5(datas.get(4).getName());
                    orderItem.setgiavi_price_5(datas.get(4).getPrice());
                } else {
                    orderItem.setgiavi_text_5(null);
                    orderItem.setgiavi_price_5(0);
                }

                if (datas.size() > 5) {
                    orderItem.setgiavi_text_6(datas.get(5).getName());
                    orderItem.setgiavi_price_6(datas.get(5).getPrice());
                } else {
                    orderItem.setgiavi_text_6(null);
                    orderItem.setgiavi_price_6(0);
                }

                if (datas.size() > 6) {
                    orderItem.setgiavi_text_7(datas.get(6).getName());
                    orderItem.setgiavi_price_7(datas.get(6).getPrice());
                } else {
                    orderItem.setgiavi_text_7(null);
                    orderItem.setgiavi_price_7(0);
                }

                if (datas.size() > 7) {
                    orderItem.setgiavi_text_8(datas.get(7).getName());
                    orderItem.setgiavi_price_8(datas.get(7).getPrice());
                } else {
                    orderItem.setgiavi_text_8(null);
                    orderItem.setgiavi_price_8(0);
                }

                if (datas.size() > 8) {
                    orderItem.setgiavi_text_9(datas.get(8).getName());
                    orderItem.setgiavi_price_9(datas.get(8).getPrice());
                } else {
                    orderItem.setgiavi_text_9(null);
                    orderItem.setgiavi_price_9(0);
                }

                if (datas.size() > 9) {
                    orderItem.setgiavi_text_10(datas.get(9).getName());
                    orderItem.setgiavi_price_10(datas.get(9).getPrice());
                } else {
                    orderItem.setgiavi_text_10(null);
                    orderItem.setgiavi_price_10(0);
                }
            }
            orderItem.initExtraItemList();

            orderItem.setThanhtien(tinhThanhTien(orderItem));
            orderItem.setGiatien(tinhGiaTien(orderItem));
            orderItem.setTienthue(tinhTienThue(orderItem));
            orderItem.setTiengiam((tinhTienChuaGiam(orderItem) * orderItem.getPhantramgiam()) / 100);
            orderItem.initExtraItemList();

            mView.resetSelectedList();
            mView.showOrderTempList(ordersTempList);
        }
    }

    @Override
    public void updateKitchenNoteList(List<Integer> positions, List<KitchenNote> datas) {

        for (int i = 0; i < positions.size(); i++) {
            OrdersTemp orderItem = ordersTempList.get(positions.get(i));
            if (datas != null) {
                if (datas.size() < 1) {
                    orderItem.setGhichu(null);
                    orderItem.setGhichu2(null);
                } else {
                    if (datas.size() > 0) {
                        orderItem.setGhichu(datas.get(0).getName());
                    } else {
                        orderItem.setGhichu(null);
                    }
                    if (datas.size() > 1) {
                        orderItem.setGhichu2(datas.get(1).getName());
                    } else {
                        orderItem.setGhichu2(null);
                    }
                }
            }
            orderItem.initExtraItemList();
        }
        mView.resetSelectedList();
        mView.showOrderTempList(ordersTempList);
    }

    @Override
    public void updateMonDiKem(int position, ItemsPopup itemsPopup) {
        OrdersTemp orderItem = ordersTempList.get(position);

        if (orderItem.getPhanangomco() == null) {

            orderItem.setPhanangomco(itemsPopup.getPopup_name());
            orderItem.setphanangomco_value(itemsPopup.getPopup_price());

        } else if (orderItem.getPhanangomco2() == null) {

            orderItem.setPhanangomco2(itemsPopup.getPopup_name());
            orderItem.setPhanangomco2Value(itemsPopup.getPopup_price());

        } else if (orderItem.getPhanangomco3() == null) {

            orderItem.setPhanangomco3(itemsPopup.getPopup_name());
            orderItem.setPhanangomco3Value(itemsPopup.getPopup_price());

        } else if (orderItem.getPhanangomco4() == null) {

            orderItem.setPhanangomco4(itemsPopup.getPopup_name());
            orderItem.setPhanangomco4Value(itemsPopup.getPopup_price());
        } else if (orderItem.getPhanangomco5() == null) {

            orderItem.setPhanangomco5(itemsPopup.getPopup_name());
            orderItem.setPhanangomco5Value(itemsPopup.getPopup_price());

        } else if (orderItem.getPhanangomco6() == null) {

            orderItem.setPhanangomco6(itemsPopup.getPopup_name());
            orderItem.setPhanangomco6Value(itemsPopup.getPopup_price());

        } else if (orderItem.getPhanangomco7() == null) {

            orderItem.setPhanangomco7(itemsPopup.getPopup_name());
            orderItem.setPhanangomco7Value(itemsPopup.getPopup_price());

        } else if (orderItem.getPhanangomco8() == null) {

            orderItem.setPhanangomco8(itemsPopup.getPopup_name());
            orderItem.setPhanangomco8Value(itemsPopup.getPopup_price());

        } else if (orderItem.getPhanangomco9() == null) {

            orderItem.setPhanangomco9(itemsPopup.getPopup_name());
            orderItem.setPhanangomco9Value(itemsPopup.getPopup_price());

        }
        orderItem.initExtraItemList();
        orderItem.setThanhtien(tinhThanhTien(orderItem));
        orderItem.setGiatien(tinhGiaTien(orderItem));
        orderItem.setTienthue(tinhTienThue(orderItem));
        orderItem.setTiengiam((tinhTienChuaGiam(orderItem) * orderItem.getPhantramgiam()) / 100);
        orderItem.initExtraItemList();
        mView.resetSelectedList();
        mView.showOrderTempList(ordersTempList);
    }


    @Override
    public void doChangeValueTable(String inTable, boolean isLayBanOrDatMon) {
        if (!SessionApp.is_updated_table) {
            if (ordersTempListAll != null && ordersTempListAll.size() > 0) {
                boolean havePrinted = false;
                for (OrdersTemp ordersTemp : ordersTempListAll) {
                    if (ordersTemp.isInsertedToDb()) {
                        havePrinted = true;
                        break;
                    } else {
                        havePrinted = false;
                    }
                }


                if (havePrinted) {
                    double tongtien = 0;
                    for (int i = 0; i < ordersTempListAll.size(); i++) {
                        tongtien += ordersTempListAll.get(i).getThanhtien();
                    }
                    table.setTrangthai("using");
                    table.setTongtien(tongtien);
                    if (table.getUsername() != null && !table.getUsername().isEmpty()) {
                        table.setUsername(table.getUsername());
                    } else table.setUsername(WapriManager.getUser().getUsername());
                    table.setThoigian(Util.getDate("dt"));
                    if (getTongTienTable() == 0) table.setTongtien(null);
                    else table.setTongtien(getTongTienTable());
                } else {
                    if (table != null) {
                        table.setTrangthai("empty");
                        table.setTongtien(null);
                        table.setUsername(null);
                    }
                }
            } else {
                if (table != null) {
                    table.setTrangthai("empty");
                    table.setGiam(0);
                    table.setUsername(null);
                    table.setThoigian(null);
                    table.setis_selling_on_tablet("N");
                    table.setTongtien(null);
                }
            }

            if (inTable.equals("out")) {
                table.setis_selling_on_tablet("N");
                table.setIs_selling_on_POS("N");
                table.setfrom_tablet("Y");

            } else {
                table.setTrangthai("using");
                if (isLayBanOrDatMon) {
                    table.setUsername(WapriManager.getUser().getUsername());
                } else {
                    if (table.getUsername() != null && !table.getUsername().isEmpty()) {
                        table.setUsername(table.getUsername());
                    } else table.setUsername(WapriManager.getUser().getUsername());
                }

                table.setfrom_tablet("Y");
                table.setis_selling_on_tablet("Y");
            }
            table.setThoigian(Util.getDate("dt"));
            new UpdateTableToDbTask(table).execute();
        }

    }

    @Override
    public void doUpdateGiamGiaTable(String data) {
        double tongtien = 0;
        int phanTramGiam = Integer.parseInt(data);
        table.setGiam(phanTramGiam);
        for (OrdersTemp ordersTemp : ordersTempList) {
            if (ordersTemp.getTaichohaymangve() == WapriManager.Taichohaymangve_.TaiCho || ordersTemp.getBophanphucvu().equals("PhaChe19")) {
                ordersTemp.setThue(19);
                ordersTemp.setTienthue(tinhTienThue(ordersTemp));

            } else {
                ordersTemp.setThue(7);
                ordersTemp.setTienthue(tinhTienThue(ordersTemp));
            }

            tongtien += ordersTemp.getThanhtien();
        }
        table.setTrangthai("using");
        if (mView.getPermission()) table.setUsername(WapriManager.getUser().getUsername());

        table.setTongtien(tongtien - (tongtien * convertToDouble(data) / 100));
        if (data.isEmpty()) {
            data = table.getGiam() + "";
        }

        double thue7 = 0;
        double thue19 = 0;
        for (OrdersTemp ordersTemp : ordersTempList) {
            if (!ordersTemp.getDanhdaubixoa().equals("Y")) {
                if (ordersTemp.getThue() == 7) {
                    thue7 += ordersTemp.getTienthue();
                } else {
                    thue19 += ordersTemp.getTienthue();
                }
            }

        }
        table.setTienthue7giam(WapriManager.tinhTienThueGiam(0, phanTramGiam));
        table.setTienthue19giam(WapriManager.tinhTienThueGiam(0, phanTramGiam));
        table.setTiengiam((tongtien * convertToDouble(data)) / 100);

        table.setThoigian(Util.getDate("dt"));
        mView.setTongtienView();
        new UpdateTableToDbTask(table).execute();
    }

    @Override
    public void removeOrder(List<Integer> positionsSelected) {
        boolean isContainBoth = false;
        boolean isJustContainPrintedOrder = false;
        int havePrinted = 0;
        int notPrinted = 0;
        final List<OrdersTemp> removeList = new ArrayList<>();
        for (int i = 0; i < positionsSelected.size(); i++) {
            OrdersTemp ordersTemp = ordersTempList.get(positionsSelected.get(i));

            if (ordersTemp.getInlaimonfromtablet().equals("Y") || ordersTemp.getDaprintchua().equals("Y")) {
                havePrinted = 1;
                ordersTempsWillUpdateCancelReason.add(ordersTemp);
//                ordersTemp.setDanhdaubixoa("Y");
            } else {
                notPrinted = 1;
                removeList.add(ordersTemp);
                ordersTempsWillDelete.add(ordersTemp);
            }
        }

        if (havePrinted > 0 && notPrinted > 0) {
            isContainBoth = true;
            isJustContainPrintedOrder = false;
        } else if (havePrinted > 0 && notPrinted == 0) {
            isJustContainPrintedOrder = true;
            isContainBoth = false;
        } else if (havePrinted == 0 && notPrinted > 0) {
            isJustContainPrintedOrder = false;
            isContainBoth = false;
        }


        if (isContainBoth) {
            mView.showDialog(App.Companion.self().getString(R.string.alert_remove_at_the_same_printed_and_not_print));
        } else if (isJustContainPrintedOrder) {
            if (WapriManager.getUser().getDuocQuyenXoaMatHangDaChonKhiDaInBep().equals("Y"))
                mView.showCancelReasonDialog();
            else
                mView.showDialog(Util.getStringFromResouce(R.string.you_do_not_have_permission_feature));

        } else {

            Util.showCustomDialogConfirm(mView.getCurrentContext(), Util.getStringFromResouce(R.string.are_u_w_to_delete), new Util.ConfirmDialogInterface() {
                @Override
                public void onPositiveClickListener() {
                    for (int i = 0; i < removeList.size(); i++) {
                        ordersTempListAll.remove(removeList.get(i));
                        new DeleteOrderToDbTask(removeList.get(i).getId()).execute();
                    }
                    mView.resetSelectedList();
                    getOffOrderTempList(getViTriNgoi());
                }
                @Override
                public void onNegativeClickListener() {
                    ordersTempsWillDelete.clear();
                    ordersTempsWillDelete = new ArrayList<>();
                }
            });
        }


//        if(!isJustContainPrintedOrder){
//            for (int i = 0; i < removeList.size(); i++) {
//                ordersTempList.remove(removeList.get(i));
//            }
//            mView.resetSelectedList();
//            mView.showOrderTempList(ordersTempList);
//        }else{
//            mView.showCancelReasonDialog();
//            mView.showDialog("Không thể xóa cùng lúc các món đã được xuống bếp và chưa xuống bếp!");
//        }


    }

    @Override
    public void increaseNumberOrder(int positionSelected) {
        OrdersTemp ordersTemp = ordersTempList.get(positionSelected);
        if (ordersTemp.getInlaimonfromtablet().equals("Y") || ordersTemp.getDaprintchua().equals("Y")) {
            mView.showDialog(App.Companion.self().getString(R.string.can_not_increase_decrease_the_food_that_was_printed_in_the_kitchen));
        } else {
            int soluong = ordersTemp.getSoluong();
            ordersTemp.setSoluong(++soluong);
            ordersTemp.setThanhtien(tinhThanhTien(ordersTemp));
            ordersTemp.setGiatien(tinhGiaTien(ordersTemp));
            ordersTemp.setTienthue(tinhTienThue(ordersTemp));
            ordersTemp.setTiengiam((tinhTienChuaGiam(ordersTemp) * ordersTemp.getPhantramgiam()) / 100);
//            mView.resetSelectedList();
            mView.showOrderTempList(ordersTempList);
        }
    }

    @Override
    public void decreaseNumberOrder(int positionSelected) {
        OrdersTemp ordersTemp = ordersTempList.get(positionSelected);
        if (ordersTemp.getInlaimonfromtablet().equals("Y") || ordersTemp.getDaprintchua().equals("Y")) {
            mView.showDialog(App.Companion.self().getString(R.string.can_not_increase_decrease_the_food_that_was_printed_in_the_kitchen));
        } else {
            int soluong = ordersTemp.getSoluong();
            if (soluong > 1) {
                ordersTemp.setSoluong(--soluong);
                ordersTemp.setThanhtien(tinhThanhTien(ordersTemp));
                ordersTemp.setGiatien(tinhGiaTien(ordersTemp));
                ordersTemp.setTienthue(tinhTienThue(ordersTemp));
                ordersTemp.setTiengiam((tinhTienChuaGiam(ordersTemp) * ordersTemp.getPhantramgiam()) / 100);
//                mView.resetSelectedList();
                mView.showOrderTempList(ordersTempList);
//            new UpdateOrderToDbTask(ordersTemp).execute();
            } else {
                mView.showAlert(App.Companion.self().getString(R.string.the_amount_must_bigger_than_1));
            }
        }
    }

    @Override
    public void hideSomeOrderFeature(List<Integer> positionsSelected) {
        if (positionsSelected.size() == 1) {
            OrdersTemp ordersTemp = ordersTempList.get(positionsSelected.get(0));
            if (ordersTemp.getInlaimonfromtablet().equals("Y") || ordersTemp.getDaprintchua().equals("Y")) {
                mView.showOrderFeaturePrinted();
            } else {
                mView.showFullOrderFeature();
            }
            mView.showSpiceFromSinglePrinted();
        } else if (positionsSelected.size() > 1) {
            boolean isMatch = false;
            for (int i = 0; i < positionsSelected.size(); i++) {
                if (positionsSelected.get(i) < ordersTempList.size()) {
                    OrdersTemp ordersTemp = ordersTempList.get(positionsSelected.get(i));
                    if (ordersTemp.getInlaimonfromtablet().equals("Y") || ordersTemp.getDaprintchua().equals("Y")) {
                        isMatch = true;
                        break;
                    } else {
                        isMatch = false;
                    }
                }
            }
            mView.hideSpiceFromMultiPrinted();
            if (isMatch) {
                mView.showOrderFeaturePrinted();
            } else {
                mView.showOrderFeatureMulti();
            }
        }

    }

    @Override
    public void cancelSelectedOrders() {
        mView.cancelSelectedOrders();
        mView.showOrderTempList(ordersTempList);
    }


    @Override
    public void goToChangeToOtherTable(List<Integer> positionsSelected) {
        List<OrdersTemp> ordersTempsWillChange = new ArrayList<>();
        for (int i = 0; i < positionsSelected.size(); i++) {
            OrdersTemp orderItem = ordersTempList.get(positionsSelected.get(i));
//            orderItem.setThue(7);
            ordersTempsWillChange.add(orderItem);
        }
        mView.goToChangeToOtherTable(ordersTempsWillChange, ordersTempList);
    }

    @Override
    public void goToDivideBill() {
        List<OrdersTemp> ordersTempsRestore = new ArrayList<>();
        for (OrdersTemp ob : ordersTempListAll) {
            OrdersTemp object = new OrdersTemp();
            try {
                object = (OrdersTemp) ob.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            ordersTempsRestore.add(object);
        }

        mView.goToDivideBill(getOrderTempList(), ordersTempsRestore);
    }


    @Override
    public void goToSeparateToOtherTable() {
        SessionApp.TABLE_NAME_WILL_GO = table.getName();
        SessionApp.CUSTOMER_POSITIONS_CURRENT_TABLE = table.getSoghengoi();
        List<Integer> positionsHaveOrder = new ArrayList<>();
        for (OrdersTemp ob : ordersTempListAll) {
            if (!positionsHaveOrder.contains(Integer.valueOf(ob.getThutukhachtrenban()))) {
                positionsHaveOrder.add(Integer.valueOf(ob.getThutukhachtrenban()));
            }
        }
        SessionApp.POSITIONS_HAVE_ORDER_CURRENT_TABLE = positionsHaveOrder;
        mView.goToSeparateToOtherTable();
    }

    @Override
    public void goToPayment() {
//        if(WapriManager.configuration().huyThanhToan.get()){
//            SharedPrefs.getInstance().put(SharedPrefs.KEY_TABLE+table.getName(),false);
//        }else{
//            SharedPrefs.getInstance().put(SharedPrefs.KEY_TABLE+table.getName(),true);
//        }
        SessionApp.SIZE_OF_ALL_ORDER = getOrderTempList().size();
        SessionApp.SIZE_OF_POSITION_ORDER = getOrderTempListByPosition().size();
        setOrderTempRecoveryList(getOrderTempList());
        mView.goToPayment();
    }


    @Override
    public void doOrder() {
        if (getOrderTempList() != null && getOrderTempList().size() > 0) {
            if (WapriManager.configuration().tuDongInBep.get()) {
                final List<OrdersTemp> bepList = new ArrayList<>();
                final List<OrdersTemp> nuocList = new ArrayList<>();
                final List<OrdersTemp> sushiList = new ArrayList<>();

                for (OrdersTemp ordersTemp : getOrderTempList()) {
                    if (!ordersTemp.getDaprintchua().equals("Y")) {
                        if (ordersTemp.getBophanphucvu().equals("Sushi")) {
                            sushiList.add(ordersTemp);
                        } else if (ordersTemp.getBophanphucvu().contains("PhaChe")) {
                            nuocList.add(ordersTemp);
                        } else {
                            bepList.add(ordersTemp);
                        }
                    }
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (bepList.size() > 0) PrinterUtils.Print(bepList, "Bep");
                        if (nuocList.size() > 0) PrinterUtils.Print(nuocList, "Nuoc");
                        if (sushiList.size() > 0) PrinterUtils.Print(sushiList, "Sushi");
                    }
                }).start();


                new InsertAllOrdersToDbTask(true).execute();
            } else {
                new InsertAllOrdersToDbTask(false).execute();
            }
        } else {
            new DeleteAllOrdersToDbTask(ordersTempsWillDelete).execute();
            mView.showAlert(App.Companion.self().getString(R.string.empty_table));
        }


        // Cancel orders
        if (ordersTempsWillUpdateCancelReason != null && !ordersTempsWillUpdateCancelReason.isEmpty()) {
            if (WapriManager.configuration().tuDongInBep.get()) {
                List<OrdersTemp> bepList = new ArrayList<>();
                List<OrdersTemp> nuocList = new ArrayList<>();
                List<OrdersTemp> sushiList = new ArrayList<>();

                for (OrdersTemp ordersTemp : ordersTempsWillUpdateCancelReason) {
                    if (ordersTemp.getBophanphucvu().equals("Sushi")) {
                        sushiList.add(ordersTemp);
                    } else if (ordersTemp.getBophanphucvu().contains("PhaChe")) {
                        nuocList.add(ordersTemp);
                    } else {
                        bepList.add(ordersTemp);
                    }
                }
                if (bepList.size() > 0) PrinterUtils.PrintCancel(bepList, "Bep");
                if (nuocList.size() > 0) PrinterUtils.PrintCancel(nuocList, "Nuoc");
                if (sushiList.size() > 0) PrinterUtils.PrintCancel(sushiList, "Sushi");

                new UpdateTableToDbTask(table).execute();
                new UpdateAllLyDoHuyTask(ordersTempsWillUpdateCancelReason).execute();
            } else {
//                new UpdateTableToDbTask(table).execute();
//                new UpdateAllLyDoHuyTask(ordersTempsWillUpdateCancelReason).execute();
            }
        }

    }


    private int getViTriNgoi() {
        return mView.getViTriNgoi();
    }

    private List<OrdersTemp> getLoaiListBoPhanPhucVu(List<OrdersTemp> listData, String bophanphucvu) {
        List<OrdersTemp> listType = new ArrayList<>();
        if(listData!=null && !listData.isEmpty()){
            for (OrdersTemp ordersTemp : listData) {
                if (ordersTemp.getBophanphucvu().equals(bophanphucvu)) {
                    listType.add(ordersTemp);
                }
            }
        }
        return listType;
    }


    private double tinhThanhTien(OrdersTemp orderTemp) {
        // count all include spice price
        double totalSpicePrice = 0;
        for (ExtraItem extraItem : orderTemp.getExtraItemList()) {
            if (extraItem.getFlag() == WapriManager.FeatureOrder.GIA_VI || extraItem.getFlag() == WapriManager.FeatureOrder.PHAN_AN) {
                totalSpicePrice += extraItem.getValue();
            }
        }

        double tongtien = (orderTemp.getSoluong() * orderTemp.getorig_price_no_extra()) + (totalSpicePrice * orderTemp.getSoluong());
        double thanhtien = tongtien - ((tongtien * orderTemp.getPhantramgiam()) / 100);
        return thanhtien;
    }

    private double tinhGiaTien(OrdersTemp orderTemp) {
        // count all include spice price
        double totalSpicePrice = 0;
        for (ExtraItem extraItem : orderTemp.getExtraItemList()) {
            if (extraItem.getFlag() == WapriManager.FeatureOrder.GIA_VI || extraItem.getFlag() == WapriManager.FeatureOrder.PHAN_AN) {
                totalSpicePrice += extraItem.getValue();
            }
        }
        double tongtien = (orderTemp.getSoluong() * orderTemp.getorig_price_no_extra()) + (totalSpicePrice * orderTemp.getSoluong());
        return tongtien;
    }

    private double tinhTienChuaGiam(OrdersTemp orderTemp) {
        // count all include spice price
        double totalSpicePrice = 0;
        for (ExtraItem extraItem : orderTemp.getExtraItemList()) {
            if (extraItem.getFlag() == WapriManager.FeatureOrder.GIA_VI || extraItem.getFlag() == WapriManager.FeatureOrder.PHAN_AN) {
                totalSpicePrice += extraItem.getValue();
            }
        }
        double tongtien = (orderTemp.getSoluong() * orderTemp.getorig_price_no_extra()) + (totalSpicePrice * orderTemp.getSoluong());
        return tongtien;
    }

    private double convertToDouble(String data) {
        try {
            return Double.parseDouble(data);
        } catch (NumberFormatException ex) {
            mView.showAlert(ex.getMessage());
            return 0.0;
        }
    }


    @Override
    public double getSummerPrice() {
        double summer = 0;
        double summerOf1005 = 0;
        for (OrdersTemp order : ordersTempList) {
            if (order.getDanhdaubixoa() != null && order.getDanhdaubixoa().equals("Y")) {
            } else if (order.getitems_id().equals("1005")) {
                summerOf1005 += order.getThanhtien();
            } else {
                summer += order.getThanhtien();
            }
//            summer += order.getThanhtien();
        }


        double resultWithout1005 = summer - ((summer * table.getGiam()) / 100);
        double result = resultWithout1005 + summerOf1005;
        table.setTongtien(result);
        return result;
    }

    @Override
    public double getTongTienTable() {
        double summer = 0;
        for (OrdersTemp order : ordersTempListAll) {
            if (order.getDanhdaubixoa() != null && order.getDanhdaubixoa().equals("Y")) {
//                summer -= order.getThanhtien();
            } else {
                summer += order.getThanhtien();
            }
//            summer += order.getThanhtien();
        }
        double result = summer - ((summer * table.getGiam()) / 100);
        table.setTongtien(result);
        return result;
    }


    @Override
    public void removeOrderTempListHasChanged(List<OrdersTemp> ordersTempsWillChange) {
        List<OrdersTemp> ordersTempsWillChangeDelete = new ArrayList<>();
        for (OrdersTemp ordersTemp : ordersTempsWillChange) {
            ordersTempsWillChangeDelete.add(ordersTemp);
        }
        for (OrdersTemp ordersTemp : ordersTempsWillChangeDelete) {
            ordersTempListAll.remove(ordersTemp);
        }
        getOffOrderTempList(getViTriNgoi());
        getTongTienTable();
        new UpdateTableToDbTask(table).execute();
    }

    @Override
    public void updateCancelReason(String reason) {
        for (OrdersTemp ordersTemp : ordersTempsWillUpdateCancelReason) {
            ordersTemp.setDanhdaubixoa("Y");
            ordersTemp.setDaprintxoachua("N");
            if (!WapriManager.configuration().tuDongInBep.get()) {
                ordersTemp.setInlaimonfromtablet("Y");
            } else ordersTemp.setInlaimonfromtablet("N");

            ordersTemp.setThanhtien(0.0);
            ordersTemp.setorig_price_no_extra(0.0);
            ordersTemp.setorig_price_from_item(0.0);
            ordersTemp.setGiatien(0.0);
            ordersTemp.setTienthue(0.0);
            ordersTemp.setLydohuyxoa(reason);
        }
        new UpdateAllLyDoHuyTask(ordersTempsWillUpdateCancelReason).execute();

        table.setTongtien(getTongTienTable());
        mView.resetSelectedList();
        mView.showOrderTempList(ordersTempList);
    }

    @Override
    public Table getTable() {
        return table;
    }

    @Override
    public void doOrderByFoodId(String data) {
        int beforeX = data.indexOf("*");

        if (beforeX != -1) {
            String id = data.substring(0, beforeX);
            String number = data.substring(beforeX + 1);
            if (!number.equals("")) {
                int soLuong = Integer.parseInt(number);
                new SelectItemsByInputCodeTask(id, soLuong).execute();
            } else {
                new SelectItemsByInputCodeTask(id, 0).execute();
            }
        } else {
            String number = data.substring(beforeX + 1);
            if (!number.equals("")) {
                new SelectItemsByInputCodeTask(data, 0).execute();
            } else {
                new SelectItemsByInputCodeTask(data, 0).execute();
            }
        }
    }

    @Override
    public void updateTableInfo(Table table) {
        new UpdateTableToDbTask(table).execute();
    }

    @Override
    public void deleteOrderTemp(OrdersTemp ordersTemp) {
        ordersTempListAll.remove(ordersTemp);
        getOffOrderTempList(getViTriNgoi());
        new DeleteOrderToDbTask(ordersTemp.getId()).execute();
    }

    @Override
    public void tinhTienGiamTable() {
        double tongtien = 0;
        for (OrdersTemp ordersTemp : ordersTempList) {
            tongtien += ordersTemp.getThanhtien();
        }
        table.setTiengiam((tongtien * table.getGiam()) / 100);
    }

    private double tinhPhanTram(OrdersTemp ordersTemp) {
        return (ordersTemp.getPhantramgiam() * ordersTemp.getorig_price_no_extra()) / 100;
    }


    private double tinhTienThue(OrdersTemp ordersTemp) {
        if (ordersTemp.getBophanphucvu().equals("Sonstiges0"))
            return 0.0;
        else if (ordersTemp.getTaichohaymangve() == WapriManager.Taichohaymangve_.TaiCho || ordersTemp.getBophanphucvu().equals("PhaChe19")) {
            return WapriManager.tinhTienThue19(ordersTemp.getGiatien(), ordersTemp.getPhantramgiam());
        } else
            return WapriManager.tinhTienThue7(ordersTemp.getGiatien(), ordersTemp.getPhantramgiam());
    }


    private MatchItem checkIsExist(OrdersTemp ordersTempList, WapriManager.FeatureOrder
            featureOrder) {
        boolean isMatch = false;
        int positionMatch = -1;

        for (int i = 0; i < ordersTempList.getExtraItemList().size(); i++) {
            if (ordersTempList.getExtraItemList().get(i).getFlag() == featureOrder) {
                isMatch = true;
                positionMatch = i;
                break;
            } else {
                isMatch = false;
            }
        }
        MatchItem match = new MatchItem(isMatch, positionMatch);
        return match;
    }

    private class GetAllOrderTempListTask extends AsyncTask<Void, Void, List<OrdersTemp>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<OrdersTemp> doInBackground(Void... voids) {

            JSONArray js = MySqlHandleUtils.selectStament("SELECT thutukhachtrenban FROM wapri.orders_temp where banso = " + "'" + table.getName() + "'");
            ArrayList<OrdersTemp> ordersTemps = new Gson().fromJson(js.toString(), new TypeToken<List<OrdersTemp>>() {
            }.getType());
            return ordersTemps;
        }

        @Override
        protected void onPostExecute(List<OrdersTemp> datas) {
            super.onPostExecute(datas);
            mView.dismissLoading();


        }
    }

    private class SelectItemsByInputCodeTask extends AsyncTask<Void, Void, List<Items>> {
        String items_id;
        int soLuong;

        public SelectItemsByInputCodeTask(String items_id, int soLuong) {
            this.items_id = items_id;
            this.soLuong = soLuong;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<Items> doInBackground(Void... voids) {

            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.items where items_id = " + "'" + items_id + "'" + " or tenhang like '" + items_id + "%'");
            ArrayList<Items> items = new Gson().fromJson(js.toString(), new TypeToken<List<Items>>() {
            }.getType());
            if (items.size() > 0) {
                for (Items ob : items) {
                    ob.setGiaBan();
                }
                if (SessionApp.isHappyHour && !SessionApp.isAplyAllHappyHour) {
                    if (SessionApp.happyHour_detail.size() > 0) {
                        for (int i = 0; i < items.size(); i++) {
                            for (int j = 0; j < SessionApp.happyHour_detail.size(); j++) {
                                if (items.get(i).getId().equals(SessionApp.happyHour_detail.get(j).getItems_id())) {
                                    items.get(i).setPhanTramGiam(SessionApp.happyHour.getPhantramgiam());
                                }
                            }
                        }
                    }
                }

            }


            return items;
        }

        @Override
        protected void onPostExecute(List<Items> datas) {
            super.onPostExecute(datas);
            mView.dismissLoading();
            if (datas.size() < 1) {
                mView.showAlert(App.Companion.self().getString(R.string.the_item_does_not_exist));
            } else {
                if (datas.size() > 0 && datas.size() < 2) {
                    addOrder(datas.get(0), soLuong);
                } else {
                    mView.showFoundItemsDialog(datas, soLuong);
                }

            }


        }
    }

    private class GetOrderTempListByCustomerPositionTask extends AsyncTask<Void, Void, List<OrdersTemp>> {

        int position_table;

        public GetOrderTempListByCustomerPositionTask(int position_table) {
            this.position_table = position_table;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<OrdersTemp> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectOrderTempListByCustomerPositionStament(table, position_table);
            ArrayList<OrdersTemp> ordersTemps = new Gson().fromJson(js.toString(), new TypeToken<List<OrdersTemp>>() {
            }.getType());
            return ordersTemps;
        }

        @Override
        protected void onPostExecute(List<OrdersTemp> datas) {
            super.onPostExecute(datas);
            mView.dismissLoading();
            ordersTempListAll = datas;
            ordersTempList = datas;
            if (datas != null && datas.size() > 0) {
                for (OrdersTemp ordersTemp : ordersTempList) {
                    ordersTemp.initExtraItemList();
                    ordersTemp.setGiatien(tinhGiaTien(ordersTemp));
//                    if (table.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho || ordersTemp.getBophanphucvu().equals("PhaChe19")) {
//                        ordersTemp.setThue(19);
//                        ordersTemp.setTienthue(tinhTienThue(ordersTemp));
//                    } else {
//                        ordersTemp.setThue(7);
//                        ordersTemp.setTienthue(tinhTienThue(ordersTemp));
//                    }
//
//                    if (ordersTemp.getBophanphucvu().equals("Sonstiges0")) {
//                        ordersTemp.setThue(0);
//                        ordersTemp.setTienthue(tinhTienThue(ordersTemp));
//                    }
                    ordersTemp.setInsertedToDb(true);
                }
            }

            mView.showOrderTempList(ordersTempList);
            doChangeValueTable("inside", mView.getPermission());

//            Boolean isNotYetPayment = SharedPrefs.getInstance().get(SharedPrefs.KEY_TABLE+SessionApp.global_table.getName(),Boolean.class);
//            if(isNotYetPayment!=null && isNotYetPayment){
//                goToPayment();
//            }
        }
    }

    private class InsertAllOrdersToDbTask extends AsyncTask<Void, Void, Void> {
        List<OrdersTemp> listInserted;

        boolean isPrinted;

        public InsertAllOrdersToDbTask(boolean isPrinted) {
            this.isPrinted = isPrinted;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            listInserted = new ArrayList<>();
            for (OrdersTemp ordersTemp : getOrderTempList()) {
                if (!ordersTemp.getDaprintchua().equals("Y")) {
                    if (isPrinted) ordersTemp.setDaprintchua("Y");
                    else ordersTemp.setDaprintchua("N");

                    listInserted.add(ordersTemp);

                    ordersTemp.setInsertedToDb(true);
                    new InsertOrderToDbTask(ordersTemp).execute();
                }
                if (ordersTemp.getDanhdaubixoa().equals("Y") && ordersTemp.getDaprintxoachua().equals("N")) {
                    ordersTemp.setInlaimonfromtablet("N");
                    new UpdateInBepOrderToDbTask(ordersTemp).execute();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mView.dismissLoading();
//            new UpdateAllInBepTask(listInserted).execute();
            new DeleteAllOrdersToDbTask(ordersTempsWillDelete).execute();
        }
    }


    private class InsertInBepOfflineToDbTask extends AsyncTask<Void, Void, Void> {
        List<OrdersTemp> listInserted;


        public InsertInBepOfflineToDbTask(List<OrdersTemp> listInserted) {
            this.listInserted = listInserted;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (OrdersTemp ordersTemp : listInserted) {
                ordersTemp.setDaprintchua("Y");
                ordersTemp.setInsertedToDb(true);
                new InsertOrderToDbTask(ordersTemp).execute();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mView.dismissLoading();
        }
    }


    private class InsertOrderToDbTask extends AsyncTask<Void, Void, Integer> {

        OrdersTemp ordersTemp;

        public InsertOrderToDbTask(OrdersTemp ordersTemp) {
            this.ordersTemp = ordersTemp;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            result = MySqlHandleUtils.insertOrderItem(ordersTemp);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                ordersTemp.setInlaimonfromtablet("N");
                ordersTemp.setDaprintchua("Y");
//                getOrderTempList(getViTriNgoi());
            } else if (result == 2) {
                if (!ordersTemp.getDaprintchua().equals("Y")) {
                    new UpdateOrderToDbTask(ordersTemp).execute();
                }

                if (currentFuture.equals("InBep")) {
                    ordersTemp.setInlaimonfromtablet("Y");
                    ordersTemp.setDaprintchua("Y");
                    ordersTemp.setInsertedToDb(true);
                    new UpdateOrderToDbTask(ordersTemp).execute();
                }
            }
//            else if(result == 2){
//              new UpdateOrderToDbTask(ordersTemp).execute();
//            }
        }
    }


    private class UpdateOrderToDbTask extends AsyncTask<Void, Void, Integer> {

        OrdersTemp ordersTemp;

        public UpdateOrderToDbTask(OrdersTemp ordersTemp) {
            this.ordersTemp = ordersTemp;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;
            ordersTemp.getInlaimonfromtablet();
            ordersTemp.setThoigian(Util.getDate("dt"));
            result = MySqlHandleUtils.updateOrderTempItem(ordersTemp);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            currentFuture = "";
            if (result == 1) {

            } else {
//                mView.showAlert("Có lỗi trong quá trình thêm dữ liệu");
            }
        }
    }

    private class UpdateInBepOrderToDbTask extends AsyncTask<Void, Void, Integer> {

        OrdersTemp ordersTemp;

        public UpdateInBepOrderToDbTask(OrdersTemp ordersTemp) {
            this.ordersTemp = ordersTemp;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;
            ordersTemp.getInlaimonfromtablet();
            result = MySqlHandleUtils.updateInBepOrderTemp(ordersTemp);
            return result;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            ordersTemp.setDaprintxoachua("Y");
        }
    }

    private class UpdateAllInBepTask extends AsyncTask<Void, Void, Void> {
        List<OrdersTemp> ordersTempList_;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        public UpdateAllInBepTask(List<OrdersTemp> ordersTempList) {
            this.ordersTempList_ = ordersTempList;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (OrdersTemp ordersTemp : ordersTempList_) {
                ordersTemp.setInlaimonfromtablet("Y");
                new UpdateInBepOrderToDbTask(ordersTemp).execute();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mView.dismissLoading();
            new DeleteAllOrdersToDbTask(ordersTempsWillDelete).execute();
        }
    }

    private class UpdateAllLyDoHuyTask extends AsyncTask<Void, Void, Void> {

        List<OrdersTemp> ordersTempList_;

        public UpdateAllLyDoHuyTask(List<OrdersTemp> ordersTempList) {
            this.ordersTempList_ = ordersTempList;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (OrdersTemp ordersTemp : ordersTempList_) {
                new UpdateOrderToDbTask(ordersTemp).execute();
//                new UpdateLyDoHuyTask(ordersTemp).execute();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mView.resetSelectedList();
            ordersTempsWillUpdateCancelReason.clear();
            ordersTempsWillUpdateCancelReason = new ArrayList<>();
        }
    }


    private class UpdateLyDoHuyTask extends AsyncTask<Void, Void, Integer> {

        OrdersTemp ordersTemp;

        public UpdateLyDoHuyTask(OrdersTemp ordersTemp) {
            this.ordersTemp = ordersTemp;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            result = MySqlHandleUtils.updateLyDoHuyOrderTemp(ordersTemp.getId(), ordersTemp.getLydohuyxoa());
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

        }
    }

    private class UpdateOrderTieuDeToDbTask extends AsyncTask<Void, Void, Integer> {

        String id;
        String name;

        public UpdateOrderTieuDeToDbTask(String id, String name) {
            this.id = id;
            this.name = name;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            result = MySqlHandleUtils.updateTieuDeDotDatOrderToTable(id, name);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                getNewOrderTempList(getViTriNgoi());
//                mView.resetSelectedList();
            } else {
//                mView.showAlert("Có lỗi trong quá trình thêm dữ liệu");
            }
        }
    }

    private class DeleteAllOrdersToDbTask extends AsyncTask<Void, Void, Void> {

        List<OrdersTemp> ordersTempsWillDelete;

        public DeleteAllOrdersToDbTask(List<OrdersTemp> ordersTempsWillDelete) {
            this.ordersTempsWillDelete = ordersTempsWillDelete;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            if (ordersTempsWillDelete != null) {
                for (OrdersTemp ordersTemp : ordersTempsWillDelete) {
                    new DeleteOrderToDbTask(ordersTemp.getId()).execute();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mView.dismissLoading();
            mView.goToTable();
        }
    }


    private class DeleteOrderToDbTask extends AsyncTask<Void, Void, Integer> {

        String id;

        public DeleteOrderToDbTask(String id) {
            this.id = id;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            result = MySqlHandleUtils.deleteRecordFromOrderTemp(id);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
//                getOrderTempList(getViTriNgoi());
//                mView.resetSelectedList();
            } else {
//                mView.showAlert("Có lỗi trong quá trình thêm dữ liệu");
            }
        }
    }


    private class UpdateTableToDbTask extends AsyncTask<Void, Void, Integer> {

        Table table;

        public UpdateTableToDbTask(Table table) {
            this.table = table;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            if (table != null) {
                if (MySqlHandleUtils.updateTableChangeNew(table) != 0)
                    return 1;
                else return 0;
            }
            return 1;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {

            } else {

            }
        }
    }

    private class MatchItem {
        public boolean isMatched;
        public int positionMatched;

        public MatchItem(boolean isMatched, int positionMatched) {
            this.isMatched = isMatched;
            this.positionMatched = positionMatched;
        }
    }
}
