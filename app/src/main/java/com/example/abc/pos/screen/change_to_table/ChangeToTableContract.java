package com.example.abc.pos.screen.change_to_table;

import com.example.abc.pos.model.Table;

import java.util.List;

public interface ChangeToTableContract {
    interface View{
         void showListTable(List<Table> tables);
        void showLoading();
        void dismissLoading();
        void showAlert(String data);
        void goBack();
    }

    interface Presenter{
        void getEmptyTable();
        void doChangeTable(Table table_old, Table table_new);
        void setTableNew(Table tableNew);
    }
}
