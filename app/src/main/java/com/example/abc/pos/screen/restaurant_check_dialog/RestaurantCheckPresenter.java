package com.example.abc.pos.screen.restaurant_check_dialog;

import android.os.AsyncTask;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.model.Guschein;
import com.example.abc.pos.model.RestaurantCheck;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class RestaurantCheckPresenter implements RestaurantCheckConstract.Presenter {

    private RestaurantCheckConstract.View mView;
    private List<RestaurantCheck> restaurantCheckList;

    public RestaurantCheckPresenter(RestaurantCheckConstract.View mView) {
        this.mView = mView;
        new SelectTask().execute();
    }

    @Override
    public void getDataInput() {

    }

    @Override
    public void showKeyboard() {
        mView.showKeyboard("Restaurant Check","0.00","00",SessionApp.KeyboardType.TIEN);
    }


    private class SelectTask extends AsyncTask<Void,Void,List<Configuration>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Configuration> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.configuration where keyid like '%RestaurantCheckValue%'");
            ArrayList<Configuration> config =  new Gson().fromJson(js.toString(), new TypeToken<List<Configuration>>(){}.getType());
            return config;
        }

        @Override
        protected void onPostExecute(List<Configuration> config) {
            super.onPostExecute(config);
            restaurantCheckList = new ArrayList<>();
            restaurantCheckList.add(0,new RestaurantCheck(Util.getStringFromResouce(R.string.Free_price)));
            for(Configuration ob: config){
                if(ob.getKeyid().equals("RestaurantCheckValue1")){
                    restaurantCheckList.add(new RestaurantCheck(ob.getValue()));
                } else if(ob.getKeyid().equals("RestaurantCheckValue2")){
                    restaurantCheckList.add(new RestaurantCheck(ob.getValue()));
                } else if(ob.getKeyid().equals("RestaurantCheckValue3")){
                    restaurantCheckList.add(new RestaurantCheck(ob.getValue()));
                } else if(ob.getKeyid().equals("RestaurantCheckValue4")){
                    restaurantCheckList.add(new RestaurantCheck(ob.getValue()));
                } else if(ob.getKeyid().equals("RestaurantCheckValue5")){
                    restaurantCheckList.add(new RestaurantCheck(ob.getValue()));
                }
            }
            mView.showRestaurantCheckList(restaurantCheckList);
        }
    }
}
