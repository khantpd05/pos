package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Connection;
import com.example.abc.pos.model.Spice;
import com.example.abc.pos.utils.SharedPrefs;

import java.util.List;

public class SpiceAdapter extends RecyclerView.Adapter<SpiceAdapter.ViewHolder> {
    private RecyclerView rvList;
    private List<Spice> list;
    private ItemClickListener mClickListener;
    private int selectedPosition = -1;

    public SpiceAdapter(RecyclerView rc, Context context, List<Spice> list, ItemClickListener clickListener) {
        rvList = rc;
        this.list = list;
        mClickListener = clickListener;
    }

    public void setNewDatas(List<Spice> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spice_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tv_name.setText(list.get(position).getName());
        holder.tv_price.setText(String.format("%.2f", list.get(position).getPrice()));
        holder.ll_wrap.setTag(position);
        if (selectedPosition != -1) {
            holder.ll_wrap.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else {

        }


        if (position == selectedPosition) {
            holder.ll_wrap.setBackgroundColor(Color.parseColor("#33FF80"));
        } else {
            holder.ll_wrap.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView tv_name, tv_price;
        LinearLayout ll_wrap;

        ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            ll_wrap = itemView.findViewById(R.id.ll_wrap);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {

            View containingView = rvList.findContainingItemView(view);
            int position = rvList.getChildAdapterPosition(containingView);
            if (position == RecyclerView.NO_POSITION) {
                return;
            }


            if (mClickListener != null) {
                mClickListener.onItemClick(position, list.get(position));
            }


            itemCheckChanged(ll_wrap);
        }

        @Override
        public boolean onLongClick(View v) {

            View containingView = rvList.findContainingItemView(v);
            int position = rvList.getChildAdapterPosition(containingView);

            if (mClickListener != null) {
                mClickListener.onItemLongClick(position, list.get(position));
            }

            itemCheckChanged(ll_wrap);
            return false;
        }
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

public void resetSelected(){
    selectedPosition = -1;
    notifyDataSetChanged();
}

    public interface ItemClickListener {
        void onItemClick(int position, Spice spice);
        void onItemLongClick(int position, Spice spice);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
