package com.example.abc.pos.screen.change_order_to_table;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.TablesAdapter;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.screen.change_order_to_customer_position.ChangeOrderCustomerPositionFragment;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangeOrderToTableFragment extends BaseFragment implements ChangeOrderToTableContract.View,
        TablesAdapter.ItemClickListener, View.OnClickListener, ChangeOrderCustomerPositionFragment.OnPositionClick, ChangeOrderCustomerPositionFragment.OnChangeOrderToTableSuccessful {

    private RecyclerView rc_table;

    private TablesAdapter table_adapter;

    private ChangeOrderToTablePresenter presenter;

    private TextView btn_okay;

    private Table table;
    private Table table_new;

    private LinearLayout btn_exit;

    private String username;
    private String currentTable;
    private String selectedTable;
    private WapriManager.taichohaymangve taiChoHayMangVe;
    private int soghengoi;
    private List<OrdersTemp> ordersTempsWillChange;

    private OnChangeOrderToTableSuccessful listener;

    @Override
    public void onChangeOrderToTableSuccessfulListener(List<OrdersTemp> ordersTempsWillChange) {
        listener.onChangeOrderToTableSuccessfulListener(ordersTempsWillChange);
    }

    public interface OnChangeOrderToTableSuccessful {
        void onChangeOrderToTableSuccessfulListener(List<OrdersTemp> ordersTempsWillChange);
    }

    public ChangeOrderToTableFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ChangeOrderToTableFragment(List<OrdersTemp> ordersTempsWillChange, String username, String currentTable, OnChangeOrderToTableSuccessful listener) {
        // Required empty public constructor
        this.ordersTempsWillChange = ordersTempsWillChange;
        this.username = username;
        this.currentTable = currentTable;
        this.listener = listener;
    }

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_change_to_table;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_to_table, container, false);

        rc_table = view.findViewById(R.id.rc_table);

        rc_table.setLayoutManager(new GridLayoutManager(getContext(),4));
        table_adapter = new TablesAdapter(getContext(),new ArrayList<Table>(),this);
        rc_table.setAdapter(table_adapter);

        btn_okay = view.findViewById(R.id.btn_okay);
        btn_exit = view.findViewById(R.id.btn_exit);
        btn_okay.setOnClickListener(this);
        btn_exit.setOnClickListener(this);

        presenter = new ChangeOrderToTablePresenter(this);
        presenter.getEmptyTable(username,currentTable);

        return view;
    }


    @Override
    public void showListTable(List<Table> tables) {
        table_adapter.setNewDatas(tables);
    }

    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
        Util.popFragment(getActivity());
    }

    @Override
    public void goToCustomerPosition(List<Integer> positionsHaveOrder) {
        ((BaseActivity) getActivity()).pushFragmentNotReplace(new ChangeOrderCustomerPositionFragment(ordersTempsWillChange,positionsHaveOrder,soghengoi,selectedTable,taiChoHayMangVe,this,this), true);
    }

    @Override
    public void onItemClick(View view, Table table) {
        SessionApp.global_destination_table = table;
        selectedTable = table.getName();
        taiChoHayMangVe = table.getTaichohaymangve();
        soghengoi = table.getSoghengoi();
        btn_okay.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_okay:
                presenter.goToCustomerPosition(selectedTable);
                break;
            case R.id.btn_exit:
                Util.popFragment(getActivity());
                break;
        }
    }

    @Override
    public void onCustomerPositionClickListener() {
        if(getActivity()!=null)
        ((BaseActivity)getActivity()).popFragment();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
