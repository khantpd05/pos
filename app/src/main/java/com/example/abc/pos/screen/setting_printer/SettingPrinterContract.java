package com.example.abc.pos.screen.setting_printer;

public interface SettingPrinterContract {
    interface View{
        void exit();
    }

    interface Presenter{
        void updateNewConfig();
        void exit();
    }
}
