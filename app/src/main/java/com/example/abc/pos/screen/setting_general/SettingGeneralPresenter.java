package com.example.abc.pos.screen.setting_general;

import android.databinding.ObservableField;
import android.os.AsyncTask;
import android.support.annotation.IntDef;

import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.viewmodel.GeneralConfiguration;
import com.example.abc.pos.utils.SharedPrefs;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;

public class SettingGeneralPresenter implements SettingGeneralContract.Presenter {
    private SettingGeneralContract.View mView;
    private final String KEY_SETTING_GENERAL = "setting_general";
    public GeneralConfiguration viewModel;
    public ObservableField<Boolean> isHavePermission = new ObservableField<>(false);

    public static final int NOTHING = 0;
    public static final int GUTSCHEIN = 1;
    public static final int RESTAURANT_CHECK = 2;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({NOTHING, GUTSCHEIN, RESTAURANT_CHECK})
    public @interface OperationsDef { }


    public SettingGeneralPresenter(SettingGeneralContract.View mView) {
        this.mView = mView;
        if(SessionApp.admin_password.equals("11032002")) isHavePermission.set(true);
        else isHavePermission.set(false);

        loadData();
    }

    @Override
    public void loadData() {
        GeneralConfiguration configuration = SharedPrefs.getInstance().get(KEY_SETTING_GENERAL, GeneralConfiguration.class);
        if (configuration != null) {
            viewModel = configuration;
        } else {
            GeneralConfiguration config = new GeneralConfiguration();
            config.tuDongInBep.set(true);
            config.canhBaoThanhToan.set(true);
            config.huyThanhToan.set(true);
            config.tuDongKetNoi.set(true);

            viewModel = config;

            SharedPrefs.getInstance().put(KEY_SETTING_GENERAL, config);
        }
        getListValue(NOTHING);
    }

    @Override
    public void getListValue(@OperationsDef  int type) {
        new SelectTask(type).execute();
    }

    @Override
    public void goGutscheinSetting() {
        getListValue(GUTSCHEIN);
    }

    @Override
    public void goRestaurantSetting() {
        getListValue(RESTAURANT_CHECK);
    }

    @Override
    public void updateNewConfig() {
        SharedPrefs.getInstance().put(KEY_SETTING_GENERAL, viewModel);
    }

    @Override
    public void exit() {
        mView.exit();
    }


    private class SelectTask extends AsyncTask<Void, Void, Void> {

        int type;

        public SelectTask(int type) {
            this.type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.configuration where keyid like '%GouponValue%' or keyid like '%RestaurantCheckValue%'");
            ArrayList<Configuration> config = new Gson().fromJson(js.toString(), new TypeToken<List<Configuration>>() {
            }.getType());


            viewModel.gutscheins.set(new ArrayList<Configuration>());
            viewModel.restaurantCheck.set(new ArrayList<Configuration>());

            for (int i = 0; i < 5; i++) {
                int iGuts = 0;
                Configuration tempGuts = null;
                int iRes = 0;
                Configuration tempRes = null;
                for (Configuration ob : config) {
                    if (ob.getKeyid().equals("GouponValue" + (i + 1))) {
                        iGuts = 1;
                        tempGuts = ob;
                    }

                    if (ob.getKeyid().equals("RestaurantCheckValue" + (i + 1))) {
                        iRes = 1;
                        tempRes = ob;
                    }
                }

                if (iGuts == 0) {
                    viewModel.gutscheins.get().add(new Configuration(-1, "GouponValue" + (i + 1), "0.00"));
                } else {
                    viewModel.gutscheins.get().add(tempGuts);
                }


                if (iRes == 0) {
                    viewModel.restaurantCheck.get().add(new Configuration(-1, "RestaurantCheckValue" + (i + 1), "0.00"));
                } else {
                    viewModel.restaurantCheck.get().add(tempRes);
                }

            }
            SharedPrefs.getInstance().put(KEY_SETTING_GENERAL, viewModel);
            return null;
        }

        @Override
        protected void onPostExecute(Void config) {
            super.onPostExecute(config);
            if(type == 1){
                mView.goGutscheinSetting(viewModel.gutscheins.get());
            }else if(type == 2){
                mView.goRestaurantSetting(viewModel.restaurantCheck.get());
            }

            mView.dismissLoading();
        }
    }
}
