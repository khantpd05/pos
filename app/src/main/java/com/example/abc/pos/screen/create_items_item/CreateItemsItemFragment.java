package com.example.abc.pos.screen.create_items_item;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.colorpciker.ColorPickerDialog;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.screen.setting_menu.SettingMenuFragment;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import java.util.List;

import top.defaults.colorpicker.ColorPickerPopup;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class CreateItemsItemFragment extends BaseFragment implements CreateItemsItemConstract.View, View.OnClickListener {

    private Spinner spn_food_type;
    private TextView btn_colorPicker, tv_title;
    private EditText edt_name, edt_price, edt_items_id;
    private LinearLayout ll_wrap_itemsId,ll_exit;
    private Button btn_add;
    private String colorCode = "";
    private CreateItemsItemPresenter presenter;
    private static final String KEY_MENU_ITEM = "menu_item";
    private Items item;
    private int nhom_hang;
    private String loai_mon_an;

    public CreateItemsItemFragment() {
        // Required empty public constructor
    }

    public CreateItemsItemFragment(int nhom_hang, String loai_mon_an) {
        // Required empty public constructor
        this.nhom_hang = nhom_hang;
        this.loai_mon_an = loai_mon_an;
    }

    public static CreateItemsItemFragment newInstance(Items param1) {
        CreateItemsItemFragment fragment = new CreateItemsItemFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_MENU_ITEM, param1);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_create_items_item;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            item = (Items) getArguments().getSerializable(KEY_MENU_ITEM);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_items_item, container, false);

        spn_food_type = view.findViewById(R.id.spn_food_type);

        tv_title = view.findViewById(R.id.tv_title);


        btn_colorPicker = view.findViewById(R.id.btn_colorPicker);
        btn_colorPicker.setOnClickListener(this);
        btn_colorPicker.setBackgroundColor(2136303871);

        btn_add = view.findViewById(R.id.btn_add);
        btn_add.setOnClickListener(this);

        edt_name = view.findViewById(R.id.edt_name);

        edt_price = view.findViewById(R.id.edt_price);

        edt_items_id = view.findViewById(R.id.edt_items_id);

        ll_wrap_itemsId = view.findViewById(R.id.ll_wrap_itemsId);

        ll_exit = view.findViewById(R.id.ll_exit);
        ll_exit.setOnClickListener(this);

        presenter = new CreateItemsItemPresenter(this,loai_mon_an);
        presenter.loadData();
        initUpdateField();

        return view;
    }

    private AdapterView.OnItemSelectedListener OnCatSpinnerCL = new AdapterView.OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (((TextView) parent.getChildAt(0)) != null)
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);

        }

        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public void showSpinnerData(List<String> datas) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_food_type_item, datas);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_food_type.setAdapter(adapter);
        spn_food_type.setOnItemSelectedListener(OnCatSpinnerCL);
    }

    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }

    @Override
    public void initUpdateField() {
        if (item != null) {
            btn_add.setText(App.Companion.self().getText(R.string.update));
            btn_add.setBackgroundResource(0);
            edt_items_id.setText(item.getId());
            edt_items_id.setEnabled(true);
            tv_title.setText(App.Companion.self().getText(R.string.Edit));
            edt_name.setText(item.getTenhang());
            edt_price.setText(item.getGiale1()+"");
            if (item.getColor() != null && !item.getColor().isEmpty()) {
                btn_colorPicker.setBackgroundColor(Integer.valueOf(item.getColor()));
            }
            Util.selectSpinnerItemByValue(spn_food_type, Util.getValueFromKey(Util.getEnumBoPhanPhucVu(), item.getBophanphucvu()));
        }
    }

    @Override
    public void goBack() {
        Util.popFragment(getActivity());
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_colorPicker: {
                new ColorPickerPopup.Builder(getContext())
                        .initialColor(2136303871) // Set initial color
                        .showIndicator(true)
                        .showValue(false)
                        .build()
                        .show(btn_colorPicker, new ColorPickerPopup.ColorPickerObserver() {
                            @Override
                            public void onColorPicked(int color) {
                                btn_colorPicker.setBackgroundColor(color);
                                colorCode = String.valueOf(color);

                            }

                            @Override
                            public void onColor(int color, boolean fromUser) {

                            }
                        });
                break;
            }

            case R.id.btn_add: {
                String giaTien = edt_price.getText().toString();
                if(giaTien.isEmpty()){
                    giaTien = "0.00";
                }
                if (item != null) {
                    if (colorCode != null && !colorCode.isEmpty()) {
                        item.setColor(colorCode);
                    }else{
                        item.setColor(item.getColor());
                    }
                    String old_id = item.getId();
                    if(edt_items_id.getText().toString().trim().isEmpty() || edt_name.getText().toString().trim().isEmpty()){
                        showAlert(Util.getStringFromResouce(R.string.please_enter_full_information));
                    }else{
                        item.setId(edt_items_id.getText().toString());
                        item.setTenhang(edt_name.getText().toString());
                        item.setBonname(edt_name.getText().toString());
                        String responsible_type = (String) Util.getKeyFromValue(Util.getEnumBoPhanPhucVu(), spn_food_type.getSelectedItem().toString());
                        item.setBophanphucvu(responsible_type);
                        item.setGiale1(Double.parseDouble(giaTien));
                        item.setGialemangve1(Double.parseDouble(giaTien));

                        presenter.updateItem(old_id,item);
                    }
                } else {
                    if(colorCode==null || colorCode.equals(""))
                    colorCode = "2136303871";
                    String responsible_type = (String) Util.getKeyFromValue(Util.getEnumBoPhanPhucVu(), spn_food_type.getSelectedItem().toString());
                    presenter.addNewItem(edt_items_id.getText().toString(), nhom_hang, edt_name.getText().toString(), Double.parseDouble(giaTien), colorCode, responsible_type);
                }
                break;
            }
            case R.id.ll_exit:
                Util.popFragment(getActivity());
                break;
        }
    }
}
