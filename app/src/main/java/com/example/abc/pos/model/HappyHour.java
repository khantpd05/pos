package com.example.abc.pos.model;

public class HappyHour {
    private int id;
    private String thu2;
    private String thu3;
    private String thu4;
    private String thu5;
    private String thu6;
    private String thu7;
    private String chunhat;
    private String ngaybatdau;
    private String ngayketthuc;
    private int giobatdau;
    private int phutbatdau;
    private int gioketthuc;
    private int phutketthuc;
    private int phantramgiam;
    private String apdungtatcamathang;
    private String orderfrom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThu2() {
        return thu2;
    }

    public void setThu2(String thu2) {
        this.thu2 = thu2;
    }

    public String getThu3() {
        return thu3;
    }

    public void setThu3(String thu3) {
        this.thu3 = thu3;
    }

    public String getThu4() {
        return thu4;
    }

    public void setThu4(String thu4) {
        this.thu4 = thu4;
    }

    public String getThu5() {
        return thu5;
    }

    public void setThu5(String thu5) {
        this.thu5 = thu5;
    }

    public String getThu6() {
        return thu6;
    }

    public void setThu6(String thu6) {
        this.thu6 = thu6;
    }

    public String getThu7() {
        return thu7;
    }

    public void setThu7(String thu7) {
        this.thu7 = thu7;
    }

    public String getChunhat() {
        return chunhat;
    }

    public void setChunhat(String chunhat) {
        this.chunhat = chunhat;
    }

    public String getNgaybatdau() {
        return ngaybatdau;
    }

    public void setNgaybatdau(String ngaybatdau) {
        this.ngaybatdau = ngaybatdau;
    }

    public String getNgayketthuc() {
        return ngayketthuc;
    }

    public void setNgayketthuc(String ngayketthuc) {
        this.ngayketthuc = ngayketthuc;
    }

    public int getGiobatdau() {
        return giobatdau;
    }

    public void setGiobatdau(int giobatdau) {
        this.giobatdau = giobatdau;
    }

    public int getPhutbatdau() {
        return phutbatdau;
    }

    public void setPhutbatdau(int phutbatdau) {
        this.phutbatdau = phutbatdau;
    }

    public int getGioketthuc() {
        return gioketthuc;
    }

    public void setGioketthuc(int gioketthuc) {
        this.gioketthuc = gioketthuc;
    }

    public int getPhutketthuc() {
        return phutketthuc;
    }

    public void setPhutketthuc(int phutketthuc) {
        this.phutketthuc = phutketthuc;
    }

    public int getPhantramgiam() {
        return phantramgiam;
    }

    public void setPhantramgiam(int phantramgiam) {
        this.phantramgiam = phantramgiam;
    }

    public String getApdungtatcamathang() {
        return apdungtatcamathang;
    }

    public void setApdungtatcamathang(String apdungtatcamathang) {
        this.apdungtatcamathang = apdungtatcamathang;
    }

    public String getOrderfrom() {
        return orderfrom;
    }

    public void setOrderfrom(String orderfrom) {
        this.orderfrom = orderfrom;
    }
}
