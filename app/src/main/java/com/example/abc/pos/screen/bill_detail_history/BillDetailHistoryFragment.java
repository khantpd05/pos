package com.example.abc.pos.screen.bill_detail_history;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.BillAdapter;
import com.example.abc.pos.adapter.BillDetailAdapter;
import com.example.abc.pos.model.Orders;
import com.example.abc.pos.model.OrdersDetail;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.utils.Util;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("ValidFragment")
public class BillDetailHistoryFragment extends Fragment implements BillDetailHistoryContract.View, View.OnClickListener {

    private BillDetailAdapter adapter;
    private BillDetailHistoryPresenter presenter;
    private ExpandableListView expand_list_bill;
    private LinearLayout ll_extra_feature, ll_exit;
    private TextView tv_table_name, tv_bill_code, tv_time, tv_summer;
    private Orders order;

    public BillDetailHistoryFragment() {
        // Required empty public constructor
    }

    public BillDetailHistoryFragment(Orders order) {
        // Required empty public constructor
        this.order = order;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bill_detail_history, container, false);

        if(savedInstanceState!=null){
            Util.popFragment(getActivity());
        }else{
            expand_list_bill = view.findViewById(R.id.expand_list_bill);
            adapter = new BillDetailAdapter(getContext(), new ArrayList<OrdersDetail>());
            expand_list_bill.setAdapter(adapter);

            ll_extra_feature = view.findViewById(R.id.ll_extra_feature);
            ll_exit = view.findViewById(R.id.ll_exit);

            ll_exit.setOnClickListener(this);

            tv_table_name = view.findViewById(R.id.tv_table_name);
            tv_bill_code = view.findViewById(R.id.tv_bill_code);
            tv_time = view.findViewById(R.id.tv_time);
            tv_summer = view.findViewById(R.id.tv_summer);

            presenter = new BillDetailHistoryPresenter(this,order);
            presenter.showBillInfo();
            presenter.loadBillHistoryDetailList(order.getId());
        }


        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }



    @Override
    public void showBillDetailHistory(List<OrdersDetail> ordersDetailList) {
        adapter.setNewDatas(ordersDetailList);
    }

    @Override
    public void showBillInfo(String tableName, String billCode, String time, String summer) {
        tv_table_name.setText(Util.getStringFromResouce(R.string.table_name)+": "+tableName);
        tv_bill_code.setText(Util.getStringFromResouce(R.string.code)+" "+billCode);
        tv_time.setText(time);
        tv_summer.setText(summer);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ll_exit:
                Util.popFragment(getActivity());
                break;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("key","temp");
    }
}
