package com.example.abc.pos.utils;

import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.ExtraItem;
import com.example.abc.pos.model.OrdersTemp;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import static com.example.abc.pos.base.WapriManager.configuration;
import static com.example.abc.pos.base.WapriManager.printerConfiguration;

public class PrinterUtils {


    public static void Print(final List<OrdersTemp> ordersTemps, final String printerType){
        try {
            doPrint(ordersTemps,printerType);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void PrintCancel(final List<OrdersTemp> ordersTemps, final String printerType){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    doCancelPrint(ordersTemps,printerType);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private PrinterUtils(){



    }

    private static void doPrint(List<OrdersTemp> ordersTemps, String printerType) throws IOException {
        // gia lap thiet lap
        int pagePrinter = Integer.parseInt(printerConfiguration().maxChar.get());
        boolean isGhiChu = true;
        // khoi tao
        Socket socket = null;
        if(printerType.equals("Bep")){
            socket = new Socket(printerConfiguration().ip_bep.get(), Integer.parseInt(printerConfiguration().port_bep.get()));
        }else if(printerType.equals("Nuoc")){
            socket = new Socket(printerConfiguration().ip_nuoc.get(), Integer.parseInt(printerConfiguration().port_nuoc.get()));
        }else if(printerType.equals("Sushi")){
            socket = new Socket(printerConfiguration().ip_sushi.get(), Integer.parseInt(printerConfiguration().port_sushi.get()));
        }


        PrintWriter oStream = new PrintWriter(socket.getOutputStream());

        // format

        // căn lề
        CodePrint.setCharset(socket.getOutputStream(), Integer.parseInt(printerConfiguration().charSet.get()));
        CodePrint.feed(socket.getOutputStream(), 2);
        CodePrint.canLe(socket.getOutputStream(), 1);
        CodePrint.setWidthHeight(socket.getOutputStream(), 16);

        // Header (quay bep or nuoc, ten nhan vien, ngay gio)
        String info = ordersTemps.get(0).getUsername() + " " + Util.getDate("dt");
        String stringTop = CodePrint.MakePrintString2(pagePrinter, ordersTemps.get(0).getBophanphucvu(), info);
        CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), stringTop);
        CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");
        CodePrint.feed(socket.getOutputStream(), 2);

        // Ten ban (Title)
        CodePrint.setWidthHeight(socket.getOutputStream(), 56);
        CodePrint.setBold(socket.getOutputStream(), true);
        CodePrint.canLe(socket.getOutputStream(), 1);
        CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), ordersTemps.get(0).getBanso());
        CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");

        // set font style to default (in dam, width height)
        CodePrint.setBold(socket.getOutputStream(), false);
        CodePrint.setNormalWidthHeight(socket.getOutputStream());

        // decorate (--------)
        CodePrint.canLe(socket.getOutputStream(), 0);
        String gachChan = CodePrint.FormatString(pagePrinter, "-");
        CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), gachChan);
        CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");


        // Chi tiet order
        for (OrdersTemp ordersTemp : ordersTemps) {
            // so luong
            CodePrint.setWidthHeight(socket.getOutputStream(), 56);
            CodePrint.setBold(socket.getOutputStream(), true);

            String soLuongVaMa = ordersTemp.getitems_id() + " x " + ordersTemp.getSoluong();
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), soLuongVaMa);
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");
            CodePrint.setBold(socket.getOutputStream(), false);
            // ten hang
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), ordersTemp.getTenhang());
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");
            CodePrint.setNormalWidthHeight(socket.getOutputStream());
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");
            // có ghi chứ hay không
            if (ordersTemp.getExtraItemList().size()>0 ) {
                CodePrint.setWidthHeight(socket.getOutputStream(), 32);
                CodePrint.KhoangCachSpacing(socket.getOutputStream(),40);
                for(int i = 0; i < ordersTemp.getExtraItemList().size(); i++){
                    ExtraItem extraItem = ordersTemp.getExtraItemList().get(i);
                    String khoangCach = "   ";
                    // gia vi ghi chu
                    if(extraItem.getFlag() ==  WapriManager.FeatureOrder.PHAN_AN){
                        khoangCach = khoangCach + extraItem.getSoluong() + " x ";
                    }
                    CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), khoangCach + extraItem.getName().replace("+ ",""));
                    CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");

                }
                CodePrint.KhoangCachSpacing(socket.getOutputStream(),30);
                CodePrint.setNormalWidthHeight(socket.getOutputStream());
            }

            CodePrint.setNormalWidthHeight(socket.getOutputStream());

            // gach chan
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), gachChan);
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");
        }
        // end format
        CodePrint.feed(socket.getOutputStream(), 7);
        CodePrint.setCharset(socket.getOutputStream(),  Integer.parseInt(printerConfiguration().charSet.get()));
        CodePrint.cutPrint(socket.getOutputStream());
        CodePrint.beepPrint(socket.getOutputStream());
        CodePrint.clearPrint(socket.getOutputStream());
        oStream.close();
        socket.close();
    }


    private static void doCancelPrint(List<OrdersTemp> ordersTemps, String printerType) throws IOException {
        // gia lap thiet lap
        int pagePrinter = Integer.parseInt(printerConfiguration().maxChar.get());
        boolean isGhiChu = true;
        // khoi tao
        Socket socket = null;
        if(printerType.equals("Bep")){
            socket = new Socket(printerConfiguration().ip_bep.get(), Integer.parseInt(printerConfiguration().port_bep.get()));
        }else if(printerType.equals("Nuoc")){
            socket = new Socket(printerConfiguration().ip_nuoc.get(), Integer.parseInt(printerConfiguration().port_nuoc.get()));
        }else if(printerType.equals("Sushi")){
            socket = new Socket(printerConfiguration().ip_sushi.get(), Integer.parseInt(printerConfiguration().port_sushi.get()));
        }


        PrintWriter oStream = new PrintWriter(socket.getOutputStream());

        // format

        // căn lề
        CodePrint.setCharset(socket.getOutputStream(), Integer.parseInt(printerConfiguration().charSet.get()));
        CodePrint.feed(socket.getOutputStream(), 2);
        CodePrint.canLe(socket.getOutputStream(), 1);
        CodePrint.setWidthHeight(socket.getOutputStream(), 16);

        // Header (quay bep or nuoc, ten nhan vien, ngay gio)
        String info = ordersTemps.get(0).getUsername() + " " + Util.getDate("dt");
        String stringTop = CodePrint.MakePrintString2(pagePrinter, "STORNIERUNG", info);
        CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), stringTop);
        CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");
        CodePrint.feed(socket.getOutputStream(), 2);

        // Ten ban (Title)
        CodePrint.setWidthHeight(socket.getOutputStream(), 56);
        CodePrint.setBold(socket.getOutputStream(), true);
        CodePrint.canLe(socket.getOutputStream(), 1);
        CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), ordersTemps.get(0).getBanso());
        CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");

        // set font style to default (in dam, width height)
        CodePrint.setBold(socket.getOutputStream(), false);
        CodePrint.setNormalWidthHeight(socket.getOutputStream());

        // decorate (--------)
        CodePrint.canLe(socket.getOutputStream(), 0);
        String gachChan = CodePrint.FormatString(pagePrinter, "-");
        CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), gachChan);
        CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");


        // Chi tiet order
        for (OrdersTemp ordersTemp : ordersTemps) {
            // so luong
            CodePrint.setWidthHeight(socket.getOutputStream(), 56);
            CodePrint.setBold(socket.getOutputStream(), true);

            String soLuongVaMa = ordersTemp.getitems_id() + " x " + ordersTemp.getSoluong();
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), soLuongVaMa);
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");
            CodePrint.setBold(socket.getOutputStream(), false);
            // ten hang
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), ordersTemp.getTenhang());
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");
            CodePrint.setNormalWidthHeight(socket.getOutputStream());
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");



            // có ghi chứ hay không
            if (ordersTemp.getExtraItemList().size()>0 ) {
                CodePrint.setWidthHeight(socket.getOutputStream(), 32);
                CodePrint.KhoangCachSpacing(socket.getOutputStream(),40);
                for(int i = 0; i < ordersTemp.getExtraItemList().size(); i++){
                    ExtraItem extraItem = ordersTemp.getExtraItemList().get(i);
                    String khoangCach = "   ";
                    // gia vi ghi chu
                    if(extraItem.getFlag() ==  WapriManager.FeatureOrder.PHAN_AN){
                        khoangCach = khoangCach + extraItem.getSoluong() + " x ";
                    }
                    CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), khoangCach + ordersTemp.getExtraItemList().get(i).getName());
                    CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");

                }
                CodePrint.KhoangCachSpacing(socket.getOutputStream(),30);
                CodePrint.setNormalWidthHeight(socket.getOutputStream());
                CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");
            }

            // ten hang
            String khoangCach = "   ";
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(),khoangCach+ ordersTemp.getLydohuyxoa());
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");
            CodePrint.setNormalWidthHeight(socket.getOutputStream());
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");

//            // Lý do hủy
//            String khoangCach = "   ";
//            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), khoangCach + ordersTemp.getLydohuyxoa());
//            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");
//            CodePrint.feed(socket.getOutputStream(), 2);
//            CodePrint.KhoangCachSpacing(socket.getOutputStream(),0);
//            CodePrint.setNormalWidthHeight(socket.getOutputStream());

            // gach chan
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), gachChan);
            CodePrint.converStringAndPrintBySocket(socket.getOutputStream(), "\n");
        }
        // end format
        CodePrint.feed(socket.getOutputStream(), 7);
        CodePrint.setCharset(socket.getOutputStream(),  Integer.parseInt(printerConfiguration().charSet.get()));
        CodePrint.cutPrint(socket.getOutputStream());
        CodePrint.beepPrint(socket.getOutputStream());
        CodePrint.clearPrint(socket.getOutputStream());
        oStream.close();
        socket.close();
    }
}
