package com.example.abc.pos.screen.spice;

import android.os.AsyncTask;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.ItemsMontuychon;
import com.example.abc.pos.model.Spice;
import com.example.abc.pos.model.Tieudedotdat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class SpicePresenter implements SpiceContract.Presenter {
    private SpiceContract.View mView;
    private List<Spice> spiceList;
    private List<ItemsMontuychon> spiceItemList;
    public SpicePresenter(SpiceContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void loadData(List<Spice> datas) {
        if(datas!=null && !datas.isEmpty()){
            spiceList = datas;
            mView.showListSpice(spiceList);
        }else {
            spiceList = new ArrayList<>();
        }


        new SelectSpiceItemTask("1").execute();
    }

    @Override
    public void getAllPageSpiceItem(String id) {
        new SelectSpiceItemTask(id).execute();
    }

    @Override
    public void getSpiceList() {
        mView.getSpiceList(spiceList);
    }


    @Override
    public void showKeyboard(String title) {
        mView.showKeyboard(title);
    }

    @Override
    public void updatePriceItem(int positionSelected, String data) {
        spiceList.get(positionSelected).setPrice(Double.parseDouble(data));
        mView.showListSpice(spiceList);
    }

    @Override
    public void deletePriceItem(int positionSelected) {
        spiceList.remove(positionSelected);
        mView.showListSpice(spiceList);
    }

    @Override
    public void addNewItem(ItemsMontuychon item,String data) {
        if(!data.trim().isEmpty()){
            if(spiceList.size()<10){
                Spice spice = new Spice();
                spice.setName(data);

                if(item.getGiatc1()>0 && item.getTextdisplay().equals(data)){
                    spice.setPrice(item.getGiatc1());
                    spiceList.add(spice);
                    mView.showListSpice(spiceList);
                    mView.setEditTextEmpty();
                }else{
                    spiceList.add(spice);
                    mView.showListSpice(spiceList);
                    mView.setEditTextEmpty();
                    mView.doClickItem(spiceList.size()-1);
                }
            }else{
                mView.showAlert(App.Companion.self().getString(R.string.the_maximum_spice));
            }
        }else {
            mView.showAlert(App.Companion.self().getString(R.string.enter_spice));
        }
    }

    private class SelectSpiceItemTask extends AsyncTask<Void,Void,List<ItemsMontuychon>> {
        String id;

        public SelectSpiceItemTask(String id) {
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected List<ItemsMontuychon> doInBackground(Void... voids) {
            spiceItemList =  MySqlHandleUtils.selectSpiceStament(id);
            return  spiceItemList;
        }

        @Override
        protected void onPostExecute(List<ItemsMontuychon> itemsMontuychons) {
            super.onPostExecute(itemsMontuychons);
            mView.dismissLoading();
            if(itemsMontuychons!=null && !itemsMontuychons.isEmpty()) mView.showListSpiceItem(itemsMontuychons);
            else {
                List<ItemsMontuychon>  spiceItemList = new ArrayList<>();
                for(int i =0 ; i <6 ; i++){
                    for(int j = 0 ; j < 12; j++){
                        ItemsMontuychon ob = new ItemsMontuychon();
                        spiceItemList.add(ob);
                    }
                }
                mView.showListSpiceItem(spiceItemList);
            }
        }
    }
}
