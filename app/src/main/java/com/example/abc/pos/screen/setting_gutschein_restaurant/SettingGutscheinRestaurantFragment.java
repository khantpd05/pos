package com.example.abc.pos.screen.setting_gutschein_restaurant;


import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.databinding.FragmentSettingGutscheinRestaurantBinding;
import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;
import com.example.abc.pos.viewmodel.GeneralConfiguration;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingGutscheinRestaurantFragment extends BaseFragment implements SettingGutscheinRestaurantContract.View{

    private SettingGutscheinRestaurantPresenter presenter;
    private List<Configuration> datas;
    public GeneralConfiguration config;
    public int type;
    public SettingGutscheinRestaurantFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public SettingGutscheinRestaurantFragment(GeneralConfiguration config, List<Configuration> datas, int type) {
        this.config = config;
        this.datas = datas;
        this.type = type;
    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_setting_gutschein_restaurant;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentSettingGutscheinRestaurantBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_setting_gutschein_restaurant,container,false);

        presenter = new SettingGutscheinRestaurantPresenter(this);

        binding.setPresenter(presenter);

        presenter.getData(datas);

        return binding.getRoot();
    }


    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }

    @Override
    public int getTypeOfList() {
        return type;
    }


    @Override
    public GeneralConfiguration getConfig() {
        return config;
    }

    @Override
    public void exit() {
        Util.popFragment(getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
