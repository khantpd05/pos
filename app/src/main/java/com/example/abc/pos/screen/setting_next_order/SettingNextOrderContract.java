package com.example.abc.pos.screen.setting_next_order;

import com.example.abc.pos.model.Tieudedotdat;

import java.util.List;

public interface SettingNextOrderContract {
    interface View{
        void showNextOrderList(List<Tieudedotdat> datas);
        void showLoading();
        void dismissLoading();
        void showCustomDialog(Tieudedotdat ob);
        void showAlert(String data);
        void hideEditStuff();
    }
    interface Presenter{
        void loadData();
        void deleteRow(int id);
        void updateOrderTask(Tieudedotdat ob);
        void insertOrderTask(String name);
    }
}
