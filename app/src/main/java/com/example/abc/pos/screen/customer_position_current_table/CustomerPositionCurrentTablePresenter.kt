package com.example.abc.pos.screen.change_order_to_customer_position

import com.example.abc.pos.base.SessionApp
import java.util.ArrayList

class CustomerPositionCurrentTablePresenter(var view: CustomerPositionCurrentTableContract.View): CustomerPositionCurrentTableContract.Presenter{



    override fun goToCustomerPositionSeparate() {
        view.goToCustomerPositionSeparate()
    }


    override fun loadData(positions: Int) {
        view.showPosition(positions)
    }

}