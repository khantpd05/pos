package com.example.abc.pos.model;

public class LyDoHuyXoa {
    private int id;
    private String name;


    public LyDoHuyXoa() {
    }

    public LyDoHuyXoa(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
