package com.example.abc.pos.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.model.KichCoMonAn;

import java.util.List;

public class KichCoMonAdapter extends RecyclerView.Adapter<KichCoMonAdapter.ViewHolder> {
    private List<KichCoMonAn> list;
    private ItemClickListener mClickListener;
    private Context context;
    private int selectedPosition = -1;

    public KichCoMonAdapter(Context context,List<KichCoMonAn> list, ItemClickListener clickListener) {
        this.list = list;
        this.context = context;
        mClickListener = clickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kich_co_mon, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tv_name.setText(list.get(position).getKichCoMonDisplay());
        holder.tv_price.setText(String.format("%,.2f", list.get(position).getGiaBan()));
        if(list.get(position).getKichCoMon().equals("Nho")){
            holder.img_size.setBackground(context.getResources().getDrawable(R.drawable.small_ic));
        }else if(list.get(position).getKichCoMon().equals("Trung")){
            holder.img_size.setBackground(context.getResources().getDrawable(R.drawable.medium_ic));
        }else{
            holder.img_size.setBackground(context.getResources().getDrawable(R.drawable.large_ic));
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tv_name,tv_price;
        ImageView img_size;
        ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            img_size = itemView.findViewById(R.id.img_size);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (position == RecyclerView.NO_POSITION) {
                return;
            }


            if (mClickListener != null) {
                mClickListener.onItemClick(position, list.get(position));
            }
        }

    }

    public interface ItemClickListener {
        void onItemClick(int position, KichCoMonAn kichCoMon);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
