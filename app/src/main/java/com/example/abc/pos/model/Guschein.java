package com.example.abc.pos.model;

public class Guschein {
    private String value;

    public Guschein(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
