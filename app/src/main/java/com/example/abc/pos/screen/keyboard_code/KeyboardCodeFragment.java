package com.example.abc.pos.screen.keyboard_code;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.databinding.FragmentKeyboardCodeBinding;

import java.util.Locale;

import faranjit.currency.edittext.CurrencyEditText;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class KeyboardCodeFragment extends BottomSheetDialogFragment implements KeyboardCodeConstract.View {
    TextView edt_input;
    public interface OnKeyboardCodePress{
        void onKeyCodePressListener(String data);
    }
    Dialog dialog;
    OnKeyboardCodePress mListener;
    String hint;
    KeyboardCodePresenter presenter;
    SessionApp.KeyboardType keyboardType;

    public KeyboardCodeFragment(){

    }


    public KeyboardCodeFragment(String hint,OnKeyboardCodePress mListener) {
        // Required empty public constructor
        this.mListener = mListener;
        this.hint = hint;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentKeyboardCodeBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_keyboard_code,container,false);
        View view = binding.getRoot();
        ((EditText) view.findViewById(R.id.edt_input)).setRawInputType(InputType.TYPE_CLASS_TEXT);
        ((EditText) view.findViewById(R.id.edt_input)).setTextIsSelectable(true);
        presenter = new KeyboardCodePresenter(this);
        presenter.mViewModel.hint.set(hint);
        binding.setPresenter(presenter);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();

        if (dialog != null) {
            DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
            int height = displayMetrics.heightPixels;
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = (int) (height*0.50);

            Window window = getDialog().getWindow();
            WindowManager.LayoutParams windowParams = window.getAttributes();
            windowParams.dimAmount = 0f;
            windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(windowParams);
        }
        final View view = getView();
        view.post(new Runnable() {
            @Override
            public void run() {
                View parent = (View) view.getParent();
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
                CoordinatorLayout.Behavior behavior = params.getBehavior();
                BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) behavior;
                bottomSheetBehavior.setPeekHeight(view.getMeasuredHeight());
            }
        });
    }

    @Override
    public void getDataInput(String data) {
        mListener.onKeyCodePressListener(data);
    }


}
