package com.example.abc.pos.screen.customer_position


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.view.ContextThemeWrapper
import android.text.style.TtsSpan.ARG_NUMBER
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.example.abc.pos.R
import com.example.abc.pos.base.BaseFragment
import com.example.abc.pos.screen.order.OrderFragment
import com.example.abc.pos.utils.Util
import java.util.ArrayList


@SuppressLint("ValidFragment")
class CustomerPositionFragment : BaseFragment(), CustomerPositionContract.View {
    var positions: Int = 0
    lateinit var positionsHaveOrder: ArrayList<Int>
    lateinit var listener: OnPositionClick

    override fun getFragmentLayoutId(): Int {
        return R.layout.fragment_customer_position
    }
    companion object {
        fun newInstance(positions: Int, positionsHaveOrder: ArrayList<Int>): CustomerPositionFragment {
            val args = Bundle()
            args.putInt("positions", positions)
            args.putIntegerArrayList("positionsHaveOrder", positionsHaveOrder)
            val frag = CustomerPositionFragment()
            frag.arguments = args

            return frag
        }
    }
    fun setValue(positions: Int, positionsHaveOrder: List<Int>, listener: OnPositionClick) {

    }

    interface OnPositionClick {
        fun onCustomerPositionClickListener(position: Int)
    }

    var ll_top: LinearLayout? = null
    var ll_bot: LinearLayout? = null
    var ll_exit: LinearLayout? = null
    var ll_prepared_table: LinearLayout? = null
    var presenter: CustomerPositionContract.Presenter? = null
    lateinit var main_table: TextView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_customer_position, container, false)


        if (getArguments() != null) {
            positions = getArguments()?.getInt("positions")!!
            positionsHaveOrder = getArguments()?.getIntegerArrayList("positionsHaveOrder")!!
        }

        main_table = view.findViewById(R.id.main_table)
        ll_top = view.findViewById(R.id.ll_top)
        ll_bot = view.findViewById(R.id.ll_bot)
        ll_exit = view.findViewById(R.id.ll_exit)

        ll_exit?.setOnClickListener {
            Util.popFragment(activity)
        }

        ll_prepared_table = view.findViewById(R.id.ll_prepared_table)

        presenter = CustomerPositionPresenter(this)
        presenter?.loadData(positions)


        main_table.setOnClickListener {
            presenter?.returnOrder(0)
        }

        listener = OrderFragment.getOrderFragmentListener()

        return view
    }


    override fun showPosition(positions: Int) {
        var halfSize = positions / 2


        if (positionsHaveOrder.size > 0) {
            main_table.setBackground(ContextCompat.getDrawable(context!!, R.drawable.bg_radius_all_position))
        } else {
            main_table.setBackground(ContextCompat.getDrawable(context!!, R.drawable.bg_radius_customer_position))
        }

        for (i in 0 until halfSize) {
            var k = 0
            for (j in 0 until positionsHaveOrder.size) {
                if ((i + 1) == positionsHaveOrder[j]) {
                    k = positionsHaveOrder[j]
                    break
                }
            }
            if (k != 0) {
                createNewButton(ll_top, i, R.style.TextCustomerPositionGreen)
            } else {
                createNewButton(ll_top, i, R.style.TextCustomerPosition)
            }
        }


        for (i in halfSize until positions) {
            var k = 0
            for (j in 0 until positionsHaveOrder.size) {
                if (i + 1 == positionsHaveOrder[j]) {
                    k = positionsHaveOrder[j]
                }
            }
            if (k != 0) {
                createNewButton(ll_bot, i, R.style.TextCustomerPositionGreen)
            } else {
                createNewButton(ll_bot, i, R.style.TextCustomerPosition)
            }
        }

        for (i in positions..positions + 4) {
            var k = 0
            for (j in 0 until positionsHaveOrder.size) {
                if (i + 1 == positionsHaveOrder[j]) {
                    k = positionsHaveOrder[j]
                } else {
                }
            }
            if (k != 0) {
                createNewButton(ll_prepared_table, i, R.style.TextCustomerPositionGreen)
            } else {
                createNewButton(ll_prepared_table, i, R.style.TextCustomerPosition)
            }
        }
    }


    private fun createNewButton(parent: LinearLayout?, i: Int, theme: Int) {
        var button = Button(ContextThemeWrapper(context, theme), null, 0)
        button.id = i + 1
        button.text = (i + 1).toString()
        val scale = context!!.resources.displayMetrics.density
        var valueTextSize = resources.getDimension(R.dimen.text_20)
        val pixels = (valueTextSize * scale + 0.5f).toInt()
        button.layoutParams = LinearLayout.LayoutParams(pixels, pixels)
        setMargins(button, 8, 8, 8, 8)
        parent?.addView(button)

        button.setOnClickListener {
            presenter?.returnOrder(button.text.toString().toInt())
        }
    }

    private fun setMargins(view: View, left: Int, top: Int, right: Int, bottom: Int) {
        if (view.layoutParams is ViewGroup.MarginLayoutParams) {
            val p = view.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }

    override fun returnOrder(position: Int) {
        (listener?.onCustomerPositionClickListener(position))
        Util.popFragment(activity)
    }
}
