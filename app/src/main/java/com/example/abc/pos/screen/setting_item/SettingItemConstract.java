package com.example.abc.pos.screen.setting_item;

import com.example.abc.pos.model.Items;

import java.util.List;

public interface SettingItemConstract {
    interface View{
        void showListItems(List<Items> items);
        void showLoading();
        void dismissLoading();
        void showAlert(String data);
    }

    interface Presenter{
        void loadData(String menu_id);
        void deleteRow(String id);
    }
}
