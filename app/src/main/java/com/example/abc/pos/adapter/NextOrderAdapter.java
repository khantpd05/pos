package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Tieudedotdat;
import com.example.abc.pos.utils.Constants;
import com.example.abc.pos.utils.Util;

import java.util.List;

public class NextOrderAdapter extends RecyclerView.Adapter<NextOrderAdapter.ViewHolder> {
    private List<Tieudedotdat> mItems;
    private ItemClickListener mClickListener;
    private int selectedPosition = -1;
    public NextOrderAdapter( List<Tieudedotdat> items, ItemClickListener clickListener){
        mItems = items;
        mClickListener = clickListener;
    }

    public void setNewDatas(List<Tieudedotdat> items){
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.next_order_item,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_name.setText(mItems.get(position).getName());
        holder.ll_parent.setTag(position);

        if(selectedPosition != -1){
            if(position == selectedPosition){

                holder.ll_parent.setBackgroundColor(Color.parseColor("#059139"));
            }else {
                if(position%2==0){
                    holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));
                }else{
                    holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_ODD));
                }
            }
        }else{
            if(position%2==0){
                holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));
            }else{
                holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_ODD));
            }
        }

//        holder.tv_color.setText(mMenus.get(position).getName());
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_name;
        LinearLayout ll_parent;
        ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            ll_parent = itemView.findViewById(R.id.ll_parent);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(getAdapterPosition(), mItems.get(getAdapterPosition()));

            itemCheckChanged(ll_parent);
        }
    }

    public void removeItem(int position){
        mItems.remove(position);
        notifyDataSetChanged();
    }

    public interface ItemClickListener {
        void onItemClick(int position, Tieudedotdat item);
    }
}
