package com.example.abc.pos.model;

public class NameDevice {
    private String name;

    public NameDevice() {
    }

    public NameDevice(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
