package com.example.abc.pos.screen.login

import android.databinding.ObservableField
import android.os.AsyncTask
import com.example.abc.pos.App
import com.example.abc.pos.R
import com.example.abc.pos.base.SessionApp
import com.example.abc.pos.base.SessionApp.admin_password
import com.example.abc.pos.base.WapriManager
import com.example.abc.pos.database.MySqlHandleUtils
import com.example.abc.pos.model.*
import com.example.abc.pos.utils.SharedPrefs
import com.example.abc.pos.utils.Util
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.OutputStream
import java.io.PrintWriter
import java.net.Socket
import java.util.*

class LoginPresenter(mView: LoginConstract.View) : LoginConstract.Presenter {


    var mView: LoginConstract.View = mView
    var isNotEmpty = ObservableField<Boolean>(true)
    var password = ObservableField<String>("")
    var token = ObservableField<String>("")
    var isLogged = ObservableField<Boolean>(false)

    lateinit var mobile_Login: MobileLogin

    var loginMethod: ObservableField<String> = ObservableField("")
    var device_name: ObservableField<String> = ObservableField("")

    private var deviceName = ""

    private var imei = ""
    private var STATE_LOGIN = 0

    private val NOT_LOGGED = 0
    private val STILL_LOGGED = 1
    private val EXPIRED = 2

    private val TALBE = 1
    private val SETTING = 2

    private var table_or_setting = 0


    init {
        setNameDevice()
        loginMethod.set(Util.getStringFromResouce(R.string.Login))

        if (SessionApp.WIFI_STATE == 0) {
            mView.showToast(App.self()!!.getString(R.string.Please_turn_on_wifi))
        } else {
            connectToDb()
        }
    }

    override fun setImei(imei: String) {
        this.imei = imei
        SessionApp.IMEI = imei
        getLoginStateTask(imei).execute()
    }

    override fun setNameDevice() {
        val data = SharedPrefs.getInstance().get(SharedPrefs.KEY_DEVICE_NAME, String::class.java)
        deviceName = if (data != null && !data.isEmpty()){
            data
        }else{
            ""
        }
        device_name.set(deviceName)

    }

    override fun connectToDb() {
        var connectionList = SharedPrefs.getInstance().ipListSharePreference
        MySqlHandleUtils.getInstance().disconnectToMySql()
        if (connectionList != null) {
            for (item in connectionList) {
                if (item.isConnected) {
                    ConnectToDbTask(item.ip).execute()
                    break
                }
            }
        }
    }

    override fun requireLogin() {

    }


    override fun getTextButtonClick() {

    }

    override fun goMoneyBox() {
        mView.goMoneyBox()
    }

    override fun goAny() {
        mView.goAny()
    }

    override fun login() {

    }


    override fun goSetting() {
            if (password.get() == "11032002") {
                admin_password = password.get()
                mView.goSetting(true)
            }else{
                mView.goSetting(false)
            }
    }

    override fun goTables() {
        if (deviceName != null && !deviceName.isEmpty()){
            if (SessionApp.WIFI_STATE == 0) {
                mView.showToast(App.self().getString(R.string.Please_turn_on_wifi))
                MySqlHandleUtils.getInstance().disconnectToMySql()
            } else {
                if (MySqlHandleUtils.getInstance().connection != null) {
                    LoginTask(Util.md5(password.get())).execute()
                }
            }
        }
        else{
            mView.showToast(Util.getStringFromResouce(R.string.please_setup_your_device_name))
        }

    }


    private inner class ConnectToDbTask(var ip: String) : AsyncTask<String, Void, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
            mView.showLoading()
        }

        override fun doInBackground(vararg integers: String?): String {
            MySqlHandleUtils.getInstance()
            return MySqlHandleUtils.getInstance().connectToMySql(ip)
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            mView.dismissLoading()
            if (result.equals("ok")) {
                mView.showToast(App.self().getString(R.string.Connect_successful))
                SessionApp.WIFI_STATE = 1
                SelectRestaurantName().execute()
            } else {
                mView.showToast(App.self().getString(R.string.Connect_failed))
            }
        }
    }

    override fun closeApp() {
        mView.closeApp()
    }


    private inner class getLoginStateTask(var deviceId: String) : AsyncTask<Void, Void, List<MobileLogin>>() {

        override fun onPreExecute() {
            super.onPreExecute()
            mView.showLoading()
        }

        override fun doInBackground(vararg p0: Void): List<MobileLogin> {

            var js = MySqlHandleUtils.selectStament("select * from wapri.mobilelogin where mobile_name = " + "'" + deviceId + "'")

            var mobileLogin: ArrayList<MobileLogin> = Gson().fromJson<ArrayList<MobileLogin>>(js.toString(), object : TypeToken<List<MobileLogin>>() {}.type)

            return mobileLogin
        }

        override fun onPostExecute(result: List<MobileLogin>) {
            super.onPostExecute(result)
            if (result != null && result.size > 0) {
                var mobileLogin = result[0]

                if (mobileLogin.token != null && mobileLogin.time_timeout != null
                        && Util.StringToDate(mobileLogin.time_timeout).after(Util.StringToDate(Util.getDate("dt"))) && mobileLogin.user_accepted != null) {
                    isLogged.set(true)

                    mobile_Login = mobileLogin
                    token.set(mobileLogin.token.toString())
                } else {
                    isLogged.set(false)
                }
            } else {

            }
            mView.dismissLoading()
//            isExistDevice = result != null && result.size > 0
        }
    }



    private inner class LoginTask(var password: String) : AsyncTask<Void, Void, List<Users>>() {

        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg p0: Void): List<Users> {

            var js = MySqlHandleUtils.selectStament("select * from wapri.users where password = " + "'" + password + "'")

            var users: ArrayList<Users> = Gson().fromJson<ArrayList<Users>>(js.toString(), object : TypeToken<List<Users>>() {}.type)

            return users
        }

        override fun onPostExecute(result: List<Users>) {
            super.onPostExecute(result)
            admin_password = ""
            if (result != null && result.size > 0) {
                WapriManager.setUser(result[0])
                getTimeZoneTask().execute()
                if (result[0].tienbandau <= 0.0) {
                    mView.goMoneyBox()
                } else {
                    mView.goTables()
                }
                ExecuteGetAllItems().execute()
            } else {
                mView.showToast(App.self().getString(R.string.Wrong_password))
            }
        }
    }



    private inner class GoToTableByLoginOTPTask(var user_name: String) : AsyncTask<Void, Void, List<Users>>() {

        override fun doInBackground(vararg p0: Void): List<Users> {

            var js = MySqlHandleUtils.selectStament("select * from wapri.users where username = " + "'" + user_name + "'")

            var users: ArrayList<Users> = Gson().fromJson<ArrayList<Users>>(js.toString(), object : TypeToken<List<Users>>() {}.type)

            return users
        }

        override fun onPostExecute(result: List<Users>) {
            super.onPostExecute(result)

            if (result != null && result.size > 0) {
                WapriManager.setUser(result[0])
                getTimeZoneTask().execute()
                if (result[0].tienbandau <= 0.0) {
                    mView.goMoneyBox()
                } else {
                    mView.goTables()
                }

            } else {
                mView.showToast(App.self().getString(R.string.Wrong_password))
            }
        }
    }


    private inner class SettingTask(var password_: String) : AsyncTask<Void, Void, List<Users>>() {

        override fun doInBackground(vararg p0: Void): List<Users> {

            var js = MySqlHandleUtils.selectStament("select * from wapri.users where password = " + "'" + password_ + "'")

            var users: ArrayList<Users> = Gson().fromJson<ArrayList<Users>>(js.toString(), object : TypeToken<List<Users>>() {}.type)

            return users
        }

        override fun onPostExecute(result: List<Users>) {
            super.onPostExecute(result)
            admin_password = ""
            if (result != null && result.size > 0) {
                if (result[0].coQuyenCauHinhThietBi == "Y") {
                    mView.goSetting(true)
                }
            } else {
//                if(password.get()=="11032002"){
//                    mView.goSetting(true)
//                }else{
//                    mView.goSetting(false)
//                }
            }
        }
    }


    private inner class getTimeZoneTask : AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg p0: Void): String {
            var js = MySqlHandleUtils.selectTimeZoneMySQL()
            return js
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            WapriManager.setTime_zone(result)
        }
    }

    private inner class SelectRestaurantName : AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg p0: Void): String {
            WapriManager.STORE_NAME = MySqlHandleUtils.selectThongtincuahang()
            return ""
        }
    }





    private inner class ExecuteGetAllItems : AsyncTask<Void, Void, Void>() {
        override fun onPreExecute() {
            super.onPreExecute()
            mView.showLoading()
        }

        override fun doInBackground(vararg voids: Void): Void? {
            GettingMenusFoodItem().execute()
            GettingMenusDrinkItem().execute()
            GettingItemsFromMenuTask().execute()
            GettingItems_PopupFromMenuTask().execute()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            mView.dismissLoading()
        }

    }

    private inner class GettingMenusFoodItem : AsyncTask<Void, Void, List<Menu>>() {


        override fun doInBackground(vararg voids: Void): ArrayList<Menu>? {
            val js = MySqlHandleUtils.selectStament("select * from menu where id in (select nhomhang from items where bophanphucvu not like '%PhaChe%' group by nhomhang) ")
            return Gson().fromJson<ArrayList<Menu>>(js.toString(), object : TypeToken<List<Menu>>() {

            }.type)
        }

        override fun onPostExecute(menus: List<Menu>) {
            super.onPostExecute(menus)
            SessionApp.menus_food = menus
        }
    }

    private inner class GettingMenusDrinkItem : AsyncTask<Void, Void, List<Menu>>() {


        override fun doInBackground(vararg voids: Void): ArrayList<Menu>? {
            val js = MySqlHandleUtils.selectStament("select * from menu where id in (select nhomhang from items where bophanphucvu like '%PhaChe%' group by nhomhang)")
            return Gson().fromJson<ArrayList<Menu>>(js.toString(), object : TypeToken<List<Menu>>() {

            }.type)
        }

        override fun onPostExecute(menus: List<Menu>) {
            super.onPostExecute(menus)
            SessionApp.menus_drink = menus
        }
    }

    private inner class GettingItemsFromMenuTask: AsyncTask<Void, Void, MutableList<Items>>() {


        override fun doInBackground(vararg voids: Void): MutableList<Items> {
            val js = MySqlHandleUtils.selectStament("select * from wapri.items")
            val items = Gson().fromJson<ArrayList<Items>>(js.toString(), object : TypeToken<List<Items>>() {

            }.type)

            for (ob in items) {
                ob.setGiaBan()
            }
            return items
        }

        override fun onPostExecute(items: MutableList<Items>?) {
            super.onPostExecute(items)
            val firstOb = Items()
            firstOb.tenhang = App.self().getString(R.string.Come_back)
            items?.add(0, firstOb)
            SessionApp.items_global = items
        }
    }

    private inner class GettingItems_PopupFromMenuTask: AsyncTask<Void, Void, MutableList<ItemsPopup>>() {
        override fun doInBackground(vararg voids: Void): MutableList<ItemsPopup> {
            val js = MySqlHandleUtils.selectStament("select * from wapri.items_popup")
            val items = Gson().fromJson<ArrayList<ItemsPopup>>(js.toString(), object : TypeToken<List<ItemsPopup>>() {

            }.type)


            return items
        }

        override fun onPostExecute(items: MutableList<ItemsPopup>?) {
            super.onPostExecute(items)
            SessionApp.items_popup = items
        }
    }

}