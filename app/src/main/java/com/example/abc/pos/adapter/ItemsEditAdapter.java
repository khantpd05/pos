package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.utils.Constants;
import com.example.abc.pos.utils.Util;

import java.util.List;

public class ItemsEditAdapter extends RecyclerView.Adapter<ItemsEditAdapter.ViewHolder> {
    private List<Items> mItems;
    private ItemClickListener mClickListener;
    private int selectedPosition = -1;
    public ItemsEditAdapter(Context context, List<Items> items, ItemClickListener clickListener){
        mItems = items;
        mClickListener = clickListener;
    }

    public void setNewDatas(List<Items> items){
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_edit_item,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_itemId.setText(mItems.get(position).getId()+"");
        holder.tv_name.setText(mItems.get(position).getTenhang());
        holder.tv_price.setText(String.format("%.2f",mItems.get(position).getGiale1())+"");

        holder.tv_serve_type.setText(Util.getValueFromKey(Util.getEnumBoPhanPhucVu(),mItems.get(position).getBophanphucvu()));

        if(mItems.get(position).getColor() != null){
            holder.tv_color.setBackgroundColor(Integer.parseInt(mItems.get(position).getColor()));
        }else{
            holder.tv_color.setBackgroundColor(Color.parseColor("#204d7f"));
        }

        holder.ll_parent.setTag(position);

        if(selectedPosition != -1){
            if(position == selectedPosition){

                holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_CHOSE_GREEN));
            }else {
                if(position%2==0){
                    holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));
                }else{
                    holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_ODD));
                }
            }

        }else{
            if(position%2==0){
                holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));
            }else{
                holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_ODD));
            }
        }

//        holder.tv_color.setText(mMenus.get(position).getName());
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_itemId,tv_name,tv_price,tv_color,tv_serve_type;
        LinearLayout ll_parent;
        ViewHolder(View itemView) {
            super(itemView);
            tv_itemId = itemView.findViewById(R.id.tv_itemId);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_color = itemView.findViewById(R.id.tv_color);
            tv_serve_type = itemView.findViewById(R.id.tv_serve_type);
            ll_parent = itemView.findViewById(R.id.ll_parent);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(getAdapterPosition(), mItems.get(getAdapterPosition()));

            itemCheckChanged(ll_parent);
        }
    }

    public void removeItem(int position){
        mItems.remove(position);
        notifyDataSetChanged();
    }

    public interface ItemClickListener {
        void onItemClick(int position, Items item);
    }
}
