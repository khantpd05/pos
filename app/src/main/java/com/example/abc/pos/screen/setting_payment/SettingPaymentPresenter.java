package com.example.abc.pos.screen.setting_payment;

import android.databinding.ObservableField;
import android.os.AsyncTask;
import android.support.annotation.IntDef;

import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.screen.setting_general.SettingGeneralContract;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.viewmodel.GeneralConfiguration;
import com.example.abc.pos.viewmodel.PaymentConfiguration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import static com.example.abc.pos.utils.SharedPrefs.KEY_SETTING_PAYMENT;

public class SettingPaymentPresenter implements SettingPaymentContract.Presenter {
    private SettingPaymentContract.View mView;
    public PaymentConfiguration viewModel;


    public SettingPaymentPresenter(SettingPaymentContract.View mView) {
        this.mView = mView;
        loadData();
    }

    @Override
    public void loadData() {
        PaymentConfiguration configuration = SharedPrefs.getInstance().get(KEY_SETTING_PAYMENT, PaymentConfiguration.class);
        if (configuration != null) {
            viewModel = configuration;
        } else {
            PaymentConfiguration config = new PaymentConfiguration();
            config.TIP.set(true);
            config.bewirtung.set(true);
            config.cash.set(true);
            config.credit_card.set(true);
            config.gutschein.set(true);
            config.restaurant_check.set(true);
            config.cancel.set(true);

            viewModel = config;

            SharedPrefs.getInstance().put(KEY_SETTING_PAYMENT, config);
        }
    }



    @Override
    public void updateNewConfig() {
        SharedPrefs.getInstance().put(KEY_SETTING_PAYMENT, viewModel);
    }

    @Override
    public void exit() {
        mView.exit();
    }


}
