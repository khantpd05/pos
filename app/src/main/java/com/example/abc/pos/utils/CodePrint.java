package com.example.abc.pos.utils;

import android.location.Address;
import android.net.Network;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Xml;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.charset.Charset;

public class CodePrint {
    public static String cDecimalSeparator = ".";
    public static String cDigitGrouping = ",";
    // ghi string
    public static void converStringAndPrintBySocket(OutputStream outputStream, String sInput) {
        try {
            outputStream.write(sInput.getBytes("Cp858"));
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // lệnh cắt
    public static void cutPrint(OutputStream outputStream) {
        try {
            outputStream.write(0x1D);
            outputStream.write("V".getBytes());
            outputStream.write(0);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    // căn lề trái phải
    /* 1 = giua
        0 = left
        2 = right
     */
    public static void canLe(OutputStream outputStream, int val) {
        try {
            outputStream.write(0x1B);
            outputStream.write("a".getBytes());
            outputStream.write(val);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // khoảng cách của spacing
    public static void KhoangCachSpacing(OutputStream outputStream, int val) {
        try {
            //function ESC 3
            outputStream.write(0x1B);
            outputStream.write("3".getBytes());
            outputStream.write(val);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // tiếng beep sau khi in
    public static void beepPrint(OutputStream outputStream) {
        try {
            outputStream.write(0x1B);
            outputStream.write("(A".getBytes());
            outputStream.write(4);
            outputStream.write(0);
            outputStream.write(48);
            outputStream.write(55);
            outputStream.write(3);
            outputStream.write(15);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // set cao rộng
    public static void setWidthHeight(OutputStream outputStream, int size) {
        try {
            outputStream.write(0x1B);
            outputStream.write("!".getBytes());
            outputStream.write((int) (size));
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // default cao, rộng
    public static void setNormalWidthHeight(OutputStream outputStream) {
        try {
            outputStream.write(0x1B);
            outputStream.write("!".getBytes());
            outputStream.write((int) (0));
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // làm 7 dòng mới feed
    public static void feed(OutputStream outputStream, int line) {
        try {
            outputStream.write(0x1B);
            outputStream.write("d".getBytes());
            outputStream.write(line);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // set in đậm
    public static void setBold(OutputStream outputStream, boolean check) {
        try {
            outputStream.write(0x1B);
            outputStream.write("E".getBytes());
            outputStream.write((int) (check ? 1 : 0));
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // set font
    public static void setFont(OutputStream outputStream, int front) {
        try {
            outputStream.write(0x1B);
            outputStream.write("R".getBytes());
            outputStream.write((int) front);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // set charset
    public static void setCharset(OutputStream outputStream, int charset) {
        try {
            outputStream.write(0x1B);
            outputStream.write("t".getBytes());
            outputStream.write((int) (charset));
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String FormatString(int SizeString, String data) {
        String output = data;
        for (int i = 0; i < SizeString; i++) {
            output = output + data;
        }
        return output;
    }
    public static String MakePrintString3(int iRecLineChars, String strBuf, String strBuf2, String strBuf3, @NonNull boolean bCanhDeu3Cot) {
        strBuf = strBuf;
        strBuf2 = strBuf2;
        strBuf3 = strBuf3;
        // <<<step5>>>--Start
        String strValue;
        int iSpace1 = (iRecLineChars / 2);
        int iSpace2 = (iRecLineChars / 4);
        int iSpace3 = (iRecLineChars / 4);
        String tab = " ";
        if ((bCanhDeu3Cot == true)) {
            iSpace1 = (iRecLineChars / 3);
            iSpace2 = ((iRecLineChars / 3) - 1);
            iSpace3 = ((iRecLineChars / 3) + 1);
            if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
                iSpace1 = (iSpace1 + 1);
            }
        }

        if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
            iSpace3 = (iSpace3 + 1);
        }

        if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
            iSpace1 = (iSpace1 + 1);
        }

        if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
            iSpace2 = (iSpace2 + 1);
        }

        // ''''''''''''''''''''''''''''''
        if ((strBuf.length() <= iSpace1)) {
            while ((strBuf.length() < iSpace1)) {
                strBuf = (strBuf + tab);
            }
        } else {
            strBuf = strBuf.substring(0, iSpace1);
        }

        if ((strBuf2.length() <= iSpace2)) {
            while ((strBuf2.length() < iSpace2)) {
                strBuf2 = (tab + strBuf2);
            }

        } else {
            strBuf2 = strBuf2.substring(0, iSpace2);
        }

        if ((strBuf3.length() <= iSpace3)) {
            while ((strBuf3.length() < iSpace3)) {
                strBuf3 = (tab + strBuf3);
            }

        } else {
            strBuf3 = strBuf3.substring(0, iSpace3);
        }

        // ''''''''''''''''''''''''''''''
        strValue = (strBuf + (strBuf2 + strBuf3));
        int iSpac;
        iSpac = (iRecLineChars - strValue.length());
        if ((iSpac > 0)) {
            while ((iSpac > 0)) {
                strBuf = (strBuf + tab);
                iSpac = (iSpac - 1);
            }

            strValue = (strBuf + (strBuf2 + strBuf3));
        }

        return strValue;
    }

    public static String MakePrintString3_canh_bang_nhau(int iRecLineChars, String strBuf, String strBuf2, String strBuf3) {
        strBuf = strBuf.toString();
        strBuf2 = strBuf2.toString();
        strBuf3 = strBuf3.toString();
        // <<<step5>>>--Start
        String strValue;
        int iSpace1 = ((iRecLineChars / 3) + 2);
        int iSpace2 = ((iRecLineChars / 3) - 1);
        int iSpace3 = ((iRecLineChars / 3) - 1);
        String tab = " ";
        if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
            iSpace1 = (iSpace1 + 1);
        }

        if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
            iSpace2 = (iSpace2 + 1);
        }

        if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
            iSpace3 = (iSpace3 + 1);
        }

        // ''''''''''''''''''''''''''''''
        if ((strBuf.length() <= iSpace1)) {
            while ((strBuf.length() < iSpace1)) {
                strBuf = (strBuf + tab);
            }
        } else {
            strBuf = strBuf.substring(0, iSpace1);
        }

        if ((strBuf2.length() <= iSpace2)) {
            while ((strBuf2.length() < iSpace2)) {
                strBuf2 = (tab + strBuf2);
            }

        } else {
            strBuf2 = strBuf2.substring(0, iSpace2);
        }

        if ((strBuf3.length() <= iSpace3)) {
            while ((strBuf3.length() < iSpace3)) {
                strBuf3 = (tab + strBuf3);
            }

        } else {
            strBuf3 = strBuf3.substring(0, iSpace3);
        }

        // ''''''''''''''''''''''''''''''
        strValue = (strBuf + (strBuf2 + strBuf3));
        int iSpac;
        iSpac = (iRecLineChars - strValue.length());
        if ((iSpac > 0)) {
            while ((iSpac > 0)) {
                strBuf = (strBuf + tab);
                iSpac = (iSpac - 1);
            }

            strValue = (strBuf + (strBuf2 + strBuf3));
        }

        return strValue;
    }

    public static String MakePrintString3_hoadon(int iRecLineChars, String strBuf, String strBuf2, String strBuf3) {
        strBuf = strBuf.toString();
        strBuf2 = strBuf2.toString();
        strBuf3 = strBuf3.toString();
        // <<<step5>>>--Start
        String strValue;
        int iSpace1 = (3 * (iRecLineChars / 5));
        int iSpace2 = ((iRecLineChars / 5) - 2);
        int iSpace3 = ((iRecLineChars / 5) + 2);
        String tab = " ";
        if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
            iSpace2 = (iSpace2 + 1);
        }

        if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
            iSpace3 = (iSpace3 + 1);
        }

        if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
            iSpace1 = (iSpace1 + 1);
        }

        if ((strBuf.length() <= iSpace1)) {
            while ((strBuf.length() < iSpace1)) {
                strBuf = (strBuf + tab);
            }

        } else {
            strBuf = strBuf.substring(0, iSpace1);
        }

        if ((strBuf2.length() <= iSpace2)) {
            while ((strBuf2.length() < iSpace2)) {
                strBuf2 = (tab + strBuf2);
            }

        } else {
            strBuf2 = strBuf2.substring(0, iSpace2);
        }

        if ((strBuf3.length() <= iSpace3)) {
            while ((strBuf3.length() < iSpace3)) {
                strBuf3 = (tab + strBuf3);
            }

        } else {
            strBuf3 = strBuf3.substring(0, iSpace3);
        }

        strValue = (strBuf + (strBuf2 + strBuf3));
        int iSpac;
        iSpac = (iRecLineChars - strValue.length());
        if ((iSpac > 0)) {
            while ((iSpac > 0)) {
                strBuf = (strBuf + tab);
                iSpac = (iSpac - 1);
            }

            strValue = (strBuf + (strBuf2 + strBuf3));
        }

        return strValue;
        // <<<step5>>>--End
    }

    public static String MakePrintString3_dotdatmon(int iRecLineChars, String strBuf, String strBuf2, String strBuf3) {
        strBuf = strBuf.toString();
        strBuf2 = strBuf2.toString();
        strBuf3 = strBuf3.toString();
        // <<<step5>>>--Start
        String strValue;
        int iSpace1 = ((iRecLineChars / 3) + 8);
        int iSpace2 = (iSpace1 - 4);
        int iSpace3 = (iSpace1 - 4);
        String tab = "_";
        if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
            iSpace2 = (iSpace2 + 1);
        }

        if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
            iSpace3 = (iSpace3 + 1);
        }

        if (iSpace1 + iSpace2 + iSpace3 < iRecLineChars) {
            iSpace1 = (iSpace1 + 1);
        }

        if ((strBuf.length() <= iSpace1)) {
            while ((strBuf.length() < iSpace1)) {
                strBuf = (strBuf + tab);
            }

        } else {
            strBuf = strBuf.substring(0, iSpace1);
        }

        if ((strBuf2.length() <= iSpace2)) {
            while ((strBuf2.length() < iSpace2)) {
                strBuf2 = (tab + strBuf2);
            }

        } else {
            strBuf2 = strBuf2.substring(0, iSpace2);
        }

        if ((strBuf3.length() <= iSpace3)) {
            while ((strBuf3.length() < iSpace3)) {
                strBuf3 = (tab + strBuf3);
            }

        } else {
            strBuf3 = strBuf3.substring(0, iSpace3);
        }

        strValue = (strBuf + (strBuf2 + strBuf3));
        int iSpac;
        iSpac = (iRecLineChars - strValue.length());
        if ((iSpac > 0)) {
            while ((iSpac > 0)) {
                strBuf = (strBuf + tab);
                iSpac = (iSpac - 1);
            }

            strValue = (strBuf + (strBuf2 + strBuf3));
        }

        return strValue;
        // <<<step5>>>--End
    }

    public static String MakePrintString2(int iRecLineChars, String strBuf, String strBuf2) {
        // <<<step5>>>--Start
        strBuf = strBuf.toString();
        strBuf2 = strBuf2.toString();
        String strValue;
        int iSpaceForStrBuf = (iRecLineChars - strBuf2.length());
        String tab = "";
        int i;
        int k;
        if ((iSpaceForStrBuf >= strBuf.length())) {
            k = (iSpaceForStrBuf - strBuf.length());
            for (i = 0; (i <= k); i++) {
                tab = (tab + " ");
            }

        } else if ((iSpaceForStrBuf < strBuf.length())) {
            strBuf = strBuf.substring(0, iSpaceForStrBuf);
        }

        strValue = (strBuf
                + (tab + strBuf2));
        return strValue;
        // <<<step5>>>--End
    }

    public static String MakePrintString4(int iRecLineChars, String str1, String str2, String str3, String str4, @Nullable boolean bFormatToDecimal) {
        String s;
        String t;
        s = cDecimalSeparator;
        t = cDigitGrouping;
        // <<<step5>>>--Start
        String strValue;
        int iSpace1 = (int) Math.floor((iRecLineChars / 4));
        int iSpace2 = iSpace1;
        int iSpace3 = iSpace1;
        int iSpace4 = iSpace1;
        if (iSpace1 + iSpace2 + iSpace3 + iSpace4 < iRecLineChars) {
            iSpace2 = (iSpace2 + 1);
        }

        if (iSpace1 + iSpace2 + iSpace3 + iSpace4 < iRecLineChars) {
            iSpace3 = (iSpace3 + 1);
        }

        if (iSpace1 + iSpace2 + iSpace3 + iSpace4 < iRecLineChars) {
            iSpace4 = (iSpace4 + 1);
        }

        if (iSpace1 + iSpace2 + iSpace3 + iSpace4 < iRecLineChars) {
            iSpace1 = (iSpace1 + 1);
        }

        iSpace1 = (iSpace1 - 1);
        iSpace2 = (iSpace2 + 1);
        if ((bFormatToDecimal == true)) {
            if ((str2 != "")) {
                str2 = str2.replace(".", cDecimalSeparator);
            }

            if ((str3 != "")) {
                str3 = str3.replace(".", cDecimalSeparator);
            }

            if ((str4 != "")) {
                str4 = str4.replace(".", cDecimalSeparator);
            }

        }

        String tab = " ";
        while ((str1.length() < iSpace1)) {
            str1 = (str1 + tab);
        }

        while ((str2.length() < iSpace2)) {
            str2 = (tab + str2);
        }

        while ((str3.length() < iSpace3)) {
            str3 = (tab + str3);
        }

        while ((str4.length() < iSpace4)) {
            str4 = (tab + str4);
        }

        strValue = (str1
                + (str2
                + (str3 + str4)));
        return strValue;
        // <<<step5>>>--End
    }


    public static void clearPrint(OutputStream outputStream) {
        try {
            outputStream.write(0x1D);
            outputStream.write("@".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
