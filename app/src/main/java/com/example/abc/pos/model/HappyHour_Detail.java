package com.example.abc.pos.model;

public class HappyHour_Detail {
    private int id;
    private int happyhour_id;
    private String items_id;
    private String tenhang;
    private double giahappyhour;
    private double giahappyhour2;
    private double giahappyhour3;
    private double giamangvehappyhour;
    private double giamangvehappyhour2;
    private double giamangvehappyhour3;




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHappyhour_id() {
        return happyhour_id;
    }

    public void setHappyhour_id(int happyhour_id) {
        this.happyhour_id = happyhour_id;
    }

    public String getItems_id() {
        return items_id;
    }

    public void setItems_id(String items_id) {
        this.items_id = items_id;
    }

    public String getTenhang() {
        return tenhang;
    }

    public void setTenhang(String tenhang) {
        this.tenhang = tenhang;
    }

    public double getGiahappyhour() {
        return giahappyhour;
    }

    public void setGiahappyhour(double giahappyhour) {
        this.giahappyhour = giahappyhour;
    }

    public double getGiahappyhour2() {
        return giahappyhour2;
    }

    public void setGiahappyhour2(double giahappyhour2) {
        this.giahappyhour2 = giahappyhour2;
    }

    public double getGiahappyhour3() {
        return giahappyhour3;
    }

    public void setGiahappyhour3(double giahappyhour3) {
        this.giahappyhour3 = giahappyhour3;
    }

    public double getGiamangvehappyhour() {
        return giamangvehappyhour;
    }

    public void setGiamangvehappyhour(double giamangvehappyhour) {
        this.giamangvehappyhour = giamangvehappyhour;
    }

    public double getGiamangvehappyhour2() {
        return giamangvehappyhour2;
    }

    public void setGiamangvehappyhour2(double giamangvehappyhour2) {
        this.giamangvehappyhour2 = giamangvehappyhour2;
    }

    public double getGiamangvehappyhour3() {
        return giamangvehappyhour3;
    }

    public void setGiamangvehappyhour3(double giamangvehappyhour3) {
        this.giamangvehappyhour3 = giamangvehappyhour3;
    }
}
