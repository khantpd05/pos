package com.example.abc.pos.screen.keyboard;

import com.example.abc.pos.viewmodel.Keyboard;

public class KeyboardPresenter  implements KeyboardConstract.Presenter {
    public Keyboard mViewModel;

    public KeyboardConstract.View mView;


    public KeyboardPresenter(KeyboardConstract.View mView) {
        this.mView = mView;
        mViewModel = new Keyboard();
        mViewModel.title.set("Server IP:");
        mViewModel.key11.set(".");
    }

    @Override
    public void getDataInput() {
        mView.getDataInput(mViewModel.input.get());
    }
}
