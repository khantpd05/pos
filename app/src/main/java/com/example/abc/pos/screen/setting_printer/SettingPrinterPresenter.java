package com.example.abc.pos.screen.setting_printer;

import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.viewmodel.PrinterConfiguration;

public class SettingPrinterPresenter implements SettingPrinterContract.Presenter{
    public PrinterConfiguration printerConfig ;
    SettingPrinterContract.View mView;

    public SettingPrinterPresenter(SettingPrinterContract.View mView) {
        printerConfig = SharedPrefs.getInstance().get(SharedPrefs.KEY_SETTING_PRINTER, PrinterConfiguration.class);
        this.mView = mView;
    }

    @Override
    public void updateNewConfig() {
        SharedPrefs.getInstance().put(SharedPrefs.KEY_SETTING_PRINTER,printerConfig);
    }

    @Override
    public void exit() {
        mView.exit();
    }
}
