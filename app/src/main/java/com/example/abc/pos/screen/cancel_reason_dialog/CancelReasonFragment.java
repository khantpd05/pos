package com.example.abc.pos.screen.cancel_reason_dialog;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.CancelReasonAdapter;
import com.example.abc.pos.adapter.GuscheinAdapter;
import com.example.abc.pos.model.Guschein;
import com.example.abc.pos.model.LyDoHuyXoa;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class CancelReasonFragment extends BottomSheetDialogFragment implements CancelReasonConstract.View, CancelReasonAdapter.ItemClickListener {


    public interface OnCancelReasonSelected{
        void onCancelReasonSelectedListener(String data);
    }

    Dialog dialog;
    OnCancelReasonSelected mListener;
    CancelReasonPresenter presenter;
    CancelReasonAdapter adapter;
    RecyclerView rc_cancel_reason;

    public CancelReasonFragment(OnCancelReasonSelected mListener) {
        // Required empty public constructor
        this.mListener = mListener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cancel_reason,container,false);
        rc_cancel_reason = view.findViewById(R.id.rc_cancel_reason);
        adapter = new CancelReasonAdapter(getContext(),new ArrayList<LyDoHuyXoa>(),this);
        rc_cancel_reason.setAdapter(adapter);
        rc_cancel_reason.setLayoutManager(new LinearLayoutManager(getContext()));
        presenter = new CancelReasonPresenter(this);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();

        if (dialog != null) {
            DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
            int height = displayMetrics.heightPixels;
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = (int) (height*0.60);
        }
        final View view = getView();
        view.post(new Runnable() {
            @Override
            public void run() {
                View parent = (View) view.getParent();
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
                CoordinatorLayout.Behavior behavior = params.getBehavior();
                BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) behavior;
                bottomSheetBehavior.setPeekHeight(view.getMeasuredHeight());
            }
        });
    }

    @Override
    public void showCancelReasonList(List<LyDoHuyXoa> cancelReasonList) {
        adapter.setNewDatas(cancelReasonList);
    }

    @Override
    public void getDataInput(String data) {
        mListener.onCancelReasonSelectedListener(data);
        dialog.dismiss();
    }

    @Override
    public void onItemClick(int position, LyDoHuyXoa item) {
        mListener.onCancelReasonSelectedListener(item.getName());
        dialog.dismiss();
    }

}
