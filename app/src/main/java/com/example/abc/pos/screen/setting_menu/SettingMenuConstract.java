package com.example.abc.pos.screen.setting_menu;

import com.example.abc.pos.model.Menu;

import java.util.List;

public interface SettingMenuConstract {
    interface View{
        void showListMenu(List<Menu> menus);
        void showLoading();
        void dismissLoading();
        void showAlert(String data);
    }
    interface Presenter{
        void loadData();
        void deleteRow(int id);
    }
}
