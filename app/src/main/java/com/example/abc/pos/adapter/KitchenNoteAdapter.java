package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.model.KitchenNote;

import java.util.List;

public class KitchenNoteAdapter extends RecyclerView.Adapter<KitchenNoteAdapter.ViewHolder> {
    private RecyclerView rvList;
    private List<KitchenNote> list;
    private ItemClickListener mClickListener;
    private int selectedPosition = -1;

    public KitchenNoteAdapter(RecyclerView rc, Context context, List<KitchenNote> list, ItemClickListener clickListener) {
        rvList = rc;
        this.list = list;
        mClickListener = clickListener;
    }

    public void setNewDatas(List<KitchenNote> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kitchen_note, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tv_name.setText(list.get(position).getName());
        holder.ll_wrap.setTag(position);
        if (selectedPosition != -1) {
            holder.ll_wrap.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else {

        }


        if (position == selectedPosition) {
            holder.ll_wrap.setBackgroundColor(Color.parseColor("#33FF80"));
        } else {
            holder.ll_wrap.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener  {
        TextView tv_name;
        LinearLayout ll_wrap;

        ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            ll_wrap = itemView.findViewById(R.id.ll_wrap);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {

            View containingView = rvList.findContainingItemView(view);
            int position = rvList.getChildAdapterPosition(containingView);


            if (position == RecyclerView.NO_POSITION) {
                return;
            }
            if (mClickListener != null) {
                mClickListener.onItemClick(position, list.get(position));
            }
            itemCheckChanged(ll_wrap);
        }

        @Override
        public boolean onLongClick(View v) {
            View containingView = rvList.findContainingItemView(v);
            int position = rvList.getChildAdapterPosition(containingView);

            if (mClickListener != null) {
                mClickListener.onItemLongClick(position, list.get(position));
            }

            itemCheckChanged(ll_wrap);
            return false;
        }
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }


    public interface ItemClickListener {
        void onItemClick(int position, KitchenNote kitchenNote);
        void onItemLongClick(int position, KitchenNote kitchenNote);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
