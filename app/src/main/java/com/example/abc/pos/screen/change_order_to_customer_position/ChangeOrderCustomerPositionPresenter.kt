package com.example.abc.pos.screen.change_order_to_customer_position

import android.os.AsyncTask
import android.util.Log
import com.example.abc.pos.base.SessionApp
import com.example.abc.pos.base.WapriManager
import com.example.abc.pos.database.MySqlHandleUtils
import com.example.abc.pos.model.OrdersTemp
import com.example.abc.pos.utils.Util

class ChangeOrderCustomerPositionPresenter(var view: ChangeOrderCustomerPositionContract.View) : ChangeOrderCustomerPositionContract.Presenter {
    private lateinit var tableName: String
    private lateinit var taichohaymangve: WapriManager.taichohaymangve
    override fun updateChangeOrderToTable(ordersTempsWillChange: List<OrdersTemp>, vitrikhac: Int, table_name: String) {
        tableName = table_name
        ExcuseAllDbTask(ordersTempsWillChange, vitrikhac, table_name).execute()
    }


    override fun loadData(positions: Int, taichohaymangve: WapriManager.taichohaymangve) {
        view.showPosition(positions)
        this.taichohaymangve = taichohaymangve
    }


    private inner class ExcuseAllDbTask(var ordersTempsWillChange: List<OrdersTemp>, var vitrikhac: Int, var table_name: String) : AsyncTask<Void, Void, Void>() {

        override fun onPreExecute() {
            super.onPreExecute()
            view.showLoading()
        }

        override fun doInBackground(vararg voids: Void): Void? {
            for (orderTemp in ordersTempsWillChange) {
                if (orderTemp.isInsertedToDb) {
                    orderTemp.banso = table_name
                    orderTemp.thutukhachtrenban = vitrikhac
//                    if(taichohaymangve == WapriManager.taichohaymangve.TaiCho){
//                        orderTemp.taichohaymangve = WapriManager.Taichohaymangve_.TaiCho
//                    }else{
//                        orderTemp.taichohaymangve = WapriManager.Taichohaymangve_.MangVe
//                    }
                    if (SessionApp.global_table.taichohaymangve == WapriManager.taichohaymangve.MangVe && taichohaymangve == WapriManager.taichohaymangve.TaiCho) {
                        orderTemp.taichohaymangve = WapriManager.Taichohaymangve_.TaiCho
                    }
                    if (taichohaymangve == WapriManager.taichohaymangve.MangVe) {
                        orderTemp.taichohaymangve = WapriManager.Taichohaymangve_.MangVe
                    }

                    if ((taichohaymangve != WapriManager.taichohaymangve.MangVe && orderTemp.getTaichohaymangve() == WapriManager.Taichohaymangve_.TaiCho)
                            || orderTemp.bophanphucvu == "PhaChe19") {
                        orderTemp.thue = 19
                        orderTemp.tienthue = tinhTienThue(orderTemp)
                    } else {
                        orderTemp.thue = 7
                        orderTemp.tienthue = tinhTienThue(orderTemp)
                    }
                    orderTemp.inlaimonfromtablet = "N"
                    orderTemp.daprintchua = "Y"
                    UpdateOrderToDbTask(orderTemp).execute()
                } else {
                    orderTemp.banso = table_name
                    orderTemp.thutukhachtrenban = vitrikhac

                    if(orderTemp.daprintchua != "Y"){
                        orderTemp.inlaimonfromtablet = "Y"
                        orderTemp.daprintchua = "Y"
                    }


//                    if(taichohaymangve == WapriManager.taichohaymangve.TaiCho){
//                        orderTemp.taichohaymangve = WapriManager.Taichohaymangve_.TaiCho
//                    }else{
//                        orderTemp.taichohaymangve = WapriManager.Taichohaymangve_.MangVe
//                    }
//
                    if (SessionApp.global_table.taichohaymangve == WapriManager.taichohaymangve.MangVe && taichohaymangve == WapriManager.taichohaymangve.TaiCho) {
                        orderTemp.taichohaymangve = WapriManager.Taichohaymangve_.TaiCho
                    }

                    if (taichohaymangve == WapriManager.taichohaymangve.MangVe) {
                        orderTemp.taichohaymangve = WapriManager.Taichohaymangve_.MangVe
                    }
                    if ((taichohaymangve != WapriManager.taichohaymangve.MangVe && orderTemp.getTaichohaymangve() == WapriManager.Taichohaymangve_.TaiCho)
                            || orderTemp.bophanphucvu == "PhaChe19") {
                        orderTemp.thue = 19
                        orderTemp.tienthue = tinhTienThue(orderTemp)
                    } else {
                        orderTemp.thue = 7
                        orderTemp.tienthue = tinhTienThue(orderTemp)
                    }


                    InsertOrderToDbTask(orderTemp).execute()
                }
            }

            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            view.dismissLoading()
            view.onChangeSuccessful()
            UpdateTableStateToDbTask(ordersTempsWillChange).execute()
        }
    }

    private inner class InsertOrderToDbTask(var ordersTemp: OrdersTemp) : AsyncTask<Void, Void, Int>() {
        override fun doInBackground(vararg voids: Void): Int? {
            val result: Int
            result = MySqlHandleUtils.insertOrderItem(ordersTemp)
            return result
        }

        override fun onPostExecute(result: Int?) {
            super.onPostExecute(result)
            if (result == 2) {
                UpdateOrderToDbTask(ordersTemp).execute()
            }
        }
    }


    private fun tinhTienThue(ordersTemp: OrdersTemp): Double {
        return if (ordersTemp.bophanphucvu == "Sonstiges0") 0.0
        else if ((taichohaymangve != WapriManager.taichohaymangve.MangVe && ordersTemp.getTaichohaymangve() == WapriManager.Taichohaymangve_.TaiCho)
                || ordersTemp.bophanphucvu == "PhaChe19") {
            WapriManager.tinhTienThue19(ordersTemp.giatien, ordersTemp.phantramgiam)
        } else
            WapriManager.tinhTienThue7(ordersTemp.giatien, ordersTemp.phantramgiam)
    }

    private inner class UpdateOrderToDbTask(var ordersTemp: OrdersTemp) : AsyncTask<Void, Void, Int>() {


        override fun doInBackground(vararg voids: Void): Int? {
            val result: Int

            result = MySqlHandleUtils.updateOrderTempItem(ordersTemp)
            return result
        }
    }


    private inner class UpdateTableStateToDbTask(var ordersTempsWillChange: List<OrdersTemp>) : AsyncTask<Void, Void, Int>() {

        override fun onPreExecute() {
            super.onPreExecute()
            view.showLoading()
        }

        override fun doInBackground(vararg voids: Void): Int? {
            val result: Int
            var summerItem = 0.0
            for (orderTemp in ordersTempsWillChange) {
                summerItem += orderTemp.thanhtien
            }
            if (SessionApp.global_destination_table.tongtien != null) {
                SessionApp.global_destination_table.tongtien += summerItem
            }
            SessionApp.global_destination_table.trangthai = "using"
            SessionApp.global_destination_table.username = WapriManager.getUser().username
            SessionApp.global_destination_table.thoigian = Util.getDate("dt")
            result = MySqlHandleUtils.updateTableState(SessionApp.global_destination_table)
            return result
        }

        override fun onPostExecute(result: Int?) {
            super.onPostExecute(result)
            view.dismissLoading()
            if (result == 1) {
                view.returnOrder()
            } else {
                //  mView.showAlert("Có lỗi trong quá trình thêm dữ liệu");
            }
        }
    }
}