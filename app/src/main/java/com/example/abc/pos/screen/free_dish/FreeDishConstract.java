package com.example.abc.pos.screen.free_dish;

import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.OrdersTemp;

import java.util.List;

public interface FreeDishConstract {
    interface View{
        void showSpinner(List<String> datas);
        void renderButtonText(List<WapriManager.bophanphucvu> datas);
        void returnOrder();
        void addFreeDishOrder(OrdersTemp ordersTemp);
    }

    interface Presenter{
        void loadData();
        void renderButtonText();
        void addOrder(String food_name, double price, String food_type, String bophanphucvu);
    }
}
