package com.example.abc.pos

import android.app.Application
import android.content.Context
import com.google.gson.Gson

class App: Application() {
     var mGSon: Gson ?= null


    companion object {
        private lateinit var mSelf: App
        private var activityVisible: Boolean = false
        fun self(): App {
            return mSelf
        }
        fun isActivityVisible(): Boolean {
            return activityVisible // return true or false
        }
        fun activityResumed() {
            activityVisible = true// this will set true when activity resumed

        }

        fun activityPaused() {
            activityVisible = false// this will set false when activity paused

        }
    }





    override fun onCreate() {
        super.onCreate()
        mSelf = this
        mGSon = Gson()
    }

    protected override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
    }

    fun getGSon(): Gson? {
        return mGSon
    }
}