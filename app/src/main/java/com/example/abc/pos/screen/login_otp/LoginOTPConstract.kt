package com.example.abc.pos.screen.login_otp

import com.example.abc.pos.model.Users

interface LoginOTPConstract {
    interface View {
        fun getDeviceId():String
        fun goMoneyBox()
        fun goAny()
        fun goSetting(isHavePermission: Boolean)
        fun showToast(data: String)
        fun showLoading()
        fun goTables()
        fun dismissLoading()
        fun alertDialog(data:String?)
        fun closeApp()
        fun checkWifi()
        fun logout()
        fun showOTPCode(otp:String)
        fun showKeyboard()
    }

    interface Presenter {
        fun getTextButtonClick()
        fun goMoneyBox()
        fun goAny()
        fun goTables()
        fun goSetting(data:String?)
        fun connectToDb()
        fun login()
        fun closeApp()
        fun requireLogin()
        fun requireSetting()
        fun setImei(imei:String)
        fun setNameDevice()
        fun logout()
        fun doLogout()
    }
}