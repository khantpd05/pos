package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.model.Orders;
import com.example.abc.pos.utils.Constants;
import com.example.abc.pos.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class BillAdapter extends RecyclerView.Adapter<BillAdapter.ViewHolder> {
    private List<Orders> mOrderList;
    private ItemClickListener mClickListener;
    private Context mContext;
    private int selectedPosition = -1;

    public BillAdapter(Context context, List<Orders> ordersList, ItemClickListener clickListener) {
        mContext = context;
        mOrderList = ordersList;
        mClickListener = clickListener;
    }

    public void setNewDatas(List<Orders> ordersList) {
        mOrderList = ordersList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bill, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_table_name.setText(mOrderList.get(position).getBanso());
        try{
            String[] parts = mOrderList.get(position).getId().split("-");
            String bill_code = parts[0]+"-"+parts[2];
            holder.tv_bill_code.setText(bill_code);

        }catch (Exception ex){
            holder.tv_bill_code.setText( mOrderList.get(position).getId());
        }
        holder.ll_wrap_group.setTag(position);

        /** set font family **/
        holder.tv_table_name.setTypeface(ResourcesCompat.getFont(mContext, Constants.ColorConst.BSD_BOLD));
        holder.tv_bill_code.setTypeface(ResourcesCompat.getFont(mContext, Constants.ColorConst.BSD_BOLD));
        holder.tv_time.setTypeface(ResourcesCompat.getFont(mContext, Constants.ColorConst.BSD_BOLD));
        holder.tv_paymethod.setTypeface(ResourcesCompat.getFont(mContext, Constants.ColorConst.BSD_BOLD));
        holder.tv_summer.setTypeface(ResourcesCompat.getFont(mContext, Constants.ColorConst.BSD_BOLD));


        holder.tv_table_name.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
        holder.tv_bill_code.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
        holder.tv_time.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
        holder.tv_paymethod.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
        holder.tv_summer.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));

        String danhdaubixoa = mOrderList.get(position).getDelete();

        if (selectedPosition != -1) {
            if (position == selectedPosition) {

                holder.ll_wrap_group.setBackgroundColor(Color.parseColor("#FF000000"));
            } else {
                if (position % 2 == 0) {
                    holder.ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));
                } else {
                    holder.ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_ODD));
                }
            }
        } else {
            if (position % 2 == 0) {
                holder.ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));
            } else {
                holder.ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_ODD));
            }
        }

        try {
            String originalString = mOrderList.get(position).getThoigian();
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(originalString);
            String newTimeExtract = new SimpleDateFormat("H:mm").format(date);
            holder.tv_time.setText(newTimeExtract);
        } catch (ParseException e) {
            holder.tv_time.setText("");
            e.printStackTrace();
        }




        if (mOrderList.get(position).getLoaiIn().equals("None"))
            holder.tv_paymethod.setText(Util.getStringFromResouce(R.string.not_bon));
        else if (mOrderList.get(position).getLoaiIn().equals("BON")||mOrderList.get(position).getLoaiIn().equals("BON_COPY")){
            if(mOrderList.get(position).getCachtra() == WapriManager.cachtra.TienMat){
                holder.tv_paymethod.setText(Util.getStringFromResouce(R.string.Cash));
            }else{
                holder.tv_paymethod.setText(Util.getStringFromResouce(R.string.Credit_card));
            }
        }

        else if (mOrderList.get(position).getLoaiIn().equals("BON_BS")){
            if(mOrderList.get(position).getCachtra() == WapriManager.cachtra.TienMat){
                holder.tv_paymethod.setText("Bewirtung/" + Util.getStringFromResouce(R.string.Cash));
            }else{
                holder.tv_paymethod.setText("Bewirtung/" + Util.getStringFromResouce(R.string.Credit_card));
            }

        }


        holder.tv_summer.setText(String.format("%.2f", mOrderList.get(position).getGiatien()));

//        holder.ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));

        if(danhdaubixoa!=null){
            if (danhdaubixoa.equals("Y")) {
                holder.ll_wrap_text.setBackground(mContext.getResources().getDrawable(R.drawable.strike_through));
            } else {
                holder.ll_wrap_text.setBackground(null);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mOrderList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_table_name, tv_bill_code, tv_time, tv_paymethod, tv_summer;
        LinearLayout ll_wrap_group,ll_wrap_text;

        ViewHolder(View itemView) {
            super(itemView);
            tv_table_name = itemView.findViewById(R.id.tv_table_name);
            tv_bill_code = itemView.findViewById(R.id.tv_bill_code);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_paymethod = itemView.findViewById(R.id.tv_paymethod);
            tv_summer = itemView.findViewById(R.id.tv_summer);
            ll_wrap_group = itemView.findViewById(R.id.ll_wrap_group);
            ll_wrap_text = itemView.findViewById(R.id.ll_wrap_text);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null){
                if(getAdapterPosition()>=0){
                    mClickListener.onItemClick(view, mOrderList.get(getAdapterPosition()));
                    itemCheckChanged(ll_wrap_group);
                }

            }

        }
    }


    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    public interface ItemClickListener {
        void onItemClick(View view, Orders order);
    }
}
