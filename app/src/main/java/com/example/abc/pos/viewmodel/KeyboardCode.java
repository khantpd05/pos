package com.example.abc.pos.viewmodel;

import android.databinding.ObservableField;

import com.example.abc.pos.base.SessionApp;

public class KeyboardCode {
    public ObservableField<String> input = new ObservableField<>("");
    public ObservableField<String> hint = new ObservableField<>("");
    public ObservableField<String> key_m = new ObservableField<>("");
    public ObservableField<String> key_1 = new ObservableField<>("");
    public ObservableField<String> key_2 = new ObservableField<>("");
    public ObservableField<String> key_3 = new ObservableField<>("");
    public ObservableField<String> key_4 = new ObservableField<>("");
    public ObservableField<String> key_5 = new ObservableField<>("");
    public ObservableField<String> key_6 = new ObservableField<>("");
    public ObservableField<String> key_7 = new ObservableField<>("");
    public ObservableField<String> key_8 = new ObservableField<>("");
    public ObservableField<String> key_9 = new ObservableField<>("");
}
