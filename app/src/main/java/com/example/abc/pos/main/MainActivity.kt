package com.example.abc.pos.main

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import com.example.abc.pos.R
import com.example.abc.pos.base.BaseActivity
import com.example.abc.pos.base.CustomizedExceptionHandler
import com.example.abc.pos.base.SessionApp
import com.example.abc.pos.base.WapriManager
import com.example.abc.pos.broadcast.WifiReceiver
import com.example.abc.pos.model.Users
import com.example.abc.pos.screen.login.LoginFragment
import com.example.abc.pos.screen.login_otp.LoginOTPFragment
import com.example.abc.pos.service.RealtimeService
import com.example.abc.pos.utils.Constants
import android.view.KeyEvent.KEYCODE_VOLUME_UP
import android.view.KeyEvent.KEYCODE_VOLUME_DOWN
import android.app.Activity
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.os.AsyncTask
import android.os.Handler
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.view.View.OnSystemUiVisibilityChangeListener
import com.example.abc.pos.database.MySqlHandleUtils
import com.example.abc.pos.model.Table


class MainActivity : BaseActivity(),MainConstract.View , Runnable{

    var loginFragment : LoginFragment ?= null
    var loginOTPFragment : LoginOTPFragment ?= null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Thread.setDefaultUncaughtExceptionHandler(CustomizedExceptionHandler(
                "/mnt/sdcard/"))
        setContentView(R.layout.activity_main)
        startService(Intent(this,RealtimeService::class.java))
        if(savedInstanceState!=null){
            WapriManager.setUser(savedInstanceState.getSerializable("global_user") as Users?)
            SessionApp.IMEI = savedInstanceState.getString("imei")
        }

        if(savedInstanceState!=null){
            if(WapriManager.configuration().otpLogin.get()!!) {
                loginOTPFragment = supportFragmentManager.getFragment(savedInstanceState,"loginOTPFragment") as LoginOTPFragment?
            }else{
                loginFragment = supportFragmentManager.getFragment(savedInstanceState, "loginFragment") as LoginFragment?
            }

        }else{
            if(WapriManager.configuration().otpLogin.get()!!){
                loginOTPFragment = LoginOTPFragment()
                showLoginOTPPage()
            }else{
                loginFragment = LoginFragment()
                showLoginPage()
            }
        }


        setImmersiveMode()

        val decorView = window.decorView
        decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
                setImmersiveMode()
            }
        }


//        hideSystemUI()
    }


    override fun showLoginPage() {
        pushFragmentNotBackStack(loginFragment, Constants.TransitionType.NONE, true)
    }

    override fun showLoginOTPPage() {
        pushFragmentNotBackStack(loginOTPFragment, Constants.TransitionType.NONE, true)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        if(WapriManager.configuration().otpLogin.get()!!) {
            if (loginOTPFragment != null) {
                if (loginOTPFragment?.isAdded!!) {
                    supportFragmentManager.putFragment(outState, "loginOTPFragment", loginOTPFragment)
                }
            }
        }else{
            if (loginFragment != null) {
                if (loginFragment?.isAdded!!) {
                    supportFragmentManager.putFragment(outState, "loginFragment", loginFragment)
                }
            }
        }
        outState?.putSerializable("global_user", WapriManager.getUser())
        outState?.putString("imei", SessionApp.IMEI)

    }


    private fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    // Shows the system bars by removing all the flags
// except for the ones that make the content appear under the system bars.
    private fun showSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }

    override fun onBackPressed() {

    }

    var wifiReceiver = WifiReceiver()
    override fun onStart() {
        super.onStart()

        var intentFilter = IntentFilter("android.net.wifi.WIFI_STATE_CHANGED")
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT)
        registerReceiver(wifiReceiver,intentFilter)

    }

    private val _handler = Handler()

    override fun onStop() {
        super.onStop()
        _handler.removeCallbacks(this)
        unregisterReceiver(wifiReceiver)
    }


    override fun run() {
        setImmersiveMode()
    }



    @SuppressLint("NewApi")
    fun setImmersiveMode() {
        setImmersiveMode(this)
    }

    @SuppressLint("NewApi")
    fun setImmersiveMode(activity: Activity) {
        activity.window
                .decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    fun isFragmentAvailable(): Boolean {
        return android.os.Build.VERSION.SDK_INT >= 19
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        if (hasFocus) {
            _handler.removeCallbacks(this)
            _handler.postDelayed(this, 300)
        } else {
            _handler.removeCallbacks(this)
        }
    }

    fun onKeyDown(keyCode: Int) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            _handler.removeCallbacks(this)
            _handler.postDelayed(this, 500)
        }
    }


}
