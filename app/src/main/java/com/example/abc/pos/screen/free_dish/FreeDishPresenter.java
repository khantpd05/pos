package com.example.abc.pos.screen.free_dish;

import android.os.AsyncTask;
import android.util.Log;

import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.screen.order.OrderPresenter;
import com.example.abc.pos.utils.Util;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class FreeDishPresenter implements FreeDishConstract.Presenter {
    private FreeDishConstract.View mView;

    public FreeDishPresenter(FreeDishConstract.View view) {
        mView = view;
        loadData();
        renderButtonText();
    }

    @Override
    public void loadData() {
        List<String> categories = new ArrayList<>();
        categories.add(WapriManager.loai_mon_an.KhaiVi.display);
        categories.add(WapriManager.loai_mon_an.Nuoc.display);
        categories.add(WapriManager.loai_mon_an.MonChinh.display);
        categories.add(WapriManager.loai_mon_an.TrangMieng.display);
        mView.showSpinner(categories);
    }

    @Override
    public void renderButtonText() {
        List<WapriManager.bophanphucvu> bophanphuvus = new ArrayList<>();
        bophanphuvus.add(WapriManager.bophanphucvu.Bep);
        bophanphuvus.add(WapriManager.bophanphucvu.PhaChe19);
        bophanphuvus.add(WapriManager.bophanphucvu.Sushi);
        mView.renderButtonText(bophanphuvus);
    }


    @Override
    public void addOrder(String food_name, double price, String food_type, String bophanphucvu) {
        OrdersTemp ordersTemp = new OrdersTemp();
        Random ran = new Random();
        Date date = new Date();
        String idGenera = WapriManager.DEVICE_NAME+"x"+date.getTime() + "" + ran.nextInt(10000);
        ordersTemp.setId(idGenera);
        ordersTemp.setBanso(SessionApp.TABLE_NAME);
        ordersTemp.setitems_id("1003");
        ordersTemp.setNgaymua(Util.getDate("d"));
        ordersTemp.setTenhang(food_name);
        ordersTemp.setBonname(food_name);
        ordersTemp.setThue(19);
        ordersTemp.setorig_price_from_item(price);
        ordersTemp.setorig_price_no_extra(price);
        ordersTemp.setGiatien(price);
        ordersTemp.setSoluong(1);
        ordersTemp.setPhantramgiam(0);
        ordersTemp.setThanhtien(price);
        ordersTemp.setUsername(WapriManager.getUser().getUsername());
        ordersTemp.setThoigian(Util.getDate("dt"));
        ordersTemp.setBophanphucvu(bophanphucvu);
        ordersTemp.setloai_mon_an(food_type);
        if(SessionApp.global_table!=null){
            if (SessionApp.global_table.getTaichohaymangve() == WapriManager.taichohaymangve.TaiCho) {
                ordersTemp.setTaichohaymangve(WapriManager.Taichohaymangve_.TaiCho);
            }else{
                ordersTemp.setTaichohaymangve(WapriManager.Taichohaymangve_.MangVe);
            }
        }else{
            ordersTemp.setTaichohaymangve(WapriManager.Taichohaymangve_.TaiCho);
        }
        if(SessionApp.CUSTOMER_POSITION == 0){
            ordersTemp.setThutukhachtrenban(1);
        }else{
            ordersTemp.setThutukhachtrenban(SessionApp.CUSTOMER_POSITION);
        }

        ordersTemp.setDaprintxoachua("N");
        ordersTemp.setDaprintchua("N");
        ordersTemp.setInlaimonfromtablet("N");
        mView.addFreeDishOrder(ordersTemp);
//        new InsertOrderToDbTask(ordersTemp).execute();
    }


    private class InsertOrderToDbTask extends AsyncTask<Void, Void, Integer> {

        OrdersTemp ordersTemp;

        public InsertOrderToDbTask(OrdersTemp ordersTemp) {
            this.ordersTemp = ordersTemp;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            result = MySqlHandleUtils.insertOrderItem(ordersTemp);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                mView.returnOrder();
            } else {

            }
        }
    }
}
