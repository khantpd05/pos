package com.example.abc.pos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.model.TableType;

import java.util.List;

public class TablesTypeAdapter extends RecyclerView.Adapter<TablesTypeAdapter.ViewHolder> {
    private List<TableType> mTables;
    private ItemClickListener mClickListener;
    private int selectedPosition = -1;
    private boolean isAll;
    private Context mContext;
    public TablesTypeAdapter(Context context, List<TableType> tables, boolean isAll,ItemClickListener mClickListener ){
        mTables = tables;
        mContext = context;
        this.isAll = isAll;
        this.mClickListener = mClickListener;
    }

    public void setNewDatas( List<TableType> tables,boolean isAll){
        mTables = tables;
        this.isAll = isAll;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_type_item,parent,false);

        return new ViewHolder(view);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.myTextView.setText(mTables.get(position).getName());
        holder.ll_wrap.setTag(position);
        if(isAll){


            holder.ll_wrap.setBackground(mContext.getResources().getDrawable(R.drawable.bg_btn_xanhdam));
            holder.myTextView.setTextColor(Color.parseColor("#FFFFFF"));
        }else{
            if(position == selectedPosition){
                holder.ll_wrap.setBackground(mContext.getResources().getDrawable(R.drawable.bg_btn_xanhdam));
                holder.myTextView.setTextColor(Color.parseColor("#FFFFFF"));
            }else{
                holder.ll_wrap.setBackground(mContext.getResources().getDrawable(R.drawable.bg_btn_xamdam));
                holder.myTextView.setTextColor(Color.parseColor("#FFFFFF"));
            }
        }


    }

    @Override
    public int getItemCount() {
        return mTables.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        LinearLayout ll_wrap;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.info_text);
            ll_wrap = itemView.findViewById(R.id.ll_wrap);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null){
                if(mTables!=null && mTables.size()>0)
                mClickListener.onItemClick(view, mTables.get(getAdapterPosition()));
            }
            isAll = false;
            itemCheckChanged(ll_wrap);
        }
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }


    public interface ItemClickListener {
        void onItemClick(View view, TableType table);
    }
}
