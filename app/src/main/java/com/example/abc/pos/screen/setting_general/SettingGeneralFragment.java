package com.example.abc.pos.screen.setting_general;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.abc.pos.R;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.databinding.FragmentSettingGenrealBinding;
import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.screen.setting_gutschein_restaurant.SettingGutscheinRestaurantFragment;
import com.example.abc.pos.utils.Util;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingGeneralFragment extends BaseFragment implements SettingGeneralContract.View{

    SettingGeneralPresenter presenter;
    public SettingGeneralFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_setting_genreal;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentSettingGenrealBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_setting_genreal, container, false);

        presenter = new SettingGeneralPresenter(this);

        binding.setPresenter(presenter);

        return binding.getRoot();
    }



    @Override
    public void showAlert(String data) {

    }

    @Override
    public void goGutscheinSetting(List<Configuration> datas) {
        ((BaseActivity) getActivity()).pushFragment(new SettingGutscheinRestaurantFragment(presenter.viewModel,datas,0), true);
    }

    @Override
    public void goRestaurantSetting(List<Configuration> datas) {
        ((BaseActivity) getActivity()).pushFragment(new SettingGutscheinRestaurantFragment(presenter.viewModel,datas,1), true);
    }

    @Override
    public void exit() {
        Util.popFragment(getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.updateNewConfig();
    }
}
