package com.example.abc.pos.model;

public class KichCoMonAn {
   private String kichCoMon;
   private String kichCoMonDisplay;
   private double giaBan;

    public KichCoMonAn(String kichCoMon, String kichCoMonDisplay, double giaBan) {
        this.kichCoMon = kichCoMon;
        this.kichCoMonDisplay = kichCoMonDisplay;
        this.giaBan = giaBan;
    }

    public String getKichCoMon() {
        return kichCoMon;
    }

    public void setKichCoMon(String kichCoMon) {
        this.kichCoMon = kichCoMon;
    }

    public double getGiaBan() {
        return giaBan;
    }

    public void setGiaBan(double giaBan) {
        this.giaBan = giaBan;
    }

    public String getKichCoMonDisplay() {
        return kichCoMonDisplay;
    }

    public void setKichCoMonDisplay(String kichCoMonDisplay) {
        this.kichCoMonDisplay = kichCoMonDisplay;
    }
}
