package com.example.abc.pos.screen.next_order_dialog;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.GuscheinAdapter;
import com.example.abc.pos.adapter.NextOrderDialogAdapter;
import com.example.abc.pos.model.Guschein;
import com.example.abc.pos.model.Tieudedotdat;

import java.util.ArrayList;
import java.util.List;
import com.example.abc.pos.databinding.FragmentNextOrderDialogBinding;
import com.example.abc.pos.screen.order.OrderFragment;
import com.example.abc.pos.utils.KToast;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class NextOrderDialogFragment extends BottomSheetDialogFragment implements NextOrderDialogContract.View, NextOrderDialogAdapter.ItemClickListener {


    public interface OnGetNextOrder{
        void onGetNextOrderListener(String data);
        void onUpdateNextOrderListener(String orderId,String data);
    }
    Dialog dialog;
    OnGetNextOrder mListener;
    NextOrderDialogPresenter presenter;
    NextOrderDialogAdapter adapter;
    RecyclerView rc_next_order;
    String hint;
    String orderId;
    boolean isUpdate;

    public NextOrderDialogFragment(){

    }

    public NextOrderDialogFragment(String hint,String orderId,boolean isUpdate,OnGetNextOrder mListener) {
        // Required empty public constructor
        this.hint = hint;
        this.orderId = orderId;
        this.isUpdate = isUpdate;
        this.mListener = mListener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentNextOrderDialogBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_next_order_dialog,container,false);
        View view = binding.getRoot();

        rc_next_order = view.findViewById(R.id.rc_next_order);
        adapter = new NextOrderDialogAdapter(getContext(),new ArrayList<Tieudedotdat>(),this);
        rc_next_order.setAdapter(adapter);
        rc_next_order.setLayoutManager(new LinearLayoutManager(getContext()));
        presenter = new NextOrderDialogPresenter(this);
        presenter.tieuDeDotDat.set(hint);
        binding.setPresenter(presenter);
        if(mListener==null)
        mListener = OrderFragment.getOrderFragmentListener();
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();

        if (dialog != null) {
            DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
            int height = displayMetrics.heightPixels;
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = (int) (height*0.60);
        }
        final View view = getView();
        view.post(new Runnable() {
            @Override
            public void run() {
                View parent = (View) view.getParent();
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
                CoordinatorLayout.Behavior behavior = params.getBehavior();
                BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) behavior;
                bottomSheetBehavior.setPeekHeight(view.getMeasuredHeight());
            }
        });
    }

    @Override
    public void showTieuDeDotDat(List<Tieudedotdat> tieudedotdats) {
        adapter.setNewDatas(tieudedotdats);
    }

    @Override
    public void getDataInput(String data) {
        presenter.tieuDeDotDat.set(data);
        dialog.dismiss();
    }

    @Override
    public void enterPress(String data) {
        if(!isUpdate){
            mListener.onGetNextOrderListener(data);
        }
        else{
            mListener.onUpdateNextOrderListener(orderId,data);
        }
        dialog.dismiss();
    }

    @Override
    public void showToast(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }


    @Override
    public void onItemClick(int position, Tieudedotdat item) {
        presenter.getDataInput(item);
    }

}
