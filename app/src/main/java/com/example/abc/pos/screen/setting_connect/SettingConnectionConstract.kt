package com.example.abc.pos.screen.setting_connect

import com.example.abc.pos.model.Connection

interface SettingConnectionConstract {
    interface View {
        fun showKeyboard()
        fun showIpList(connectionList: List<Connection>)
        fun goBack()
    }

    interface Presenter {
        fun loadIpSaved()
        fun doRemoveIp(connection: Connection?)
        fun updateIpList(ip:String)
        fun goBack()
    }
}