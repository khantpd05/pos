package com.example.abc.pos.screen.combine_to_table;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.TablesAdapter;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CombineToTableFragment extends BaseFragment implements CombineToTableContract.View, TablesAdapter.ItemClickListener, View.OnClickListener {

    private RecyclerView rc_table;

    private TablesAdapter table_adapter;

    private CombineToTablePresenter presenter;

    private TextView btn_okay;

    private List<Table> tableList;
    private Table table_new;

    public CombineToTableFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public CombineToTableFragment(List<Table> tableList) {
        // Required empty public constructor
        this.tableList = tableList;
    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_combine_to_table;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_combine_to_table, container, false);

        if(savedInstanceState!=null){
            Util.popFragment(getActivity());
        }else{
            rc_table = view.findViewById(R.id.rc_table);

            rc_table.setLayoutManager(new GridLayoutManager(getContext(), 4));
            table_adapter = new TablesAdapter(getContext(), new ArrayList<Table>(), this);
            rc_table.setAdapter(table_adapter);

            btn_okay = view.findViewById(R.id.btn_okay);
            btn_okay.setOnClickListener(this);

            LinearLayout btn_exit = view.findViewById(R.id.btn_exit);
            btn_exit.setOnClickListener(this);

            presenter = new CombineToTablePresenter(tableList, this);
        }



        return view;
    }


    @Override
    public void showListTable(List<Table> tables) {
        table_adapter.setNewDatas(tables);
    }

    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
        Util.popFragment(getActivity());
    }

    @Override
    public void goBack() {

        for (int i = 0; i < 2; i++) {
            if (getActivity() != null) {
                ((BaseActivity) getActivity()).popFragment();
            }
        }

    }


    @Override
    public void onItemClick(View view, Table table) {
        btn_okay.setVisibility(View.VISIBLE);
        table_new = table;
        presenter.setTableNew(table_new);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_okay:
                presenter.doChangeTable(table_new);
                break;
            case R.id.btn_exit:
                Util.popFragment(getActivity());
                break;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("key","temp");
    }
}
