package com.example.abc.pos.model;
public class ItemsMontuychon {
	private int id;
	private String color;
	private String textdisplay;
	private double giatc1;
	private double giatc2;
	private double giatc3;

	public ItemsMontuychon() {
	}

	public ItemsMontuychon( String textdisplay, String color, double giatc1, double giatc2, double giatc3) {
		this.color = color;
		this.textdisplay = textdisplay;
		this.giatc1 = giatc1;
		this.giatc2 = giatc2;
		this.giatc3 = giatc3;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTextdisplay() {
		return textdisplay;
	}

	public void setTextdisplay(String textdisplay) {
		this.textdisplay = textdisplay;
	}

	public double getGiatc1() {
		return giatc1;
	}

	public void setGiatc1(double giatc1) {
		this.giatc1 = giatc1;
	}

	public double getGiatc2() {
		return giatc2;
	}

	public void setGiatc2(double giatc2) {
		this.giatc2 = giatc2;
	}

	public double getGiatc3() {
		return giatc3;
	}

	public void setGiatc3(double giatc3) {
		this.giatc3 = giatc3;
	}
}
