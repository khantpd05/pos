package com.example.abc.pos.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.model.ItemsMontuychon;
import com.example.abc.pos.model.ItemsPopup;

import java.util.List;

public class ItemPopupAdapter extends RecyclerView.Adapter<ItemPopupAdapter.ViewHolder> {
    private List<ItemsPopup> mDatas;
    private ItemClickListener mClickListener;
    private Context context;


    public ItemPopupAdapter(Context context, List<ItemsPopup> datas, ItemClickListener mClickListener) {
        mDatas = datas;
        this.context = context;
        this.mClickListener = mClickListener;
    }

    public void setNewDatas(List<ItemsPopup> datas) {
        mDatas = datas;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_popup_item, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        ItemsPopup item = mDatas.get(position);
        holder.tv_name.setText(item.getPopup_name());


//        DisplayMetrics displaymetrics = new DisplayMetrics();
//        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
//        int devicewidth = displaymetrics.widthPixels / 6;
//        holder.ll_wrap.getLayoutParams().width = devicewidth;
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_name;
        LinearLayout ll_wrap;

        ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_popup_name);
            ll_wrap = itemView.findViewById(R.id.ll_wrap);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(getAdapterPosition(), mDatas.get(getAdapterPosition()));

        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }


    public interface ItemClickListener {
        void onItemClick(int position, ItemsPopup data);
    }


}
