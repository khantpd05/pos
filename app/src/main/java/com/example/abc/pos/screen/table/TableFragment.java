package com.example.abc.pos.screen.table;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.DrawableTableAdapter;
import com.example.abc.pos.adapter.TablesAdapter;
import com.example.abc.pos.adapter.TablesTypeAdapter;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.ItemsPopup;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.model.SpacesItemDecoration;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.model.TableType;
import com.example.abc.pos.screen.bill_history.BillHistoryFragment;
import com.example.abc.pos.screen.change_table.ChangeTableFragment;
import com.example.abc.pos.screen.combine_table.CombineTableFragment;
import com.example.abc.pos.screen.kich_co_mon_dialog.KichCoMonDialogFragment;
import com.example.abc.pos.screen.lay_ban_dialog.LayBanDialogFragment;
import com.example.abc.pos.screen.money_box.MoneyBoxFragment;
import com.example.abc.pos.screen.order.OrderFragment;
import com.example.abc.pos.screen.payment.PaymentFragment;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TableFragment extends BaseFragment implements TableConstract.View,
        TablesTypeAdapter.ItemClickListener, TablesAdapter.ItemClickListener, LayBanDialogFragment.OnItemTypeSelected, DrawableTableAdapter.ItemClickListener {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private DrawableTableAdapter drawableTableAdapter;
//    private View navHeader;

    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtWebsite;
    private Toolbar toolbar;
    private ImageButton img_menu;

    private RecyclerView recyclerView;
    private RecyclerView rc_tables_type;
    private RecyclerView rc_drawable;

    private BottomSheetBehavior mBottomSheetBehavior;

    private TablePresenter presenter;

    private TablesAdapter table_adapter;

    private TablesTypeAdapter table_type_adapter;

    private TextView tv_user;

    //action bar;
    private TextView tv_lg_name;


    interface LogOutListenner {
        void onLogoutListenner();
    }

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_table;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_table, container, false);
        // Inflate the layout for this fragment
        toolbar = view.findViewById(R.id.toolbar);
        drawer = view.findViewById(R.id.drawer_layout);

        /* navigation init */
        rc_drawable = view.findViewById(R.id.rc_drawable);
        img_menu = view.findViewById(R.id.img_menu);
//        navigationView =  view.findViewById(R.id.nav_view);
        ((BaseActivity) getActivity()).setSupportActionBar(toolbar);
        ((BaseActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        setUpNavigationView();
        drawer.closeDrawers();

        /* bottom menu init */
        View bottomShet = view.findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomShet);

        /* table type list init */
        rc_tables_type = view.findViewById(R.id.rc_tables_type);
        rc_tables_type.setLayoutManager(new GridLayoutManager(getContext(), 3));
        table_type_adapter = new TablesTypeAdapter(getContext(), new ArrayList<TableType>(), true, this);
        rc_tables_type.setHasFixedSize(true);
        rc_tables_type.setAdapter(table_type_adapter);

        /* table list init */
        recyclerView = view.findViewById(R.id.rc_tables);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 4));
        table_adapter = new TablesAdapter(getContext(), new ArrayList<Table>(), this);
        table_adapter.setHasStableIds(true);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.size_r5);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerView.setAdapter(table_adapter);


//            oStream.println("HI,test from Android Device");
//            oStream.println("\n\n\n");
//            oStream.close();
//            sock.close();


        /* navHeader init*/
//        navHeader =  navigationView.getHeaderView(0);
//        tv_user = navHeader.findViewById(R.id.tv_user);
//        tv_user.setText(WapriManager.getUser().getUsername());
        tv_lg_name = view.findViewById(R.id.tv_lg_name);
        tv_lg_name.setText(WapriManager.getUser().getUsername());


        presenter = new TablePresenter(this);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                presenter.loadData();

                presenter.showPendingPayment();

            }
        }, 300);


        if (savedInstanceState != null) {
            Gson gson = new Gson();

            SessionApp.items_global = gson.fromJson(SharedPrefs.getInstance().get("listType_items",String.class), new TypeToken<List<Items>>() {
            }.getType());

            SessionApp.menus_food = gson.fromJson(SharedPrefs.getInstance().get("menus_food",String.class), new TypeToken<List<Menu>>() {
            }.getType());

            SessionApp.menus_drink = gson.fromJson(SharedPrefs.getInstance().get("menus_drink",String.class), new TypeToken<List<Menu>>() {
            }.getType());

            SessionApp.items_popup = gson.fromJson(SharedPrefs.getInstance().get("listType_items_poup",String.class), new TypeToken<List<ItemsPopup>>() {
            }.getType());

        }

        return view;
    }

    ActionBarDrawerToggle actionBarDrawerToggle;

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//
//            // This method will trigger on item Click of navigation menu
//            @Override
//            public boolean onNavigationItemSelected(MenuItem menuItem) {
//
//                //Check to see which item was being clicked and perform appropriate action
//                switch (menuItem.getItemId()) {
//                    //Replacing the main content with ContentFragment Which is our Inbox View;
//                    case R.id.nav_change_table:
//                        ((BaseActivity) getActivity()).pushFragmentNotReplace(new ChangeTableFragment(), true);
//                        drawer.closeDrawers();
//                        break;
//                    case R.id.nav_combine_table:
//                        ((BaseActivity) getActivity()).pushFragmentNotReplace(new CombineTableFragment(), true);
//                        break;
//                    case R.id.nav_has_customer:
//                        SessionApp.tableType = SessionApp.TableShowType.CO_KHACH;
//                        presenter.loadAllTablesHaveCustomer();
//                        drawer.closeDrawers();
//                        break;
//                    case R.id.nav_all_table:
//                        SessionApp.tableType = SessionApp.TableShowType.TAT_CA;
//                        SessionApp.isAll = true;
//                        presenter.loadData();
//                        drawer.closeDrawers();
//                        break;
//
//                    case R.id.nav_bill:
//                        ((BaseActivity) getActivity()).pushFragmentNotReplace(new BillHistoryFragment(), true);
//                        break;
//
//                    case R.id.nav_exit:
//                        Util.popFragment(getActivity());
//                        break;
//                    default:
//
//                }
//
//                //Checking if the item is in checked state or not, if not make it in checked state
//                if (menuItem.isChecked()) {
//                    menuItem.setChecked(false);
//                } else {
//                    menuItem.setChecked(true);
//                }
//                menuItem.setChecked(true);
//
//                return true;
//            }
//        });

//        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
//                super.onDrawerClosed(drawerView);
//            }
//
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
//                super.onDrawerOpened(drawerView);
//            }
//        };
        rc_drawable.setLayoutManager(new LinearLayoutManager(getContext()));
        int imgs[] = {R.drawable.doiban_icon, R.drawable.gopban_icon, R.drawable.bancokhach_icon, R.drawable.tatcaban_icon, R.drawable.dshoadon_icon, R.drawable.ic_trove};
        String titles[] = {Util.getStringFromResouce(R.string.Change_table), Util.getStringFromResouce(R.string.combine_table)
                , Util.getStringFromResouce(R.string.table_has_customer), Util.getStringFromResouce(R.string.all_table)
                , Util.getStringFromResouce(R.string.Invoice_list), Util.getStringFromResouce(R.string.Exit)};
        drawableTableAdapter = new DrawableTableAdapter(getContext(), imgs, titles, this);
        rc_drawable.setAdapter(drawableTableAdapter);

        drawer.setDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
        img_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(rc_drawable)) {
                    drawer.closeDrawer(rc_drawable);
                } else if (!drawer.isDrawerOpen(rc_drawable)) {
                    drawer.openDrawer(rc_drawable);
                }
            }
        });
        //Setting the actionbarToggle to drawer layout


        //calling sync state is necessary or else your hamburger icon wont show up

    }

    @Override
    public void showToast(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }

    @Override
    public void showListTable(List<Table> tables) {
        table_adapter.setNewDatas(tables);
    }

    @Override
    public void showListTableType(List<TableType> tables, boolean isAll) {
        table_type_adapter.setNewDatas(tables, isAll);
    }

    @Override
    public void showLayBan(List<String> datas) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        LayBanDialogFragment layBanDialogFragment = new LayBanDialogFragment(datas, this);
        layBanDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        layBanDialogFragment.show(fm, KichCoMonDialogFragment.class.getSimpleName());
    }

    @Override
    public void goToOrder(Table table, String data) {
        ((BaseActivity) getActivity()).pushFragmentNotReplace(new OrderFragment(table, data), true);
    }

    @Override
    public void showCustomDialog(String text) {
        Util.showCustomDialog(getContext(), text);
    }

    @Override
    public void goToPayment() {
        ((BaseActivity) getActivity()).pushFragmentNotReplace(new PaymentFragment(), true);
    }

    @Override
    public void hideKeyBoard() {
        if (getActivity() != null)
            Util.hideKeyboard(getActivity());
    }

    @Override
    public void exit() {
        if (getActivity() != null){
            if(BaseActivity.Companion.getFragmentNames() != null && !BaseActivity.Companion.getFragmentNames().isEmpty()){
                if(BaseActivity.Companion.getFragmentNames().contains(MoneyBoxFragment.class.getSimpleName())){
                    for(int i = 0 ; i < 2 ; i++)
                    Util.popFragment(getActivity());
                }else{
                    Util.popFragment(getActivity());
                }
            }else{
                Util.popFragment(getActivity());
            }
        }

    }

    @Override
    public void onItemClick(View view, TableType table) {
        SessionApp.isAll = false;
        SessionApp.tableType = SessionApp.TableShowType.TAT_CA;
        SessionApp.current_table_type = table;
        table_adapter.setNewDatas(SessionApp.current_table_type.getTables());
    }

    @Override
    public void onItemClick(View view, Table table) {
//        if((!table.getis_selling_on_tablet().equals("Y") && !table.getIs_selling_on_POS().equals("Y"))
//                || (WapriManager.getUser().getCoQuyenPhanQuyenSuDungHeThong().equals("Y")
//                || WapriManager.getUser().getCoQuyenQuanLyToanHeThong().equals("Y"))){
//            SessionApp.global_table = table;
//            presenter.goToOrder(table);
//        }else{
//            Util.showCustomDialog(getContext(),Util.getStringFromResouce(R.string.you_can_not_control_the_table_is_booking));
//        }
        presenter.goToOrder(table);
    }

    @Override
    public void onItemTypeSelectedListener(String data) {
        goToOrder(SessionApp.global_table, data);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        SessionApp.isHappyHour = false;
        SessionApp.isAplyAllHappyHour = false;
        if (SessionApp.happyHour_detail != null)
            SessionApp.happyHour_detail.clear();
        if (SessionApp.happyHour != null)
            SessionApp.happyHour = null;
        SessionApp.table_fragment_name = "";
    }

    @Override
    public void onSaveInstanceState(@NonNull final Bundle outState) {
        super.onSaveInstanceState(outState);
        if (presenter != null)
            presenter.stopRepeatingTask();

        Gson gson = new Gson();

        //
        Type listType_items = new TypeToken<List<Items>>() {
        }.getType();

        String json_items = gson.toJson(SessionApp.items_global, listType_items);

        SharedPrefs.getInstance().put("listType_items", json_items);

        //
        Type listType_items_poup = new TypeToken<List<ItemsPopup>>() {
        }.getType();

        String json_items_popup = gson.toJson(SessionApp.items_popup, listType_items_poup);

        SharedPrefs.getInstance().put("listType_items_poup", json_items_popup);

        //
        Type listType_menu_food = new TypeToken<List<Items>>() {
        }.getType();

        String json_menu_food = gson.toJson(SessionApp.menus_food, listType_menu_food);
        SharedPrefs.getInstance().put("menus_food", json_menu_food);

        //
        Type listType_menu_drink = new TypeToken<List<Menu>>() {
        }.getType();

        String json_menu_drink = gson.toJson(SessionApp.menus_drink, listType_menu_drink);
        SharedPrefs.getInstance().put("menus_drink", json_menu_drink);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter != null)
            presenter.startRepeatingTask();
    }

    @Override
    public void onItemClick(int position) {
        switch (position) {
            case 0:
                ((BaseActivity) getActivity()).pushFragmentNotReplace(new ChangeTableFragment(), true);
                drawer.closeDrawers();
                break;
            case 1:
                ((BaseActivity) getActivity()).pushFragmentNotReplace(new CombineTableFragment(), true);
                drawer.closeDrawers();
                break;
            case 2:
                SessionApp.tableType = SessionApp.TableShowType.CO_KHACH;
                presenter.loadAllTablesHaveCustomer();
                drawer.closeDrawers();
                break;
            case 3:
                SessionApp.tableType = SessionApp.TableShowType.TAT_CA;
                SessionApp.isAll = true;
                presenter.getAllTable();
                drawer.closeDrawers();
                break;

            case 4:
                ((BaseActivity) getActivity()).pushFragmentNotReplace(new BillHistoryFragment(), true);
                break;

            case 5:
                exit();
                break;
            default:

        }
    }
}
