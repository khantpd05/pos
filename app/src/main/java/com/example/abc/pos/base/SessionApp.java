package com.example.abc.pos.base;

import com.example.abc.pos.model.HappyHour;
import com.example.abc.pos.model.HappyHour_Detail;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.ItemsPopup;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.model.TableType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SessionApp {

    /* wifi state */
    public static int WIFI_STATE = -1;

    /* mobile login */
    public static String IMEI = "";

    /* Chuc nang table */
    public static TableType current_table_type;
    public static boolean isAll = true;
    public static TableShowType tableType = TableShowType.TAT_CA;
    public static boolean is_updated_table = false;
    public static String table_fragment_name = "";

    public enum TableShowType{
        CO_KHACH, TAT_CA
    }

    /* Chuc nang tach ban */
    public static String SEPARATE_OR_CHANGE_TABLE;
    public static String SEPARATE_TABLE_KEY = "separate_table";
    public static String CHANGE_TABLE_KEY = "change_table";
    public static String TABLE_NAME_WILL_GO;
    public static String TABLE_NAME_WILL_COME;
    public static int POSITION_CUSTOMER_WILL_GO;
    public static int POSITION_CUSTOMER_WILL_COME;

    public static int CUSTOMER_POSITIONS_CURRENT_TABLE;
    public static int CUSTOMER_POSITIONS_SEPARATE_TABLE;
    public static List<Integer> POSITIONS_HAVE_ORDER_CURRENT_TABLE;
    public static List<Integer> POSITIONS_HAVE_ORDER_SEPARATE_TABLE;

    /* chuc nang order */
    public static String TABLE_NAME;
    public static int CUSTOMER_POSITION;
    public static int FIRST_SELECTED_POSITION = -1;
    public static List<OrdersTemp> global_orderTemp_list;
    public static Table global_table = new Table();
    public static int SIZE_OF_ALL_ORDER = -1;
    public static int SIZE_OF_POSITION_ORDER = -1;
    public static List<Menu> menus_food = new ArrayList<>();
    public static List<Menu> menus_drink = new ArrayList<>();
    public static List<Items> items_global = new ArrayList<>();
    public static List<ItemsPopup> items_popup = new ArrayList<>();

    /* chuc nang thanh toan */
    public enum OrderOrPayment{
        ORDER, PAYMENT
    }
    public static String from_DivideOrder_or_Order= "";
    /* keyboard */
    public static KeyboardType keyboardType;
    public enum KeyboardType{
        PHAN_TRAM, TIEN, IP, SO_LUONG, PASSWORD
    }

    /* table come */
    public static Table global_destination_table;

    /* MoneyBox */
    public static boolean isSetting = false;


    /* Admin */
    public static String admin_password = "";

    /* Happy Hour */
    public static boolean isHappyHour = false;
    public static boolean isAplyAllHappyHour = false;
    public static HappyHour happyHour;
    public static List<HappyHour_Detail> happyHour_detail;
    public static Date endDate;

}
