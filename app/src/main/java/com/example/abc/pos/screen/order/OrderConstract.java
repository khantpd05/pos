package com.example.abc.pos.screen.order;

import android.content.Context;

import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.ItemsPopup;
import com.example.abc.pos.model.KitchenNote;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Spice;
import com.example.abc.pos.model.Table;

import java.util.List;

interface OrderConstract {
    interface View {
        Context getCurrentContext();

        void showChucNangOrder();

        void setHienThiBanGiam();

        void showAlert(String data);

        void showDialog(String data);

        void goToCustomerPosition(List<OrdersTemp> datas);

        void showOrderTempList(List<OrdersTemp> datas);

        void showLoading();

        void dismissLoading();

        void showFullOrderFeature();

        void showOrderFeatureMulti();

        void showOrderFeaturePrinted();

        void hideSpiceFromMultiPrinted();

        void showSpiceFromSinglePrinted();

        void showKeyboard(String title, String hint, String key11, SessionApp.KeyboardType keyboardType);

        void showKeyboardCode(String hint);

        void showKichCoMonDialog(Items item,int soLuong);

        void showFoundItemsDialog(List<Items> item,int soLuong);

        void showNextOrderDialog();

        void showCancelReasonDialog();

        void showItemsPopup(int positionOrderTemp,List<ItemsPopup> itemsPopupList);

        void goToGiaVi(List<Spice> datas);

        void goToKitchenNote(List<KitchenNote> datas);

        void resetSelectedList();

        int getViTriNgoi();

        void goToChangeToOtherTable(List<OrdersTemp> ordersTempsWillChange,List<OrdersTemp> datas);

        void goToDivideBill(List<OrdersTemp> orderTempList,List<OrdersTemp> restoreOrderTempList);

        void goToSeparateToOtherTable();

        void setTongtienView();

        void goToTable();

        void goToPayment();

        void goBack();

        void cancelSelectedOrders();

        boolean getPermission();

        void setMarkSignList();
    }

    interface Presenter {
        void loadData(Table table);

        void getNewOrderTempList(int positionCustomer);

        void getOffOrderTempList(int positionCustomer);

        List<OrdersTemp> getOrderTempList();

        List<OrdersTemp> getOrderTempRecoveryList();

        List<OrdersTemp> getOrderTempListByPosition();

        void setTable(Table table);

        void setOrderTempRecoveryList(List<OrdersTemp> orderTempList);

        void setOrderTempList(List<OrdersTemp> orderTempList);

        void goToCustomerPosition();

        void addNewOrder(Items item, int soLuong);

        void addOrder(Items item, int soLuong);

        void doAddOrderTemp(Items item, int soLuong);

        void addOrderFromFreeDish(OrdersTemp ordersTemp);

        void updateHeaderOrder(String orderId, String name);

        void updateGoHome(List<Integer> positionsSelected);

        void updateInBep(List<Integer> positionsSelected);

        void showChucNangOrder();

        void showKeyboard(String title,String hint,String key11,SessionApp.KeyboardType keyboardType);

        void showKeyboardCode();

        void showNextOrderDialog();

        void updateFreePrice(int positionSelected, String data);

        void updateGiamGiaMonAn(List<Integer> positionsSelected, String data);

        void updateGiamGiaTatCa(String data);

        void updateGiaVi();

        void goToGiaVi(int position);

        void goToKitchenNote(List<Integer> positions);

        void updateSpiceList(int position,List<Spice> datas);

        void updateKitchenNoteList(List<Integer> positions,List<KitchenNote> datas);

        void updateMonDiKem(int position,ItemsPopup itemsPopup);

        void doChangeValueTable(String inTable,boolean isLayBanOrDatMon);

        void doUpdateGiamGiaTable(String data);

        void removeOrder(List<Integer> positionsSelected);

        void increaseNumberOrder(int positionSelected);

        void decreaseNumberOrder(int positionSelected);

        void hideSomeOrderFeature(List<Integer> positionsSelected);

        void cancelSelectedOrders();

        void goToChangeToOtherTable(List<Integer> positionsSelected);

        void goToDivideBill();

        void goToSeparateToOtherTable();

        void goToPayment();

        void doOrder();

        double getSummerPrice();

        double getTongTienTable();

        void removeOrderTempListHasChanged(List<OrdersTemp> ordersTempsWillChange);

        void updateCancelReason(String reason);

        Table getTable();

        void doOrderByFoodId(String data);

        void updateTableInfo(Table table);

        void deleteOrderTemp(OrdersTemp ordersTemp);

        void tinhTienGiamTable();
    }
}
