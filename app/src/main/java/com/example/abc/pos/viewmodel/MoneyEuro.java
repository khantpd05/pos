package com.example.abc.pos.viewmodel;

import android.databinding.ObservableField;

public class MoneyEuro {
    public ObservableField<String> _1Cent = new ObservableField<>("0");
    public ObservableField<String> _2Cent = new ObservableField<>("0");
    public ObservableField<String> _5Cent = new ObservableField<>("0");
    public ObservableField<String> _10Cent = new ObservableField<>("0");
    public ObservableField<String> _20Cent = new ObservableField<>("0");
    public ObservableField<String> _50Cent = new ObservableField<>("0");
    public ObservableField<String> _1Euro = new ObservableField<>("0");
    public ObservableField<String> _2Euro = new ObservableField<>("0");
    public ObservableField<String> _5Euro = new ObservableField<>("0");
    public ObservableField<String> _10Euro = new ObservableField<>("0");
    public ObservableField<String> _20Euro = new ObservableField<>("0");
    public ObservableField<String> _50Euro = new ObservableField<>("0");
    public ObservableField<String> _100Euro = new ObservableField<>("0");
    public ObservableField<String> _200Euro = new ObservableField<>("0");
    public ObservableField<String> total = new ObservableField<>("0.00");
    public ObservableField<Integer> positionStored = new ObservableField<>(1);
}
