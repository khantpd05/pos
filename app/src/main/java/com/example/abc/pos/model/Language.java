package com.example.abc.pos.model;

public class Language {
    private int mId;
    private String mName;
    private String mLanguageCode;
    private String mCountryCode;

    public Language(int id, String name, String language_code, String country_code) {
        mId = id;
        mName = name;
        mLanguageCode = language_code;
        mCountryCode = country_code;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }


    public String getLanguageCode() {
        return mLanguageCode;
    }

    public String getCountryCode() {
        return mCountryCode;
    }
}
