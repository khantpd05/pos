package com.example.abc.pos.model;

import com.example.abc.pos.base.WapriManager;

public class Spice {
    private String name;
    private double price;

    public Spice(String name, double price) {
        this.name = name;
        this.price = price;
    }


    public Spice() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
