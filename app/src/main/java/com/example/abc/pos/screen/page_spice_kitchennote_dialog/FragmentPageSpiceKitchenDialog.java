package com.example.abc.pos.screen.page_spice_kitchennote_dialog;


import android.annotation.SuppressLint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.PageAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class FragmentPageSpiceKitchenDialog extends DialogFragment implements PageAdapter.ItemClickListener {

    private RecyclerView rc_page;
    private PageAdapter adapter;
    private OnPageItemClick listener;



    public interface OnPageItemClick{
        void onPageItemClickListener(String data);
    }

    public FragmentPageSpiceKitchenDialog(OnPageItemClick listener) {
        // Required empty public constructor
        this.listener = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_page_spice_kitchen_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rc_page = view.findViewById(R.id.rc_page);
        adapter = new PageAdapter(initPages(),this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rc_page.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                linearLayoutManager.getOrientation());
        rc_page.addItemDecoration(dividerItemDecoration);
        rc_page.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
//        getDialog().getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    private List<String> initPages(){
        List<String> pages = new ArrayList<>();
        for(int i = 1; i <= 8 ; i++){
            pages.add(i+"");
        }
        return pages;
    }

    @Override
    public void onItemClick(int position, String item) {
        listener.onPageItemClickListener(item);
        getDialog().cancel();
    }
}
