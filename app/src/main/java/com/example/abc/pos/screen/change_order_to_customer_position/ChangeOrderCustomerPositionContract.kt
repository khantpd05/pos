package com.example.abc.pos.screen.change_order_to_customer_position

import com.example.abc.pos.base.WapriManager
import com.example.abc.pos.model.OrdersTemp

interface ChangeOrderCustomerPositionContract {
    interface View {
        fun showAlert(data: String)
        fun showPosition(positions: Int)
        fun returnOrder()
        fun showLoading()
        fun dismissLoading()
        fun onChangeSuccessful()
    }

    interface Presenter {
        fun loadData(position: Int, taichohaymangve: WapriManager.taichohaymangve)
        fun updateChangeOrderToTable(ordersTempsWillChange: List<OrdersTemp>, vitrikhac: Int, table_name: String)
    }
}