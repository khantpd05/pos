package com.example.abc.pos.screen.setting

interface SettingConstract {
    interface View{
        fun showSettingLanguage()
        fun goSettingLanguage ()
        fun goSettingConnection()
        fun goSettingMenu()
        fun goSettingNextOrder()
        fun goSettingNumberDevice()
        fun goSettingGeneral()
        fun goSettingPayment()
        fun goSettingPrinter()
        fun goBack()
    }

    interface Presenter{
        fun settingLanguage()
        fun showSettingLanguage()
        fun goSettingLanguage ()
        fun goSettingConnection()
        fun goSettingMenu()
        fun goSettingNextOrder()
        fun goSettingNumberDevice()
        fun goSettingGeneral()
        fun goSettingPayment()
        fun goSettingPrinter()
        fun goBack()
    }
}