package com.example.abc.pos.screen.setting_gutschein_restaurant;

import android.databinding.ObservableField;
import android.os.AsyncTask;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.viewmodel.GeneralConfiguration;

import java.util.ArrayList;
import java.util.List;

public class SettingGutscheinRestaurantPresenter implements SettingGutscheinRestaurantContract.Presenter {
    SettingGutscheinRestaurantContract.View mView;
    List<Configuration> datas = new ArrayList<>();
    public ObservableField<String> title = new ObservableField<>("");
    public ObservableField<String> value1 = new ObservableField<>("");
    public ObservableField<String> value2 = new ObservableField<>("");
    public ObservableField<String> value3 = new ObservableField<>("");
    public ObservableField<String> value4 = new ObservableField<>("");
    public ObservableField<String> value5 = new ObservableField<>("");
    private final String KEY_SETTING_GENERTAL = "setting_general";

    public SettingGutscheinRestaurantPresenter(SettingGutscheinRestaurantContract.View mView) {
        this.mView = mView;
        if(mView.getTypeOfList()==0){
            title.set("Gutschein");
        }else{
            title.set("Restaurant Check");
        }
    }

    @Override
    public void getData(List<Configuration> datas) {
        if (datas != null && !datas.isEmpty()) {
            this.datas = datas;

            value1.set(datas.get(0).getValue());
            value2.set(datas.get(1).getValue());
            value3.set(datas.get(2).getValue());
            value4.set(datas.get(3).getValue());
            value5.set(datas.get(4).getValue());
        }

    }

    @Override
    public void updateValues() {
        new UpdateToDbTask().execute();
    }

    @Override
    public void exit() {
        updateValues();
    }





    private class UpdateToDbTask extends AsyncTask<Void, Void, Integer> {
        @Override
        protected Integer doInBackground(Void... voids) {
            if(datas.size()>0){

                    GeneralConfiguration config = mView.getConfig();


                    // update gutschein
                    if (mView.getTypeOfList() == 0) {
                        config.gutscheins.get().get(0).setValue(value1.get());
                        config.gutscheins.get().get(1).setValue(value2.get());
                        config.gutscheins.get().get(2).setValue(value3.get());
                        config.gutscheins.get().get(3).setValue(value4.get());
                        config.gutscheins.get().get(4).setValue(value5.get());
                            for (Configuration ob : config.gutscheins.get()) {
                                if (ob.getId().equals(-1)) {
                                    MySqlHandleUtils.insertRestaurantAndGutschein(ob.getKeyid(),ob.getValue());
                                }else{
                                    MySqlHandleUtils.updateValues(ob.getValue(), ob.getKeyid());
                                }
                            }
                    }

                    //update restaurant check
                    else{
                        config.restaurantCheck.get().get(0).setValue(value1.get());
                        config.restaurantCheck.get().get(1).setValue(value2.get());
                        config.restaurantCheck.get().get(2).setValue(value3.get());
                        config.restaurantCheck.get().get(3).setValue(value4.get());
                        config.restaurantCheck.get().get(4).setValue(value5.get());
                        for (Configuration ob : config.restaurantCheck.get()) {
                            if (ob.getId().equals(-1)) {
                                MySqlHandleUtils.insertRestaurantAndGutschein(ob.getKeyid(),ob.getValue());
                            }else{
                                MySqlHandleUtils.updateValues(ob.getValue(), ob.getKeyid());
                            }
                        }
                    }

                    SharedPrefs.getInstance().put(KEY_SETTING_GENERTAL, config);
                    return 1;

            }else{
                return 0;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                mView.showAlert (App.Companion.self().getString(R.string.Update_successful));
                mView.exit();
            } else {
                mView.showAlert(App.Companion.self().getString(R.string.There_was_an_error_updating_the_data));
            }
        }

    }
}
