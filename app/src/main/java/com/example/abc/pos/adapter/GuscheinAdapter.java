package com.example.abc.pos.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.model.Guschein;

import java.util.List;

public class GuscheinAdapter extends RecyclerView.Adapter<GuscheinAdapter.ViewHolder> {
    private List<Guschein> mItems;
    private ItemClickListener mClickListener;

    public GuscheinAdapter(Context context, List<Guschein> items, ItemClickListener clickListener){
        mItems = items;
        mClickListener = clickListener;
    }

    public void setNewDatas(List<Guschein> items){
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_guschein,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.myTextView.setText(mItems.get(position).getValue());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.info_text);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(getAdapterPosition(), mItems.get(getAdapterPosition()));
        }
    }

    public interface ItemClickListener {
        void onItemClick(int position, Guschein item);
    }
}
