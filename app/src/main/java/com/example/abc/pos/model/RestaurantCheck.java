package com.example.abc.pos.model;

public class RestaurantCheck {
    private String value;

    public RestaurantCheck(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
