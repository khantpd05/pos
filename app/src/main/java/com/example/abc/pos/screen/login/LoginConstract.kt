package com.example.abc.pos.screen.login

import com.example.abc.pos.model.Users

interface LoginConstract {
    interface View {
        fun getDeviceId():String
        fun goMoneyBox()
        fun goAny()
        fun goSetting(isHavePermission: Boolean)
        fun showToast(data: String)
        fun showLoading()
        fun goTables()
        fun dismissLoading()
        fun alertDialog(data:String?)
        fun closeApp()
        fun checkWifi()
        fun showAlert()
        fun dismissAlert()
    }

    interface Presenter {
        fun getTextButtonClick()
        fun goMoneyBox()
        fun goAny()
        fun goTables()
        fun goSetting()
        fun connectToDb()
        fun login()
        fun closeApp()
        fun requireLogin()
        fun setImei(imei:String)
        fun setNameDevice()
    }
}