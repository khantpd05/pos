package com.example.abc.pos.screen.setting_connect


import android.app.Activity
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView

import com.example.abc.pos.R
import java.util.ArrayList
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.abc.pos.App
import com.example.abc.pos.adapter.ConnectionAdapter
import com.example.abc.pos.base.SessionApp
import com.example.abc.pos.model.Connection
import com.example.abc.pos.screen.keyboard.KeyboardFragment
import com.example.abc.pos.utils.Util


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 *
 */
class SettingConnectionFragment : Fragment(), SettingConnectionConstract.View, KeyboardFragment.OnKeyboardPress,
        ConnectionAdapter.ItemClickListener, View.OnClickListener {


    var img_status: ImageView? = null
    var rc_connection: RecyclerView? = null
    var ll_extra_feature: LinearLayout? = null
    var btn_remove: TextView? = null
    var adapter: ConnectionAdapter? = null
    var datas: ArrayList<Connection>? = null
    var presenter: SettingConnectionPresenter? = null
    var connection: Connection? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_setting_connection, container, false)
        var button = view.findViewById<Button>(R.id.btn_add)
        rc_connection = view.findViewById(R.id.rc_connection)
        adapter = ConnectionAdapter(rc_connection, context, ArrayList<Connection>(), this)
        datas = ArrayList()
        rc_connection?.adapter = adapter
        rc_connection?.layoutManager = LinearLayoutManager(context)
        adapter?.setNewDatas(datas)

        var ll_exit: LinearLayout = view.findViewById(R.id.ll_exit)
        ll_exit.setOnClickListener(this)

        ll_extra_feature = view.findViewById(R.id.ll_extra_feature)
        ll_extra_feature?.setOnClickListener(this)

        btn_remove = view.findViewById(R.id.btn_remove)
        btn_remove?.setOnClickListener(this)


        presenter = SettingConnectionPresenter(this)

        button.setOnClickListener {
            showKeyboard()
        }
        return view
    }

    override fun onItemClick(connection: Connection?) {
        ll_extra_feature?.setVisibility(View.VISIBLE)
        this.connection = connection
    }

    override fun onItemLongClick(connection: Connection?, position: Int) {

    }

    override fun onKeyPressListener(ip: String) {
        presenter?.updateIpList(ip)
    }

    override fun showKeyboard() {
        var keyboardFragment = KeyboardFragment(this, App.self().getString(R.string.IP_address), "192.168.1.1", ".", SessionApp.KeyboardType.IP)
        keyboardFragment.show(activity?.supportFragmentManager, "")
    }

    override fun showIpList(connectionList: List<Connection>) {
        adapter?.setNewDatas(connectionList)
    }

    override fun goBack() {
        Util.popFragment(activity)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ll_exit -> presenter?.goBack()

            R.id.btn_remove -> {
                Util.showCustomDialogConfirm(context,App.self().getString(R.string.Are_you_sure_you_want_to_delete), object : Util.ConfirmDialogInterface{
                    override fun onNegativeClickListener() {

                    }

                    override fun onPositiveClickListener() {
                        presenter?.doRemoveIp(connection)
                    }
                })
            }
        }
    }
}
