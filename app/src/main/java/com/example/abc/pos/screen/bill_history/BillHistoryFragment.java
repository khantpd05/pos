package com.example.abc.pos.screen.bill_history;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.BillAdapter;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.Orders;
import com.example.abc.pos.screen.bill_detail_history.BillDetailHistoryFragment;
import com.example.abc.pos.screen.create_menu_item.CreateMenuItemFragment;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class BillHistoryFragment extends Fragment implements BillHistoryContract.View, BillAdapter.ItemClickListener, View.OnClickListener {

    private BillAdapter adapter;
    private BillHistoryPresenter presenter;
    private RecyclerView rc_bill;
    private LinearLayout ll_extra_feature, ll_exit, ll_detail, ll_print_again, ll_change_paymethod;
    private Orders order;

    public BillHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bill_history, container, false);

        rc_bill = view.findViewById(R.id.rc_bill);
        rc_bill.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BillAdapter(getContext(), new ArrayList<Orders>(), this);
        rc_bill.setHasFixedSize(true);
        rc_bill.setAdapter(adapter);

        ll_extra_feature = view.findViewById(R.id.ll_extra_feature);
        ll_exit = view.findViewById(R.id.ll_exit);
        ll_exit.setOnClickListener(this);

        ll_detail = view.findViewById(R.id.ll_detail);
        ll_detail.setOnClickListener(this);

        ll_print_again = view.findViewById(R.id.ll_print_again);
        ll_print_again.setOnClickListener(this);

        ll_change_paymethod = view.findViewById(R.id.ll_change_paymethod);
        ll_change_paymethod.setOnClickListener(this);

        presenter = new BillHistoryPresenter(this);
        presenter.loadBillHistoryList();

        registerForContextMenu(ll_print_again);
        registerForContextMenu(ll_change_paymethod);
        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void showBillHistory(List<Orders> ordersList) {
        adapter.setNewDatas(ordersList);
    }

    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }

    @Override
    public void showDialog(String data) {
        Util.showCustomDialog(getContext(), data);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void showInLaiHoaDon(List<String> datas) {

    }

    @Override
    public void showDoiCachTraTien(List<String> datas) {

    }

    @Override
    public void onItemClick(View view, Orders order) {
        ll_extra_feature.setVisibility(View.VISIBLE);
        this.order = order;
        presenter.setOrders(order);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_detail:
                ((BaseActivity) getActivity()).pushFragmentNotReplace(new BillDetailHistoryFragment(order), true);
                break;

            case R.id.ll_print_again:
                getActivity().openContextMenu(view);
                break;

            case R.id.ll_change_paymethod:
                if(order.getLadonhangcodoicachthanhtoantien().equals("N"))
                getActivity().openContextMenu(view);
                else showAlert(Util.getStringFromResouce(R.string.already_change_method));
                break;

            case R.id.ll_exit:
                Util.popFragment(getActivity());
                break;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.ll_print_again) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_print_again, menu);
            menu.setHeaderTitle(Util.getStringFromResouce(R.string.choose_print_type));
        }else{
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_change_paymethod, menu);
            if(order.getCachtra() == WapriManager.cachtra.TienMat){
                menu.findItem(R.id.cash).setVisible(false);
                menu.findItem(R.id.credit).setVisible(true);
            }else{
                menu.findItem(R.id.cash).setVisible(true);
                menu.findItem(R.id.credit).setVisible(false);
            }
            menu.setHeaderTitle(Util.getStringFromResouce(R.string.choose_payment_method));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.inBon) {
            presenter.updateInLaiHoaDon("BON");
        } else if (item.getItemId() == R.id.bewirtung) {
            presenter.updateInLaiHoaDon("BON_BS");
        } else if (item.getItemId() == R.id.cash){
            presenter.updateDoiCachTraTien("TienMat->CreditCard",1);

        } else if (item.getItemId() == R.id.credit){
            presenter.updateDoiCachTraTien("CreditCard->TienMat",0);
        }
        return super.onContextItemSelected(item);
    }
}
