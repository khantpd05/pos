package com.example.abc.pos.screen.setting_connect

import com.example.abc.pos.model.Connection
import com.example.abc.pos.utils.Constants
import com.example.abc.pos.utils.SharedPrefs
import com.example.abc.pos.utils.Util

class SettingConnectionPresenter(val mView: SettingConnectionFragment) : SettingConnectionConstract.Presenter {


    lateinit var connectionList: ArrayList<Connection>

    init {
        loadIpSaved()
    }

    override fun updateIpList(ip: String) {
        if (ip != "") {
            var connection = Connection()
            var connections = SharedPrefs.getInstance().ipListSharePreference
            if (connections != null && !connections.isEmpty()) {
                connection.id = (connections.get(connections.size - 1).id + 1)
            } else {
                connection.id = 0
            }

            connection.ip = ip

            SharedPrefs.getInstance().addConnectionSaveDraft(connection)

            connectionList.add(connection)
            mView.showIpList(connectionList)
        }
    }

    override fun loadIpSaved() {
        if (SharedPrefs.getInstance().ipListSharePreference != null) {
            connectionList = SharedPrefs.getInstance().ipListSharePreference
            if (connectionList != null)
                mView.showIpList(connectionList)
            else{
                connectionList = ArrayList()
                var connection = Connection()
                connection.id = 0
                connection.ip = Constants.IP_PRINTER
                connection.isConnected = true
                connectionList.add(connection)
                mView.showIpList(connectionList)
            }

        } else {
            connectionList = ArrayList()
            var connection = Connection()
            connection.id = 0
            connection.ip = Constants.IP_PRINTER
            connection.isConnected = true
            connectionList.add(connection)
            mView.showIpList(connectionList)
        }

    }

    override fun doRemoveIp(connection: Connection?) {
        if (connectionList != null) {
            SharedPrefs.getInstance().removeSavedConectionSaved(connection)
            connectionList.remove(connection)

            mView.showIpList(connectionList)
        }
    }

    override fun goBack() {
        mView.goBack()
    }

}