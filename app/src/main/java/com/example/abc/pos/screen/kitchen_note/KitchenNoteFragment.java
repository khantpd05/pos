package com.example.abc.pos.screen.kitchen_note;


import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.adapter.KitchenNoteAdapter;
import com.example.abc.pos.adapter.SpiceAdapter;
import com.example.abc.pos.adapter.SpiceItemAdapter;
import com.example.abc.pos.model.ItemsMontuychon;
import com.example.abc.pos.model.KitchenNote;
import com.example.abc.pos.model.Spice;
import com.example.abc.pos.screen.page_spice_kitchennote_dialog.FragmentPageSpiceKitchenDialog;
import com.example.abc.pos.screen.spice.SpiceFragment;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class KitchenNoteFragment extends Fragment implements KitchenNoteContract.View, KitchenNoteAdapter.ItemClickListener, SpiceItemAdapter.ItemClickListener, FragmentPageSpiceKitchenDialog.OnPageItemClick {



    public interface OnUpdateKitcheNoteList{
        void onUpdateKitchenNoteListListener(List<KitchenNote> spiceList);
    }

    private OnUpdateKitcheNoteList listener;

    private EditText edt_name;

    private Button btn_add;

    private LinearLayout ll_exit;

    private TextView tv_page;

    private RecyclerView rc_kitchen_note;

    private KitchenNoteAdapter adapter;

    private KitchenNoteContract.Presenter presenter;

    private List<KitchenNote> kitchenNoteList;

    private RecyclerView rc_kitchen_note_item;

    private SpiceItemAdapter adapter_kitchen_note_item;

    private KitchenNoteFragment this_;

    public KitchenNoteFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public KitchenNoteFragment(OnUpdateKitcheNoteList listener,List<KitchenNote> datas){
        this.listener = listener;
        this.kitchenNoteList = datas;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kitchen_note, container, false);

        this_ = this;

        rc_kitchen_note = view.findViewById(R.id.rc_kitchen_note);
        adapter = new KitchenNoteAdapter(rc_kitchen_note,getContext(),new ArrayList<KitchenNote>(),this);
        rc_kitchen_note.setAdapter(adapter);
        rc_kitchen_note.setLayoutManager(new LinearLayoutManager(getContext()));

        rc_kitchen_note_item = view.findViewById(R.id.rc_kitchen_note_item);
        adapter_kitchen_note_item = new SpiceItemAdapter(getContext(),new ArrayList<ItemsMontuychon>(),this);
        rc_kitchen_note_item.setAdapter(adapter_kitchen_note_item);
        rc_kitchen_note_item.setLayoutManager(new GridLayoutManager(getContext(),12, GridLayoutManager.HORIZONTAL, false));

        edt_name = view.findViewById(R.id.edt_name);
        btn_add = view.findViewById(R.id.btn_add);

        ll_exit = view.findViewById(R.id.ll_exit);
        ll_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.popFragment(getActivity());
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.addNewItem(edt_name.getText().toString());
            }
        });

        tv_page = view.findViewById(R.id.tv_page);
        tv_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentPageSpiceKitchenDialog editNameDialogFragment = new FragmentPageSpiceKitchenDialog(this_);
                editNameDialogFragment.show(fm, null);

            }
        });

        presenter = new KitchenNotePresenter(this);
        presenter.loadData(kitchenNoteList);

        return view;
    }


    @Override
    public void onItemClick(int position, KitchenNote spice) {

    }

    @Override
    public void onItemLongClick(final int position, KitchenNote kitchenNote) {
        Util.showCustomDialogConfirm(getActivity(), Util.getStringFromResouce(R.string.are_u_w_to_delete), new Util.ConfirmDialogInterface() {
            @Override
            public void onPositiveClickListener() {
                presenter.deletePriceItem(position);
            }
            @Override
            public void onNegativeClickListener() {

            }
        });
    }

    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }

    @Override
    public void showListKitchenNote(List<KitchenNote> datas) {
        adapter.setNewDatas(datas);
    }

    @Override
    public void showListKitchenNoteItem(List<ItemsMontuychon> datas) {
        adapter_kitchen_note_item.setNewDatas(datas);
    }

    @Override
    public void getKitchenNoteList(List<KitchenNote> datas) {
        listener.onUpdateKitchenNoteListListener(datas);
    }


    @Override
    public void onPageItemClickListener(String data) {
        tv_page.setText(data);
        presenter.getAllPageSpiceItem(data);
    }

    @Override
    public void setEditTextEmpty() {
        edt_name.setText("");
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void doClickItem(final int pos) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                rc_kitchen_note.findViewHolderForAdapterPosition(pos).itemView.performClick();
            }
        },1);
    }


    @Override
    public void onItemClick(int position, ItemsMontuychon data) {
        if(!edt_name.getText().toString().isEmpty()){
            if(data.getTextdisplay()!=null) {
                edt_name.setText(edt_name.getText().toString() + ", " + data.getTextdisplay());
            }
        }else{
            edt_name.setText(data.getTextdisplay());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.getKitchenNoteList();
    }
}
