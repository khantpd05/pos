package com.example.abc.pos.database;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.broadcast.WifiReceiver;
import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.ItemsMontuychon;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.model.MobileLogin;
import com.example.abc.pos.model.Orders;
import com.example.abc.pos.model.OrdersDetail;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.model.Tieudedotdat;
import com.example.abc.pos.model.UsersTienbandau;
import com.example.abc.pos.model.UsersTienbandauTemplate;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.utils.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MySqlHandleUtils {
    private static MySqlHandleUtils db;
    private static Connection connection;
    private static String url = "jdbc:mysql://10.0.3.2:3306/wapri";
    private static String user = "root";
    private static String password = "1103Q0904tM";

    private MySqlHandleUtils() {

    }

    public static MySqlHandleUtils getInstance() {
        if (db == null) {
            db = new MySqlHandleUtils();

        }
        return db;
    }

    public Connection getConnection() {
        return connection;
    }

    public String connectToMySql(String url) {
        try {
            if (connection == null) {
                Class.forName("com.mysql.jdbc.Driver");
                Properties props = new Properties();
                props.setProperty("user", user);
                props.setProperty("password", password);
                props.setProperty("connectTimeout", "3000");
                connection = DriverManager.getConnection("jdbc:mysql://" + url.trim() + ":3306/wapri", props);
            }
            return "ok";
        } catch (Exception e) {
            e.printStackTrace();
            connection = null;
//            StringWriter errors = new StringWriter();
//            e.printStackTrace(new PrintWriter(errors));

            return "errors";
        } finally {

        }
    }

    public void disconnectToMySql() {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void connectToDb() {
        List<com.example.abc.pos.model.Connection> connectionList = SharedPrefs.getInstance().getIpListSharePreference();
        MySqlHandleUtils.getInstance().disconnectToMySql();
        if (connectionList != null) {
            for (com.example.abc.pos.model.Connection connection : connectionList) {
                if (connection.isConnected()) {
                    new ConnectToDbTask(connection.getIp()).execute();
                    break;
                }
            }
        }
    }

    private static class ConnectToDbTask extends AsyncTask<String, Void, String> {
        String ip;

        public ConnectToDbTask(String ip) {
            this.ip = ip;
        }

        @Override
        protected String doInBackground(String... strings) {
            MySqlHandleUtils.getInstance();
            return MySqlHandleUtils.getInstance().connectToMySql(ip);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!result.equals("ok")) {
                connectToDb();
            }
        }
    }

    private static JSONArray getJsonArrayFromSql(Statement st, ResultSet rs) {
        JSONArray jsonArray = new JSONArray();
        try {
            try {
                ResultSetMetaData rsMeta = rs.getMetaData();
                int columnCnt = rsMeta.getColumnCount();
                List<String> columnNames = new ArrayList<>();


                for (int i = 1; i <= columnCnt; i++) {
                    columnNames.add(rsMeta.getColumnName(i));
                }

                while (rs.next()) {
                    JSONObject obj = new JSONObject();
                    for (int i = 1; i <= columnCnt; i++) {
                        String key = columnNames.get(i - 1);
                        String value = rs.getString(i);
                        obj.put(key, value);
                    }
                    jsonArray.put(obj);
                }
            } catch (Exception ex) {
                Log.d("///", ex.getMessage());
            } finally {
                rs.close();
                st.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public static List<ItemsMontuychon> selectSpiceStament(String id) {
        PreparedStatement preparedStatement = null;
        List<ItemsMontuychon> itemsMontuychonList = new ArrayList<>();
        if (connection != null) {
            String selectQuery;
            selectQuery = "SELECT * FROM wapri.items_montuychon where id = " + id;
            try {
                preparedStatement = connection.prepareStatement(selectQuery);
                ResultSet rs = preparedStatement.executeQuery();

                ResultSetMetaData rsMeta = rs.getMetaData();
                int columnCnt = rsMeta.getColumnCount();

                int i = 100;
                int j = 100;

                while (rs.next()) {
                    for (int k = 1; k <= columnCnt; k++) {
                        if (i != 700) {
                            String textdisplay = rs.getString("textdisplay_" + i);
                            String color = rs.getString("color_" + i);
                            double giact1 = rs.getDouble("giatc1_" + i);
                            double giact2 = rs.getDouble("giatc2_" + i);
                            double giact3 = rs.getDouble("giatc3_" + i);
                            itemsMontuychonList.add(new ItemsMontuychon(textdisplay, color, giact1, giact2, giact3));
                        } else {
                            break;
                        }


                        if (i >= (j + 11)) {
                            i = i + 88;
                            j = i + 1;
                        }
                        i++;
                    }
                }

                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return itemsMontuychonList;
    }


    public static List<ItemsMontuychon> selectKitchenNoteStament(String id) {
        PreparedStatement preparedStatement = null;
        List<ItemsMontuychon> itemsMontuychonList = new ArrayList<>();
        if (connection != null) {
            String selectQuery;
            selectQuery = "SELECT * FROM wapri.ghichumonan where id = " + id;
            try {
                preparedStatement = connection.prepareStatement(selectQuery);
                ResultSet rs = preparedStatement.executeQuery();

                ResultSetMetaData rsMeta = rs.getMetaData();
                int columnCnt = rsMeta.getColumnCount();

                int i = 100;
                int j = 100;

                while (rs.next()) {
                    for (int k = 1; k <= columnCnt; k++) {
                        if (i != 700) {
                            String textdisplay = rs.getString("textdisplay_" + i);
                            String color = rs.getString("color_" + i);
                            double giact1 = rs.getDouble("giatc1_" + i);
                            double giact2 = rs.getDouble("giatc2_" + i);
                            double giact3 = rs.getDouble("giatc3_" + i);
                            itemsMontuychonList.add(new ItemsMontuychon(textdisplay, color, giact1, giact2, giact3));
                        } else {
                            break;
                        }


                        if (i >= (j + 11)) {
                            i = i + 88;
                            j = i + 1;
                        }
                        i++;
                    }
                }

                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return itemsMontuychonList;
    }

    /* mobile login */
    public static int insertToMobileLogin(MobileLogin mobileLogin) {
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO wapri.mobilelogin " + "(mobile_name, token , is_accepted, date_created) VALUES" + "(?,?,?,?)";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);
                preparedStatement.setString(1, mobileLogin.getMobile_name());
                preparedStatement.setString(2, mobileLogin.getToken());
                preparedStatement.setString(3, mobileLogin.getIs_accepted());
                preparedStatement.setString(4, mobileLogin.getDate_created());
                // execute insert SQL stetement
                preparedStatement.executeUpdate();
                return 1;
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return 0;
            } finally {
                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int update_moibleLogin(MobileLogin mobileLogin) {
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE wapri.mobilelogin SET token = ?, is_accepted = ? , user_accepted = ? , time_accepted = ? ," +
                "time_timeout = ? , date_created = ?" + " WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);

                if (mobileLogin.getToken() != null)
                    preparedStatement.setString(1, mobileLogin.getToken());
                else preparedStatement.setNull(1, Types.VARCHAR);

                if (mobileLogin.getIs_accepted() != null)
                    preparedStatement.setString(2, mobileLogin.getIs_accepted());
                else preparedStatement.setNull(2, Types.VARCHAR);

                preparedStatement.setNull(3, Types.VARCHAR);
                preparedStatement.setNull(4, Types.VARCHAR);
                preparedStatement.setNull(5, Types.VARCHAR);

                if (mobileLogin.getDate_created() != null)
                    preparedStatement.setString(6, mobileLogin.getDate_created());
                else preparedStatement.setNull(6, Types.VARCHAR);

                preparedStatement.setInt(7, mobileLogin.getId());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();
                return 1;
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }


    /* ket tien */
    public static List<UsersTienbandauTemplate> select_users_tienbandau_template() {
        PreparedStatement preparedStatement = null;
        List<UsersTienbandauTemplate> arr = new ArrayList<>();

        if (connection != null) {
            String selectQuery;
            selectQuery = "SELECT * FROM wapri.users_tienbandau_template";
            try {
                preparedStatement = connection.prepareStatement(selectQuery);
                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    UsersTienbandauTemplate usersTienbandauTemplate = new UsersTienbandauTemplate();
                    usersTienbandauTemplate.setId(rs.getInt("id"));

                    usersTienbandauTemplate.setEur500(rs.getInt("500eur"));
                    usersTienbandauTemplate.setEur200(rs.getInt("200eur"));
                    usersTienbandauTemplate.setEur100(rs.getInt("100eur"));
                    usersTienbandauTemplate.setEur50(rs.getInt("50eur"));
                    usersTienbandauTemplate.setEur20(rs.getInt("20eur"));
                    usersTienbandauTemplate.setEur10(rs.getInt("10eur"));
                    usersTienbandauTemplate.setEur5(rs.getInt("5eur"));
                    usersTienbandauTemplate.setEur2(rs.getInt("2eur"));
                    usersTienbandauTemplate.setEur1(rs.getInt("1eur"));

                    usersTienbandauTemplate.setCent50(rs.getInt("50cent"));
                    usersTienbandauTemplate.setCent20(rs.getInt("20cent"));
                    usersTienbandauTemplate.setCent10(rs.getInt("10cent"));
                    usersTienbandauTemplate.setCent5(rs.getInt("5cent"));
                    usersTienbandauTemplate.setCent2(rs.getInt("2cent"));
                    usersTienbandauTemplate.setCent1(rs.getInt("1cent"));

                    usersTienbandauTemplate.setTotalnumbers(rs.getInt("totalnumbers"));
                    usersTienbandauTemplate.setTotalmoney(rs.getDouble("totalmoney"));
                    usersTienbandauTemplate.setUsername(rs.getString("username"));
                    usersTienbandauTemplate.setOffset(rs.getByte("offset"));
                    arr.add(usersTienbandauTemplate);
                }

                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return arr;
    }


    public static List<UsersTienbandauTemplate> select_users_tienbandau_template_by_offset(int offset) {
        PreparedStatement preparedStatement = null;
        List<UsersTienbandauTemplate> arr = new ArrayList<>();

        if (connection != null) {
            String selectQuery;
            selectQuery = "SELECT * FROM wapri.users_tienbandau_template where offset = " + offset;
            try {
                preparedStatement = connection.prepareStatement(selectQuery);
                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    UsersTienbandauTemplate usersTienbandauTemplate = new UsersTienbandauTemplate();
                    usersTienbandauTemplate.setId(rs.getInt("id"));

                    usersTienbandauTemplate.setEur500(rs.getInt("500eur"));
                    usersTienbandauTemplate.setEur200(rs.getInt("200eur"));
                    usersTienbandauTemplate.setEur100(rs.getInt("100eur"));
                    usersTienbandauTemplate.setEur50(rs.getInt("50eur"));
                    usersTienbandauTemplate.setEur20(rs.getInt("20eur"));
                    usersTienbandauTemplate.setEur10(rs.getInt("10eur"));
                    usersTienbandauTemplate.setEur5(rs.getInt("5eur"));
                    usersTienbandauTemplate.setEur2(rs.getInt("2eur"));
                    usersTienbandauTemplate.setEur1(rs.getInt("1eur"));

                    usersTienbandauTemplate.setCent50(rs.getInt("50cent"));
                    usersTienbandauTemplate.setCent20(rs.getInt("20cent"));
                    usersTienbandauTemplate.setCent10(rs.getInt("10cent"));
                    usersTienbandauTemplate.setCent5(rs.getInt("5cent"));
                    usersTienbandauTemplate.setCent2(rs.getInt("2cent"));
                    usersTienbandauTemplate.setCent1(rs.getInt("1cent"));

                    usersTienbandauTemplate.setTotalnumbers(rs.getInt("totalnumbers"));
                    usersTienbandauTemplate.setTotalmoney(rs.getDouble("totalmoney"));
                    usersTienbandauTemplate.setUsername(rs.getString("username"));
                    usersTienbandauTemplate.setOffset(rs.getByte("offset"));
                    arr.add(usersTienbandauTemplate);
                }

                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return arr;
    }


    public static List<UsersTienbandauTemplate> select_users_name_tienbandau_by_name(String userName) {
        PreparedStatement preparedStatement = null;
        List<UsersTienbandauTemplate> arr = new ArrayList<>();

        if (connection != null) {
            String selectQuery;
            selectQuery = "SELECT username FROM wapri.users_tienbandau where username = " + "'" + userName + "'";
            try {
                preparedStatement = connection.prepareStatement(selectQuery);
                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    UsersTienbandauTemplate usersTienbandauTemplate = new UsersTienbandauTemplate();
                    usersTienbandauTemplate.setUsername(rs.getString("username"));
                    arr.add(usersTienbandauTemplate);
                }

                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return arr;
    }

    public static int insert_into_users_tienbandau_template(UsersTienbandauTemplate tienbandauTemplate) {

        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO wapri.users_tienbandau_template " + "(500eur, 200eur, 100eur, 50eur, 20eur, 10eur, 5eur, 2eur, 1eur, 50cent, 20cent, 10cent," +
                "5cent, 2cent, 1cent, totalnumbers, totalmoney, username, offset) VALUES" + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);

                preparedStatement.setInt(1, tienbandauTemplate.getEur500());
                preparedStatement.setInt(2, tienbandauTemplate.getEur200());
                preparedStatement.setInt(3, tienbandauTemplate.getEur100());
                preparedStatement.setInt(4, tienbandauTemplate.getEur50());
                preparedStatement.setInt(5, tienbandauTemplate.getEur20());
                preparedStatement.setInt(6, tienbandauTemplate.getEur10());
                preparedStatement.setInt(7, tienbandauTemplate.getEur5());
                preparedStatement.setInt(8, tienbandauTemplate.getEur2());
                preparedStatement.setInt(9, tienbandauTemplate.getEur1());
                preparedStatement.setInt(10, tienbandauTemplate.getCent50());
                preparedStatement.setInt(11, tienbandauTemplate.getCent20());
                preparedStatement.setInt(12, tienbandauTemplate.getCent10());
                preparedStatement.setInt(13, tienbandauTemplate.getCent5());
                preparedStatement.setInt(14, tienbandauTemplate.getCent2());
                preparedStatement.setInt(15, tienbandauTemplate.getCent1());
                preparedStatement.setInt(16, tienbandauTemplate.getTotalnumbers());
                preparedStatement.setDouble(17, tienbandauTemplate.getTotalmoney());
                preparedStatement.setString(18, tienbandauTemplate.getUsername());
                preparedStatement.setByte(19, tienbandauTemplate.getOffset());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int update_record_users_tienbandau_template(UsersTienbandauTemplate tienbandauTemplate) {
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE wapri.users_tienbandau_template SET 500eur = ?, 200eur = ? ,100eur = ? , 50eur = ? , 20eur = ?, 10eur =?, " +
                "5eur = ?, 2eur = ?, 1eur = ?, 50cent = ?, 20cent = ?, 10cent = ?, 5cent = ?, 2cent = ?, 1cent = ?, totalnumbers = ?," +
                "totalmoney = ?" + " WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);

                preparedStatement.setInt(1, tienbandauTemplate.getEur500());
                preparedStatement.setInt(2, tienbandauTemplate.getEur200());
                preparedStatement.setInt(3, tienbandauTemplate.getEur100());
                preparedStatement.setInt(4, tienbandauTemplate.getEur50());
                preparedStatement.setInt(5, tienbandauTemplate.getEur20());
                preparedStatement.setInt(6, tienbandauTemplate.getEur10());
                preparedStatement.setInt(7, tienbandauTemplate.getEur5());
                preparedStatement.setInt(8, tienbandauTemplate.getEur2());
                preparedStatement.setInt(9, tienbandauTemplate.getEur1());
                preparedStatement.setInt(10, tienbandauTemplate.getCent50());
                preparedStatement.setInt(11, tienbandauTemplate.getCent20());
                preparedStatement.setInt(12, tienbandauTemplate.getCent10());
                preparedStatement.setInt(13, tienbandauTemplate.getCent5());
                preparedStatement.setInt(14, tienbandauTemplate.getCent2());
                preparedStatement.setInt(15, tienbandauTemplate.getCent1());
                preparedStatement.setInt(16, tienbandauTemplate.getTotalnumbers());
                preparedStatement.setDouble(17, tienbandauTemplate.getTotalmoney());
                preparedStatement.setInt(18, tienbandauTemplate.getId());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();
                return 1;
            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int insert_into_users_tienbandau(UsersTienbandau tienbandau) {

        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO wapri.users_tienbandau " + "(500eur, 200eur, 100eur, 50eur, 20eur, 10eur," +
                " 5eur, 2eur, 1eur, 50cent, 20cent, 10cent," +
                "5cent, 2cent, 1cent, totalnumbers, totalmoney, username, type, dateadd) VALUES" + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);

                preparedStatement.setInt(1, tienbandau.getEur500());
                preparedStatement.setInt(2, tienbandau.getEur200());
                preparedStatement.setInt(3, tienbandau.getEur100());
                preparedStatement.setInt(4, tienbandau.getEur50());
                preparedStatement.setInt(5, tienbandau.getEur20());
                preparedStatement.setInt(6, tienbandau.getEur10());
                preparedStatement.setInt(7, tienbandau.getEur5());
                preparedStatement.setInt(8, tienbandau.getEur2());
                preparedStatement.setInt(9, tienbandau.getEur1());
                preparedStatement.setInt(10, tienbandau.getCent50());
                preparedStatement.setInt(11, tienbandau.getCent20());
                preparedStatement.setInt(12, tienbandau.getCent10());
                preparedStatement.setInt(13, tienbandau.getCent5());
                preparedStatement.setInt(14, tienbandau.getCent2());
                preparedStatement.setInt(15, tienbandau.getCent1());
                preparedStatement.setInt(16, tienbandau.getTotalnumbers());
                preparedStatement.setDouble(17, tienbandau.getTotalmoney());
                preparedStatement.setString(18, tienbandau.getUsername());
                preparedStatement.setString(19, tienbandau.getType());
                preparedStatement.setString(20, tienbandau.getDateadd());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int update_into_users_tienbandau(UsersTienbandau tienbandau) {

        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE wapri.users_tienbandau SET 500eur = ?, 200eur = ? ,100eur = ? , 50eur = ? , 20eur = ?, 10eur =?, " +
                "5eur = ?, 2eur = ?, 1eur = ?, 50cent = ?, 20cent = ?, 10cent = ?, 5cent = ?, 2cent = ?, 1cent = ?, totalnumbers = ?," +
                "totalmoney = ? , dateadd = ?" + " WHERE username = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);

                preparedStatement.setInt(1, tienbandau.getEur500());
                preparedStatement.setInt(2, tienbandau.getEur200());
                preparedStatement.setInt(3, tienbandau.getEur100());
                preparedStatement.setInt(4, tienbandau.getEur50());
                preparedStatement.setInt(5, tienbandau.getEur20());
                preparedStatement.setInt(6, tienbandau.getEur10());
                preparedStatement.setInt(7, tienbandau.getEur5());
                preparedStatement.setInt(8, tienbandau.getEur2());
                preparedStatement.setInt(9, tienbandau.getEur1());
                preparedStatement.setInt(10, tienbandau.getCent50());
                preparedStatement.setInt(11, tienbandau.getCent20());
                preparedStatement.setInt(12, tienbandau.getCent10());
                preparedStatement.setInt(13, tienbandau.getCent5());
                preparedStatement.setInt(14, tienbandau.getCent2());
                preparedStatement.setInt(15, tienbandau.getCent1());
                preparedStatement.setInt(16, tienbandau.getTotalnumbers());
                preparedStatement.setDouble(17, tienbandau.getTotalmoney());
                preparedStatement.setString(18, tienbandau.getDateadd());
                preparedStatement.setString(19, tienbandau.getUsername());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int updateTienBanDauUser(double tienBanDau, int id) {
        PreparedStatement preparedStatement = null;

        String updateTableSQL = "UPDATE wapri.users SET tienbandau = ?  WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(updateTableSQL);
                preparedStatement.setDouble(1, tienBanDau);
                preparedStatement.setInt(2, id);

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static String selectOrderNotPrintFromTable(int id) {
        PreparedStatement preparedStatement = null;
        String daprintchua = "";
        if (connection != null) {
            String selectQuery;
            selectQuery = "SELECT daprintchua FROM wapri.orders_temp where banso = ? and daprintchua = 'N' group by daprintchua";
            try {
                preparedStatement = connection.prepareStatement(selectQuery);
                preparedStatement.setInt(1, id);
                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    daprintchua = rs.getString("daprintchua");
                }
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daprintchua;
    }

    public static String selectMaSoHoaDon() {
        PreparedStatement preparedStatement = null;
        String id = "";
        if (connection != null) {
            String selectQuery;
            selectQuery = "SELECT SUBSTRING_INDEX(id, '-', -1) as sothutu FROM wapri.orders_" + Util.currrentYear() + " WHERE DATE(NOW()) = DATE(ngaymua) and LEFT(id,locate('-',id)-1) = '" + WapriManager.DEVICE_NAME + "' ORDER BY id DESC limit 1";
            try {
                preparedStatement = connection.prepareStatement(selectQuery);
                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    id = rs.getString("sothutu");
                }
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return id;
    }

    public static String selectThongtincuahang() {
        PreparedStatement preparedStatement = null;
        String restaurantName = "";
        if (connection != null) {
            String selectQuery;
            selectQuery = "SELECT tencuahang FROM wapri.thongtincuahang";
            try {
                preparedStatement = connection.prepareStatement(selectQuery);
                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    restaurantName = rs.getString("tencuahang");
                }

                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return restaurantName;
    }


    public static String selectTimeZoneMySQL() {
        PreparedStatement preparedStatement = null;
        String timeZone = "";
        if (connection != null) {
            String selectQuery;
            selectQuery = "SELECT TIMEDIFF(NOW(), UTC_TIMESTAMP) timezone";
            try {
                preparedStatement = connection.prepareStatement(selectQuery);
                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    timeZone = rs.getString("timezone");
                }

                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return timeZone;
    }

    public static JSONArray selectStament(String qr) {
        JSONArray jsonArray = new JSONArray();

        if (connection != null) {
            try {
                Statement st = connection.createStatement();
                ResultSet rs = st.executeQuery(qr);

                try {
                    ResultSetMetaData rsMeta = rs.getMetaData();
                    int columnCnt = rsMeta.getColumnCount();
                    List<String> columnNames = new ArrayList<>();


                    for (int i = 1; i <= columnCnt; i++) {
                        columnNames.add(rsMeta.getColumnName(i));
                    }

                    while (rs.next()) {
                        JSONObject obj = new JSONObject();
                        for (int i = 1; i <= columnCnt; i++) {
                            String key = columnNames.get(i - 1);
                            String value = rs.getString(i);
                            obj.put(key, value);
                        }
                        jsonArray.put(obj);
                    }
                } catch (Exception ex) {
                    Log.d("///", ex.getMessage() + "");
                } finally {
                    rs.close();
                    st.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                connectToDb();
            }
        } else {
            connectToDb();
        }

        return jsonArray;
    }


    public static int insertRecordIntoItem(Items item) {

        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO wapri.items " + "(items_id, nhomhang , tenhang, bonname, giale1, giale2, giale3, gialemangve1, color, loai_mon_an, bophanphucvu) VALUES" + "(?,?,?,?,?,?,?,?,?,?,?)";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);
                preparedStatement.setString(1, item.getId());
                preparedStatement.setInt(2, item.getNhomhang());
                preparedStatement.setString(3, item.getTenhang());
                preparedStatement.setString(4, item.getBonname());
                preparedStatement.setDouble(5, item.getGiale1());
                preparedStatement.setNull(6, Types.DOUBLE);
                preparedStatement.setNull(7, Types.DOUBLE);
                preparedStatement.setDouble(8, item.getGialemangve1());
                preparedStatement.setString(9, item.getColor());
                preparedStatement.setString(10, item.getLoaiMonAn());
                preparedStatement.setString(11, item.getBophanphucvu());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                if (e.getMessage().contains("Duplicate")) return 2;
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int updateRecordIntoItem(String old_id, Items item) {
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE wapri.items SET items_id = ?, tenhang = ? , bonname = ?, giale1 = ? , gialemangve1 = ?, color = ? , bophanphucvu = ?" + " WHERE items_id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);

                preparedStatement.setString(1, item.getId());
                preparedStatement.setString(2, item.getTenhang());
                preparedStatement.setString(3, item.getBonname());
                preparedStatement.setDouble(4, item.getGiale1());
                preparedStatement.setDouble(5, item.getGialemangve1());
                preparedStatement.setString(6, item.getColor());
                preparedStatement.setString(7, item.getBophanphucvu());
                preparedStatement.setString(8, old_id);

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                if (e.getMessage().contains("Duplicate"))
                    return 2;
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int insertRecordIntoMenu(Menu menu) {

        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO wapri.menu " + "(name, thutusapxepkhihienthi, color, loai_mon_an) VALUES" + "(?,?,?,?)";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);

                preparedStatement.setString(1, menu.getName());
                preparedStatement.setInt(2, menu.getThutusapxepkhihienthi());
                preparedStatement.setString(3, menu.getColor());
                preparedStatement.setString(4, menu.getLoaiMonAn());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }


    public static int updateTableState(Table table) {
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE wapri.table SET trangthai = ?, username = ? , thoigian = ?, tongtien = ?, giam = ?" + " WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);
                preparedStatement.setString(1, table.getTrangthai());
                if (table.getUsername() != null) {
                    preparedStatement.setString(2, table.getUsername());
                } else {
                    preparedStatement.setNull(2, Types.VARCHAR);
                }
                if (table.getUsername() != null) {
                    preparedStatement.setString(3, table.getThoigian());
                } else {
                    preparedStatement.setNull(3, Types.VARCHAR);
                }

                if (table.getTongtien() != null) {
                    preparedStatement.setDouble(4, table.getTongtien());
                } else {
                    preparedStatement.setNull(4, Types.DOUBLE);
                }
                preparedStatement.setInt(5, table.getGiam());

                preparedStatement.setInt(6, table.getId());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();
                return 1;
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return 0;
            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int updateRecordIntoMenu(Menu menu) {
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE wapri.menu SET name = ? , thutusapxepkhihienthi = ? , color = ? , loai_mon_an = ?" + " WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);

                preparedStatement.setString(1, menu.getName());
                preparedStatement.setInt(2, menu.getThutusapxepkhihienthi());
                preparedStatement.setString(3, menu.getColor());
                preparedStatement.setString(4, menu.getLoaiMonAn());
                preparedStatement.setInt(5, menu.getId());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int deleteRecordFromMenu(int id) {
        PreparedStatement preparedStatement = null;

        String deleteTableSQL = "DELETE from wapri.menu WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(deleteTableSQL);
                preparedStatement.setInt(1, id);
                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }


    public static int deleteRecordFromItem(String id) {
        PreparedStatement preparedStatement = null;

        String deleteTableSQL = "DELETE from wapri.items WHERE items_id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(deleteTableSQL);
                preparedStatement.setString(1, id);
                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    /* TieuDeDotDat */

    public static int insertRecordIntoTieuDeDotDat(String name) {
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "insert into wapri.tieudedotdat (`name`) values (" + "'" + name + "')";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);
                // execute insert SQL stetement
                preparedStatement.execute(insertTableSQL);
                return 1;

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int updateNextOrderItem(Tieudedotdat item) {
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE wapri.tieudedotdat SET name = ?" + " WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);

                preparedStatement.setString(1, item.getName());
                preparedStatement.setInt(2, item.getId());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();
                return 1;
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return 0;
            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int deleteRecordFromTieuDeDotDat(int id) {
        PreparedStatement preparedStatement = null;

        String deleteTableSQL = "DELETE from wapri.tieudedotdat WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(deleteTableSQL);
                preparedStatement.setInt(1, id);
                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    /* restaurant check and gutschein */
    public static int updateValues(String value, String keyid) {
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE wapri.configuration SET value = ?" + " WHERE keyid = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);

                preparedStatement.setString(1, value);
                preparedStatement.setString(2, keyid);

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int insertRestaurantAndGutschein(String keyid, String value) {
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "insert into wapri.configuration (`keyid`, `value`) values (" + "'" + keyid + "'" + ", '" + value + "')";


        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);
                // execute insert SQL stetement
                preparedStatement.execute(insertTableSQL);
                return 1;

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            return 0;
        }

    }


    /* change table */

    public static int updateTableChangeOld(Table table_old) {
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE wapri.table SET trangthai = ? , username = ? , tongtien = ? , giam = ? , " +
                "tiengiam =? , tienthue19giam = ? , tienthue7giam = ? , thoigian =?"
                + " WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);

                preparedStatement.setString(1, table_old.getTrangthai());
                preparedStatement.setString(2, table_old.getUsername());
                preparedStatement.setNull(3, Types.DOUBLE);
                preparedStatement.setNull(4, Types.TINYINT);
                preparedStatement.setNull(5, Types.DOUBLE);
                preparedStatement.setNull(6, Types.DOUBLE);
                preparedStatement.setNull(7, Types.DOUBLE);
                preparedStatement.setString(8, table_old.getThoigian());
                preparedStatement.setInt(9, table_old.getId());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int updateTableChangeNew(Table table_new) {
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE wapri.table SET trangthai = ? , username = ? , tongtien = ? , giam = ? , " +
                "tiengiam =? , tienthue19giam = ? , tienthue7giam = ? , thoigian =?, from_tablet = ? ,is_selling_on_tablet = ?, is_selling_on_POS = ? "
                + " WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);

                preparedStatement.setString(1, table_new.getTrangthai());
                preparedStatement.setString(2, table_new.getUsername());

                if (table_new.getTongtien() != null && table_new.getTongtien() > 0)
                    preparedStatement.setDouble(3, table_new.getTongtien());
                else preparedStatement.setNull(3, Types.DOUBLE);

                if (table_new.getGiam() != null)
                    preparedStatement.setInt(4, table_new.getGiam());
                else preparedStatement.setNull(4, Types.TINYINT);

                if (table_new.getTiengiam() != null)
                    preparedStatement.setDouble(5, table_new.getTiengiam());
                else preparedStatement.setNull(5, Types.DOUBLE);

                if (table_new.getTienthue19giam() != null)
                    preparedStatement.setDouble(6, table_new.getTienthue19giam());
                else preparedStatement.setNull(6, Types.DOUBLE);

                if (table_new.getTienthue7giam() != null)
                    preparedStatement.setDouble(7, table_new.getTienthue7giam());
                else preparedStatement.setNull(7, Types.DOUBLE);


                if (table_new.getThoigian() != null)
                    preparedStatement.setString(8, table_new.getThoigian());
                else preparedStatement.setNull(8, Types.VARCHAR);



                preparedStatement.setString(9, table_new.getfrom_tablet());

                preparedStatement.setString(10, table_new.getis_selling_on_tablet());
                preparedStatement.setString(11, table_new.getIs_selling_on_POS());
                preparedStatement.setInt(12, table_new.getId());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            return 0;
        }

    }

    /* chức năng của table*/
    public static int updateChangeOrderTableToNewTable(OrdersTemp ordersTemp, String table_name) {
        PreparedStatement preparedStatement = null;
        String updateTableSQL = "UPDATE wapri.orders_temp SET banso = ? , thue = ? , tienthue = ? , taichohaymangve = ? WHERE id = ?";
        int result;
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(updateTableSQL);
                preparedStatement.setString(1, table_name);
                preparedStatement.setInt(2, ordersTemp.getThue());
                preparedStatement.setDouble(3, ordersTemp.getTienthue());
                preparedStatement.setString(4, ordersTemp.getTaichohaymangve().value);
                preparedStatement.setString(5, ordersTemp.getId());
                // execute insert SQL stetement
                result = preparedStatement.executeUpdate();
                return result;
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return 0;
            } finally {
                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }



    /* Order */

    public static JSONArray selectOrderTempListByCustomerPositionStament(Table table, int position) {
        JSONArray jsonArray = new JSONArray();
        PreparedStatement preparedStatement = null;
        if (connection != null) {
            String selectQuery;
            if (position == 0) {
                selectQuery = "SELECT * FROM wapri.orders_temp where banso = ?  ORDER by id asc";
                try {

                    preparedStatement = connection.prepareStatement(selectQuery);
                    preparedStatement.setString(1, table.getName());


                    ResultSet rs = preparedStatement.executeQuery();

                    jsonArray = getJsonArrayFromSql(preparedStatement, rs);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                selectQuery = "SELECT * FROM wapri.orders_temp where banso = ? AND thutukhachtrenban = ? ORDER by id asc";
                try {

                    preparedStatement = connection.prepareStatement(selectQuery);
                    preparedStatement.setString(1, table.getName());
                    preparedStatement.setInt(2, position);


                    ResultSet rs = preparedStatement.executeQuery();

                    jsonArray = getJsonArrayFromSql(preparedStatement, rs);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }


        } else {

        }

        return jsonArray;
    }


    public static int insertOrderItem(OrdersTemp ordersTemp) {

        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO wapri.orders_temp " + "(id, banso, items_id, ngaymua, tenhang, bonname, thue, orig_price_from_item," +
                "orig_price_no_extra, giatien, thanhtien, soluong, username, thoigian," +
                " bophanphucvu, taichohaymangve, thutukhachtrenban, daprintxoachua, tienthue, tiengiam, daprintchua," +
                " phantramgiam, giavi_text, giavi_price, giavi_text_2, giavi_price_2, giavi_text_3, giavi_price_3," +
                " giavi_text_4, giavi_price_4, giavi_text_5, giavi_price_5, giavi_text_6, giavi_price_6," +
                " giavi_text_7, giavi_price_7, giavi_text_8, giavi_price_8, giavi_text_9, giavi_price_9," +
                " giavi_text_10, giavi_price_10, ghichu, ghichu2, inlaimonfromtablet, loai_mon_an," +
                " phanangomco , phanangomco_soluong,  phanangomco_value," +
                " phanangomco2, phanangomco2_soluong, phanangomco2_value," +
                " phanangomco3, phanangomco3_soluong, phanangomco3_value," +
                " phanangomco4, phanangomco4_soluong, phanangomco4_value," +
                " phanangomco5, phanangomco5_soluong, phanangomco5_value," +
                " phanangomco6, phanangomco6_soluong, phanangomco6_value," +
                " phanangomco7, phanangomco7_soluong, phanangomco7_value," +
                " phanangomco8, phanangomco8_soluong, phanangomco8_value," +
                " phanangomco9, phanangomco9_soluong, phanangomco9_value, kichcomonan) VALUES" + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);
                preparedStatement.setString(1, ordersTemp.getId());
                preparedStatement.setString(2, ordersTemp.getBanso());
                preparedStatement.setString(3, ordersTemp.getitems_id());
                preparedStatement.setString(4, ordersTemp.getNgaymua());
                preparedStatement.setString(5, ordersTemp.getTenhang());
                preparedStatement.setString(6, ordersTemp.getBonname());
                preparedStatement.setDouble(7, ordersTemp.getThue());
                preparedStatement.setDouble(8, ordersTemp.getorig_price_from_item());
                preparedStatement.setDouble(9, ordersTemp.getorig_price_no_extra());
                preparedStatement.setDouble(10, ordersTemp.getGiatien());
                preparedStatement.setDouble(11, ordersTemp.getThanhtien());
                preparedStatement.setDouble(12, ordersTemp.getSoluong());
                preparedStatement.setString(13, ordersTemp.getUsername());
                preparedStatement.setString(14, ordersTemp.getThoigian());
                preparedStatement.setString(15, ordersTemp.getBophanphucvu());
                preparedStatement.setString(16, ordersTemp.getTaichohaymangve().value);
                preparedStatement.setInt(17, ordersTemp.getThutukhachtrenban());
                preparedStatement.setString(18, ordersTemp.getDaprintxoachua());
                preparedStatement.setDouble(19, ordersTemp.getTienthue());
                preparedStatement.setDouble(20, ordersTemp.getTiengiam());
                preparedStatement.setString(21, ordersTemp.getDaprintchua());

                preparedStatement.setDouble(22, ordersTemp.getPhantramgiam());

                preparedStatement.setString(23, ordersTemp.getgiavi_text());
                preparedStatement.setDouble(24, ordersTemp.getgiavi_price());

                preparedStatement.setString(25, ordersTemp.getgiavi_text_2());
                preparedStatement.setDouble(26, ordersTemp.getgiavi_price_2());

                preparedStatement.setString(27, ordersTemp.getgiavi_text_3());
                preparedStatement.setDouble(28, ordersTemp.getgiavi_price_3());

                preparedStatement.setString(29, ordersTemp.getgiavi_text_4());
                preparedStatement.setDouble(30, ordersTemp.getgiavi_price_4());

                preparedStatement.setString(31, ordersTemp.getgiavi_text_5());
                preparedStatement.setDouble(32, ordersTemp.getgiavi_price_5());

                preparedStatement.setString(33, ordersTemp.getgiavi_text_6());
                preparedStatement.setDouble(34, ordersTemp.getgiavi_price_6());

                preparedStatement.setString(35, ordersTemp.getgiavi_text_7());
                preparedStatement.setDouble(36, ordersTemp.getgiavi_price_7());

                preparedStatement.setString(37, ordersTemp.getgiavi_text_8());
                preparedStatement.setDouble(38, ordersTemp.getgiavi_price_8());

                preparedStatement.setString(39, ordersTemp.getgiavi_text_9());
                preparedStatement.setDouble(40, ordersTemp.getgiavi_price_9());

                preparedStatement.setString(41, ordersTemp.getgiavi_text_10());
                preparedStatement.setDouble(42, ordersTemp.getgiavi_price_10());

                preparedStatement.setString(43, ordersTemp.getGhichu());
                preparedStatement.setString(44, ordersTemp.getGhichu2());
                preparedStatement.setString(45, ordersTemp.getInlaimonfromtablet());
                preparedStatement.setString(46, ordersTemp.getloai_mon_an());

                preparedStatement.setString(47, ordersTemp.getPhanangomco());
                preparedStatement.setInt(48, ordersTemp.getphanangomco_soluong());
                preparedStatement.setDouble(49, ordersTemp.getphanangomco_value());

                preparedStatement.setString(50, ordersTemp.getPhanangomco2());
                preparedStatement.setInt(51, ordersTemp.getphanangomco2_soluong());
                preparedStatement.setDouble(52, ordersTemp.getPhanangomco2Value());

                preparedStatement.setString(53, ordersTemp.getPhanangomco3());
                preparedStatement.setInt(54, ordersTemp.getphanangomco3_soluong());
                preparedStatement.setDouble(55, ordersTemp.getPhanangomco3Value());

                preparedStatement.setString(56, ordersTemp.getPhanangomco4());
                preparedStatement.setInt(57, ordersTemp.getphanangomco4_soluong());
                preparedStatement.setDouble(58, ordersTemp.getPhanangomco4Value());

                preparedStatement.setString(59, ordersTemp.getPhanangomco5());
                preparedStatement.setInt(60, ordersTemp.getphanangomco5_soluong());
                preparedStatement.setDouble(61, ordersTemp.getPhanangomco5Value());

                preparedStatement.setString(62, ordersTemp.getPhanangomco6());
                preparedStatement.setInt(63, ordersTemp.getphanangomco6_soluong());
                preparedStatement.setDouble(64, ordersTemp.getPhanangomco6Value());

                preparedStatement.setString(65, ordersTemp.getPhanangomco7());
                preparedStatement.setInt(66, ordersTemp.getphanangomco7_soluong());
                preparedStatement.setDouble(67, ordersTemp.getPhanangomco7Value());

                preparedStatement.setString(68, ordersTemp.getPhanangomco8());
                preparedStatement.setInt(69, ordersTemp.getphanangomco8_soluong());
                preparedStatement.setDouble(70, ordersTemp.getPhanangomco8Value());

                preparedStatement.setString(71, ordersTemp.getPhanangomco9());
                preparedStatement.setInt(72, ordersTemp.getphanangomco9_soluong());
                preparedStatement.setDouble(73, ordersTemp.getPhanangomco9Value());

                preparedStatement.setString(74, ordersTemp.getKichcomonan());


                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                if (e.getMessage().contains("Duplicate")) return 2;
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;

    }


    public static int updateTieuDeDotDatOrderToTable(String orderId, String name) {
        PreparedStatement preparedStatement = null;

        String sql = "UPDATE wapri.orders_temp SET tenhang = ?, bonname = ?  WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, name);
                preparedStatement.setString(3, orderId);
                // execute insert SQL stetement
                preparedStatement.executeUpdate();
                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int updateLyDoHuyOrderTemp(String orderId, String lydo) {
        PreparedStatement preparedStatement = null;

        String sql = "UPDATE wapri.orders_temp SET danhdaubixoa = ?, lydohuyxoa = ?  WHERE id = ?";

        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, "Y");
                preparedStatement.setString(2, lydo);
                preparedStatement.setString(3, orderId);
                // execute insert SQL stetement
                preparedStatement.executeUpdate();
                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int updateInBepOrderTemp(OrdersTemp ordersTemp) {
        PreparedStatement preparedStatement = null;

        String sql = "UPDATE wapri.orders_temp SET inlaimonfromtablet = ?, daprintxoachua = ?  WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(sql);

                preparedStatement.setString(1, ordersTemp.getInlaimonfromtablet());
                preparedStatement.setString(2, ordersTemp.getDaprintxoachua());
                preparedStatement.setString(3, ordersTemp.getId());
                // execute insert SQL stetement
                preparedStatement.executeUpdate();
                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int updateOrderTempItem(OrdersTemp ordersTemp) {

        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE wapri.orders_temp " + "SET banso = ?, items_id = ?, ngaymua = ?, tenhang = ?, bonname = ?, thue =?," +
                "orig_price_from_item = ?, orig_price_no_extra = ?, giatien = ?, thanhtien = ?, phantramgiam = ?, tiengiam = ?, soluong = ?," +
                " username = ?, thoigian = ?, daprintchua = ?, phantramgiamtrentoandonhang = ?, danhdaubixoa = ?, giavi_text = ?, giavi_price = ?," +
                " giavi_text_2 = ?, giavi_price_2 = ?, giavi_text_3 = ?, giavi_price_3 = ?, giavi_text_4 = ?, giavi_price_4 = ?, giavi_text_5 = ?," +
                " giavi_price_5 = ?, giavi_text_6 = ?, giavi_price_6 = ?, giavi_text_7 = ?, giavi_price_7 = ?, giavi_text_8 = ?, giavi_price_8 = ?," +
                " giavi_text_9 = ?, giavi_price_9 = ?, ghichu = ?, ghichu2 = ?, bophanphucvu = ?, taichohaymangve = ?, thutukhachtrenban = ?," +
                " daprintxoachua = ? , daprintchua = ?, tienthue = ?, inlaimonfromtablet = ?, lydohuyxoa = ?" +
                " WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);
                preparedStatement.setString(1, ordersTemp.getBanso());
                preparedStatement.setString(2, ordersTemp.getitems_id());
                preparedStatement.setString(3, ordersTemp.getNgaymua());
                preparedStatement.setString(4, ordersTemp.getTenhang());
                preparedStatement.setString(5, ordersTemp.getBonname());
                preparedStatement.setDouble(6, ordersTemp.getThue());
                preparedStatement.setDouble(7, ordersTemp.getorig_price_from_item());
                preparedStatement.setDouble(8, ordersTemp.getorig_price_no_extra());
                preparedStatement.setDouble(9, ordersTemp.getGiatien());
                preparedStatement.setDouble(10, ordersTemp.getThanhtien());
                preparedStatement.setDouble(11, ordersTemp.getPhantramgiam());
                preparedStatement.setDouble(12, ordersTemp.getTiengiam());
                preparedStatement.setInt(13, ordersTemp.getSoluong());
                preparedStatement.setString(14, ordersTemp.getUsername());
                preparedStatement.setString(15, ordersTemp.getThoigian());
                preparedStatement.setString(16, ordersTemp.getDaprintxoachua());
                preparedStatement.setInt(17, ordersTemp.getPhantramgiam());
                preparedStatement.setString(18, ordersTemp.getDanhdaubixoa());

                preparedStatement.setString(19, ordersTemp.getgiavi_text());
                preparedStatement.setDouble(20, ordersTemp.getgiavi_price());

                preparedStatement.setString(21, ordersTemp.getgiavi_text_2());
                preparedStatement.setDouble(22, ordersTemp.getgiavi_price_2());

                preparedStatement.setString(23, ordersTemp.getgiavi_text_3());
                preparedStatement.setDouble(24, ordersTemp.getgiavi_price_3());

                preparedStatement.setString(25, ordersTemp.getgiavi_text_4());
                preparedStatement.setDouble(26, ordersTemp.getgiavi_price_4());

                preparedStatement.setString(27, ordersTemp.getgiavi_text_5());
                preparedStatement.setDouble(28, ordersTemp.getgiavi_price_5());

                preparedStatement.setString(29, ordersTemp.getgiavi_text_6());
                preparedStatement.setDouble(30, ordersTemp.getgiavi_price_6());

                preparedStatement.setString(31, ordersTemp.getgiavi_text_7());
                preparedStatement.setDouble(32, ordersTemp.getgiavi_price_7());

                preparedStatement.setString(33, ordersTemp.getgiavi_text_8());
                preparedStatement.setDouble(34, ordersTemp.getgiavi_price_8());

                preparedStatement.setString(35, ordersTemp.getgiavi_text_9());
                preparedStatement.setDouble(36, ordersTemp.getgiavi_price_9());

                preparedStatement.setString(37, ordersTemp.getGhichu());
                preparedStatement.setString(38, ordersTemp.getGhichu2());

                preparedStatement.setString(39, ordersTemp.getBophanphucvu());
                preparedStatement.setString(40, ordersTemp.getTaichohaymangve().value);
                preparedStatement.setInt(41, ordersTemp.getThutukhachtrenban());
                preparedStatement.setString(42, ordersTemp.getDaprintxoachua());
                preparedStatement.setString(43, ordersTemp.getDaprintchua());
                preparedStatement.setDouble(44, ordersTemp.getTienthue());
                preparedStatement.setString(45, ordersTemp.getInlaimonfromtablet());
                preparedStatement.setString(46, ordersTemp.getLydohuyxoa());
                preparedStatement.setString(47, ordersTemp.getId());


                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int updateChangeOrderToTable(String orderId, int thutukhachtrenban, String table_name) {
        PreparedStatement preparedStatement = null;

        String updateTableSQL = "UPDATE wapri.orders_temp SET banso = ?, thutukhachtrenban = ?  WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(updateTableSQL);
                preparedStatement.setString(1, table_name);
                preparedStatement.setInt(2, thutukhachtrenban);
                preparedStatement.setString(3, orderId);
                // execute insert SQL stetement
                preparedStatement.executeUpdate();
                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }


    public static int deleteRecordFromOrderTemp(String id) {
        PreparedStatement preparedStatement = null;

        String deleteTableSQL = "DELETE from wapri.orders_temp WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(deleteTableSQL);
                preparedStatement.setString(1, id);
                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }



    /* Order 2018 Table */

    public static int insertOrder2018Item(Orders orders) {

        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO wapri.orders_" + Util.currrentYear() + " (id, ngaymua, thoigian, linktodetails, nhanvien, banso," +
                "giatien, include_total_tienmat_hoac_the, include_total_coupon_value, include_total_restaurantcheck_value, cachtra, giam," +
                " tiengiam, tienthue19giam, tienthue7giam, dathanhtoantienvoinhanvienchua, datongketcuoingaychua, `delete`, ladonhangtralai," +
                " taichohaymangve, tienkhachdua, tienthoilai, orderfrom, dainxacnhanlaixe, ladonhangcomathangbixoa, ladonhangcodoicachthanhtoantien," +
                "daprintchua, from_tablet, loaiIn, inlaihoadonfromtablet ) VALUES" + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertTableSQL);
                preparedStatement.setString(1, orders.getId());
                preparedStatement.setString(2, orders.getNgaymua());
                preparedStatement.setString(3, orders.getThoigian());
                preparedStatement.setString(4, orders.getLinktodetails());
                preparedStatement.setString(5, orders.getNhanvien());
                preparedStatement.setString(6, orders.getBanso());
                preparedStatement.setDouble(7, orders.getGiatien());
                preparedStatement.setDouble(8, orders.getInclude_total_tienmat_hoac_the());
                preparedStatement.setDouble(9, orders.getInclude_total_coupon_value());
                preparedStatement.setDouble(10, orders.getInclude_total_restaurantcheck_value());
                preparedStatement.setString(11, orders.getCachtra().value);
                preparedStatement.setDouble(12, orders.getGiam());
                preparedStatement.setDouble(13, orders.getTiengiam());
                preparedStatement.setDouble(14, orders.getTienthue19giam());
                preparedStatement.setDouble(15, orders.getTienthue7giam());
                preparedStatement.setString(16, orders.getDathanhtoantienvoinhanvienchua());
                preparedStatement.setString(17, orders.getDatongketcuoingaychua());
                preparedStatement.setString(18, orders.getDelete());
                preparedStatement.setString(19, orders.getLadonhangtralai());
                preparedStatement.setString(20, orders.getTaichohaymangve());
                preparedStatement.setDouble(21, orders.getTienkhachdua());
                preparedStatement.setDouble(22, orders.getTienthoilai());
                preparedStatement.setString(23, orders.getOrderfrom());
                preparedStatement.setString(24, orders.getDainxacnhanlaixe());
                preparedStatement.setString(25, orders.getLadonhangcomathangbixoa());
                preparedStatement.setString(26, orders.getLadonhangcodoicachthanhtoantien());
                preparedStatement.setString(27, orders.getDaprintchua());
                preparedStatement.setString(28, orders.getfrom_tablet());
                preparedStatement.setString(29, orders.getLoaiIn());
                preparedStatement.setString(30, orders.getInlaihoadonfromtablet());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }


    public static int insertOrderDetail2018Item(OrdersDetail ordersDetail) {

        PreparedStatement preparedStatement = null;

        String insertSQL = "INSERT INTO wapri.orders_detail_" + Util.currrentYear() + " (id, linktodetails, orders_id, ngaymua, banso, items_id, tenhang, bonname," +
                "kichcomonan, soluong, thue, tienthue, orig_price_from_item, orig_price_no_extra, giatien, thanhtien, phantramgiam, tiengiam," +
                "username, loatdat, thoigian, taichohaymangve, danhdaubixoa, dathanhtoantienvoinhanvienchua, quaythanhtoanso," +
                "  giavi_text, giavi_price, giavi_text_2, giavi_price_2, giavi_text_3, giavi_price_3," +
                "  giavi_text_4, giavi_price_4, giavi_text_5, giavi_price_5, giavi_text_6, giavi_price_6," +
                "  giavi_text_7, giavi_price_7, giavi_text_8, giavi_price_8, giavi_text_9, giavi_price_9," +
                "  giavi_text_10, giavi_price_10, bophanphucvu, daprintchua, datongketcuoingaychua, orderfrom, is_tiengiamtructiep," +
                "  lydohuyxoa, loai_mon_an, daprintxoachua, kichcomonanngonngu, phantramgiamtrentoandonhang," +
                "  phanangomco , phanangomco_soluong,  phanangomco_value," +
                "  phanangomco2, phanangomco2_soluong, phanangomco2_value," +
                "  phanangomco3, phanangomco3_soluong, phanangomco3_value," +
                "  phanangomco4, phanangomco4_soluong, phanangomco4_value," +
                "  phanangomco5, phanangomco5_soluong, phanangomco5_value," +
                "  phanangomco6, phanangomco6_soluong, phanangomco6_value," +
                "  phanangomco7, phanangomco7_soluong, phanangomco7_value," +
                "  phanangomco8, phanangomco8_soluong, phanangomco8_value," +
                "  phanangomco9, phanangomco9_soluong, phanangomco9_value, cachtra, ghichu ,ghichu2 ) VALUES" + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(insertSQL);
                preparedStatement.setString(1, ordersDetail.getId());
                preparedStatement.setString(2, ordersDetail.getLinktodetails());
                preparedStatement.setString(3, ordersDetail.getorders_id());
                preparedStatement.setString(4, ordersDetail.getNgaymua());
                preparedStatement.setString(5, ordersDetail.getBanso());
                preparedStatement.setString(6, ordersDetail.getitems_id());
                preparedStatement.setString(7, ordersDetail.getTenhang());
                preparedStatement.setString(8, ordersDetail.getBonname());
                preparedStatement.setString(9, ordersDetail.getKichcomonan());
                preparedStatement.setInt(10, ordersDetail.getSoluong());
                preparedStatement.setInt(11, ordersDetail.getThue());
                preparedStatement.setDouble(12, ordersDetail.getTienthue());
                preparedStatement.setDouble(13, ordersDetail.getorig_price_from_item());
                preparedStatement.setDouble(14, ordersDetail.getorig_price_no_extra());
                preparedStatement.setDouble(15, ordersDetail.getGiatien());
                preparedStatement.setDouble(16, ordersDetail.getThanhtien());
                preparedStatement.setDouble(17, ordersDetail.getPhantramgiam());
                preparedStatement.setDouble(18, ordersDetail.getTiengiam());
                preparedStatement.setString(19, ordersDetail.getUsername());
                preparedStatement.setString(20, ordersDetail.getLoatdat());
                preparedStatement.setString(21, ordersDetail.getThoigian());
                preparedStatement.setString(22, ordersDetail.getTaichohaymangve());
                preparedStatement.setString(23, ordersDetail.getDanhdaubixoa());
                preparedStatement.setString(24, ordersDetail.getDathanhtoantienvoinhanvienchua());
                preparedStatement.setString(25, ordersDetail.getQuaythanhtoanso());

                preparedStatement.setString(26, ordersDetail.getgiavi_text());
                preparedStatement.setDouble(27, ordersDetail.getgiavi_price());

                preparedStatement.setString(28, ordersDetail.getgiavi_text_2());
                preparedStatement.setDouble(29, ordersDetail.getgiavi_price_2());

                preparedStatement.setString(30, ordersDetail.getgiavi_text_3());
                preparedStatement.setDouble(31, ordersDetail.getgiavi_price_3());

                preparedStatement.setString(32, ordersDetail.getgiavi_text_4());
                preparedStatement.setDouble(33, ordersDetail.getgiavi_price_4());

                preparedStatement.setString(34, ordersDetail.getgiavi_text_5());
                preparedStatement.setDouble(35, ordersDetail.getgiavi_price_5());

                preparedStatement.setString(36, ordersDetail.getgiavi_text_6());
                preparedStatement.setDouble(37, ordersDetail.getgiavi_price_6());

                preparedStatement.setString(38, ordersDetail.getgiavi_text_7());
                preparedStatement.setDouble(39, ordersDetail.getgiavi_price_7());

                preparedStatement.setString(40, ordersDetail.getgiavi_text_8());
                preparedStatement.setDouble(41, ordersDetail.getgiavi_price_8());

                preparedStatement.setString(42, ordersDetail.getgiavi_text_9());
                preparedStatement.setDouble(43, ordersDetail.getgiavi_price_9());

                preparedStatement.setString(44, ordersDetail.getgiavi_text_10());
                preparedStatement.setDouble(45, ordersDetail.getgiavi_price_10());

                preparedStatement.setString(46, ordersDetail.getBophanphucvu());
                preparedStatement.setString(47, ordersDetail.getDaprintchua());
                preparedStatement.setString(48, ordersDetail.getDatongketcuoingaychua());
                preparedStatement.setString(49, ordersDetail.getOrderfrom());
                preparedStatement.setString(50, ordersDetail.getis_tiengiamtructiep());
                preparedStatement.setString(51, ordersDetail.getLydohuyxoa());
                preparedStatement.setString(52, ordersDetail.getloai_mon_an());
                preparedStatement.setString(53, ordersDetail.getDaprintxoachua());
                preparedStatement.setString(54, ordersDetail.getKichcomonanngonngu());
                preparedStatement.setInt(55, ordersDetail.getPhantramgiamtrentoandonhang());

                preparedStatement.setString(56, ordersDetail.getPhanangomco());
                preparedStatement.setInt(57, ordersDetail.getphanangomco_soluong());
                preparedStatement.setDouble(58, ordersDetail.getphanangomco_value());

                preparedStatement.setString(59, ordersDetail.getPhanangomco2());
                preparedStatement.setInt(60, ordersDetail.getphanangomco2_soluong());
                preparedStatement.setDouble(61, ordersDetail.getPhanangomco2Value());

                preparedStatement.setString(62, ordersDetail.getPhanangomco3());
                preparedStatement.setInt(63, ordersDetail.getphanangomco3_soluong());
                preparedStatement.setDouble(64, ordersDetail.getPhanangomco3Value());

                preparedStatement.setString(65, ordersDetail.getPhanangomco4());
                preparedStatement.setInt(66, ordersDetail.getphanangomco4_soluong());
                preparedStatement.setDouble(67, ordersDetail.getPhanangomco4Value());

                preparedStatement.setString(68, ordersDetail.getPhanangomco5());
                preparedStatement.setInt(69, ordersDetail.getphanangomco5_soluong());
                preparedStatement.setDouble(70, ordersDetail.getPhanangomco5Value());

                preparedStatement.setString(71, ordersDetail.getPhanangomco6());
                preparedStatement.setInt(72, ordersDetail.getphanangomco6_soluong());
                preparedStatement.setDouble(73, ordersDetail.getPhanangomco6Value());

                preparedStatement.setString(74, ordersDetail.getPhanangomco7());
                preparedStatement.setInt(75, ordersDetail.getphanangomco7_soluong());
                preparedStatement.setDouble(76, ordersDetail.getPhanangomco7Value());

                preparedStatement.setString(77, ordersDetail.getPhanangomco8());
                preparedStatement.setInt(78, ordersDetail.getphanangomco8_soluong());
                preparedStatement.setDouble(79, ordersDetail.getPhanangomco8Value());

                preparedStatement.setString(80, ordersDetail.getPhanangomco9());
                preparedStatement.setInt(81, ordersDetail.getphanangomco9_soluong());
                preparedStatement.setDouble(82, ordersDetail.getPhanangomco9Value());
                preparedStatement.setString(83, ordersDetail.getCachtra().value);
                preparedStatement.setString(84, ordersDetail.getGhichu());
                preparedStatement.setString(85, ordersDetail.getGhichu2());
                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }


    public static int updateAllOrderTempByAmount(OrdersTemp ordersTemp) {
        PreparedStatement preparedStatement = null;

        String updateTableSQL = "UPDATE wapri.orders_temp SET soluong = ?,thanhtien = ?, tienthue= ?, giatien = ?, tiengiam = ? WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(updateTableSQL);
                preparedStatement.setInt(1, ordersTemp.getSoluong());
                preparedStatement.setDouble(2, ordersTemp.getThanhtien());
                preparedStatement.setDouble(3, ordersTemp.getTienthue());
                preparedStatement.setDouble(4, ordersTemp.getGiatien());
                preparedStatement.setDouble(5, ordersTemp.getTiengiam());
                preparedStatement.setString(6, ordersTemp.getId());
                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    public static int deleteOrderTempItem(OrdersTemp ordersTemp) {
        PreparedStatement preparedStatement = null;

        String deleteTableSQL = "DELETE from wapri.orders_temp where id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(deleteTableSQL);
                preparedStatement.setString(1, ordersTemp.getId());
                preparedStatement.executeUpdate();
                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    /* order _ 2018 */

    public static int updateOrderTask(Orders orders) {
        PreparedStatement preparedStatement = null;

        String updateTableSQL = "UPDATE wapri.orders_" + Util.currrentYear() + " SET loaiIn = ?, lichsudoicachthanhtoantien = ?, ladonhangcodoicachthanhtoantien = ?, inlaihoadonfromtablet = ?," +
                "daprintchua = ?, cachtra = ? WHERE id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(updateTableSQL);
                preparedStatement.setString(1, orders.getLoaiIn());
                preparedStatement.setString(2, orders.getLichsudoicachthanhtoantien());
                preparedStatement.setString(3, orders.getLadonhangcodoicachthanhtoantien());
                preparedStatement.setString(4, orders.getInlaihoadonfromtablet());
                preparedStatement.setString(5, orders.getDaprintchua());
                preparedStatement.setString(6, orders.getCachtra().value);
                preparedStatement.setString(7, orders.getId());

                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }


    public static int updateCachTraOrderDetailTask(String cachTra, String orders_id) {
        PreparedStatement preparedStatement = null;

        String updateTableSQL = "UPDATE wapri.orders_detail_" + Util.currrentYear() + " set cachtra = ? where orders_id = ?";
        if (connection != null) {
            try {
                preparedStatement = connection.prepareStatement(updateTableSQL);
                preparedStatement.setString(1, cachTra);
                preparedStatement.setString(2, orders_id);
                // execute insert SQL stetement
                preparedStatement.executeUpdate();

                return 1;

            } catch (SQLException e) {

                System.out.println(e.getMessage());
                return 0;

            } finally {

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }
}
