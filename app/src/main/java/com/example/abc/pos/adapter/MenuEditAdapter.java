package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.utils.Constants;
import com.example.abc.pos.utils.Util;

import java.util.List;

public class MenuEditAdapter extends RecyclerView.Adapter<MenuEditAdapter.ViewHolder> {
    private List<Menu> mMenus;
    private ItemClickListener mClickListener;
    private int selectedPosition = -1;
    public MenuEditAdapter(Context context, List<Menu> menus, ItemClickListener clickListener){
        mMenus = menus;
        mClickListener = clickListener;
    }

    public void setNewDatas( List<Menu> menus){
        mMenus = menus;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_edit_item,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_order.setText(mMenus.get(position).getThutusapxepkhihienthi()+"");
        holder.tv_name.setText(mMenus.get(position).getName());
        holder.tv_dis_type.setText(Util.getValueFromKey(Util.getEnumFoodType(),mMenus.get(position).getLoaiMonAn()));
        if(mMenus.get(position).getColor() != null){
            holder.tv_color.setBackgroundColor(Integer.parseInt(mMenus.get(position).getColor()));
        }else{
            holder.tv_color.setBackgroundColor(Color.parseColor("#204d7f"));
        }

        holder.ll_parent.setTag(position);

        if(selectedPosition != -1){
            if(position == selectedPosition){

                holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_CHOSE_GREEN));
            }else {
                if(position%2==0){
                    holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));
                }else{
                    holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_ODD));
                }
            }

        }else{
            if(position%2==0){
                holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));
            }else{
                holder.ll_parent.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_ODD));
            }
        }




//        holder.tv_color.setText(mMenus.get(position).getName());
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mMenus.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_order,tv_name,tv_color,tv_dis_type;
        LinearLayout ll_parent;
        ViewHolder(View itemView) {
            super(itemView);
            tv_order = itemView.findViewById(R.id.tv_order);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_color = itemView.findViewById(R.id.tv_color);
            tv_dis_type = itemView.findViewById(R.id.tv_dis_type);
            ll_parent = itemView.findViewById(R.id.ll_parent);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(getAdapterPosition(), mMenus.get(getAdapterPosition()));

            itemCheckChanged(ll_parent);
        }
    }

    public interface ItemClickListener {
        void onItemClick(int position, Menu menu);
    }
}
