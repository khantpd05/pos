package com.example.abc.pos.screen.table;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.HappyHour;
import com.example.abc.pos.model.HappyHour_Detail;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.model.MobileLogin;
import com.example.abc.pos.model.Orders;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.PendingPaymentSync;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.model.TableType;
import com.example.abc.pos.screen.keyboard.KeyboardFragment;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class TablePresenter implements TableConstract.Presenter {
    private TableConstract.View mView;
    private int mInterval = 3000; // 5 seconds by default, can be changed later
    private Handler mHandler;
    private ArrayList<Table> mTables;
    private List<String> arrTableName;


    public TablePresenter(TableConstract.View view) {
        mView = view;
    }

    @Override
    public void loadData() {
        mHandler = new Handler();
        startRepeatingTask();
        new GetHappyHourTask().execute();
        new SelectAllTableTask().execute();
    }




    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
//                new SelectTableHaveOrder().execute();
//                if(arrTableName!=null && !arrTableName.isEmpty()){
//                    for(String tableName: arrTableName ){
//                        new UpdateStateTable(tableName,"using").execute();
//                    }
//                }
                if (WapriManager.configuration().otpLogin.get()) {
                    new getLoginStateTask().execute();
                }

                if (SessionApp.tableType == SessionApp.TableShowType.TAT_CA)
                    new SelectAllTableTask().execute();
                else new SelectAllTableHaveCustomerTask().execute();
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    @Override
    public void startRepeatingTask() {
        if(mHandler!=null && mStatusChecker!=null)
        mStatusChecker.run();
    }

    @Override
    public void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }



    @Override
    public void loadAllTablesHaveCustomer() {
        new SelectAllTableHaveCustomerTask().execute();
    }

    @Override
    public void getAllTable() {
        new SelectAllTableTask().execute();
    }

    private void goToOrderAfterCheck(Table table){
        if (WapriManager.getUser().getUsername().equals(table.getUsername()) || table.getUsername() == null || table.getUsername().equals("")) {
            mView.goToOrder(table, "");
            SessionApp.global_table = table;
        } else {
            SessionApp.global_table = table;
            List<String> datas = new ArrayList<>();
            if (WapriManager.getUser().getDuocQuyenDatMonDum().equals("Y")) {
                datas.add(Util.getStringFromResouce(R.string.order_for_table_top));
            }
            if (WapriManager.getUser().getDuocQuyenLayBanVaThanhToan().equals("Y")) {
                datas.add(Util.getStringFromResouce(R.string.take_table_and_payment));
            }
            if (datas.size() > 0) {
                mView.showLayBan(datas);
            } else {
                mView.showCustomDialog(App.Companion.self().getString(R.string.do_not_have_permission));
            }

        }
    }


    @Override
    public void goToOrder(Table table) {

        if((!table.getis_selling_on_tablet().equals("Y") && !table.getIs_selling_on_POS().equals("Y"))
                || (WapriManager.getUser().getCoQuyenPhanQuyenSuDungHeThong().equals("Y")
                || WapriManager.getUser().getCoQuyenQuanLyToanHeThong().equals("Y"))){
            goToOrderAfterCheck(table);

        }else{
            new GetLastTimeManipulationWithTable(table).execute();
        }



    }

    @Override
    public void showPendingPayment() {
        PendingPaymentSync paymentSync = SharedPrefs.getInstance().get(PendingPaymentSync.KEY, PendingPaymentSync.class);
        if (paymentSync != null && paymentSync.getUserName().equals(WapriManager.getUser().getUsername()))
            mView.goToPayment();
    }

    private class SelectTableHaveOrder extends AsyncTask<Void, Void, ArrayList<OrdersTemp>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<OrdersTemp> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT banso FROM wapri.orders_temp group by banso");
            ArrayList<OrdersTemp> tables = new Gson().fromJson(js.toString(), new TypeToken<List<OrdersTemp>>() {
            }.getType());
            return tables;
        }

        @Override
        protected void onPostExecute(ArrayList<OrdersTemp> ordersTemps) {
            super.onPostExecute(ordersTemps);
            arrTableName = new ArrayList<>();
            for (OrdersTemp ordersTemp : ordersTemps) {
                arrTableName.add(ordersTemp.getBanso());
            }

        }
    }


    private class UpdateMobileLoginTask extends AsyncTask<Void, Void, Integer> {

        MobileLogin mobileLogin;

        public UpdateMobileLoginTask(MobileLogin mobileLogin) {
            this.mobileLogin = mobileLogin;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result = MySqlHandleUtils.update_moibleLogin(mobileLogin);

            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

        }
    }


    private class SelectAllTableHaveCustomerTask extends AsyncTask<Void, Void, ArrayList<Table>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Table> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT t.id, t.name, t.vitri, t.trangthai, t.soghengoi, t.username, t.tongtien, t.tabletype, t.status,giam," +
                    "t.tiengiam, t.tienthue19giam, t.tienthue7giam, t.taichohaymangve, t.thoigian, t.vitritronghayngoainha, t.orderfrom, t.from_tablet," +
                    "t.is_selling_on_tablet, t.is_selling_on_POS," +
                    "(select count(o.id) from wapri.orders_temp o where o.banso = t.name AND o.daprintchua = 'N' AND o.items_id NOT IN (1001,1005,10004) AND o.bophanphucvu <> 'Sonstiges0') as total_orders_not_printed, " +
                    "(select count(o.id) from wapri.orders_temp o where o.banso = t.name) as total_orders  FROM wapri.table t where (select count(o.id) from wapri.orders_temp o where o.banso = t.name) > 0");

            ArrayList<Table> tables = new Gson().fromJson(js.toString(), new TypeToken<List<Table>>() {
            }.getType());
            for(Table table : tables){
                table.setTaichoHayMangVe();
            }
            Collections.sort(tables, new Table());
            return tables;
        }

        @Override
        protected void onPostExecute(ArrayList<Table> tables) {
            super.onPostExecute(tables);

            mView.showListTable(tables);
            List<String> tables_type_names = new ArrayList<>();

            if (tables.size() > 0) {
                List<TableType> tables_type = new ArrayList<>();

                for (int i = 0; i < tables.size(); i++) {
                    if (!tables_type_names.contains(tables.get(i).getVitri())) {
                        tables_type_names.add(tables.get(i).getVitri());
                    }
                }

                for (int i = 0; i < tables_type_names.size(); i++) {
                    List<Table> newTable = new ArrayList<>();
                    for (int j = 0; j < tables.size(); j++) {
                        if (tables_type_names.get(i).equals(tables.get(j).getVitri())) {
                            newTable.add(tables.get(j));
                        }
                    }

                    TableType tableType = new TableType(tables_type_names.get(i), newTable);

                    tables_type.add(tableType);
                }
                mView.showListTableType(tables_type, SessionApp.isAll);
            }
        }
    }


    private class SelectAllTableTask extends AsyncTask<Void, Void, ArrayList<Table>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Table> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT t.id, t.name, t.vitri, t.trangthai, t.soghengoi, t.username, t.tongtien, t.tabletype, t.status,giam," +
                    "t.tiengiam, t.tienthue19giam, t.tienthue7giam, t.taichohaymangve, t.thoigian, t.vitritronghayngoainha, t.orderfrom, t.from_tablet," +
                    "t.is_selling_on_tablet, t.is_selling_on_POS," +
                    "(select count(o.id) from wapri.orders_temp o where o.banso = t.name AND o.daprintchua = 'N' AND o.items_id NOT IN (1001,1005,10004) AND o.bophanphucvu <> 'Sonstiges0') as total_orders_not_printed, " +
                    "(select count(o.id) from wapri.orders_temp o where o.banso = t.name) as total_orders  FROM wapri.table t");

            ArrayList<Table> tables = new Gson().fromJson(js.toString(), new TypeToken<List<Table>>() {
            }.getType());
            for(Table table : tables){
                table.setTaichoHayMangVe();
            }
            Collections.sort(tables, new Table());
            return tables;
        }

        @Override
        protected void onPostExecute(ArrayList<Table> tables) {
            super.onPostExecute(tables);
            mTables = tables;
            if (!SessionApp.isAll && SessionApp.current_table_type != null)
                mView.showListTable(SessionApp.current_table_type.getTables());
            else mView.showListTable(tables);
            List<String> tables_type_names = new ArrayList<>();

            if (tables.size() > 0) {
                List<TableType> tables_type = new ArrayList<>();

                for (int i = 0; i < tables.size(); i++) {
                    if (!tables_type_names.contains(tables.get(i).getVitri())) {
                        tables_type_names.add(tables.get(i).getVitri());
                    }
                }

                for (int i = 0; i < tables_type_names.size(); i++) {
                    List<Table> newTable = new ArrayList<>();
                    for (int j = 0; j < tables.size(); j++) {
                        if(tables_type_names.get(i)!=null){
                            if (tables_type_names.get(i).equals(tables.get(j).getVitri())) {
                                newTable.add(tables.get(j));
                            }
                        }

                    }

                    TableType tableType = new TableType(tables_type_names.get(i), newTable);

                    tables_type.add(tableType);
                }

                mView.showListTableType(tables_type, SessionApp.isAll);
            }
        }
    }



    private class getLoginStateTask extends AsyncTask<Void, Void, List<MobileLogin>> {

        @Override
        protected List<MobileLogin> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("select * from wapri.mobilelogin where mobile_name = " + "'" + SessionApp.IMEI + "'");
            ArrayList<MobileLogin> mobileLogin = new Gson().fromJson(js.toString(), new TypeToken<List<MobileLogin>>() {
            }.getType());
            return mobileLogin;
        }

        @Override
        protected void onPostExecute(List<MobileLogin> mobileLogins) {
            super.onPostExecute(mobileLogins);
            if (mobileLogins != null && !mobileLogins.isEmpty()) {
                MobileLogin mobileLogin = mobileLogins.get(0);
                if (mobileLogin.getToken() == null) {
                    mView.showToast(Util.getStringFromResouce(R.string.end_of_the_session));
                    stopRepeatingTask();
                    MySqlHandleUtils.getInstance().disconnectToMySql();
                    for (int i = 0; i < 7; i++) {
                        mView.exit();
                    }
                    mView.hideKeyBoard();
                }


                if (mobileLogin.getToken() != null && mobileLogin.getTime_timeout() != null && Util.StringToDate(mobileLogin.getTime_timeout()).before(Util.StringToDate(Util.getDate("dt")))) {
                    stopRepeatingTask();
                    MySqlHandleUtils.getInstance().disconnectToMySql();
                    for (int i = 0; i < 7; i++) {
                        mView.exit();
                    }
                    mView.hideKeyBoard();
                }

            }
        }
    }


    private class SelectRestaurantName extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {
            WapriManager.STORE_NAME = MySqlHandleUtils.selectThongtincuahang();
            return null;
        }
    }

    private class GetHappyHourTask extends AsyncTask<Void, Void, List<HappyHour>> {
        @Override
        protected List<HappyHour> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("select * from wapri.happyhour order by id desc limit 1");
            ArrayList<HappyHour> happyHours = new Gson().fromJson(js.toString(), new TypeToken<List<HappyHour>>() {
            }.getType());
            return happyHours;
        }

        @Override
        protected void onPostExecute(List<HappyHour> happyHours) {
            super.onPostExecute(happyHours);
            if (happyHours.size() > 0) {
                try {
                    Date currentTime = Calendar.getInstance().getTime();

                    int thu = Util.getDayOfWeek(currentTime);

                    HappyHour happyHour = happyHours.get(0);
                    SessionApp.happyHour = happyHour;
                    boolean isMatchDayOfWeek = false;
                    String daysArr[] = {happyHour.getChunhat(), happyHour.getThu2(), happyHour.getThu3(), happyHour.getThu4(), happyHour.getThu5(), happyHour.getThu6(),
                            happyHour.getThu7()};
                    for (int i = 1; i <= daysArr.length; i++) {
                        if (thu == i && daysArr[i - 1].equals("Y")) {
                            isMatchDayOfWeek = true;
                            break;
                        }
                    }


                    Date startTime = Util.StringToDate(happyHour.getNgaybatdau() + " " + happyHour.getGiobatdau() + ":" + happyHour.getPhutbatdau() + ":00");
                    Date endTime = Util.StringToDate(happyHour.getNgayketthuc() + " " + happyHour.getGioketthuc() + ":" + happyHour.getPhutketthuc() + ":00");
                    Date endTimeHH = Util.StringToDate(Util.dateToString(currentTime) + " " + happyHour.getGioketthuc() + ":" + happyHour.getPhutketthuc() + ":00");

                    SessionApp.endDate = endTimeHH;
                    if ((currentTime.after(startTime) && currentTime.before(endTime)) && isMatchDayOfWeek) {
                        SessionApp.isHappyHour = true;
                        if (happyHour.getApdungtatcamathang().equals("Y")) {
                            SessionApp.isAplyAllHappyHour = true;
                        } else {
                            SessionApp.isAplyAllHappyHour = false;
                            new GetItemsHappyHourTask(happyHour.getId()).execute();
                        }
                    } else {
                        SessionApp.isHappyHour = false;
                        SessionApp.isAplyAllHappyHour = false;
                    }
                } catch (Exception ex) {

                }
            }
        }
    }


    private class GetItemsHappyHourTask extends AsyncTask<Void, Void, List<HappyHour_Detail>> {
        int happyhour_id;

        public GetItemsHappyHourTask(int happyhour_id) {
            this.happyhour_id = happyhour_id;
        }

        @Override
        protected List<HappyHour_Detail> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("select * from wapri.happyhour_detail where happyhour_id = " + happyhour_id);
            ArrayList<HappyHour_Detail> happyHours = new Gson().fromJson(js.toString(), new TypeToken<List<HappyHour_Detail>>() {
            }.getType());
            return happyHours;
        }

        @Override
        protected void onPostExecute(List<HappyHour_Detail> happyHoursItems) {
            super.onPostExecute(happyHoursItems);
            if (happyHoursItems.size() > 0) {
                SessionApp.happyHour_detail = happyHoursItems;
            }
        }
    }

    class LastMinute{
       private Long orders_thoigian;
       private Long table_thoigian;
    }

    private class GetLastTimeManipulationWithTable extends AsyncTask<Void, Void, List<LastMinute>> {
        Table table;

        public GetLastTimeManipulationWithTable(Table table) {
            this.table = table;
        }

        @Override
        protected List<LastMinute> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament(
                    "select \n" +
                            "timestampdiff(MINUTE,max(o.thoigian),now()) as orders_thoigian, \n" +
                            "(select timestampdiff(MINUTE,max(t.thoigian),now()) \n" +
                            "from `table` t \n" +
                            "where t.name = '"+ table.getName() + "') as table_thoigian\n" +
                            "from orders_temp o where o.banso = '"+table.getName()+"'"
            );
            ArrayList<LastMinute> lastMinutes = new Gson().fromJson(js.toString(), new TypeToken<List<LastMinute>>() {
            }.getType());
            return lastMinutes;
        }

        @Override
        protected void onPostExecute(List<LastMinute> lastMinutes) {
            super.onPostExecute(lastMinutes);
            if(lastMinutes!=null && lastMinutes.size()>0){
                 Long orders_thoigian;
                 Long table_thoigian;
                orders_thoigian = lastMinutes.get(0).orders_thoigian;
                table_thoigian = lastMinutes.get(0).table_thoigian;

                if(orders_thoigian != null && orders_thoigian > 5)
                    goToOrderAfterCheck(table);
                else if(orders_thoigian == null && (table_thoigian !=null && table_thoigian > 5))
                    goToOrderAfterCheck(table);
                else
                    mView.showToast(Util.getStringFromResouce(R.string.you_can_not_control_the_table_is_booking));

            }else{
                goToOrderAfterCheck(table);
            }
        }
    }

}
