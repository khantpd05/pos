package com.example.abc.pos.screen.cancel_reason_dialog;

import com.example.abc.pos.model.Guschein;
import com.example.abc.pos.model.LyDoHuyXoa;

import java.util.List;

public interface CancelReasonConstract {
    interface View{
        void showCancelReasonList(List<LyDoHuyXoa> cancelReasonList);
        void getDataInput(String data);

    }

    interface Presenter{
        void getDataInput();

    }
}
