package com.example.abc.pos.screen.divide_bill;

import android.util.Log;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.ExtraItem;
import com.example.abc.pos.model.OrdersTemp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DivideBillPresenter implements DivideBillContract.Presenter {

    private DivideBillContract.View mView;
    private List<OrdersTemp> origOrderList;
    private List<OrdersTemp> divideOrderList;
    private double tienGiamBan;
    private int positionSelected;

    public DivideBillPresenter(DivideBillContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void loadData(List<OrdersTemp> orderList, double tienGiamBan) {
        origOrderList = orderList;
        divideOrderList = new ArrayList<>();


        this.tienGiamBan = tienGiamBan;
        mView.loadOrigOrderList(orderList);
    }

    @Override
    public void addItemToDivideOrderList(OrdersTemp ordersTemp, int position) {

        int soLuongKhop = -1;
        int iMatch = -1;
        try {
            if (ordersTemp.getSoluong() > 1) {
                positionSelected = position;
                mView.showKeyboard(App.Companion.self().getString(R.string.amount) + ":", ordersTemp.getSoluong() + "", "00", SessionApp.KeyboardType.PHAN_TRAM);
            } else {
                if (ordersTemp.getSoluong() > 0) {
                    for (int i = 0; i < divideOrderList.size(); i++) {
                        if (ordersTemp.getId().equals(divideOrderList.get(i).getId())) {
                            soLuongKhop = divideOrderList.get(i).getSoluong();
                            iMatch = i;
                            break;
                        }
                    }
                    // neu tim thay
                    if (soLuongKhop != -1) {
                        OrdersTemp ordersTempNew = (OrdersTemp) origOrderList.get(position).clone();
                        divideOrderList.get(iMatch).setSoluong(ordersTempNew.getSoluong() + soLuongKhop);
                        divideOrderList.get(iMatch).setThanhtien(tinhThanhTien(divideOrderList.get(iMatch)));
                        divideOrderList.get(iMatch).setGiatien(tinhGiaTien(divideOrderList.get(iMatch)));
                        divideOrderList.get(iMatch).setTienthue(tinhTienThue(divideOrderList.get(iMatch)));
                        divideOrderList.get(iMatch).setTiengiam((tinhTienChuaGiam( divideOrderList.get(iMatch)) *  divideOrderList.get(iMatch).getPhantramgiam()) / 100);

//                        origOrderList.remove(position);
                        origOrderList.get(position).setSoluong(0);
                        origOrderList.get(position).setThanhtien(0);
                        origOrderList.get(position).setGiatien(0);
                        origOrderList.get(position).setTienthue(0);
                        origOrderList.get(position).setTiengiam(0);

                    } else {
                        OrdersTemp ordersTempNew = (OrdersTemp) origOrderList.get(position).clone();
                        divideOrderList.add(ordersTempNew);

//                        origOrderList.remove(position);
                        origOrderList.get(position).setSoluong(0);
                        origOrderList.get(position).setThanhtien(0);
                        origOrderList.get(position).setGiatien(0);
                        origOrderList.get(position).setTienthue(0);
                        origOrderList.get(position).setTiengiam(0);
                    }
                    mView.loadOrigOrderList(origOrderList);
                    mView.loadDivideOrderList(divideOrderList);
                }
            }
        } catch (CloneNotSupportedException ex) {

        }
    }

    @Override
    public void addItemToOrigOrderList(OrdersTemp ordersTemp, int position) {

        int soLuongKhop = -1;
        int iMatch = -1;
        for (int i = 0; i < origOrderList.size(); i++) {
            if (ordersTemp.getId().equals(origOrderList.get(i).getId())) {
                soLuongKhop = origOrderList.get(i).getSoluong();
                iMatch = i;
                break;
            }
        }
        if (soLuongKhop != -1) {
            origOrderList.get(iMatch).setSoluong(ordersTemp.getSoluong() + soLuongKhop);
            origOrderList.get(iMatch).setThanhtien(tinhThanhTien(origOrderList.get(iMatch)));
            origOrderList.get(iMatch).setGiatien(tinhGiaTien(origOrderList.get(iMatch)));
            origOrderList.get(iMatch).setTienthue(tinhTienThue(origOrderList.get(iMatch)));
            origOrderList.get(iMatch).setTiengiam((tinhTienChuaGiam( origOrderList.get(iMatch)) *  origOrderList.get(iMatch).getPhantramgiam()) / 100);
            divideOrderList.remove(ordersTemp);
        } else {
            origOrderList.add(ordersTemp);
            divideOrderList.remove(ordersTemp);
        }

        mView.loadOrigOrderList(origOrderList);
        mView.loadDivideOrderList(divideOrderList);
    }

    @Override
    public double getSummerPrice() {
        double summer = 0;
        double summerOf1005 = 0;
        for (OrdersTemp order : divideOrderList) {
            if (order.getDanhdaubixoa() != null && order.getDanhdaubixoa().equals("Y")) {
            } else if (order.getitems_id().equals("1005")) {
                summerOf1005 += order.getThanhtien();
            } else {
                summer += order.getThanhtien();
            }
//            summer += order.getThanhtien();
        }


        double resultWithout1005 = summer - ((summer * tienGiamBan) / 100);
        double result = resultWithout1005 + summerOf1005;

        return result;
    }

    @Override
    public void goToPayment() {
        mView.goToPayment(origOrderList, divideOrderList);
    }

    @Override
    public void showKeyboard(int soluong) {

    }

    @Override
    public void addItemToDivideOrderListByInput(String data) {
        try {
            OrdersTemp ordersTemp = origOrderList.get(positionSelected);
            int soLuongGoc = origOrderList.get(positionSelected).getSoluong();

            if (data != null && !data.isEmpty()) {
                int soLuongNhap = Integer.parseInt(data);
                if(soLuongNhap>0){
                    if (soLuongNhap >= soLuongGoc) {
                        if (isExistInDivideList(ordersTemp, soLuongGoc)) ;
                        else {
                            OrdersTemp ordersTempNew = (OrdersTemp) ordersTemp.clone();
                            divideOrderList.add(ordersTempNew);

//                            origOrderList.remove(positionSelected);
                        origOrderList.get(positionSelected).setSoluong(0);
                        origOrderList.get(positionSelected).setThanhtien(0);
                        origOrderList.get(positionSelected).setGiatien(0);
                        origOrderList.get(positionSelected).setTienthue(0);
                        origOrderList.get(positionSelected).setTiengiam(0);

                            mView.loadOrigOrderList(origOrderList);
                            mView.loadDivideOrderList(divideOrderList);
                        }

                    } else {
                        if (isExistInDivideList(ordersTemp, soLuongNhap)) ;
                        else {
                            // tao order moi copy tu order cu
                            OrdersTemp ordersTempNew = (OrdersTemp) ordersTemp.clone();
                            ordersTempNew.setSoluong(soLuongNhap);
                            ordersTempNew.setThanhtien(tinhThanhTien(ordersTempNew));
                            ordersTempNew.setGiatien(tinhGiaTien(ordersTempNew));
                            ordersTempNew.setTienthue(tinhTienThue(ordersTempNew));
                            ordersTempNew.setTiengiam((tinhTienChuaGiam( ordersTempNew) *  ordersTempNew.getPhantramgiam()) / 100);
                            divideOrderList.add(ordersTempNew);
                            mView.loadDivideOrderList(divideOrderList);
                            //set lai so luong cho order cu
                            ordersTemp.setSoluong(ordersTemp.getSoluong() - soLuongNhap);
                            ordersTemp.setThanhtien(tinhThanhTien(ordersTemp));
                            ordersTemp.setGiatien(tinhGiaTien(ordersTemp));
                            ordersTemp.setTienthue(tinhTienThue(ordersTemp));
                            ordersTemp.setTiengiam((tinhTienChuaGiam(ordersTemp) *  ordersTemp.getPhantramgiam()) / 100);

                            mView.loadOrigOrderList(origOrderList);
                        }
                    }
                }

            } else {
                OrdersTemp ordersTempNew = (OrdersTemp) origOrderList.get(positionSelected).clone();
                if (isExistInDivideList(ordersTemp, ordersTempNew.getSoluong())) {

                } else {
                    divideOrderList.add(ordersTempNew);

//                    origOrderList.remove(positionSelected);
                    origOrderList.get(positionSelected).setSoluong(0);
                    origOrderList.get(positionSelected).setThanhtien(0);
                    origOrderList.get(positionSelected).setGiatien(0);
                    origOrderList.get(positionSelected).setTienthue(0);
                    origOrderList.get(positionSelected).setTiengiam(0);
                    mView.loadOrigOrderList(origOrderList);
                    mView.loadDivideOrderList(divideOrderList);
                }
            }
        } catch (CloneNotSupportedException ex) {

        }


    }

    private double tinhTienChuaGiam(OrdersTemp orderTemp) {
        // count all include spice price
        double totalSpicePrice = 0;
        for (ExtraItem extraItem : orderTemp.getExtraItemList()) {
            if (extraItem.getFlag() == WapriManager.FeatureOrder.GIA_VI || extraItem.getFlag() == WapriManager.FeatureOrder.PHAN_AN) {
                totalSpicePrice += extraItem.getValue();
            }
        }
        double tongtien = (orderTemp.getSoluong() * orderTemp.getorig_price_no_extra()) + (totalSpicePrice * orderTemp.getSoluong());
        return tongtien;
    }

    private boolean isExistInDivideList(OrdersTemp ordersTemp, int soLuongNhap) {
        int iMatch = -1;
        int soLuongKhop = -1;
        for (int i = 0; i < divideOrderList.size(); i++) {
            if (ordersTemp.getId().equals(divideOrderList.get(i).getId())) {
                soLuongKhop = divideOrderList.get(i).getSoluong();
                iMatch = i;
                break;
            }
        }
        if (soLuongKhop != -1) {
            divideOrderList.get(iMatch).setSoluong(soLuongKhop + soLuongNhap);
            divideOrderList.get(iMatch).setThanhtien(tinhThanhTien(divideOrderList.get(iMatch)));
            divideOrderList.get(iMatch).setGiatien(tinhGiaTien(divideOrderList.get(iMatch)));
            divideOrderList.get(iMatch).setTienthue(tinhTienThue(divideOrderList.get(iMatch)));
            divideOrderList.get(iMatch).setTiengiam((tinhTienChuaGiam(divideOrderList.get(iMatch)) *  divideOrderList.get(iMatch).getPhantramgiam()) / 100);
            mView.loadDivideOrderList(divideOrderList);

            ordersTemp.setSoluong(ordersTemp.getSoluong() - soLuongNhap);
            ordersTemp.setThanhtien(tinhThanhTien(ordersTemp));
            ordersTemp.setGiatien(tinhGiaTien(ordersTemp));
            ordersTemp.setTienthue(tinhTienThue(ordersTemp));
            ordersTemp.setTiengiam((tinhTienChuaGiam( ordersTemp) *  ordersTemp.getPhantramgiam()) / 100);
//            origOrderList.remove(ordersTemp);
            mView.loadOrigOrderList(origOrderList);
            return true;
        } else {
            return false;
        }
    }

    private double tinhThanhTien(OrdersTemp orderTemp) {
        // count all include spice price
        double totalSpicePrice = 0;
        for (ExtraItem extraItem : orderTemp.getExtraItemList()) {
            if (extraItem.getFlag() == WapriManager.FeatureOrder.GIA_VI || extraItem.getFlag() == WapriManager.FeatureOrder.PHAN_AN) {
                totalSpicePrice += extraItem.getValue();
            }
        }
        double tongtien = (orderTemp.getSoluong() * orderTemp.getorig_price_no_extra()) + (totalSpicePrice * orderTemp.getSoluong());
        double thanhtien = tongtien - ((tongtien * orderTemp.getPhantramgiam()) / 100);
        return thanhtien;
    }

    private double tinhGiaTien(OrdersTemp orderTemp) {
        // count all include spice price
        double totalSpicePrice = 0;
        for (ExtraItem extraItem : orderTemp.getExtraItemList()) {
            if (extraItem.getFlag() == WapriManager.FeatureOrder.GIA_VI || extraItem.getFlag() == WapriManager.FeatureOrder.PHAN_AN) {
                totalSpicePrice += extraItem.getValue();
            }
        }
        double tongtien = (orderTemp.getSoluong() * orderTemp.getorig_price_no_extra()) + (totalSpicePrice * orderTemp.getSoluong());
        return tongtien;
    }

    @Override
    public List<OrdersTemp> getDivideOrdertempList() {
        return divideOrderList;
    }

    @Override
    public void setDivideOrdertempList(List<OrdersTemp> ordersTempDivideList) {
        divideOrderList = ordersTempDivideList;
    }


    private double tinhTienThue(OrdersTemp ordersTemp) {
        if(ordersTemp.getBophanphucvu().equals("Sonstiges0"))
            return 0.0;
        else if (ordersTemp.getTaichohaymangve() == WapriManager.Taichohaymangve_.TaiCho || ordersTemp.getBophanphucvu().equals("PhaChe19")) {
            return WapriManager.tinhTienThue19(ordersTemp.getGiatien(), ordersTemp.getPhantramgiam());
        } else
            return WapriManager.tinhTienThue7(ordersTemp.getGiatien(), ordersTemp.getPhantramgiam());
    }
}
