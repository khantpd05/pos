package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.model.Table;

import java.util.List;

public class MenusAdapter extends RecyclerView.Adapter<MenusAdapter.ViewHolder> {
    private List<Menu> mMenus;
    private ItemClickListener mClickListener;

    public MenusAdapter(Context context, List<Menu> menus, ItemClickListener clickListener){
        mMenus = menus;
        mClickListener = clickListener;
    }

    public void setNewDatas(List<Menu> menus){
        mMenus = menus;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_item,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.myTextView.setText(mMenus.get(position).getName());
        try{
            if(mMenus.get(position).getColor()!=null)
                holder.myTextView.setBackgroundColor(Integer.valueOf(mMenus.get(position).getColor()));
            else
                holder.myTextView.setBackgroundColor(Color.parseColor("#45cc55"));
        }catch (Exception ex){

        }
    }

    @Override
    public int getItemCount() {
        return mMenus.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.info_text);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, mMenus.get(getAdapterPosition()));
        }
    }

    public interface ItemClickListener {
        void onItemClick(View view, Menu menu);
    }
}
