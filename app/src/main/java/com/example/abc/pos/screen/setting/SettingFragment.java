package com.example.abc.pos.screen.setting;


import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.databinding.FragmentSettingBinding;
import com.example.abc.pos.screen.keyboard.KeyboardFragment;
import com.example.abc.pos.screen.kich_co_mon_dialog.KichCoMonDialogFragment;
import com.example.abc.pos.screen.language.LanguageDialogFragment;
import com.example.abc.pos.screen.setting_connect.SettingConnectionFragment;
import com.example.abc.pos.screen.setting_general.SettingGeneralFragment;
import com.example.abc.pos.screen.setting_menu.SettingMenuFragment;
import com.example.abc.pos.screen.setting_next_order.SettingNextOrderFragment;
import com.example.abc.pos.screen.setting_payment.SettingPaymentFragment;
import com.example.abc.pos.screen.setting_printer.SettingPrinterFragment;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.utils.Util;
import com.example.abc.pos.viewmodel.GeneralConfiguration;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment implements SettingConstract.View, KeyboardFragment.OnKeyboardPress, LanguageDialogFragment.OnLanguageSelected {

    private ObservableField<Boolean> isHavePermission = new ObservableField<>(false);
    private SettingPresenter presenter;
    private final String KEY_DEVICE_NAME = "device_named";
    FragmentSettingBinding binding;
    public SettingFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public SettingFragment(boolean isHavePermiss) {
        // Required empty public constructor
        isHavePermission.set(isHavePermiss);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_setting, container, false);

        presenter = new SettingPresenter(this);
        presenter.isHavePermission.set(isHavePermission.get());
        binding.setPresenter(presenter);


        return binding.getRoot();
    }

    @Override
    public void goSettingConnection() {
        ((BaseActivity)getActivity()).pushFragmentNotReplace(new SettingConnectionFragment(),true);
    }

    @Override
    public void goSettingMenu() {
        ((BaseActivity)getActivity()).pushFragmentNotReplace(new SettingMenuFragment(),true);
    }

    @Override
    public void goSettingNextOrder() {
        ((BaseActivity)getActivity()).pushFragmentNotReplace(new SettingNextOrderFragment(),true);
    }

    @Override
    public void goSettingNumberDevice() {
        String data = SharedPrefs.getInstance().get(KEY_DEVICE_NAME,String.class);
        String name;
        if(data!=null && !data.isEmpty()){
            name = data;
        }else{
            name = "";
        }
        Fragment fragment = new KeyboardFragment(this,getContext().getString(R.string.device_name), name+"","00",SessionApp.KeyboardType.PHAN_TRAM);
        ((KeyboardFragment) fragment).show(getFragmentManager(),"number_device");
    }

    @Override
    public void goSettingGeneral() {
        ((BaseActivity)getActivity()).pushFragmentNotReplace(new SettingGeneralFragment(),true);
    }

    @Override
    public void onKeyPressListener(String data) {
        SharedPrefs.getInstance().put(KEY_DEVICE_NAME,"M"+data);
    }

    @Override
    public void goSettingLanguage() {
        Util.showCustomDialog(getContext(),App.Companion.self().getString(R.string.The_function_is_developing));
    }

    @Override
    public void goSettingPayment() {
        ((BaseActivity)getActivity()).pushFragmentNotReplace(new SettingPaymentFragment(),true);
    }

    @Override
    public void goSettingPrinter() {
        ((BaseActivity)getActivity()).pushFragmentNotReplace(new SettingPrinterFragment(),true);
    }

    @Override
    public void goBack() {
        Util.popFragment(getActivity());
    }

    @Override
    public void showSettingLanguage() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        LanguageDialogFragment languageDialogFragment = new LanguageDialogFragment();
        languageDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        languageDialogFragment.setListener(this);
        languageDialogFragment.show(fm, KichCoMonDialogFragment.class.getSimpleName());
    }

    @Override
    public void onLanguageSelectedListener() {
        presenter.settingLanguage();
    }



}
