package com.example.abc.pos.screen.change_order_to_table;

import com.example.abc.pos.model.Table;

import java.util.List;

public interface ChangeOrderToTableContract {
    interface View{
         void showListTable(List<Table> tables);
        void showLoading();
        void dismissLoading();
        void showAlert(String data);
        void goToCustomerPosition(List<Integer> positionsHaveOrder);

    }

    interface Presenter{
        void getEmptyTable(String username, String excludeTableName);
        void goToCustomerPosition(String tableName);
    }
}
