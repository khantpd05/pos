package com.example.abc.pos.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.example.abc.pos.App;

public class WifiStateWatcher extends BroadcastReceiver {
    public static final String NETWORK_AVAILABLE_ACTION = "com.example.abc.pos.NetworkAvailable";
    public static final String IS_NETWORK_AVAILABLE = "isNetworkAvailable";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent networkStateIntent = new Intent(NETWORK_AVAILABLE_ACTION);
        networkStateIntent.putExtra(IS_NETWORK_AVAILABLE,isConnectedToIntent(context));

    }

    private boolean isConnectedToIntent(Context context){
        try{
            if(context!=null){
                ConnectivityManager connectivityManager = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager
                        .getActiveNetworkInfo();

                // Check internet connection and accrding to state change the
                // text of activity by calling method
                return networkInfo != null && networkInfo.isConnected();
            }
            return false;
        }catch (Exception ellx){
            return  false;
        }
    }
}
