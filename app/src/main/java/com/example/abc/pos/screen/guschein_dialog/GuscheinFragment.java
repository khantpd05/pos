package com.example.abc.pos.screen.guschein_dialog;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.GuscheinAdapter;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.model.Guschein;
import com.example.abc.pos.screen.keyboard.KeyboardFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class GuscheinFragment extends BottomSheetDialogFragment implements GuscheinConstract.View, GuscheinAdapter.ItemClickListener, KeyboardFragment.OnKeyboardPress {


    @Override
    public void onKeyPressListener(String data) {
        mListener.onGuscheinSelectedListener(data);
        dismiss();
}

    public interface OnGuscheinSelected{
        void onGuscheinSelectedListener(String data);
    }
    Dialog dialog;
    OnGuscheinSelected mListener;
    GuscheinPresenter presenter;
    GuscheinAdapter adapter;
    RecyclerView rc_guschein;
    KeyboardFragment keyboardFragment;

    public GuscheinFragment(OnGuscheinSelected mListener) {
        // Required empty public constructor
        this.mListener = mListener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_guschein,container,false);
        rc_guschein = view.findViewById(R.id.rc_guschein);
        adapter = new GuscheinAdapter(getContext(),new ArrayList<Guschein>(),this);
        rc_guschein.setAdapter(adapter);
        rc_guschein.setLayoutManager(new LinearLayoutManager(getContext()));
        presenter = new GuscheinPresenter(this);
        return view;
    }

    @Override
    public void showKeyboard(String title, String hint, String key11, SessionApp.KeyboardType keyboardType) {
        keyboardFragment = new KeyboardFragment(this, title, hint, key11, keyboardType);
        keyboardFragment.show(getFragmentManager(), null);
    }

    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();

        if (dialog != null) {
            DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
            int height = displayMetrics.heightPixels;
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = (int) (height*0.60);
        }
        final View view = getView();
        view.post(new Runnable() {
            @Override
            public void run() {
                View parent = (View) view.getParent();
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
                CoordinatorLayout.Behavior behavior = params.getBehavior();
                BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) behavior;
                bottomSheetBehavior.setPeekHeight(view.getMeasuredHeight());
            }
        });
    }

    @Override
    public void showGuscheinList(List<Guschein> guscheinList) {
        adapter.setNewDatas(guscheinList);
    }

    @Override
    public void getDataInput(String data) {
        mListener.onGuscheinSelectedListener(data);
        dialog.dismiss();
    }



    @Override
    public void onItemClick(int position, Guschein item) {
        if(position == 0){
            presenter.showKeyboard();
        }else{
            mListener.onGuscheinSelectedListener(item.getValue());
            dialog.dismiss();
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        if ((keyboardFragment) != null)
            keyboardFragment.dismiss();
    }
}
