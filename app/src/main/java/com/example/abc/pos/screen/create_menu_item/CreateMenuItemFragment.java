package com.example.abc.pos.screen.create_menu_item;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.R;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.screen.setting_item.SettingItemFragment;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import java.util.List;

import top.defaults.colorpicker.ColorPickerPopup;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class CreateMenuItemFragment extends BaseFragment implements CreateMenuItemConstract.View, View.OnClickListener {

    private Spinner spn_food_type;
    private TextView btn_colorPicker, tv_title;
    private EditText edt_name, edt_thu_tu, edt_items_id;
    private LinearLayout ll_wrap_itemsId, ll_exit;
    private Button btn_add;
    private String colorCode = "";
    private CreateMenuItemPresenter presenter;
    private static final String KEY_MENU_ITEM = "menu_item";
    private Menu menu;

    public CreateMenuItemFragment() {
        // Required empty public constructor
    }


    public static CreateMenuItemFragment newInstance(Menu param1) {
        CreateMenuItemFragment fragment = new CreateMenuItemFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_MENU_ITEM, param1);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_create_menu_item;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            menu = (Menu) getArguments().getSerializable(KEY_MENU_ITEM);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_menu_item, container, false);

        spn_food_type = view.findViewById(R.id.spn_food_type);

        tv_title = view.findViewById(R.id.tv_title);


        btn_colorPicker = view.findViewById(R.id.btn_colorPicker);
        btn_colorPicker.setOnClickListener(this);
        btn_colorPicker.setBackgroundColor(2136303871);

        btn_add = view.findViewById(R.id.btn_add);
        btn_add.setOnClickListener(this);

        edt_name = view.findViewById(R.id.edt_name);

        edt_thu_tu = view.findViewById(R.id.edt_thu_tu);

        edt_items_id = view.findViewById(R.id.edt_items_id);

        ll_wrap_itemsId = view.findViewById(R.id.ll_wrap_itemsId);

        ll_exit = view.findViewById(R.id.ll_exit);
        ll_exit.setOnClickListener(this);

        presenter = new CreateMenuItemPresenter(this);
        presenter.loadData();
        initUpdateField();

        return view;
    }


    private AdapterView.OnItemSelectedListener OnCatSpinnerCL = new AdapterView.OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (((TextView) parent.getChildAt(0)) != null)
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);

        }

        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public void showSpinnerData(List<String> datas) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_food_type_item, datas);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_food_type.setAdapter(adapter);
        spn_food_type.setOnItemSelectedListener(OnCatSpinnerCL);
    }


    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }

    @Override
    public void initUpdateField() {
        if (menu != null) {
            btn_add.setText(Util.getStringFromResouce(R.string.update));
            btn_add.setBackgroundResource(0);
            tv_title.setText(Util.getStringFromResouce(R.string.Edit));
            edt_name.setText(menu.getName());
            edt_thu_tu.setText(menu.getThutusapxepkhihienthi() + "");
            if (menu.getColor() != null && !menu.getColor().isEmpty()) {
                btn_colorPicker.setBackgroundColor(Integer.valueOf(menu.getColor()));
            }
            Util.selectSpinnerItemByValue(spn_food_type, Util.getValueFromKey(Util.getEnumFoodType(), menu.getLoaiMonAn()));
        }
    }

    @Override
    public void goBack() {
        Util.popFragment(getActivity());
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_colorPicker: {
                new ColorPickerPopup.Builder(getContext())
                        .initialColor(2136303871) // Set initial color
                        .showIndicator(true)
                        .showValue(false)
                        .build()
                        .show(btn_colorPicker, new ColorPickerPopup.ColorPickerObserver() {
                            @Override
                            public void onColorPicked(int color) {
                                btn_colorPicker.setBackgroundColor(color);
                                colorCode = String.valueOf(color);

                            }

                            @Override
                            public void onColor(int color, boolean fromUser) {

                            }
                        });
                break;
            }

            case R.id.btn_add: {
                String thuTuSapXep = edt_thu_tu.getText().toString();
                if (thuTuSapXep.isEmpty()) thuTuSapXep = "99";
                if (menu != null) {
                    if (colorCode != null && !colorCode.isEmpty()) {
                        menu.setColor(colorCode);
                    } else {
                        menu.setColor(menu.getColor());
                    }
                    if (edt_name.getText().toString().trim().isEmpty()) {
                        showAlert(Util.getStringFromResouce(R.string.please_enter_full_information));
                    } else {
                        menu.setName(edt_name.getText().toString());
                        String food_type = (String) Util.getKeyFromValue(Util.getEnumFoodType(), spn_food_type.getSelectedItem().toString());
                        menu.setLoaiMonAn(food_type);
                        menu.setThutusapxepkhihienthi(Integer.parseInt(thuTuSapXep));
                        presenter.updateMenu(menu);
                    }

                } else {
                    if (colorCode == null || colorCode.equals(""))
                        colorCode = "2136303871";
                    String food_type = (String) Util.getKeyFromValue(Util.getEnumFoodType(), spn_food_type.getSelectedItem().toString());
                    presenter.addNewMenu(edt_name.getText().toString(), Integer.parseInt(thuTuSapXep), colorCode, food_type);
                }
                break;
            }
            case R.id.ll_exit:
                Util.popFragment(getActivity());
                break;

        }
    }
}
