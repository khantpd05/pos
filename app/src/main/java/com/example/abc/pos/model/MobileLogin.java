package com.example.abc.pos.model;

import com.example.abc.pos.utils.Util;

public class MobileLogin {
    private Integer id;
    private String mobile_name;
    private String token;
    private String is_accepted;
    private String user_accepted;
    private String time_accepted;
    private String time_timeout;
    private String date_created;


    public MobileLogin() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMobile_name() {
        return mobile_name;
    }

    public void setMobile_name(String mobile_name) {
        this.mobile_name = mobile_name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        if(token!=null)
        this.token = Util.md5(token);
        else this.token = "";
    }



    public String getIs_accepted() {
        return is_accepted;
    }

    public void setIs_accepted(String is_accepted) {
        this.is_accepted = is_accepted;
    }

    public String getUser_accepted() {
        return user_accepted;
    }

    public void setUser_accepted(String user_accepted) {
        this.user_accepted = user_accepted;
    }

    public String getTime_accepted() {
        return time_accepted;
    }

    public void setTime_accepted(String time_accepted) {
        this.time_accepted = time_accepted;
    }

    public String getTime_timeout() {
        return time_timeout;
    }

    public void setTime_timeout(String time_timeout) {
        this.time_timeout = time_timeout;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }
}
