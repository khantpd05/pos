package com.example.abc.pos.screen.cancel_reason_dialog;

import android.os.AsyncTask;

import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Configuration;
import com.example.abc.pos.model.Guschein;
import com.example.abc.pos.model.LyDoHuyXoa;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class CancelReasonPresenter implements CancelReasonConstract.Presenter {

    private CancelReasonConstract.View mView;

    public CancelReasonPresenter(CancelReasonConstract.View mView) {
        this.mView = mView;
        new SelectTask().execute();
    }

    @Override
    public void getDataInput() {

    }

    private class SelectTask extends AsyncTask<Void,Void,List<LyDoHuyXoa>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<LyDoHuyXoa> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.lydohuyxoa");
            ArrayList<LyDoHuyXoa> reasons =  new Gson().fromJson(js.toString(), new TypeToken<List<LyDoHuyXoa>>(){}.getType());
            return reasons;
        }

        @Override
        protected void onPostExecute(List<LyDoHuyXoa> reasons) {
            super.onPostExecute(reasons);
            mView.showCancelReasonList(reasons);
        }
    }
}
