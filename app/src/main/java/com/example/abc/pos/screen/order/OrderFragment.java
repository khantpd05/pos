package com.example.abc.pos.screen.order;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.adapter.ExpandOrderSaleAdapter;
import com.example.abc.pos.adapter.TabAdapter;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.HappyHour;
import com.example.abc.pos.model.HappyHour_Detail;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.ItemsPopup;
import com.example.abc.pos.model.KitchenNote;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.Spice;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.screen.cancel_reason_dialog.CancelReasonFragment;
import com.example.abc.pos.screen.change_order_to_table.ChangeOrderToTableFragment;
import com.example.abc.pos.screen.chuc_nang_order.ChucNangOrderFragment;
import com.example.abc.pos.screen.customer_position.CustomerPositionFragment;
import com.example.abc.pos.screen.divide_bill.DivideBillFragment;
import com.example.abc.pos.screen.found_items.FoundItemsDialogFragment;
import com.example.abc.pos.screen.free_dish.FreeDishFragment;
import com.example.abc.pos.screen.items_popup.ItemsPopupDialogFragment;
import com.example.abc.pos.screen.keyboard.KeyboardFragment;
import com.example.abc.pos.screen.keyboard_code.KeyboardCodeFragment;
import com.example.abc.pos.screen.kich_co_mon_dialog.KichCoMonDialogFragment;
import com.example.abc.pos.screen.kitchen_note.KitchenNoteFragment;
import com.example.abc.pos.screen.next_order_dialog.NextOrderDialogFragment;
import com.example.abc.pos.screen.payment.PaymentFragment;
import com.example.abc.pos.screen.separate_order_to_table.SeparateOrderToTableFragment;
import com.example.abc.pos.screen.spice.SpiceFragment;
import com.example.abc.pos.screen.tab_of_sale.drink.DrinkTabFragment;
import com.example.abc.pos.screen.tab_of_sale.menu.MenuTabFragment;
import com.example.abc.pos.utils.Constants;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.example.abc.pos.utils.Util.miliHH;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderFragment extends BaseFragment implements OrderConstract.View,
        MenuTabFragment.OnMenuItemClick,
        View.OnClickListener,
        ExpandOrderSaleAdapter.OnItemClickGroup,
        KeyboardFragment.OnKeyboardPress,
        NextOrderDialogFragment.OnGetNextOrder,
        SpiceFragment.OnUpdateSpiceList,
        KitchenNoteFragment.OnUpdateKitcheNoteList,
        CustomerPositionFragment.OnPositionClick,
        ChangeOrderToTableFragment.OnChangeOrderToTableSuccessful,
        FreeDishFragment.OnAddFreeDish,
        DivideBillFragment.OnCancelDivideBill,
        CancelReasonFragment.OnCancelReasonSelected,
        SeparateOrderToTableFragment.OnSeparateOrderToTableSuccessful,
        DivideBillFragment.OnPaymentFinishFromDivide,
        KeyboardCodeFragment.OnKeyboardCodePress,
        PaymentFragment.OnPaymentFinish,
        PaymentFragment.OnPaymentCancel,
        KichCoMonDialogFragment.OnItemKichCoMonSelected,
        DrinkTabFragment.OnDrinkItemClick,
        FoundItemsDialogFragment.OnItemFoundSelected, ChucNangOrderFragment.OnChucNangPress, ItemsPopupDialogFragment.OnItemPopupClick {


    private static OrderFragment this_;

    private Fragment keyboardFragment;
    private Fragment keyboardCodeFragment;
    private KichCoMonDialogFragment kichCoMonDialogFragment;
    private DialogFragment foundItemsDialogFragment;
    private NextOrderDialogFragment nextOrderDialogFragment;
    private CancelReasonFragment cancelReasonDialogFragment;
    private ChucNangOrderFragment chucNangOrderFragment;

    //navigation declare;
    private ImageView img_menu;
//    private NavigationView navigationView;
//    private DrawerLayout drawer;
//    private View navHeader;
//    private TextView tv_user;
//    private TextView tv_info_giam;

    //action bar;
    private TabAdapter tab_adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    /* toolbar */
    private Toolbar toolbar;
    private TextView tv_customer_position;
    private TextView table_name;
    private TextView tv_happyhour;
    private LinearLayout ll_happyhour;

    private OrderConstract.Presenter presenter;

    /* order list */
    private ExpandableListView orderTempExpandList;
    private ExpandOrderSaleAdapter orderSaleAdapter;

    /* button declare */
    private LinearLayout ll_feature_full, ll_feature_multi, ll_feature_printed;
    private LinearLayout btn_print_kitchen_multi, btn_sale_multi, btn_change_to_table_multi, btn_go_home_multi, btn_note_kitchen_multi;
    private LinearLayout btn_change_to_table_printed /*btn_go_home_printed, btn_spice_printed*/;
    private LinearLayout btn_cancel_full, btn_cancel_printed, btn_cancel_multi;
    private LinearLayout btn_print_kitchen, btn_free_price, btn_sale, btn_change_to_table, btn_go_home, btn_note_kitchen, btn_spice;
    private ImageView btn_remove, btn_decrease, btn_increase;

    /* data using */
    private List<Integer> positionSelectedGroup;
    private Table table;
    private List<OrdersTemp> ordersTempList;
    private int typeClick;
    private int customer_position = 1;

    /*menu sheet*/
    private LinearLayout menu_bar;
    private RelativeLayout menu_sheet;
    private TextView tv_summer;
    private LinearLayout btn_food_code;

    /*order sheet*/
    private ScrollView order_sheet;

    /* payment and order */
    private LinearLayout btn_payment, btn_order;

    private String permission;
    private boolean isThanhToan = true;
    private ImageView img_thanhToan;
    private TextView tv_thanhToan;

    private Bundle savedInstanceState;


    public OrderFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public OrderFragment(Table table, String permission) {
        // Required empty public constructor
        this.table = table;
        SessionApp.global_table = table;
        SessionApp.TABLE_NAME = table.getName();
        this.permission = permission;
    }

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_sale;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retain this Fragment so that it will not be destroyed when an orientation
        // change happens and we can keep our AsyncTask running
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sale, container, false);
        tabLayout = view.findViewById(R.id.tabLayout);
        viewPager = view.findViewById(R.id.viewPager);
        tab_adapter = new TabAdapter(getChildFragmentManager());

        /* menu sheet */
        tv_summer = view.findViewById(R.id.tv_summer);
        btn_food_code = view.findViewById(R.id.btn_food_code);
        menu_bar = view.findViewById(R.id.menu_bar);
        menu_bar.setOnClickListener(this);
        menu_sheet = view.findViewById(R.id.menu_sheet);
        menu_sheet.setVisibility(View.VISIBLE);

        /* Order List */
        orderTempExpandList = view.findViewById(R.id.orderTempList);
        orderSaleAdapter = new ExpandOrderSaleAdapter(getContext(), new ArrayList<OrdersTemp>(), SessionApp.OrderOrPayment.ORDER, this);
        orderTempExpandList.setAdapter(orderSaleAdapter);

        /* set up navigation */
        img_menu = view.findViewById(R.id.img_menu);
        img_menu.setOnClickListener(this);
        toolbar = view.findViewById(R.id.toolbar);
        ((BaseActivity) getActivity()).setSupportActionBar(toolbar);
//        drawer = view.findViewById(R.id.drawer_layout);
//        navigationView = view.findViewById(R.id.nav_view);
//        navHeader = navigationView.getHeaderView(0);

//        tv_user = navHeader.findViewById(R.id.tv_user);
//        tv_user.setText(WapriManager.getUser().getUsername());
//
//        tv_info_giam = navHeader.findViewById(R.id.tv_info_giam);

        /* restore state*/
        this.savedInstanceState = savedInstanceState;
        presenter = new OrderPresenter(this);
        if (savedInstanceState != null) {
            SessionApp.global_table = (Table) savedInstanceState.getSerializable("global_table");
            SessionApp.CUSTOMER_POSITION = savedInstanceState.getInt("customer_position");
            SessionApp.TABLE_NAME = savedInstanceState.getString("table_name");
            presenter.setTable(SessionApp.global_table);
            this.permission = savedInstanceState.getString("permission");

            Gson gson = new Gson();
            ordersTempList = gson.fromJson(savedInstanceState.getString("ordertempList"), new TypeToken<List<OrdersTemp>>() {
            }.getType());

//            tv_info_giam.setText(SessionApp.global_table.getGiam() + "%");
            presenter.setOrderTempList(ordersTempList);
            presenter.getOffOrderTempList(SessionApp.CUSTOMER_POSITION);

            SessionApp.isHappyHour = savedInstanceState.getBoolean("isHappyHour");

            SessionApp.happyHour = gson.fromJson(savedInstanceState.getString("happyHour"), HappyHour.class);

            SessionApp.happyHour_detail = gson.fromJson(savedInstanceState.getString("hh_detail"), new TypeToken<List<HappyHour_Detail>>() {
            }.getType());

        } else {
            initOrder();
        }

        if (table == null) {
            table = SessionApp.global_table;
        }
        this_ = this;

        tab_adapter.addFragment(new MenuTabFragment(this), App.Companion.self().getString(R.string.Dish));
        tab_adapter.addFragment(new DrinkTabFragment(this), App.Companion.self().getString(R.string.Drinks));

        viewPager.setAdapter(tab_adapter);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        /* bottom sheet */

        /* toolbar */
        table_name = view.findViewById(R.id.table_name);
        table_name.setText(table.getName());
        tv_happyhour = view.findViewById(R.id.tv_happyhour);
        ll_happyhour = view.findViewById(R.id.ll_happyhour);
        if (SessionApp.isHappyHour) {
            if (SessionApp.happyHour != null) {
                ll_happyhour.setVisibility(View.VISIBLE);
                tv_happyhour.setText(Util.getStringFromResouce(R.string.remain) + ": " + Util.getDayHourMinuteByMinute(Calendar.getInstance().getTime(), SessionApp.endDate));

                new CountDownTimer(miliHH + 60000, 30000) {
                    public void onTick(long millisUntilFinished) {
                        tv_happyhour.setText(Util.getStringFromResouce(R.string.remain) + ": " + Util.getDayHourMinuteByMinute(Calendar.getInstance().getTime(), SessionApp.endDate));
                    }

                    public void onFinish() {
                        ll_happyhour.setVisibility(View.GONE);
                        SessionApp.isHappyHour = false;
                        SessionApp.isAplyAllHappyHour = false;
                        tab_adapter.addFragment(new MenuTabFragment(this_), App.Companion.self().getString(R.string.Dish));
                        tab_adapter.addFragment(new DrinkTabFragment(this_), App.Companion.self().getString(R.string.Drinks));
                        viewPager.setAdapter(tab_adapter);
                        tabLayout.setupWithViewPager(viewPager);
                        setupTabIcons();
                    }
                }.start();
            }
        } else {
            ll_happyhour.setVisibility(View.GONE);
        }


        tv_customer_position = view.findViewById(R.id.tv_customer_position);
        String position;
        if (SessionApp.CUSTOMER_POSITION != 0)
            position = SessionApp.CUSTOMER_POSITION + "";
        else
            position = Util.getStringFromResouce(R.string.all);
        tv_customer_position.setText(position);
        tv_customer_position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.goToCustomerPosition();
            }
        });


//        drawer.closeDrawers();
//        setUpNavigationView();

        /* order sheet */
        order_sheet = view.findViewById(R.id.order_sheet);




        /* presenter init */

        getViTriNgoi();

        /* feature order button */
        initView(view);
        return view;
    }

    public static OrderFragment getOrderFragmentListener() {
        return this_;
    }

    private void setupTabIcons() {
        for (int i = 0; i < tab_adapter.getCount(); i++) {
            if (getContext() != null) {
                View customView = tab_adapter.getCustomView(getContext(), i);
                tabLayout.getTabAt(i).setCustomView(customView);
            }
        }
    }

    private void initOrder() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                presenter.loadData(table);
                SessionApp.global_destination_table = presenter.getTable();
            }
        }, 500);
    }

    private void initView(View v) {
        btn_remove = v.findViewById(R.id.btn_remove);
        btn_decrease = v.findViewById(R.id.btn_decrease);
        btn_increase = v.findViewById(R.id.btn_increase);

        ll_feature_full = v.findViewById(R.id.ll_feature_full);
        ll_feature_multi = v.findViewById(R.id.ll_feature_multi);
        ll_feature_printed = v.findViewById(R.id.ll_feature_printed);

        btn_print_kitchen = v.findViewById(R.id.btn_print_kitchen);
        btn_free_price = v.findViewById(R.id.btn_free_price);
        btn_sale = v.findViewById(R.id.btn_sale);
        btn_change_to_table = v.findViewById(R.id.btn_change_to_table);
        btn_go_home = v.findViewById(R.id.btn_go_home);
        btn_note_kitchen = v.findViewById(R.id.btn_note_kitchen);
        btn_spice = v.findViewById(R.id.btn_spice);

        btn_print_kitchen_multi = v.findViewById(R.id.btn_print_kitchen_multi);
        btn_sale_multi = v.findViewById(R.id.btn_sale_multi);
        btn_change_to_table_multi = v.findViewById(R.id.btn_change_to_table_multi);
        btn_go_home_multi = v.findViewById(R.id.btn_go_home_multi);
        btn_note_kitchen_multi = v.findViewById(R.id.btn_note_kitchen_multi);

        btn_change_to_table_printed = v.findViewById(R.id.btn_change_to_table_printed);
//        btn_go_home_printed = v.findViewById(R.id.btn_go_home_printed);
//        btn_spice_printed = v.findViewById(R.id.btn_spice_printed);

        btn_cancel_full = v.findViewById(R.id.btn_cancel_full);
        btn_cancel_multi = v.findViewById(R.id.btn_cancel_multi);
        btn_cancel_printed = v.findViewById(R.id.btn_cancel_printed);

        btn_payment = v.findViewById(R.id.btn_payment);
        btn_order = v.findViewById(R.id.btn_order);

        btn_remove.setOnClickListener(this);
        btn_decrease.setOnClickListener(this);
        btn_increase.setOnClickListener(this);

        btn_print_kitchen.setOnClickListener(this);
        btn_free_price.setOnClickListener(this);
        btn_sale.setOnClickListener(this);
        btn_change_to_table.setOnClickListener(this);
        btn_go_home.setOnClickListener(this);
        btn_note_kitchen.setOnClickListener(this);
        btn_spice.setOnClickListener(this);

        btn_print_kitchen_multi.setOnClickListener(this);
        btn_sale_multi.setOnClickListener(this);
        btn_change_to_table_multi.setOnClickListener(this);
        btn_go_home_multi.setOnClickListener(this);
        btn_note_kitchen_multi.setOnClickListener(this);

        btn_change_to_table_printed.setOnClickListener(this);
//        btn_go_home_printed.setOnClickListener(this);
//        btn_spice_printed.setOnClickListener(this);

        btn_cancel_full.setOnClickListener(this);
        btn_cancel_multi.setOnClickListener(this);
        btn_cancel_printed.setOnClickListener(this);

        btn_payment.setOnClickListener(this);
        btn_order.setOnClickListener(this);

        btn_food_code.setOnClickListener(this);

        if (table.getTaichohaymangve() == WapriManager.taichohaymangve.MangVe) {
            for (int i = 0; i < btn_go_home_multi.getChildCount(); i++) {
//                btn_go_home_printed.getChildAt(i).setVisibility(View.INVISIBLE);
                btn_go_home_multi.getChildAt(i).setVisibility(View.INVISIBLE);
                btn_go_home.getChildAt(i).setVisibility(View.INVISIBLE);
            }

            btn_go_home.setClickable(false);
            btn_go_home_multi.setClickable(false);
//            btn_go_home_printed.setClickable(false);
        }

        if (permission.equals(Util.getStringFromResouce(R.string.order_for_table_top))) {
            isThanhToan = false;
            setDisableThanhToan(v);
        }
    }

    private void setDisableThanhToan(View v) {
        if (!isThanhToan) {
            img_thanhToan = v.findViewById(R.id.img_thanhToan);
            tv_thanhToan = v.findViewById(R.id.tv_thanhToan);
            img_thanhToan.setAlpha(0.5f);
            tv_thanhToan.setAlpha(0.5f);
        }
    }


//    private void setUpNavigationView() {
//        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//
//            // This method will trigger on item Click of navigation menu
//            @Override
//            public boolean onNavigationItemSelected(MenuItem menuItem) {
//
//                //Check to see which item was being clicked and perform appropriate action
//                switch (menuItem.getItemId()) {
//                    //Replacing the main content with ContentFragment Which is our Inbox View;
//                    case R.id.nav_free_dish:
//                        if (WapriManager.getUser().getDuocQuyenNhapVaoMatHangChuaCoTrongKho().equals("Y")) {
//                            ((BaseActivity) getActivity()).pushFragmentNotReplace(new FreeDishFragment(this_), true);
//                            drawer.closeDrawers();
//                        }else{
//                            Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.you_do_not_have_permission_feature));
//                        }
//                        break;
//                    case R.id.nav_decrease_price:
//                        if (WapriManager.getUser().getDuocQuyenGiamGiaTheoPhanTramChoToanDonHang().equals("Y")) {
//                            typeClick = 2;
//                            SessionApp.keyboardType = SessionApp.KeyboardType.PHAN_TRAM;
//                            presenter.showKeyboard(App.Companion.self().getString(R.string.discount), presenter.getTable().getGiam() + "", "00", SessionApp.KeyboardType.PHAN_TRAM);
//                        } else
//                            Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.you_do_not_have_permission_feature));
//                        break;
//                    case R.id.nav_divide_bill:
//                        if (isThanhToan) {
//                            SessionApp.from_DivideOrder_or_Order = "divide_order";
//                            if (presenter.getOrderTempList().size() > 0) presenter.goToDivideBill();
//                            else
//                                Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.no_orders_yet));
//                        } else {
//                            Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.you_do_not_have_permission_feature));
//                        }
//                        break;
//                    case R.id.nav_separate_table:
////                        presenter.loadData();
//                        if (presenter.getOrderTempList().size() > 0) {
//                            SessionApp.global_orderTemp_list = presenter.getOrderTempList();
//                            presenter.goToSeparateToOtherTable();
//                            drawer.closeDrawers();
//                        } else
//                            Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.no_orders_yet));
//
//
//                        break;
//
//                    case R.id.nav_next_order:
//                        presenter.showNextOrderDialog();
//                        break;
//                    case R.id.nav_exit:
//                        if (presenter != null) {
//                            if(presenter.getOrderTempList()!=null){
//                                if (presenter.getOrderTempList().size() > 0) {
//                                    Util.alertPromp(getContext(), App.Companion.self().getString(R.string.Warning), Util.getStringFromResouce(R.string.There_are_items_still_be_ordering), new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            Util.popFragment(getActivity());
//                                        }
//                                    }, new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//
//                                        }
//                                    });
//                                } else {
//                                    Util.popFragment(getActivity());
//                                }
//                            }
//                        }
//
//                        break;
//                    default:
//                }
//
//                //Checking if the item is in checked state or not, if not make it in checked state
//                if (menuItem.isChecked()) {
//                    menuItem.setChecked(false);
//                } else {
//                    menuItem.setChecked(true);
//                }
//                menuItem.setChecked(true);
//
//                return true;
//            }
//        });
//
//        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
//                super.onDrawerClosed(drawerView);
//            }
//
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
//                super.onDrawerOpened(drawerView);
//            }
//        };
//
//        //Setting the actionbarToggle to drawer layout
//        drawer.setDrawerListener(actionBarDrawerToggle);
//
//        //calling sync state is necessary or else your hamburger icon wont show up
//        actionBarDrawerToggle.syncState();
//    }


    @Override
    public void onMenuItemClickListener(Items item) {
        presenter.addOrder(item, 0);
    }

    @Override
    public void onMenuItemLongClickListener(Items item) {
        presenter.addNewOrder(item, 0);
    }

    @Override
    public void onDrinkItemClickListener(Items item) {
        presenter.addOrder(item, 0);
    }

    @Override
    public void onDrinkItemLongClickListener(Items item) {
        presenter.addNewOrder(item, 0);
    }

    @Override
    public Context getCurrentContext() {
        return getContext();
    }

    @Override
    public void showChucNangOrder() {
        chucNangOrderFragment = new ChucNangOrderFragment(this);
        chucNangOrderFragment.show(getFragmentManager(), "");
    }

    @Override
    public void setHienThiBanGiam() {
//        tv_info_giam.setText(presenter.getTable().getGiam() + "%");
    }

    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }

    @Override
    public void showDialog(String data) {
        Util.showCustomDialog(getContext(), data);
    }

    @Override
    public void goToCustomerPosition(List<OrdersTemp> datas) {
        ordersTempList = datas;
        List<Integer> positionsHaveOrder = new ArrayList<>();
        for (OrdersTemp ob : datas) {
            if (!positionsHaveOrder.contains(Integer.valueOf(ob.getThutukhachtrenban()))) {
                positionsHaveOrder.add(Integer.valueOf(ob.getThutukhachtrenban()));
            }
        }
        CustomerPositionFragment customerPositionFragment = CustomerPositionFragment.Companion.newInstance(table.getSoghengoi(), (ArrayList<Integer>) positionsHaveOrder);
        ((BaseActivity) getActivity()).pushFragmentNotReplace(customerPositionFragment, true);
    }

    @Override
    public void showOrderTempList(List<OrdersTemp> datas) {
        tv_summer.setText(String.format("%,.2f", presenter.getSummerPrice()));
        orderSaleAdapter.setNewDatas(datas);
        orderTempExpandList.smoothScrollToPosition(datas.size() - 1);
//        orderSaleAdapter.setSelectedList(SessionApp.FIRST_SELECTED_POSITION);
    }

    @Override
    public void showFullOrderFeature() {
        ll_feature_full.setVisibility(View.VISIBLE);
        ll_feature_multi.setVisibility(View.GONE);
        ll_feature_printed.setVisibility(View.GONE);
    }

    @Override
    public void showOrderFeatureMulti() {
        ll_feature_full.setVisibility(View.GONE);
        ll_feature_multi.setVisibility(View.VISIBLE);
        ll_feature_printed.setVisibility(View.GONE);
    }

    @Override
    public void showOrderFeaturePrinted() {
        ll_feature_full.setVisibility(View.GONE);
        ll_feature_multi.setVisibility(View.GONE);
        ll_feature_printed.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSpiceFromMultiPrinted() {
//        btn_spice_printed.setClickable(false);
//        for (int i = 0; i < btn_spice_printed.getChildCount(); i++) {
//            btn_spice_printed.getChildAt(i).setVisibility(View.INVISIBLE);
//        }
    }

    @Override
    public void showSpiceFromSinglePrinted() {
//        btn_spice_printed.setClickable(true);
//        for (int i = 0; i < btn_spice_printed.getChildCount(); i++) {
//            btn_spice_printed.getChildAt(i).setVisibility(View.VISIBLE);
//        }
    }


    @Override
    public void showKeyboard(String title, String hint, String key11, SessionApp.KeyboardType keyboardType) {
        keyboardFragment = new KeyboardFragment(this, title, hint, key11, keyboardType);
        ((KeyboardFragment) keyboardFragment).show(getFragmentManager(), "");
    }

    @Override
    public void showKeyboardCode(String hint) {
        keyboardCodeFragment = new KeyboardCodeFragment(hint, this);
        ((KeyboardCodeFragment) keyboardCodeFragment).show(getFragmentManager(), null);
        resetSelectedList();
        showOrderTempList(presenter.getOrderTempListByPosition());
    }

    @Override
    public void showKichCoMonDialog(Items item, int soLuong) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        kichCoMonDialogFragment = new KichCoMonDialogFragment(item, table.getTaichohaymangve(), soLuong, this);
        kichCoMonDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        kichCoMonDialogFragment.show(fm, KichCoMonDialogFragment.class.getSimpleName());
    }

    @Override
    public void showFoundItemsDialog(List<Items> items, int soLuong) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        foundItemsDialogFragment = new FoundItemsDialogFragment(items, soLuong, this);
        foundItemsDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        foundItemsDialogFragment.show(fm, KichCoMonDialogFragment.class.getSimpleName());
    }

    @Override
    public void showNextOrderDialog() {
        nextOrderDialogFragment = new NextOrderDialogFragment("", "", false, this);
        nextOrderDialogFragment.show(getFragmentManager(), null);
        resetSelectedList();
        showOrderTempList(presenter.getOrderTempListByPosition());
    }

    @Override
    public void showCancelReasonDialog() {
        cancelReasonDialogFragment = new CancelReasonFragment(this);
        cancelReasonDialogFragment.show(getFragmentManager(), null);
    }

    @Override
    public void showItemsPopup(int positionOrdertemp, List<ItemsPopup> itemsPopupList) {
        ItemsPopupDialogFragment itemsPopupDialogFragment = new ItemsPopupDialogFragment(itemsPopupList, positionOrdertemp, this);
        itemsPopupDialogFragment.show(getFragmentManager(), "");
    }

    @Override
    public void goToGiaVi(List<Spice> datas) {
        ((BaseActivity) getActivity()).pushFragmentNotReplace(new SpiceFragment(this, datas), true);
    }

    @Override
    public void goToKitchenNote(List<KitchenNote> datas) {
        ((BaseActivity) getActivity()).pushFragmentNotReplace(new KitchenNoteFragment(this, datas), true);
    }

    @Override
    public void resetSelectedList() {
        if (positionSelectedGroup != null && positionSelectedGroup.size() > 0) {
            positionSelectedGroup.clear();
            positionSelectedGroup = new ArrayList<>();
            orderSaleAdapter.resetSelectedList();
            Util.slideDown(order_sheet);
        }
    }

    @Override
    public int getViTriNgoi() {
        SessionApp.CUSTOMER_POSITION = customer_position;
        return customer_position;
    }

    @Override
    public void goToChangeToOtherTable(List<OrdersTemp> ordersTempsWillChange, List<OrdersTemp> datas) {
        ordersTempList = datas;
//        List<Integer> positionsHaveOrder = new ArrayList<>();
//        for (OrdersTemp ob : datas) {
//            if (!positionsHaveOrder.contains(Integer.valueOf(ob.getThutukhachtrenban()))) {
//                positionsHaveOrder.add(Integer.valueOf(ob.getThutukhachtrenban()));
//            }
//        }
        ((BaseActivity) getActivity()).pushFragmentNotReplace(new ChangeOrderToTableFragment(ordersTempsWillChange, WapriManager.getUser().getUsername(), table.getName(), this), true);
        SessionApp.FIRST_SELECTED_POSITION = -1;
    }

    @Override
    public void goToDivideBill(List<OrdersTemp> orderTempList, List<OrdersTemp> restoreOrderTempList) {
        DivideBillFragment divideBillFragment = DivideBillFragment.newInstance(orderTempList, restoreOrderTempList, presenter.getTable().getGiam());

        ((BaseActivity) getActivity()).pushFragmentNotReplace(divideBillFragment, true);

        resetSelectedList();
        showOrderTempList(presenter.getOrderTempListByPosition());
    }

    @Override
    public void goToSeparateToOtherTable() {
        ((BaseActivity) getActivity()).pushFragmentNotReplace(new SeparateOrderToTableFragment(WapriManager.getUser().getUsername(), table.getName(), this), true);
    }

    @Override
    public void setTongtienView() {
        tv_summer.setText(String.format("%,.2f", presenter.getSummerPrice()));
    }

    @Override
    public void goToTable() {
        if (getActivity() != null)
            Util.popFragment(getActivity());
    }

    @Override
    public void goBack() {
        Util.popFragment(getActivity());
    }

    @Override
    public void cancelSelectedOrders() {
        resetSelectedList();
        Util.slideDown(order_sheet);
    }

    @Override
    public boolean getPermission() {
        return isThanhToan;
    }

    @Override
    public void setMarkSignList() {
        orderSaleAdapter.setMarkSign();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //Navigation
            case R.id.img_menu:
                presenter.showChucNangOrder();
                break;

            //Full
            case R.id.btn_print_kitchen:
                presenter.updateInBep(positionSelectedGroup);
                break;

            case R.id.btn_remove:
                presenter.removeOrder(positionSelectedGroup);


//                Util.alertPromp(getContext(), "Cảnh báo", "Bạn chắc chắn muốn xóa?", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                }, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
                break;
            case R.id.btn_food_code:
                presenter.showKeyboardCode();
                resetSelectedList();
                break;

            case R.id.btn_decrease:
                if (WapriManager.getUser().getDuocQuyenThayDoiSoLuongTrongDonHang().equals("Y")) {
                    if (positionSelectedGroup.size() > 1)
                        Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.can_not_increase_decrease_many_dishes_at_the_same_time));
                    else if (positionSelectedGroup.size() > 0) {
                        presenter.decreaseNumberOrder(positionSelectedGroup.get(0));
                    }
                } else
                    Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.you_do_not_have_permission_feature));


                break;

            case R.id.btn_increase:
                if (WapriManager.getUser().getDuocQuyenThayDoiSoLuongTrongDonHang().equals("Y")) {
                    if (positionSelectedGroup.size() > 1)
                        Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.can_not_increase_decrease_many_dishes_at_the_same_time));
                    else if (positionSelectedGroup.size() > 0) {
                        presenter.increaseNumberOrder(positionSelectedGroup.get(0));
                    }
                } else
                    Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.you_do_not_have_permission_feature));
                break;
            case R.id.btn_free_price:
                if (WapriManager.getUser().getDuocQuyenThayDoiGiaMatHang().equals("Y")) {
                    typeClick = 0;
                    presenter.showKeyboard(App.Companion.self().getString(R.string.Free_price), presenter.getOrderTempList().get(positionSelectedGroup.get(0)).getThanhtien() + "", "00", SessionApp.KeyboardType.TIEN);
                } else
                    Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.you_do_not_have_permission_feature));
                break;
            case R.id.btn_sale:
                if (WapriManager.getUser().getDuocQuyenGiamGiaTheoPhanTramChoMathang().equals("Y")) {
                    typeClick = 1;
                    SessionApp.keyboardType = SessionApp.KeyboardType.PHAN_TRAM;
                    presenter.showKeyboard(App.Companion.self().getString(R.string.discount), "0", "00", SessionApp.KeyboardType.PHAN_TRAM);
                } else
                    Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.you_do_not_have_permission_feature));
                break;
            case R.id.btn_change_to_table:
                presenter.goToChangeToOtherTable(positionSelectedGroup);
                break;
            case R.id.btn_go_home:
                presenter.updateGoHome(positionSelectedGroup);
                break;
            case R.id.btn_note_kitchen:
                presenter.goToKitchenNote(positionSelectedGroup);
                break;
            case R.id.btn_spice:
                if (positionSelectedGroup.size() > 0)
                    presenter.goToGiaVi(positionSelectedGroup.get(0));
                break;
            case R.id.btn_cancel_full:
                presenter.cancelSelectedOrders();
                break;
            case R.id.btn_payment:
                SessionApp.from_DivideOrder_or_Order = "order";
                if (isThanhToan) {
                    if (presenter.getOrderTempListByPosition() != null && presenter.getOrderTempListByPosition().size() > 0) {
                        if (WapriManager.configuration().canhBaoThanhToan.get()) {

                            Util.showCustomDialogConfirm(getActivity(), Util.getStringFromResouce(R.string.You_definitely_want_to_pay), new Util.ConfirmDialogInterface() {
                                @Override
                                public void onPositiveClickListener() {
                                    presenter.goToPayment();
                                }
                                @Override
                                public void onNegativeClickListener() {

                                }
                            });

                        } else {
                            presenter.goToPayment();
                        }
                    } else
                        Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.no_orders_yet));
                } else {
                    Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.you_do_not_have_permission_feature));
                }

                break;
            case R.id.btn_order:
                presenter.doOrder();
                break;
            case R.id.menu_bar:
                if (menu_sheet.getVisibility() == View.GONE) {
                    menu_sheet.setVisibility(View.VISIBLE);
                } else {
                    menu_sheet.setVisibility(View.GONE);
                }
                break;

            // Multi
            case R.id.btn_print_kitchen_multi:
                presenter.updateInBep(positionSelectedGroup);
                break;
            case R.id.btn_sale_multi:
                if (WapriManager.getUser().getDuocQuyenGiamGiaTheoPhanTramChoMathang().equals("Y")) {
                    typeClick = 1;
                    SessionApp.keyboardType = SessionApp.KeyboardType.PHAN_TRAM;
                    presenter.showKeyboard(App.Companion.self().getString(R.string.discount), "0", "00", SessionApp.KeyboardType.PHAN_TRAM);
                } else
                    Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.you_do_not_have_permission_feature));
                break;
            case R.id.btn_change_to_table_multi:

                presenter.goToChangeToOtherTable(positionSelectedGroup);
                break;
            case R.id.btn_go_home_multi:
                presenter.updateGoHome(positionSelectedGroup);
                break;
            case R.id.btn_note_kitchen_multi:
                presenter.goToKitchenNote(positionSelectedGroup);
                break;
            case R.id.btn_cancel_multi:
                presenter.cancelSelectedOrders();
                break;

            // Printed
            case R.id.btn_change_to_table_printed:

                presenter.goToChangeToOtherTable(positionSelectedGroup);
                break;
//            case R.id.btn_go_home_printed:
//                presenter.updateGoHome(positionSelectedGroup);
//                break;
//            case R.id.btn_spice_printed:
//                presenter.goToGiaVi(positionSelectedGroup.get(0));
//                break;
            case R.id.btn_cancel_printed:
                presenter.cancelSelectedOrders();
                break;
        }
    }

    @Override
    public void goToPayment() {
        ((BaseActivity) getActivity()).pushFragmentNotReplace(new PaymentFragment(presenter.getOrderTempListByPosition(), presenter.getSummerPrice(), table_name.getText().toString(), this_, this_), true);
//        presenter.tinhTienGiamTable();
        resetSelectedList();
        showOrderTempList(presenter.getOrderTempListByPosition());
    }

    @Override
    public void onItemClickGroupListener(List<Integer> positions, OrdersTemp ordersTemp) {
        positionSelectedGroup = positions;

        if (positionSelectedGroup.size() > 0) {
            SessionApp.FIRST_SELECTED_POSITION = positions.get(0);
            presenter.hideSomeOrderFeature(positionSelectedGroup);

            menu_sheet.setVisibility(View.VISIBLE);
            Util.slideUp(order_sheet);
        } else {
            SessionApp.FIRST_SELECTED_POSITION = -1;
            Util.slideDown(order_sheet);
        }

//        presenter.hideSomeOrderFeature(position);
    }

    @Override
    public void onItemHeaderListener(final OrdersTemp ordersTemp) {

        Util.showCustomDialogConfirm(getActivity(), Util.getStringFromResouce(R.string.are_u_w_to_delete), new Util.ConfirmDialogInterface() {
            @Override
            public void onPositiveClickListener() {
                presenter.deleteOrderTemp(ordersTemp);
                resetSelectedList();
                showOrderTempList(presenter.getOrderTempListByPosition());
            }

            @Override
            public void onNegativeClickListener() {

            }
        });

//        Fragment fragment = new NextOrderDialogFragment(data, orderId, true, this);
//        ((NextOrderDialogFragment) fragment).show(getFragmentManager(), null);

    }

    @Override
    public void onItemSelectedListener(int positionSelected, OrdersTemp ordersTemp) {
        List<ItemsPopup> itemsPopupList = new ArrayList<>();
        boolean isMatch = false;
        for (ItemsPopup items_popup : SessionApp.items_popup) {
            if (items_popup.getItems_id().equals(ordersTemp.getitems_id())) {
                itemsPopupList.add(items_popup);
                isMatch = true;
            }
        }
        if (isMatch) {
            showItemsPopup(positionSelected, itemsPopupList);
        }

    }


    @Override
    public void onKeyPressListener(String data) {
        if (typeClick == 0) {
            presenter.updateFreePrice(positionSelectedGroup.get(0), data);
        } else if (typeClick == 1) {
            presenter.updateGiamGiaMonAn(positionSelectedGroup, data);
        } else if (typeClick == 2) {
            if (data.isEmpty()) data = presenter.getTable().getGiam() + "";
//            tv_info_giam.setText(data + "%");
            presenter.doUpdateGiamGiaTable(data);
        }
    }


    @Override
    public void onKeyCodePressListener(String data) {
        presenter.doOrderByFoodId(data);
    }

    @Override
    public void onUpdateSpiceListListener(List<Spice> spiceList) {
        if (positionSelectedGroup != null && !positionSelectedGroup.isEmpty())
            presenter.updateSpiceList(positionSelectedGroup.get(0), spiceList);
    }

    @Override
    public void onUpdateKitchenNoteListListener(List<KitchenNote> kitchenNotes) {
        if (positionSelectedGroup != null && !positionSelectedGroup.isEmpty())
            presenter.updateKitchenNoteList(positionSelectedGroup, kitchenNotes);
    }

    @Override
    public void onStop() {
        super.onStop();

        //Khi người dùng thoát màn hình order sẽ update lại table
    }

    @Override
    public void onPause() {
        super.onPause();
        if ((keyboardFragment) != null)
            ((KeyboardFragment) keyboardFragment).dismiss();
        if ((keyboardCodeFragment) != null)
            ((KeyboardCodeFragment) keyboardCodeFragment).dismiss();
        if (kichCoMonDialogFragment != null)
            kichCoMonDialogFragment.dismiss();
        if (foundItemsDialogFragment != null)
            foundItemsDialogFragment.dismiss();
        if (nextOrderDialogFragment != null)
            nextOrderDialogFragment.dismiss();
        if (cancelReasonDialogFragment != null)
            cancelReasonDialogFragment.dismiss();
        if (chucNangOrderFragment != null)
            chucNangOrderFragment.dismiss();
    }

    @Override
    public void onCustomerPositionClickListener(int position) {
        customer_position = position;
        SessionApp.CUSTOMER_POSITION = position;
        if (customer_position == 0)
            tv_customer_position.setText("All");
        else
            tv_customer_position.setText(customer_position + "");

        resetSelectedList();
        presenter.getOffOrderTempList(position);
    }

    @Override
    public void onGetNextOrderListener(String data) {
        Items item = new Items();
        item.setId(WapriManager.TIEU_DE_DOT_DAT_ID);
        item.setTenhang(data);
        item.setBonname(data);
        item.setThue((byte) 0);
        item.setGiale1(0.0);
        item.setGiale(0.0);
        item.setLoaiMonAn("MonChinh");
        item.setBophanphucvu("Bep");
        presenter.addOrder(item, 0);
    }

    @Override
    public void onUpdateNextOrderListener(String orderId, String data) {
        presenter.updateHeaderOrder(orderId, data);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if ((keyboardFragment) != null)
            ((KeyboardFragment) keyboardFragment).dismiss();
        if ((keyboardCodeFragment) != null)
            ((KeyboardCodeFragment) keyboardCodeFragment).dismiss();
        if (kichCoMonDialogFragment != null)
            kichCoMonDialogFragment.dismiss();
        if (foundItemsDialogFragment != null)
            foundItemsDialogFragment.dismiss();
        if (nextOrderDialogFragment != null)
            nextOrderDialogFragment.dismiss();
        if (cancelReasonDialogFragment != null)
            cancelReasonDialogFragment.dismiss();
        if (chucNangOrderFragment != null)
            chucNangOrderFragment.dismiss();


        SessionApp.global_table = null;
        SessionApp.global_destination_table = null;
        SessionApp.global_orderTemp_list = null;
        SessionApp.FIRST_SELECTED_POSITION = -1;
        SessionApp.CUSTOMER_POSITION = 0;
        presenter.doChangeValueTable("out", isThanhToan);
        SessionApp.is_updated_table = false;
    }

    @Override
    public void onChangeOrderToTableSuccessfulListener(List<OrdersTemp> ordersTempsWillChange) {
        resetSelectedList();
        Util.slideDown(order_sheet);
        presenter.removeOrderTempListHasChanged(ordersTempsWillChange);
    }

    @Override
    public void onAddFreeDishListener(OrdersTemp ordersTemp) {
        presenter.addOrderFromFreeDish(ordersTemp);
    }

    @Override
    public void onCancelDivideBill(List<OrdersTemp> ordersTempList) {
        List<OrdersTemp> ordersTempListDelete = new ArrayList<>();
        for (OrdersTemp ordersTemp : ordersTempList) {
            if (ordersTemp.getSoluong() < 1) {
                ordersTempListDelete.add(ordersTemp);
            }
        }
        for (OrdersTemp ordersTemp : ordersTempListDelete) {
            ordersTempList.remove(ordersTemp);
        }
        presenter.setOrderTempList(ordersTempList);
        presenter.getOffOrderTempList(SessionApp.CUSTOMER_POSITION);
        resetSelectedList();
    }

    @Override
    public void onCancelReasonSelectedListener(String data) {
        presenter.updateCancelReason(data);
    }

    @Override
    public void onSeparateOrderToTableSuccessfulListener(List<OrdersTemp> ordersTempsWillChange) {
        resetSelectedList();
        Util.slideDown(order_sheet);
        presenter.removeOrderTempListHasChanged(ordersTempsWillChange);
    }

    @Override
    public void onPaymentFinishFromDivideListener() {
        Table table = presenter.getTable();
        table.setTongtien(presenter.getTongTienTable());
        table.setUsername(WapriManager.getUser().getUsername());
        table.setThoigian(Util.getDate("dt"));
        presenter.updateTableInfo(table);
    }

    @Override
    public void onPaymentFinishListener(List<OrdersTemp> origOrderTemp_list) {
        presenter.removeOrderTempListHasChanged(origOrderTemp_list);
        Table table = presenter.getTable();
        table.setTongtien(presenter.getTongTienTable());
        table.setUsername(WapriManager.getUser().getUsername());
        table.setThoigian(Util.getDate("dt"));
        presenter.updateTableInfo(table);
    }

    @Override
    public void onPaymentCancelListener() {
        presenter.setOrderTempList(presenter.getOrderTempRecoveryList());
        presenter.getOffOrderTempList(SessionApp.CUSTOMER_POSITION);
        resetSelectedList();
    }


    @Override
    public void onItemKichCoMonSelectedListener(Items item, int soLuong) {
        presenter.doAddOrderTemp(item, soLuong);
    }


    @Override
    public void onItemFoundSelectedListener(Items item, int soLuong) {
        presenter.addOrder(item, soLuong);
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
//        getFragmentManager().putFragment(outState,"myfragment",this_);
        outState.putSerializable("global_table", SessionApp.global_table);
        outState.putString("permission", this.permission);
        outState.putInt("customer_position", SessionApp.CUSTOMER_POSITION);
        outState.putString("table_name", SessionApp.TABLE_NAME);

        Type listType = new TypeToken<List<OrdersTemp>>() {
        }.getType();
        Gson gson = new Gson();
        String json = gson.toJson(presenter.getOrderTempList(), listType);
        outState.putString("ordertempList", json);


        outState.putBoolean("isHappyHour", SessionApp.isHappyHour);

        String happyHour = gson.toJson(SessionApp.happyHour);
        outState.putString("happyHour", happyHour);

        Type happyHourList = new TypeToken<List<HappyHour>>() {
        }.getType();

        String json_hh_detail = gson.toJson(SessionApp.happyHour_detail, happyHourList);
        outState.putString("hh_detail", json_hh_detail);
//        Util.setDataSaveInstanceFromGson(outState,presenter.getOrderTempList(),"ordertempList");
    }

    @Override
    public void onChucNangPressListener(int key) {
        switch (key) {
            case Constants.MON_TU_DO:
                if (WapriManager.getUser().getDuocQuyenNhapVaoMatHangChuaCoTrongKho().equals("Y")) {
                    ((BaseActivity) getActivity()).pushFragmentNotReplace(new FreeDishFragment(this_), true);
                } else {
                    Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.you_do_not_have_permission_feature));
                }
                break;
            case Constants.GIAM_GIA_DON_HANG:
                if (WapriManager.getUser().getDuocQuyenGiamGiaTheoPhanTramChoToanDonHang().equals("Y")) {
                    typeClick = 2;
                    SessionApp.keyboardType = SessionApp.KeyboardType.PHAN_TRAM;
                    presenter.showKeyboard(App.Companion.self().getString(R.string.discount), presenter.getTable().getGiam() + "", "00", SessionApp.KeyboardType.PHAN_TRAM);
                } else
                    Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.you_do_not_have_permission_feature));
                break;
            case Constants.CHIA_HOA_DON:
                if (isThanhToan) {
                    SessionApp.from_DivideOrder_or_Order = "divide_order";
                    if (presenter.getOrderTempList().size() > 0) presenter.goToDivideBill();
                    else
                        Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.no_orders_yet));
                } else {
                    Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.you_do_not_have_permission_feature));
                }
                break;
            case Constants.TACH_BAN:
                if (presenter.getOrderTempList().size() > 0) {
                    SessionApp.global_orderTemp_list = presenter.getOrderTempList();
                    presenter.goToSeparateToOtherTable();
                } else
                    Util.showCustomDialog(getContext(), App.Companion.self().getString(R.string.no_orders_yet));
                break;
            case Constants.DOT_DAT_KE_TIEP:
                presenter.showNextOrderDialog();
                resetSelectedList();
                Util.slideDown(order_sheet);
                break;
            case Constants.THOAT:

                break;
        }
    }

    @Override
    public void onItemPopupClickListener(int positionOrder, ItemsPopup itemsPopup) {
        presenter.updateMonDiKem(positionOrder, itemsPopup);
    }
}
