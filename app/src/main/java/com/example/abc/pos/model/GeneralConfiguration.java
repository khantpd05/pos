package com.example.abc.pos.model;

import java.util.List;

public class GeneralConfiguration {
    private boolean tuDongInBep;
    private List<Configuration> gutschein;
    private List<Configuration> restaurantCheck;
    private boolean canhBaoThanhToan;
    private boolean huyThanhToan;
    private boolean tuDongKetNoi;

    public GeneralConfiguration() {
    }

    public boolean isTuDongInBep() {
        return tuDongInBep;
    }

    public void setTuDongInBep(boolean tuDongInBep) {
        this.tuDongInBep = tuDongInBep;
    }

    public List<Configuration> getGutschein() {
        return gutschein;
    }

    public void setGutschein(List<Configuration> gutschein) {
        this.gutschein = gutschein;
    }

    public List<Configuration> getRestaurantCheck() {
        return restaurantCheck;
    }

    public void setRestaurantCheck(List<Configuration> restaurantCheck) {
        this.restaurantCheck = restaurantCheck;
    }

    public boolean isCanhBaoThanhToan() {
        return canhBaoThanhToan;
    }

    public void setCanhBaoThanhToan(boolean canhBaoThanhToan) {
        this.canhBaoThanhToan = canhBaoThanhToan;
    }

    public boolean isHuyThanhToan() {
        return huyThanhToan;
    }

    public void setHuyThanhToan(boolean huyThanhToan) {
        this.huyThanhToan = huyThanhToan;
    }

    public boolean isTuDongKetNoi() {
        return tuDongKetNoi;
    }

    public void setTuDongKetNoi(boolean tuDongKetNoi) {
        this.tuDongKetNoi = tuDongKetNoi;
    }
}
