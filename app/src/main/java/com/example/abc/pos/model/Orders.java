package com.example.abc.pos.model;
// Generated Oct 2, 2018 10:27:14 AM by Hibernate Tools 4.3.1.Final

import com.example.abc.pos.base.WapriManager;

/**
 * Orders generated by hbm2java
 */
public class Orders implements java.io.Serializable {

	private String id;
	private String ngaymua;
	private String thoigian;
	private String linktodetails;
	private String nhanvien;
	private String banso;
	private Double giatien;
	private double include_total_tienmat_hoac_the;
	private double include_total_coupon_value;
	private double include_total_restaurantcheck_value;
	private WapriManager.cachtra cachtra;
	private int giam;
	private Double tiengiam = 0.0;
	private Double tienthue19giam;
	private Double tienthue7giam;
	private String sokhachhang;
	private String chitietkhachhang;
	private String dathanhtoantienvoinhanvienchua;
	private String datongketcuoingaychua;
	private String delete;
	private String ladonhangtralai;
	private Integer tralaicuadonhang;
	private String loaiIn;
	private String taichohaymangve;
	private String nguoigiaohang;
	private String chitietkhachhangkhongcoid;
	private double tienkhachdua;
	private double tienthoilai;
	private String chinhanh;
	private String orderfrom;
	private String dainxacnhanlaixe;
	private String ladonhangcomathangbixoa;
	private String ladonhangcodoicachthanhtoantien;
	private String lichsudoicachthanhtoantien;
	private String daprintchua;
	private String from_tablet;
	private String inlaihoadonfromtablet;

	public Orders() {
	}

	public Orders(String id, String ngaymua, String linktodetails, WapriManager.cachtra cachtra, String dathanhtoantienvoinhanvienchua,
			String datongketcuoingaychua, String delete, String ladonhangtralai, String dainxacnhanlaixe,
			String ladonhangcomathangbixoa, String ladonhangcodoicachthanhtoantien, String daprintchua,
			String from_tablet) {
		this.id = id;
		this.ngaymua = ngaymua;
		this.linktodetails = linktodetails;
		this.cachtra = cachtra;
		this.dathanhtoantienvoinhanvienchua = dathanhtoantienvoinhanvienchua;
		this.datongketcuoingaychua = datongketcuoingaychua;
		this.delete = delete;
		this.ladonhangtralai = ladonhangtralai;
		this.dainxacnhanlaixe = dainxacnhanlaixe;
		this.ladonhangcomathangbixoa = ladonhangcomathangbixoa;
		this.ladonhangcodoicachthanhtoantien = ladonhangcodoicachthanhtoantien;
		this.daprintchua = daprintchua;
		this.from_tablet = from_tablet;
	}

	public String getInlaihoadonfromtablet() {
		return inlaihoadonfromtablet;
	}

	public void setInlaihoadonfromtablet(String inlaihoadonfromtablet) {
		this.inlaihoadonfromtablet = inlaihoadonfromtablet;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNgaymua() {
		return ngaymua;
	}

	public void setNgaymua(String ngaymua) {
		this.ngaymua = ngaymua;
	}

	public String getThoigian() {
		return thoigian;
	}


	public double getInclude_total_tienmat_hoac_the() {
		return include_total_tienmat_hoac_the;
	}

	public void setInclude_total_tienmat_hoac_the(double include_total_tienmat_hoac_the) {
		this.include_total_tienmat_hoac_the = include_total_tienmat_hoac_the;
	}

	public double getInclude_total_coupon_value() {
		return include_total_coupon_value;
	}

	public void setInclude_total_coupon_value(double include_total_coupon_value) {
		this.include_total_coupon_value = include_total_coupon_value;
	}

	public double getInclude_total_restaurantcheck_value() {
		return include_total_restaurantcheck_value;
	}

	public void setInclude_total_restaurantcheck_value(double include_total_restaurantcheck_value) {
		this.include_total_restaurantcheck_value = include_total_restaurantcheck_value;
	}

	public void setThoigian(String thoigian) {
		this.thoigian = thoigian;
	}

	public String getLinktodetails() {
		return linktodetails;
	}

	public void setLinktodetails(String linktodetails) {
		this.linktodetails = linktodetails;
	}

	public String getNhanvien() {
		return nhanvien;
	}

	public void setNhanvien(String nhanvien) {
		this.nhanvien = nhanvien;
	}

	public String getBanso() {
		return banso;
	}

	public void setBanso(String banso) {
		this.banso = banso;
	}

	public Double getGiatien() {
		return giatien;
	}

	public void setGiatien(Double giatien) {
		this.giatien = giatien;
	}

	public WapriManager.cachtra getCachtra() {
		return cachtra;
	}

	public void setCachtra(WapriManager.cachtra cachtra) {
		this.cachtra = cachtra;
	}

	public int getGiam() {
		return giam;
	}

	public void setGiam(int giam) {
		this.giam = giam;
	}

	public Double getTiengiam() {
		return tiengiam;
	}

	public void setTiengiam(Double tiengiam) {
		this.tiengiam = tiengiam;
	}

	public Double getTienthue19giam() {
		return tienthue19giam;
	}

	public void setTienthue19giam(Double tienthue19giam) {
		this.tienthue19giam = tienthue19giam;
	}

	public Double getTienthue7giam() {
		return tienthue7giam;
	}

	public void setTienthue7giam(Double tienthue7giam) {
		this.tienthue7giam = tienthue7giam;
	}

	public String getSokhachhang() {
		return sokhachhang;
	}

	public void setSokhachhang(String sokhachhang) {
		this.sokhachhang = sokhachhang;
	}

	public String getChitietkhachhang() {
		return chitietkhachhang;
	}

	public void setChitietkhachhang(String chitietkhachhang) {
		this.chitietkhachhang = chitietkhachhang;
	}

	public String getDathanhtoantienvoinhanvienchua() {
		return dathanhtoantienvoinhanvienchua;
	}

	public void setDathanhtoantienvoinhanvienchua(String dathanhtoantienvoinhanvienchua) {
		this.dathanhtoantienvoinhanvienchua = dathanhtoantienvoinhanvienchua;
	}

	public String getDatongketcuoingaychua() {
		return datongketcuoingaychua;
	}

	public void setDatongketcuoingaychua(String datongketcuoingaychua) {
		this.datongketcuoingaychua = datongketcuoingaychua;
	}

	public String getDelete() {
		return delete;
	}

	public void setDelete(String delete) {
		this.delete = delete;
	}

	public String getLadonhangtralai() {
		return ladonhangtralai;
	}

	public void setLadonhangtralai(String ladonhangtralai) {
		this.ladonhangtralai = ladonhangtralai;
	}

	public Integer getTralaicuadonhang() {
		return tralaicuadonhang;
	}

	public void setTralaicuadonhang(Integer tralaicuadonhang) {
		this.tralaicuadonhang = tralaicuadonhang;
	}

	public String getLoaiIn() {
		return loaiIn;
	}

	public void setLoaiIn(String loaiIn) {
		this.loaiIn = loaiIn;
	}

	public String getTaichohaymangve() {
		return taichohaymangve;
	}

	public void setTaichohaymangve(String taichohaymangve) {
		this.taichohaymangve = taichohaymangve;
	}

	public String getNguoigiaohang() {
		return nguoigiaohang;
	}

	public void setNguoigiaohang(String nguoigiaohang) {
		this.nguoigiaohang = nguoigiaohang;
	}

	public String getChitietkhachhangkhongcoid() {
		return chitietkhachhangkhongcoid;
	}

	public void setChitietkhachhangkhongcoid(String chitietkhachhangkhongcoid) {
		this.chitietkhachhangkhongcoid = chitietkhachhangkhongcoid;
	}

	public double getTienkhachdua() {
		return tienkhachdua;
	}

	public void setTienkhachdua(double tienkhachdua) {
		this.tienkhachdua = tienkhachdua;
	}

	public double getTienthoilai() {
		return tienthoilai;
	}

	public void setTienthoilai(double tienthoilai) {
		this.tienthoilai = tienthoilai;
	}

	public String getChinhanh() {
		return chinhanh;
	}

	public void setChinhanh(String chinhanh) {
		this.chinhanh = chinhanh;
	}

	public String getOrderfrom() {
		return orderfrom;
	}

	public void setOrderfrom(String orderfrom) {
		this.orderfrom = orderfrom;
	}

	public String getDainxacnhanlaixe() {
		return dainxacnhanlaixe;
	}

	public void setDainxacnhanlaixe(String dainxacnhanlaixe) {
		this.dainxacnhanlaixe = dainxacnhanlaixe;
	}

	public String getLadonhangcomathangbixoa() {
		return ladonhangcomathangbixoa;
	}

	public void setLadonhangcomathangbixoa(String ladonhangcomathangbixoa) {
		this.ladonhangcomathangbixoa = ladonhangcomathangbixoa;
	}

	public String getLadonhangcodoicachthanhtoantien() {
		return ladonhangcodoicachthanhtoantien;
	}

	public void setLadonhangcodoicachthanhtoantien(String ladonhangcodoicachthanhtoantien) {
		this.ladonhangcodoicachthanhtoantien = ladonhangcodoicachthanhtoantien;
	}

	public String getLichsudoicachthanhtoantien() {
		return lichsudoicachthanhtoantien;
	}

	public void setLichsudoicachthanhtoantien(String lichsudoicachthanhtoantien) {
		this.lichsudoicachthanhtoantien = lichsudoicachthanhtoantien;
	}

	public String getDaprintchua() {
		return daprintchua;
	}

	public void setDaprintchua(String daprintchua) {
		this.daprintchua = daprintchua;
	}

	public String getfrom_tablet() {
		return from_tablet;
	}

	public void setfrom_tablet(String from_tablet) {
		this.from_tablet = from_tablet;
	}
}
