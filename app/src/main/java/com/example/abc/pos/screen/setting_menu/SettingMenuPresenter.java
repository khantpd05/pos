package com.example.abc.pos.screen.setting_menu;

import android.os.AsyncTask;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Menu;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class SettingMenuPresenter implements SettingMenuConstract.Presenter{

    SettingMenuConstract.View mView;

    public SettingMenuPresenter(SettingMenuConstract.View mView) {
        this.mView = mView;
        loadData();
    }

    @Override
    public void loadData() {
        new SelectMenuTask().execute();
    }

    @Override
    public void deleteRow(int id) {
        new DeleteFromMenuTask(id).execute();
    }


    private class SelectMenuTask extends AsyncTask<Void,Void,List<Menu>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<Menu> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.menu");
            ArrayList<Menu> menus =  new Gson().fromJson(js.toString(), new TypeToken<List<Menu>>(){}.getType());
            return menus;
        }

        @Override
        protected void onPostExecute(List<Menu> menus) {
            super.onPostExecute(menus);
            mView.dismissLoading();
            mView.showListMenu(menus);

        }
    }


    private class DeleteFromMenuTask extends AsyncTask<Void,Void,Integer> {
        int id;
        public DeleteFromMenuTask(int id) {
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result =  MySqlHandleUtils.deleteRecordFromMenu(id);

            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            mView.dismissLoading();
            if(result == 1){
                mView.showAlert(App.Companion.self().getString(R.string.delete_successfully));
            }else{
                mView.showAlert(App.Companion.self().getString(R.string.delete_failed));
            }
        }
    }
}
