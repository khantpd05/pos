package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.ExtraItem;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class ExpandListDivideOrderAdapter extends BaseExpandableListAdapter {

    private Context context;

    private List<OrdersTemp> ordersTempList;

    private OnItemClickGroup listener;

    private int selectedGroup = -1;


    private List<ItemSelected> selectedList;

    public interface OnItemClickGroup {
        void onDivideItemClickGroupListener(OrdersTemp item, int position);
    }

    private ExpandableListView orderTempExpandList;

    public ExpandListDivideOrderAdapter(Context context, List<OrdersTemp> ordersTempList, OnItemClickGroup listener) {
        this.context = context;
        this.ordersTempList = ordersTempList;
        this.listener = listener;


    }

    public void setNewDatas(List<OrdersTemp> ordersTempList) {
        this.ordersTempList = ordersTempList;
        selectedList = new ArrayList<>();
        for (int i = 0; i < ordersTempList.size(); i++) {
            ItemSelected ob = new ItemSelected();
            selectedList.add(ob);
        }
        notifyDataSetChanged();
    }

    public void addNewItem(OrdersTemp ordersTemp) {
        this.ordersTempList.add(ordersTemp);
        notifyDataSetChanged();
    }

    @Override
    public ExtraItem getChild(int listPosition, int expandedListPosition) {
        return this.ordersTempList.get(listPosition).getExtraItemList()
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {


        String inlaimonfromtablet = getGroup(listPosition).getInlaimonfromtablet();
        String daprintchua = getGroup(listPosition).getDaprintchua();

        ViewHolderChild holder;
        if (convertView == null) {
            holder = new ViewHolderChild();

            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item_extra_item, null);

            holder.tv_item_extra_name = convertView.findViewById(R.id.tv_item_extra_name);
            holder.tv_price = convertView.findViewById(R.id.tv_price);
            holder.tv_thanhtien = convertView.findViewById(R.id.tv_thanhtien);
            holder.ll_wrap_child = convertView.findViewById(R.id.ll_wrap_child);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderChild) convertView.getTag();
            holder.tv_item_extra_name.setText("");
            holder.tv_price.setText("");
            holder.tv_thanhtien.setText("");
        }


        final String name = getChild(listPosition, expandedListPosition).getName();
        final double value = getChild(listPosition, expandedListPosition).getValue();
        final WapriManager.FeatureOrder flag = getChild(listPosition, expandedListPosition).getFlag();

        holder.tv_item_extra_name.setText(name);

        /* set color if isPrinted */
        holder.ll_wrap_child.setBackgroundColor(Color.parseColor("#282F3F"));
        if (inlaimonfromtablet.equals("Y") || daprintchua.equals("Y")) {
            holder.tv_item_extra_name.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
            holder.tv_price.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
        } else {
            holder.tv_item_extra_name.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
            holder.tv_price.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
        }

        if (flag == WapriManager.FeatureOrder.DEM_VE || flag == WapriManager.FeatureOrder.GHI_CHU_BEP) {
            holder.tv_price.setVisibility(View.INVISIBLE);

        } else {
            holder.tv_price.setVisibility(View.VISIBLE);
        }


        if (flag == WapriManager.FeatureOrder.GIAM) {
            holder.tv_price.setText(String.format("%,.2f", value) + "");
            holder.tv_thanhtien.setText(String.format("%,.2f", value) + "");
        }

        if (flag == WapriManager.FeatureOrder.GIA_VI || flag == WapriManager.FeatureOrder.GIA_TU_DO || flag == WapriManager.FeatureOrder.PHAN_AN) {
            holder.tv_price.setText(String.format("%,.2f", value) + "");
            holder.tv_thanhtien.setText(String.format("%,.2f", value) + "");
        }


        return convertView;
    }

    private class ViewHolderChild {
        public TextView tv_item_extra_name, tv_price, tv_thanhtien;
        public LinearLayout ll_wrap_child;

    }


    @Override
    public int getChildrenCount(int listPosition) {
        return ordersTempList.get(listPosition).getExtraItemList() != null && ordersTempList.get(listPosition).getExtraItemList()
                .size() > 0 ? ordersTempList.get(listPosition).getExtraItemList()
                .size() : 0;
    }

    @Override
    public OrdersTemp getGroup(int listPosition) {
        return this.ordersTempList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.ordersTempList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }


    private class ViewHolderGroup implements View.OnClickListener {
        public TextView tv_number, tv_item_id, tv_item_name, tv_orig_price, tv_price_total, tv_dotdat;
        public LinearLayout ll_wrap_group, ll_group_dotdat, ll_wrap_text;
        private int positionSelected;

        public void setPosition(int positionSelected) {
            this.positionSelected = positionSelected;
        }

        public ViewHolderGroup(View view, int positionSelected) {
            this.positionSelected = positionSelected;
            tv_item_name = view.findViewById(R.id.tv_item_name);
            tv_number = view.findViewById(R.id.tv_number);
            tv_orig_price = view.findViewById(R.id.tv_orig_price);
            tv_price_total = view.findViewById(R.id.tv_price_total);
            tv_dotdat = view.findViewById(R.id.tv_dotdat);
            ll_wrap_group = view.findViewById(R.id.ll_wrap_group);
            ll_group_dotdat = view.findViewById(R.id.ll_group_dotdat);
            ll_wrap_text = view.findViewById(R.id.ll_wrap_text);
            ll_group_dotdat.setOnClickListener(this);
            ll_wrap_group.setTag(positionSelected);
            ll_wrap_group.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ll_wrap_group:
                    listener.onDivideItemClickGroupListener(ordersTempList.get(positionSelected),positionSelected);
                    break;
                case R.id.ll_group_dotdat:
                    listener.onDivideItemClickGroupListener(ordersTempList.get(positionSelected),positionSelected);
                    break;
            }
        }

    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {


        ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(listPosition);
        ViewHolderGroup holder;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_header_item, null);
            holder = new ViewHolderGroup(convertView, listPosition);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderGroup) convertView.getTag();
            holder.setPosition(listPosition);
            holder.tv_item_name.setText("");
            holder.tv_number.setText("");
            holder.tv_orig_price.setText("");
            holder.tv_price_total.setText("");
        }
        /** set font family **/
        holder.tv_item_name.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_BOLD));
        holder.tv_number.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_BOLD));
        holder.tv_orig_price.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_BOLD));
        holder.tv_price_total.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_BOLD));

        String ten_hang = getGroup(listPosition).getTenhang();
        int so_luong = getGroup(listPosition).getSoluong();
        String item_id = getGroup(listPosition).getitems_id();
        double orig_price = getGroup(listPosition).getorig_price_no_extra();
        double total_price = getGroup(listPosition).getThanhtien();
        String inlaimonfromtablet = getGroup(listPosition).getInlaimonfromtablet();
        String daprintchua = getGroup(listPosition).getDaprintchua();
        String danhdaubixoa = getGroup(listPosition).getDanhdaubixoa();

        // set color
        if (item_id.equals("1001")) {
            holder.ll_wrap_group.setVisibility(View.GONE);
            holder.ll_group_dotdat.setVisibility(View.VISIBLE);
            holder.tv_dotdat.setText(ten_hang);
        } else {
            if (listPosition % 2 == 0) {
                holder.ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));
            } else {
                holder.ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_ODD));
            }
            holder.ll_wrap_group.setVisibility(View.VISIBLE);
            holder.ll_group_dotdat.setVisibility(View.GONE);
        }

        if (inlaimonfromtablet.equals("Y") || daprintchua.equals("Y")) {
            holder.tv_number.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
            holder.tv_item_name.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
            holder.tv_number.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
            holder.tv_orig_price.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
            holder.tv_price_total.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
        } else {
            holder.tv_number.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
            holder.tv_item_name.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
            holder.tv_number.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
            holder.tv_orig_price.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
            holder.tv_price_total.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
        }


        /* set data vao thoi */

        holder.tv_item_name.setText(ten_hang);


        holder.tv_number.setText(so_luong + " x " + item_id);


        holder.tv_orig_price.setText(String.format("%,.2f", orig_price));


        holder.tv_price_total.setText(String.format("%,.2f", total_price));

        if(danhdaubixoa!=null){
            if (danhdaubixoa.equals("Y")) {
                holder.ll_wrap_text.setBackground(context.getResources().getDrawable(R.drawable.strike_through));
                holder.tv_price_total.setText("0.0");
            } else {
                holder.ll_wrap_text.setBackground(null);
                holder.tv_price_total.setText(String.format("%,.2f", total_price));
                if (so_luong < 1 ) {
                    holder.ll_wrap_text.setBackground(context.getResources().getDrawable(R.drawable.strike_through));
                    holder.tv_price_total.setText(String.format("%,.2f", 0.00));
                } else {
                    holder.ll_wrap_text.setBackground(null);
                    holder.tv_price_total.setText(String.format("%,.2f", total_price));
                }
            }
        }else{
            if (so_luong < 1 ) {
                holder.ll_wrap_text.setBackground(context.getResources().getDrawable(R.drawable.strike_through));
                holder.tv_price_total.setText(String.format("%,.2f", 0.00));
            } else {
                holder.ll_wrap_text.setBackground(null);
                holder.tv_price_total.setText(String.format("%,.2f", total_price));
            }
        }





        return convertView;
    }

    private class ItemSelected {
        public boolean isSelected = false;
    }



    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}
