package com.example.abc.pos.screen.money_box;

import android.databinding.ObservableField;
import android.os.AsyncTask;
import android.util.Log;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.base.WapriManager.MoneyEuroType;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.model.UsersTienbandau;
import com.example.abc.pos.model.UsersTienbandauTemplate;
import com.example.abc.pos.utils.SharedPrefs;
import com.example.abc.pos.utils.Util;
import com.example.abc.pos.viewmodel.MoneyEuro;

import java.text.DecimalFormat;
import java.util.List;

public class MoneyBoxPresenter implements MoneyBoxConstract.Presenter {
    private MoneyBoxConstract.View mView;
    public MoneyEuro money = new MoneyEuro();
    private MoneyEuroType moneyEuroType;
    private UsersTienbandauTemplate usersTienbandauTemplate = new UsersTienbandauTemplate();
    private UsersTienbandau usersTienbandau = new UsersTienbandau();
    private String usernameTienBanDau;
    public ObservableField<String> enterOrSave = new ObservableField<>("Enter");

    public MoneyBoxPresenter(MoneyBoxConstract.View mView) {
        this.mView = mView;
        Integer box_pos = SharedPrefs.getInstance().get(SharedPrefs.MONEY_BOX, Integer.class);
        new SelectUserNameTienBanDauToDbTask().execute();
        if (box_pos != null && box_pos != 0) {
            new SelectUserTienBanDauToDbTask(box_pos).execute();
        } else {
            new SelectUserTienBanDauToDbTask(1).execute();
        }

    }

    @Override
    public void showKeyboard(String value, MoneyEuroType type) {
        moneyEuroType = type;
        mView.showKeyboard(Util.getStringFromResouce(R.string.amount), value);
    }

    @Override
    public void setTongTien(String data) {
        if (data.equals("")) data = "0";
        switch (moneyEuroType) {
            case CENT_1:
                money._1Cent.set(data);
                setMoneyTotal();
                break;
            case CENT_2:
                money._2Cent.set(data);
                setMoneyTotal();
                break;
            case CENT_5:
                money._5Cent.set(data);
                setMoneyTotal();
                break;
            case CENT_10:
                money._10Cent.set(data);
                setMoneyTotal();
                break;
            case CENT_20:
                money._20Cent.set(data);
                setMoneyTotal();
                break;
            case CENT_50:
                money._50Cent.set(data);
                setMoneyTotal();
                break;
            case EURO_1:
                money._1Euro.set(data);
                setMoneyTotal();
                break;
            case EURO_2:
                money._2Euro.set(data);
                setMoneyTotal();
                break;
            case EURO_5:
                money._5Euro.set(data);
                setMoneyTotal();
                break;
            case EURO_10:
                money._10Euro.set(data);
                setMoneyTotal();
                break;
            case EURO_20:
                money._20Euro.set(data);
                setMoneyTotal();
                break;
            case EURO_50:
                money._50Euro.set(data);
                setMoneyTotal();
                break;
            case EURO_100:
                money._100Euro.set(data);
                setMoneyTotal();
                break;
            case EURO_200:
                money._200Euro.set(data);
                setMoneyTotal();
                break;
        }
    }

    @Override
    public void goToDefaultMoneyBox() {
        mView.goToDefaultMoneyBox();
    }

    @Override
    public void setUsersTienbandauTemplate(UsersTienbandauTemplate ob) {
        usersTienbandauTemplate = ob;
        money._1Euro.set(getValueFromBox(ob.getEur1()));
        money._2Euro.set(getValueFromBox(ob.getEur2()));
        money._5Euro.set(getValueFromBox(ob.getEur5()));
        money._10Euro.set(getValueFromBox(ob.getEur10()));
        money._20Euro.set(getValueFromBox(ob.getEur20()));
        money._50Euro.set(getValueFromBox(ob.getEur50()));
        money._100Euro.set(getValueFromBox(ob.getEur100()));
        money._200Euro.set(getValueFromBox(ob.getEur200()));

        money._1Cent.set(getValueFromBox(ob.getCent1()));
        money._2Cent.set(getValueFromBox(ob.getCent2()));
        money._5Cent.set(getValueFromBox(ob.getCent5()));
        money._10Cent.set(getValueFromBox(ob.getCent10()));
        money._20Cent.set(getValueFromBox(ob.getCent20()));
        money._50Cent.set(getValueFromBox(ob.getCent50()));
        money.total.set(String.format("%,.2f", (ob.getTotalmoney() != null ? ob.getTotalmoney() : 0)));
    }

    private String getValueFromBox(Integer money) {
        return money != null ? money + "" : "0";
    }

    @Override
    public void goBack() {
        mView.goBack();
    }

    private void setMoneyTotal() {
        money.total.set(String.format("%,.2f", (getTotalAllType())));

    }

    private String currencyFormat(String amount) {
        DecimalFormat formatter = new DecimalFormat("###,###,##0.00");
        return formatter.format(Double.parseDouble(amount));
    }

    private void doSaveAndUpdateBox(UsersTienbandauTemplate data) {
        if (data.getUsername() != null) {
            new UpdateUserTienBanDauTempToDbTask(setNewValueUsersTienbandauTemplate(data)).execute();
        } else {
            new InsertUserTienBanDauTempToDbTask(setNewValueUsersTienbandauTemplate(data)).execute();
        }
    }

    private void doSetTienBanDau() {
        UsersTienbandau ob = setNewValueUsersTienbandau(usersTienbandau);
        if (ob.getTotalmoney() > 0) {
            if (usernameTienBanDau.equals("")) {
                new InsertUserTienBanDauToDbTask(ob).execute();
            } else {
                new UpdateUserTienBanDauToDbTask(ob).execute();
            }
        } else {
            mView.showToast(Util.getStringFromResouce(R.string.the_money_must_be_bigger_than_0));
        }

    }

    public void doClickSaveOrUpdateBox() {
        if (SessionApp.isSetting) {
            doSaveAndUpdateBox(usersTienbandauTemplate);
        } else {
            doSetTienBanDau();
        }
    }

    private UsersTienbandauTemplate setNewValueUsersTienbandauTemplate(UsersTienbandauTemplate data) {

        data.setUsername(WapriManager.getUser().getUsername());

        data.setCent1(toInt(money._1Cent.get()));
        data.setCent2(toInt(money._2Cent.get()));
        data.setCent5(toInt(money._5Cent.get()));
        data.setCent10(toInt(money._10Cent.get()));
        data.setCent20(toInt(money._20Cent.get()));
        data.setCent50(toInt(money._50Cent.get()));

        data.setEur1(toInt(money._1Euro.get()));
        data.setEur2(toInt(money._2Euro.get()));
        data.setEur5(toInt(money._5Euro.get()));
        data.setEur10(toInt(money._10Euro.get()));
        data.setEur20(toInt(money._20Euro.get()));
        data.setEur50(toInt(money._50Euro.get()));
        data.setEur100(toInt(money._100Euro.get()));
        data.setEur200(toInt(money._200Euro.get()));
        data.setTotalnumbers(valueOfTotalNumbers());
        data.setTotalmoney(getTotalAllType());
        data.setUsername(WapriManager.getUser().getUsername());
        data.setEur500(0);
        return data;
    }

    private UsersTienbandau setNewValueUsersTienbandau(UsersTienbandau data) {
        data.setUsername(WapriManager.getUser().getUsername());

        data.setCent1(toInt(money._1Cent.get()));
        data.setCent2(toInt(money._2Cent.get()));
        data.setCent5(toInt(money._5Cent.get()));
        data.setCent10(toInt(money._10Cent.get()));
        data.setCent20(toInt(money._20Cent.get()));
        data.setCent50(toInt(money._50Cent.get()));

        data.setEur1(toInt(money._1Euro.get()));
        data.setEur2(toInt(money._2Euro.get()));
        data.setEur5(toInt(money._5Euro.get()));
        data.setEur10(toInt(money._10Euro.get()));
        data.setEur20(toInt(money._20Euro.get()));
        data.setEur50(toInt(money._50Euro.get()));
        data.setEur100(toInt(money._100Euro.get()));
        data.setEur200(toInt(money._200Euro.get()));
        data.setEur500(0);
        data.setType("startdate");
        data.setTotalnumbers(valueOfTotalNumbers());

        data.setTotalmoney(getTotalAllType());
        data.setDateadd(Util.getDate("dt"));
        return data;
    }


    private int toInt(String data) {
        return Integer.valueOf(data);
    }

    private double getTotalAllType() {
        return valueOfType(money._1Cent.get(), MoneyEuroType.CENT_1) + valueOfType(money._2Cent.get(), MoneyEuroType.CENT_2) +
                valueOfType(money._5Cent.get(), MoneyEuroType.CENT_5) + valueOfType(money._10Cent.get(), MoneyEuroType.CENT_10) +
                valueOfType(money._20Cent.get(), MoneyEuroType.CENT_20) + valueOfType(money._50Cent.get(), MoneyEuroType.CENT_50) +
                valueOfType(money._1Euro.get(), MoneyEuroType.EURO_1) + valueOfType(money._2Euro.get(), MoneyEuroType.EURO_2) +
                valueOfType(money._5Euro.get(), MoneyEuroType.EURO_5) + valueOfType(money._10Euro.get(), MoneyEuroType.EURO_10) +
                valueOfType(money._20Euro.get(), MoneyEuroType.EURO_20) + valueOfType(money._50Euro.get(), MoneyEuroType.EURO_50) +
                valueOfType(money._100Euro.get(), MoneyEuroType.EURO_100) + valueOfType(money._200Euro.get(), MoneyEuroType.EURO_200);
    }

    private int valueOfTotalNumbers() {
        return toInt(money._1Cent.get()) + toInt(money._2Cent.get()) + toInt(money._5Cent.get()) + toInt(money._10Cent.get()) +
                toInt(money._20Cent.get()) + toInt(money._50Cent.get()) + toInt(money._1Euro.get()) + toInt(money._2Euro.get()) +
                toInt(money._5Euro.get()) + toInt(money._10Euro.get()) + toInt(money._20Euro.get()) + toInt(money._50Euro.get()) +
                toInt(money._100Euro.get()) + toInt(money._200Euro.get());
    }

    private double valueOfType(String money, MoneyEuroType type) {
        String _money;
        if (money != null && !money.isEmpty()) {
            _money = money;
        } else {
            _money = "0.0";
        }
        return ((Double.valueOf(_money) * type.value) / 100);
    }

    private class InsertUserTienBanDauToDbTask extends AsyncTask<Void, Void, Integer> {
        UsersTienbandau ob;

        public InsertUserTienBanDauToDbTask(UsersTienbandau ob) {
            this.ob = ob;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            result = MySqlHandleUtils.insert_into_users_tienbandau(ob);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            new UpdateTienBanDau(getTotalAllType()).execute();
            super.onPostExecute(result);
        }
    }

    private class UpdateUserTienBanDauToDbTask extends AsyncTask<Void, Void, Integer> {

        UsersTienbandau ob;

        public UpdateUserTienBanDauToDbTask(UsersTienbandau ob) {
            this.ob = ob;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            result = MySqlHandleUtils.update_into_users_tienbandau(ob);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            new UpdateTienBanDau(getTotalAllType()).execute();
            super.onPostExecute(result);
        }
    }

    private class InsertUserTienBanDauTempToDbTask extends AsyncTask<Void, Void, Integer> {

        UsersTienbandauTemplate ob;

        public InsertUserTienBanDauTempToDbTask(UsersTienbandauTemplate ob) {
            this.ob = ob;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            result = MySqlHandleUtils.insert_into_users_tienbandau_template(ob);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            mView.goToDefaultMoneyBox();
        }
    }

    private class UpdateUserTienBanDauTempToDbTask extends AsyncTask<Void, Void, Integer> {

        UsersTienbandauTemplate ob;

        public UpdateUserTienBanDauTempToDbTask(UsersTienbandauTemplate ob) {
            this.ob = ob;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;

            result = MySqlHandleUtils.update_record_users_tienbandau_template(ob);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            mView.goToDefaultMoneyBox();
        }
    }


    private class UpdateTienBanDau extends AsyncTask<Void, Void, Integer> {

        double tienBanDau;

        public UpdateTienBanDau(double tienBanDau) {
            this.tienBanDau = tienBanDau;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;
            result = MySqlHandleUtils.updateTienBanDauUser(tienBanDau, WapriManager.getUser().getId());
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            mView.goToTable();
            super.onPostExecute(result);
        }
    }

    private class SelectUserTienBanDauToDbTask extends AsyncTask<Void, Void, List<UsersTienbandauTemplate>> {
        int offset;

        public SelectUserTienBanDauToDbTask(int offset) {
            this.offset = offset;
        }

        @Override
        protected List<UsersTienbandauTemplate> doInBackground(Void... voids) {
            List<UsersTienbandauTemplate> datas;

            datas = MySqlHandleUtils.select_users_tienbandau_template_by_offset(offset);
            return datas;
        }

        @Override
        protected void onPostExecute(List<UsersTienbandauTemplate> result) {
            super.onPostExecute(result);
            if (result.size() > 0) {
                setUsersTienbandauTemplate(result.get(0));
            }
        }
    }


    private class SelectUserNameTienBanDauToDbTask extends AsyncTask<Void, Void, List<UsersTienbandauTemplate>> {

        public SelectUserNameTienBanDauToDbTask() {

        }

        @Override
        protected List<UsersTienbandauTemplate> doInBackground(Void... voids) {
            List<UsersTienbandauTemplate> datas;

            datas = MySqlHandleUtils.select_users_name_tienbandau_by_name(WapriManager.getUser().getUsername());
            return datas;
        }

        @Override
        protected void onPostExecute(List<UsersTienbandauTemplate> result) {
            super.onPostExecute(result);
            if (result.size() > 0) {
                usernameTienBanDau = result.get(0).getUsername();
            } else {
                usernameTienBanDau = "";
            }
        }
    }
}
