package com.example.abc.pos.screen.spice;

import com.example.abc.pos.model.ItemsMontuychon;
import com.example.abc.pos.model.Spice;

import java.util.List;

public interface SpiceContract {
    interface View{
        void showAlert(String data);
        void showListSpice(List<Spice> datas);
        void showListSpiceItem(List<ItemsMontuychon> datas);
        void getSpiceList(List<Spice> datas);
        void showKeyboard(String title);
        void setEditTextEmpty();
        void showLoading();
        void dismissLoading();
        void doClickItem(int pos);
        void resetAdapter();
    }

    interface Presenter{
        void addNewItem(ItemsMontuychon mSpice,String data);
        void loadData(List<Spice> datas);
        void getAllPageSpiceItem(String id);
        void getSpiceList();
        void showKeyboard(String title);
        void updatePriceItem(int positionSelected,String data);
        void deletePriceItem(int positionSelected);
    }
}
