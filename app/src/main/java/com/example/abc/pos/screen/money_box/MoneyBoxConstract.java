package com.example.abc.pos.screen.money_box;

import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.UsersTienbandauTemplate;

public interface MoneyBoxConstract {
    interface View{
        void showToast(String data);
        void showKeyboard(String title, String hint);
        void goToDefaultMoneyBox();
        void goBack();
        void goToTable();
    }

    interface Presenter{
        void showKeyboard(String value, WapriManager.MoneyEuroType type);
        void setTongTien(String data);
        void goToDefaultMoneyBox();
        void setUsersTienbandauTemplate(UsersTienbandauTemplate ob);
        void goBack();
    }
}
