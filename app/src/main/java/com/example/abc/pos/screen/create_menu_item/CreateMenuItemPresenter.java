package com.example.abc.pos.screen.create_menu_item;

import android.content.res.Resources;
import android.os.AsyncTask;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.model.Table;
import com.example.abc.pos.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreateMenuItemPresenter implements CreateMenuItemConstract.Presenter {

    private CreateMenuItemConstract.View mView;

    public CreateMenuItemPresenter(CreateMenuItemConstract.View mView) {
        this.mView = mView;

    }

    @Override
    public void loadData() {
        ArrayList<String> arr = new ArrayList<>();
        for (String value : Util.getEnumFoodType().values()) {
            arr.add(value);
        }
        mView.showSpinnerData(arr);
    }

    @Override
    public void addNewMenu(String name, int thutu, String color, String loai) {
        if(name.trim().isEmpty()){
            mView.showAlert(Util.getStringFromResouce(R.string.please_enter_full_information_of_item));
        }else{
            new insertToDbTask(name, thutu, color, loai).execute();
        }

    }


    @Override
    public void updateMenu(Menu menu) {
        new updateToDbTask(menu).execute();
    }

    private class insertToDbTask extends AsyncTask<Void, Void, Integer> {
        String color;
        String name;
        int thutu;
        String loai_mon_an;

        public insertToDbTask(String name, int thutu, String color, String loai_mon_an) {
            this.name = name;
            this.color = color;
            this.thutu = thutu;
            this.loai_mon_an = loai_mon_an;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result;
                Menu menu = new Menu();

                menu.setName(name);
                menu.setThutusapxepkhihienthi(thutu);
                menu.setColor(color);
                menu.setLoaiMonAn(loai_mon_an);
                result = MySqlHandleUtils.insertRecordIntoMenu(menu);
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                mView.showAlert(App.Companion.self().getString(R.string.create_successful));
            } else {
                mView.showAlert(App.Companion.self().getString(R.string.add_failed));
            }
        }
    }

    private class updateToDbTask extends AsyncTask<Void, Void, Integer> {

        Menu menu;

        public updateToDbTask(Menu menu) {
            this.menu = menu;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result = MySqlHandleUtils.updateRecordIntoMenu(menu);

            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                mView.showAlert(App.Companion.self().getString(R.string.update_successful));
                mView.goBack();
            } else {
                mView.showAlert(App.Companion.self().getString(R.string.update_failed));
            }
        }
    }

}
