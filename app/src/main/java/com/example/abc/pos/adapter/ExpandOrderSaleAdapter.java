package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.ExtraItem;
import com.example.abc.pos.model.OrdersTemp;
import com.example.abc.pos.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class ExpandOrderSaleAdapter extends BaseExpandableListAdapter {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;

    private Context context;

    private List<OrdersTemp> ordersTempList;

    private OnItemClickGroup listener;

    private int selectedGroup = -1;

    private List<Integer> positionsSelected = new ArrayList<>();

    private List<ItemSelected> selectedList = new ArrayList<>();

    private SessionApp.OrderOrPayment orderOrPayment;

    private boolean isAddNewOrder = false;

    public interface OnItemClickGroup {
        void onItemClickGroupListener(List<Integer> positions, OrdersTemp ordersTemp);

        void onItemHeaderListener(OrdersTemp ordersTemp);

        void onItemSelectedListener(int positionSelected, OrdersTemp ordersTemp);
    }

    private ExpandableListView orderTempExpandList;

    public ExpandOrderSaleAdapter(Context context, List<OrdersTemp> ordersTempList, SessionApp.OrderOrPayment orderOrPayment, OnItemClickGroup listener) {

        this.context = context;
        this.ordersTempList = ordersTempList;
        this.orderOrPayment = orderOrPayment;
        this.listener = listener;
    }

    public void setSelectedList(int positionSelected) {
        if (positionSelected != -1) {
            positionsSelected = new ArrayList<>();
            positionsSelected.add(Integer.valueOf(positionSelected));
            selectedList.get(positionSelected).isSelected = true;
            notifyDataSetChanged();
        }

    }

    public void setNewDatas(List<OrdersTemp> ordersTempList) {
        this.ordersTempList = ordersTempList;
        if (ordersTempList != null && ordersTempList.size() > 1) {
//            selectedList = new ArrayList<>();
            for (int i = 0; i < ordersTempList.size(); i++) {
                ItemSelected ob = new ItemSelected();
                selectedList.add(ob);
            }
        } else {
            selectedList.add(new ItemSelected());
        }

        notifyDataSetChanged();
    }

    public void setMarkSign() {
        isAddNewOrder = true;
        notifyDataSetChanged();
    }

    public void addNewItem(OrdersTemp ordersTemp) {
        this.ordersTempList.add(ordersTemp);
        notifyDataSetChanged();
    }

    @Override
    public ExtraItem getChild(int listPosition, int expandedListPosition) {
        try {
            return this.ordersTempList.get(listPosition).getExtraItemList()
                    .get(expandedListPosition);
        } catch (IndexOutOfBoundsException ex) {
            return new OrdersTemp().getExtraItemList()
                    .size() > 0 ? new OrdersTemp().getExtraItemList()
                    .get(0) : new ExtraItem();
        }
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {


        ViewHolderChild holder;
        if (convertView == null) {
            holder = new ViewHolderChild();

            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item_extra_item, null);

            holder.tv_item_extra_name = convertView.findViewById(R.id.tv_item_extra_name);
            holder.tv_price = convertView.findViewById(R.id.tv_price);
            holder.tv_thanhtien = convertView.findViewById(R.id.tv_thanhtien);
            holder.ll_wrap_child = convertView.findViewById(R.id.ll_wrap_child);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderChild) convertView.getTag();
            holder.tv_item_extra_name.setText("");
            holder.tv_price.setText("");
            holder.tv_thanhtien.setText("");
        }


        final String name = getChild(listPosition, expandedListPosition).getName();
        final double value = getChild(listPosition, expandedListPosition).getValue() > 0 ? getChild(listPosition, expandedListPosition).getValue() : 0.0;
        final WapriManager.FeatureOrder flag = getChild(listPosition, expandedListPosition).getFlag();
        String inlaimonfromtablet = getGroup(listPosition).getInlaimonfromtablet();
        String daPrintChua = getGroup(listPosition).getDaprintchua();
        holder.tv_item_extra_name.setText(name);

        /* set color if isPrinted */


        if (flag == WapriManager.FeatureOrder.DEM_VE || flag == WapriManager.FeatureOrder.GHI_CHU_BEP) {
            holder.tv_price.setVisibility(View.INVISIBLE);
        } else {
            holder.tv_price.setVisibility(View.VISIBLE);
        }

        if (orderOrPayment == SessionApp.OrderOrPayment.PAYMENT) {
            if (flag == WapriManager.FeatureOrder.DEM_VE || flag == WapriManager.FeatureOrder.GIAM || flag == WapriManager.FeatureOrder.GHI_CHU_BEP || flag == WapriManager.FeatureOrder.GIA_TU_DO) {
                holder.tv_thanhtien.setVisibility(View.INVISIBLE);
            } else {
                holder.tv_thanhtien.setVisibility(View.VISIBLE);
            }
            if (listPosition % 2 != 0) {
                holder.ll_wrap_child.setBackgroundColor(Color.parseColor("#E6E6E6"));
            } else {
                holder.ll_wrap_child.setBackgroundColor(Color.WHITE);
            }
            holder.tv_item_extra_name.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_MEDIUM));
            holder.tv_item_extra_name.setTextColor(Color.parseColor("#666666"));
            holder.tv_price.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_MEDIUM));
            holder.tv_price.setTextColor(Color.parseColor("#666666"));
        } else {
            holder.tv_thanhtien.setVisibility(View.INVISIBLE);

            if (inlaimonfromtablet.equals("Y") || daPrintChua.equals("Y")) {
                holder.tv_item_extra_name.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
                holder.tv_price.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
            } else {
                holder.tv_item_extra_name.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
                holder.tv_price.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
            }
        }

        if (flag == WapriManager.FeatureOrder.GIAM) {
            holder.tv_price.setText(String.format("%,.2f", value) + "");
            holder.tv_thanhtien.setText(String.format("%,.2f", value) + "");
        }

        if (flag == WapriManager.FeatureOrder.GIA_VI || flag == WapriManager.FeatureOrder.DEM_VE || flag == WapriManager.FeatureOrder.GIAM ||
                flag == WapriManager.FeatureOrder.GHI_CHU_BEP || flag == WapriManager.FeatureOrder.GIA_TU_DO || flag == WapriManager.FeatureOrder.PHAN_AN) {
            holder.tv_thanhtien.setVisibility(View.INVISIBLE);
        } else {
            holder.tv_thanhtien.setVisibility(View.VISIBLE);
        }

        if (flag == WapriManager.FeatureOrder.GIA_VI || flag == WapriManager.FeatureOrder.GIA_TU_DO || flag == WapriManager.FeatureOrder.PHAN_AN) {
            holder.tv_price.setText(String.format("%,.2f", value) + "");
            holder.tv_thanhtien.setText(String.format("%,.2f", value) + "");
        }

        return convertView;
    }

    private class ViewHolderChild {
        public TextView tv_item_extra_name, tv_price, tv_thanhtien;
        public LinearLayout ll_wrap_child;

    }


    @Override
    public int getChildrenCount(int listPosition) {
        return ordersTempList.get(listPosition).getExtraItemList() != null && ordersTempList.get(listPosition).getExtraItemList()
                .size() > 0 ? ordersTempList.get(listPosition).getExtraItemList()
                .size() : 0;
    }

    @Override
    public OrdersTemp getGroup(int listPosition) {
        try {
            return this.ordersTempList.get(listPosition);
        } catch (IndexOutOfBoundsException ex) {
            return this.ordersTempList.size() > 0 ? this.ordersTempList.get(0) : new OrdersTemp();
        }
    }

    @Override
    public int getGroupCount() {
        return this.ordersTempList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }


    private int lastItemPostionSelected = -1;

    private class ViewHolderGroup implements View.OnClickListener {
        public TextView tv_number, tv_item_id, tv_item_name, tv_orig_price, tv_price_total, tv_dotdat;
        public LinearLayout ll_wrap_group, ll_group_dotdat, ll_wrap_text;
        public ImageView img_mark;
        private int positionSelected;
        private ViewGroup parentView;

        public void setPosition(int positionSelected) {
            this.positionSelected = positionSelected;
        }

        public ViewHolderGroup(View view, ViewGroup parentView, int positionSelected) {
            this.positionSelected = positionSelected;
            this.parentView = parentView;
            tv_item_name = view.findViewById(R.id.tv_item_name);
            tv_number = view.findViewById(R.id.tv_number);
            tv_orig_price = view.findViewById(R.id.tv_orig_price);
            tv_price_total = view.findViewById(R.id.tv_price_total);
            tv_dotdat = view.findViewById(R.id.tv_dotdat);
            img_mark = view.findViewById(R.id.img_mark);
            ll_wrap_group = view.findViewById(R.id.ll_wrap_group);
            ll_group_dotdat = view.findViewById(R.id.ll_group_dotdat);
            ll_wrap_text = view.findViewById(R.id.ll_wrap_text);
            ll_group_dotdat.setOnClickListener(this);
            ll_wrap_group.setTag(positionSelected);
            ll_wrap_group.setOnClickListener(this);
            view.setTag(positionSelected);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ll_wrap_group:

                    if (!selectedList.get(positionSelected).isSelected) {
                        ((ExpandableListView) parentView).smoothScrollToPosition(positionSelected + 1);
                        positionsSelected.add(Integer.valueOf(positionSelected));
                        selectedList.get(positionSelected).isSelected = true;
                        ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_CHOSE));
                        if (positionsSelected.size() < 2)
                            listener.onItemSelectedListener(positionSelected, ordersTempList.get(positionSelected));
                    } else {
                        ((ExpandableListView) parentView).smoothScrollToPosition(positionSelected - 1);
                        positionsSelected.remove(Integer.valueOf(positionSelected));
                        selectedList.get(positionSelected).isSelected = false;
                        ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_NOT_CHOSE));
                    }

                    lastItemPostionSelected = positionSelected;
                    notifyDataSetChanged();

                    listener.onItemClickGroupListener(positionsSelected, ordersTempList.get(positionSelected));

                    break;
                case R.id.ll_group_dotdat:
                    listener.onItemHeaderListener(ordersTempList.get(positionSelected));
                    break;
            }
        }

    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {


        ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(listPosition);
        ViewHolderGroup holder;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_header_item, null);
            holder = new ViewHolderGroup(convertView, parent, listPosition);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderGroup) convertView.getTag();
            holder.setPosition(listPosition);
            holder.tv_item_name.setText("");
            holder.tv_number.setText("");
            holder.tv_orig_price.setText("");
            holder.tv_price_total.setText("");
        }


        if (lastItemPostionSelected != -1) {
            if (lastItemPostionSelected == listPosition) {
                holder.img_mark.setVisibility(View.VISIBLE);
            } else {
                holder.img_mark.setVisibility(View.INVISIBLE);
            }
        }


        /** set font family **/
        holder.tv_item_name.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_BOLD));
        holder.tv_number.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_BOLD));
        holder.tv_orig_price.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_BOLD));
        holder.tv_price_total.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_BOLD));


        String ten_hang = getGroup(listPosition).getTenhang();
        int so_luong = getGroup(listPosition).getSoluong();
        String danhdaubixoa = getGroup(listPosition).getDanhdaubixoa();
        String item_id = getGroup(listPosition).getitems_id();
        double orig_price = getGroup(listPosition).getorig_price_no_extra();
        double total_price = getGroup(listPosition).getThanhtien();
        String inlaimonfromtablet = getGroup(listPosition).getInlaimonfromtablet();
        String daPrintChua = getGroup(listPosition).getDaprintchua();


        /*
        kiem tra neu id 1001 la dot dat ke tiep thi show header */
        if (item_id != null) {
            if (item_id.equals("1001")) {
                holder.ll_wrap_group.setVisibility(View.GONE);
                holder.ll_group_dotdat.setVisibility(View.VISIBLE);
                holder.tv_dotdat.setText(ten_hang);
            } else {
                holder.ll_wrap_group.setVisibility(View.VISIBLE);
                holder.ll_group_dotdat.setVisibility(View.GONE);
            }
        }


        // set color
        if (inlaimonfromtablet.equals("Y") || daPrintChua.equals("Y")) {
            holder.tv_number.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
            holder.tv_item_name.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
            holder.tv_number.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
            holder.tv_orig_price.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
            holder.tv_price_total.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_PRINTED));
        } else {
            holder.tv_number.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
            holder.tv_item_name.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
            holder.tv_number.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
            holder.tv_orig_price.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
            holder.tv_price_total.setTextColor(Color.parseColor(Constants.ColorConst.TEXT_ORDER_NOT_PRINTED));
        }



        /*
            kiem tra neu start tu chuc nang order hay chuc nang thanh toan
        * */
        if (orderOrPayment == SessionApp.OrderOrPayment.ORDER) {
            holder.tv_orig_price.setVisibility(View.VISIBLE);
//            for(ItemSelected itemSelected: selectedList){
//                Log.d("/////",itemSelected.toString());
//            }
            if (listPosition < selectedList.size()) {
                if (selectedList.get(listPosition).isSelected) {
                    holder.ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_CHOSE));
                } else {
                    holder.ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_NOT_CHOSE));
                }
            }


        } else {
            holder.ll_group_dotdat.setVisibility(View.GONE);


            holder.tv_item_name.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_MEDIUM));
            holder.tv_item_name.setTextColor(Color.parseColor("#666666"));
            holder.tv_number.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_MEDIUM));
            holder.tv_number.setTextColor(Color.parseColor("#666666"));
            holder.tv_orig_price.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_MEDIUM));
            holder.tv_orig_price.setTextColor(Color.parseColor("#666666"));
            holder.tv_price_total.setTypeface(ResourcesCompat.getFont(context, Constants.ColorConst.BSD_MEDIUM));
            holder.tv_price_total.setTextColor(Color.parseColor("#666666"));


            if (listPosition % 2 != 0) {
                holder.ll_wrap_group.setBackgroundColor(Color.parseColor("#E6E6E6"));
            } else {
                holder.ll_wrap_group.setBackgroundColor(Color.WHITE);
            }

//            holder.tv_orig_price.setVisibility(View.INVISIBLE);

//            holder.ll_wrap_group.setBackgroundColor(Color.parseColor(Constants.ColorConst.BG_ORDER_EVEN));
        }

        /* set data vao thoi */

        holder.tv_item_name.setText(ten_hang);


        holder.tv_number.setText(so_luong + " x " + item_id);


        holder.tv_orig_price.setText(String.format("%,.2f", orig_price));


        holder.tv_price_total.setText(String.format("%,.2f", total_price));

        if (danhdaubixoa != null) {
            if (danhdaubixoa.equals("Y")) {
                holder.ll_wrap_text.setBackground(context.getResources().getDrawable(R.drawable.strike_through));
                holder.tv_price_total.setText("0.0");
            } else {
                holder.ll_wrap_text.setBackground(null);
                holder.tv_price_total.setText(String.format("%,.2f", total_price));
                if (so_luong < 1) {
                    holder.ll_wrap_text.setBackground(context.getResources().getDrawable(R.drawable.strike_through));
                    holder.tv_price_total.setText(String.format("%,.2f", 0.00));
                } else {
                    holder.ll_wrap_text.setBackground(null);
                    holder.tv_price_total.setText(String.format("%,.2f", total_price));
                }
            }
        } else {
            if (so_luong < 1) {
                holder.ll_wrap_text.setBackground(context.getResources().getDrawable(R.drawable.strike_through));
                holder.tv_price_total.setText(String.format("%,.2f", 0.00));
            } else {
                holder.ll_wrap_text.setBackground(null);
                holder.tv_price_total.setText(String.format("%,.2f", total_price));
            }
        }


        return convertView;
    }

    private class ItemSelected {
        public boolean isSelected = false;

        @Override
        public String toString() {
            return "ItemSelected{" +
                    "isSelected=" + isSelected +
                    '}';
        }
    }

    public void resetSelectedList() {
        positionsSelected.clear();
        positionsSelected = new ArrayList<>();
        selectedList.clear();
        selectedList = new ArrayList<>();
        notifyDataSetChanged();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}
