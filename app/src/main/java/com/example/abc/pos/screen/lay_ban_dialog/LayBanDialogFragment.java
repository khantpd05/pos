package com.example.abc.pos.screen.lay_ban_dialog;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.KichCoMonAdapter;
import com.example.abc.pos.adapter.LayBanAdapter;
import com.example.abc.pos.base.SessionApp;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.base.WapriManager.KichCoMon;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.KichCoMonAn;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class LayBanDialogFragment extends DialogFragment implements LayBanAdapter.ItemClickListener {

    RecyclerView rc_layBan;
    LayBanAdapter adapter;
    OnItemTypeSelected listener;
    List<String> datas;
    public interface OnItemTypeSelected{
        void onItemTypeSelectedListener(String data);
    }
    public LayBanDialogFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public LayBanDialogFragment( List<String> datas, OnItemTypeSelected listener) {
        this.datas = datas;
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment.

        return inflater.inflate(R.layout.fragment_lay_ban_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rc_layBan = view.findViewById(R.id.rc_layBan);
        adapter = new LayBanAdapter(getContext(), datas,this);
        rc_layBan.setLayoutManager(new LinearLayoutManager(getContext()));
        rc_layBan.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        getDialog().getWindow().setLayout((6 * width)/7, RelativeLayout.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onItemClick(int position, String data) {
        listener.onItemTypeSelectedListener(data);
        getDialog().dismiss();
    }
}
