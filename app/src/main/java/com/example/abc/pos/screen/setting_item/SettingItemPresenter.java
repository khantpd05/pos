package com.example.abc.pos.screen.setting_item;

import android.os.AsyncTask;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.Items;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class SettingItemPresenter implements SettingItemConstract.Presenter  {

    private SettingItemConstract.View mView;

    public SettingItemPresenter(SettingItemConstract.View mView) {
        this.mView = mView;
    }


    @Override
    public void loadData(String menu_id) {
        new MyTask(menu_id).execute();
    }

    @Override
    public void deleteRow(String id) {
        new DeleteFromItemsTask(id).execute();
    }

    private class MyTask extends AsyncTask<Void,Void,List<Items>> {
        String menu_id;
        public MyTask(String menu_id){
            this.menu_id = menu_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected ArrayList<Items> doInBackground(Void... voids) {
            JSONArray js = MySqlHandleUtils.selectStament("SELECT * FROM wapri.items i" +
                    " where "+menu_id+ " = i.nhomhang");
            ArrayList<Items> items =  new Gson().fromJson(js.toString(), new TypeToken<List<Items>>(){}.getType());
            return items;
        }

        @Override
        protected void onPostExecute(List<Items> items) {
            super.onPostExecute(items);
            mView.dismissLoading();
            mView.showListItems(items);
        }
    }

    private class DeleteFromItemsTask extends AsyncTask<Void,Void,Integer> {
        String id;
        public DeleteFromItemsTask(String id) {
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int result =  MySqlHandleUtils.deleteRecordFromItem(id);

            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            mView.dismissLoading();
            if(result == 1){
                mView.showAlert(App.Companion.self().getString(R.string.delete_successfully));
            }else{
                mView.showAlert(App.Companion.self().getString(R.string.delete_failed));
            }
        }
    }
}
