package com.example.abc.pos.screen.login


import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.TELEPHONY_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.example.abc.pos.App

import com.example.abc.pos.R
import com.example.abc.pos.base.BaseActivity
import com.example.abc.pos.base.BaseFragment
import com.example.abc.pos.databinding.FragmentLoginBinding
import com.example.abc.pos.screen.setting.SettingFragment
import com.example.abc.pos.screen.table.TableFragment
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.telephony.TelephonyManager
import com.example.abc.pos.base.SessionApp
import com.example.abc.pos.base.WapriManager
import com.example.abc.pos.screen.login_otp.LoginOTPFragment
import com.example.abc.pos.screen.money_box.MoneyBoxFragment
import com.example.abc.pos.utils.*

class LoginFragment : BaseFragment(), LoginConstract.View {
    override fun dismissAlert() {
        Util.dismissCustomDialog()
    }

    override fun showAlert() {
        Util.showCustomDialog(context,"Waiting...")
    }


    private var REQUEST_READ_PHONE_STATE = 1
    private var REQUEST_WRITE_EXTERNAL_STORAGE = 1
    private var imei = ""

    var loginPresenter: LoginPresenter? = null
    private val KEY_DEVICE_NAME = "device_named"

    override fun getFragmentLayoutId(): Int {
        return R.layout.fragment_login
    }




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        setLanguage()
        getDeviceInfo()
        var mBinding: FragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        var view = mBinding.root
        checkWifi()
        var edt_pw = view.findViewById<EditText>(R.id.edt_pw)
        edt_pw.keyListener = null
        loginPresenter = LoginPresenter(this)
        mBinding.presenter = loginPresenter
        getDeviceName()
        return view
    }

    override fun getDeviceId(): String {

        return imei
    }


    fun getDeviceInfo() {

//        var manager = context?.getSystemService(Context.WIFI_SERVICE) as WifiManager
//        var info = manager.connectionInfo
//        var address = info.getMacAddress()
//        imei = Util.getMacAddr()


        if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(activity!!,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        REQUEST_WRITE_EXTERNAL_STORAGE)
            } else {
                ActivityCompat.requestPermissions(activity!!,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        REQUEST_WRITE_EXTERNAL_STORAGE)
            }
        }


//        if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
//                            Manifest.permission.READ_CONTACTS)) {
//                ActivityCompat.requestPermissions(activity!!,
//                        arrayOf(Manifest.permission.READ_PHONE_STATE),
//                        REQUEST_READ_PHONE_STATE)
//            } else {
//                ActivityCompat.requestPermissions(activity!!,
//                        arrayOf(Manifest.permission.READ_PHONE_STATE),
//                        REQUEST_READ_PHONE_STATE)
//            }
//        } else {
//
//        }
    }

    override fun showToast(data: String) {
        if(activity!=null)
        KToast.show(activity, data, Toast.LENGTH_SHORT)
    }

    override fun goTables() {
        (activity as BaseActivity).pushFragment(TableFragment(), true)
    }

    override fun goSetting(isHavePermission: Boolean) {
        (activity as BaseActivity).pushFragment(SettingFragment(isHavePermission), true)
    }

    override fun goAny() {


    }

    override fun goMoneyBox() {
        (activity as BaseActivity).pushFragment(MoneyBoxFragment(), true)
    }

    override fun alertDialog(data: String?) {
        Util.showCustomDialog(context, data)
    }

    override fun closeApp() {
        var intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        startActivity(intent)
        val pid = android.os.Process.myPid()
        android.os.Process.killProcess(pid)
    }

    override fun checkWifi() {
        val connManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)

        if (mWifi.isConnected) {
            SessionApp.WIFI_STATE = 1
        } else {
            SessionApp.WIFI_STATE = 0
        }
    }

    private fun getDeviceName() {
        val device_name = SharedPrefs.getInstance().get(KEY_DEVICE_NAME, String::class.java)
        if (device_name != null && !device_name.isEmpty()) {
            WapriManager.DEVICE_NAME = device_name
        }
    }

    override fun onResume() {
        super.onResume()
        if(WapriManager.configuration().otpLogin.get()!!){
            (activity as BaseActivity).pushFragmentNotBackStack(LoginOTPFragment(), Constants.TransitionType.NONE, true)
        }
        setLanguage()
    }

    private fun setLanguage() {
        LanguageUtils.loadLocale()
    }



    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_READ_PHONE_STATE -> {
//                // If request is cancelled, the result arrays are empty.
//                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
//                    var tm = context?.getSystemService(TELEPHONY_SERVICE) as TelephonyManager
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                        imei = tm.getImei()
//                    } else {
//                        imei = tm.getDeviceId()
//                    }
//                    loginPresenter?.setImei(imei)
//                } else {
//                    imei = ""
//                }

                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }
}
