package com.example.abc.pos.screen.chuc_nang_order;

public interface ChucNangOrderConstract {
    interface View{
        void doFeatureClick(int key);
    }

    interface Presenter{
        void doFeatureClick(int key);
    }
}
