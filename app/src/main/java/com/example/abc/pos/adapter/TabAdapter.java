package com.example.abc.pos.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.R;

import java.util.ArrayList;
import java.util.List;

public class TabAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    @Override
    public int getCount() {
        return 2;
    }

    public int getCounts(){
        return mFragmentList.size();
    }

    public List<String> getmFragmentTitleList() {
        return mFragmentTitleList;
    }

    public View getCustomView(Context context, int pos) {
        View mView = LayoutInflater.from(context).inflate(R.layout.custom_tab_view, null);
        TextView mTextView =  mView.findViewById(R.id.tv_title);
        ImageView imageView = mView.findViewById(R.id.img_ic);
        mTextView.setText(getPageTitle(pos));
        if(pos == 0)
            imageView.setImageResource(R.drawable.monan_icon);
        else  imageView.setImageResource(R.drawable.monnuoc_icon);

        return mView;
    }
}