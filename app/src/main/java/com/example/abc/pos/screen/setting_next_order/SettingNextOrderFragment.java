package com.example.abc.pos.screen.setting_next_order;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abc.pos.App;
import com.example.abc.pos.R;
import com.example.abc.pos.adapter.NextOrderAdapter;
import com.example.abc.pos.base.BaseActivity;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.model.Tieudedotdat;
import com.example.abc.pos.screen.create_items_item.CreateItemsItemFragment;
import com.example.abc.pos.utils.KToast;
import com.example.abc.pos.utils.Util;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingNextOrderFragment extends BaseFragment implements SettingNextOrderContract.View,
        NextOrderAdapter.ItemClickListener, View.OnClickListener {

    private NextOrderAdapter adapter;
    private RecyclerView rc_next_order;
    private SettingNextOrderPresenter presenter;
    private LinearLayout ll_extra_feature, ll_exit;
    private EditText edit_name;
    private TextView btn_edit, btn_remove;
    private TextView tv_title;
    private Button btn_add;
    private Dialog dialog;
    private Tieudedotdat tieudedotdat;
    private int positionClicked;
    public SettingNextOrderFragment() {
        // Required empty public constructor
    }



    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_setting_next_order;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting_next_order, container, false);
        adapter = new NextOrderAdapter(new ArrayList<Tieudedotdat>(), this);
        rc_next_order = view.findViewById(R.id.rc_next_order);
        rc_next_order.setAdapter(adapter);
        rc_next_order.setLayoutManager(new LinearLayoutManager(getContext()));


        ll_extra_feature = view.findViewById(R.id.ll_extra_feature);
        ll_exit = view.findViewById(R.id.ll_exit);

        btn_edit = view.findViewById(R.id.btn_edit);
        btn_add = view.findViewById(R.id.btn_add);
        btn_remove = view.findViewById(R.id.btn_remove);

        btn_edit.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        btn_remove.setOnClickListener(this);
        ll_exit.setOnClickListener(this);


        presenter = new SettingNextOrderPresenter(this);
        return view;
    }

    @Override
    public void showNextOrderList(List<Tieudedotdat> datas) {
        adapter.setNewDatas(datas);
    }

    @Override
    public void showCustomDialog(final Tieudedotdat ob) {
        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.custom_dialog);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        dialog.getWindow().setLayout((width * 1),RelativeLayout.LayoutParams.WRAP_CONTENT);

        edit_name = dialog.findViewById(R.id.edt_name);
        btn_add = dialog.findViewById(R.id.btn_add);
        tv_title = dialog.findViewById(R.id.tv_title);

        if (ob != null) {
            tv_title.setText(App.Companion.self().getString(R.string.Edit));
            edit_name.setText(ob.getName());

        } else {
            tv_title.setText(App.Companion.self().getString(R.string.add_new));
        }

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ob != null) {
                    tieudedotdat.setName(edit_name.getText().toString());
                    presenter.updateOrderTask(tieudedotdat);
                } else {
                    presenter.insertOrderTask(edit_name.getText().toString());
                }

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void showAlert(String data) {
        if(getActivity()!=null)
            KToast.show(getActivity(), data, Toast.LENGTH_SHORT);
    }

    @Override
    public void hideEditStuff() {
        ll_extra_feature.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onItemClick(int position, Tieudedotdat item) {
        tieudedotdat = item;
        ll_extra_feature.setVisibility(View.VISIBLE);
        positionClicked = position;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:
                showCustomDialog(null);
                break;

            case R.id.btn_edit:
                showCustomDialog(tieudedotdat);
                break;

            case R.id.btn_remove: {
                Util.showCustomDialogConfirm(getContext(), Util.getStringFromResouce(R.string.are_u_w_to_delete), new Util.ConfirmDialogInterface() {
                    @Override
                    public void onPositiveClickListener() {
                        presenter.deleteRow(tieudedotdat.getId());
                        adapter.removeItem(positionClicked);
                    }
                    @Override
                    public void onNegativeClickListener() {

                    }
                });

//                Util.alertPromp(getContext(), App.Companion.self().getString(R.string.Warning), App.Companion.self().getString(R.string.are_u_w_to_delete), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                }, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });

                break;
            }
            case R.id.ll_exit:
                Util.popFragment(getActivity());
                break;
        }
    }
}
