package com.example.abc.pos.model;

import com.example.abc.pos.base.WapriManager;

public class ExtraItem {
   private String name = "";
   private double value = 0.0;
   private int soluong = 0;
   private WapriManager.FeatureOrder flag = WapriManager.FeatureOrder.DEM_VE;

    public ExtraItem(String name, double value) {
        this.name = name;
        this.value = value;
    }

    public ExtraItem(String name, double value, WapriManager.FeatureOrder flag) {
        this.name = name;
        this.value = value;
        this.flag = flag;
    }

    public ExtraItem() {
    }

    public WapriManager.FeatureOrder getFlag() {
        return flag;
    }

    public void setFlag(WapriManager.FeatureOrder flag) {
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }
}
