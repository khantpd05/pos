package com.example.abc.pos.screen.create_items_item;

import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;

import java.util.List;

public interface CreateItemsItemConstract {
    interface View{
        void showSpinnerData(List<String> datas);
        void showLoading();
        void dismissLoading();
        void showAlert(String data);
        void initUpdateField();
        void goBack();
    }
    interface Presenter{
        void loadData();
        void addNewItem(String item_id, int nhom_hang, String name, double price, String color, String loai);
        void updateItem(String old_id,Items item);
    }
}
