package com.example.abc.pos.screen.kitchen_note;

import android.os.AsyncTask;

import com.example.abc.pos.R;
import com.example.abc.pos.database.MySqlHandleUtils;
import com.example.abc.pos.model.ItemsMontuychon;
import com.example.abc.pos.model.KitchenNote;
import com.example.abc.pos.model.Spice;
import com.example.abc.pos.screen.spice.SpicePresenter;
import com.example.abc.pos.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class KitchenNotePresenter implements KitchenNoteContract.Presenter {
    private KitchenNoteContract.View mView;
    private List<KitchenNote> kitchenNoteList;
    private List<ItemsMontuychon> kitchenNoteItemList;
    public KitchenNotePresenter(KitchenNoteContract.View mView) {
        this.mView = mView;
    }


    @Override
    public void loadData(List<KitchenNote> datas) {
        if(datas!=null && !datas.isEmpty()){
            kitchenNoteList = datas;
            mView.showListKitchenNote(kitchenNoteList);
        }else {
            kitchenNoteList = new ArrayList<>();
        }
        new SelectKitcheNoteItemTask("1").execute();
    }

    @Override
    public void getAllPageSpiceItem(String id) {
        new SelectKitcheNoteItemTask(id).execute();
    }

    @Override
    public void getKitchenNoteList() {
        mView.getKitchenNoteList(kitchenNoteList);
    }

    @Override
    public void deletePriceItem(int positionSelected) {
        kitchenNoteList.remove(positionSelected);
        mView.showListKitchenNote(kitchenNoteList);
    }

    @Override
    public void addNewItem(String data) {
        if(!data.trim().isEmpty()){
            KitchenNote kitchenNote = new KitchenNote();
            kitchenNote.setName(data);
            if(kitchenNoteList.size()<2){
                kitchenNoteList.add(kitchenNote);
            }else{
                mView.showAlert(Util.getStringFromResouce(R.string.the_maximum_kitchen));
            }

            mView.showListKitchenNote(kitchenNoteList);
            mView.setEditTextEmpty();
        }else {
            mView.showAlert(Util.getStringFromResouce(R.string.please_enter_full_information));
        }
    }

    private class SelectKitcheNoteItemTask extends AsyncTask<Void,Void,List<ItemsMontuychon>> {
        String id;

        public SelectKitcheNoteItemTask(String id) {
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mView.showLoading();
        }

        @Override
        protected List<ItemsMontuychon> doInBackground(Void... voids) {
            kitchenNoteItemList =  MySqlHandleUtils.selectKitchenNoteStament(id);
            return  kitchenNoteItemList;
        }

        @Override
        protected void onPostExecute(List<ItemsMontuychon> itemsMontuychons) {
            super.onPostExecute(itemsMontuychons);
            mView.dismissLoading();
            if(itemsMontuychons!=null && !itemsMontuychons.isEmpty()) mView.showListKitchenNoteItem(itemsMontuychons);
            else {
                List<ItemsMontuychon>  spiceItemList = new ArrayList<>();
                for(int i =0 ; i <6 ; i++){
                    for(int j = 0 ; j < 12; j++){
                        ItemsMontuychon ob = new ItemsMontuychon();
                        spiceItemList.add(ob);
                    }
                }
                mView.showListKitchenNoteItem(spiceItemList);
            }
        }
    }
}
