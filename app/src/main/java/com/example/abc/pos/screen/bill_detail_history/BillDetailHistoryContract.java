package com.example.abc.pos.screen.bill_detail_history;

import com.example.abc.pos.model.Orders;
import com.example.abc.pos.model.OrdersDetail;

import java.util.List;

public interface BillDetailHistoryContract {
    interface View{
        void showBillDetailHistory(List<OrdersDetail> ordersDetailList);
        void showBillInfo(String tableName, String billCode, String time, String summer);
        void showLoading();
        void dismissLoading();
    }

    interface Presenter{
        void loadBillHistoryDetailList(String orderId);
        void showBillInfo();
    }
}
