package com.example.abc.pos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abc.pos.R;
import com.example.abc.pos.base.WapriManager;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {
    private List<Items> mItems;
    private ItemClickListener mClickListener;
    private Context mContext;
    public ItemAdapter(Context context, List<Items> items, ItemClickListener clickListener){
        mContext = context;
        mItems = items;
        mClickListener = clickListener;
    }

    public void setNewDatas(List<Items> items){
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(position == 0){
            holder.img_back.setVisibility(View.VISIBLE);
            holder.card_view.setBackgroundColor(Color.parseColor("#000000"));
            holder.fr_wrap_text.setVisibility(View.GONE);
        }else{
            holder.img_back.setVisibility(View.GONE);
            holder.fr_wrap_text.setVisibility(View.VISIBLE);
            holder.myTextView.setText(mItems.get(position).getTenhang());
            holder.tv_item_id.setText(mItems.get(position).getId());
            try{
                if(mItems.get(position).getColor()!=null)
                    holder.fr_wrap_text.setBackgroundColor(Integer.valueOf(mItems.get(position).getColor()));
                else
                    holder.fr_wrap_text.setBackgroundColor(Color.parseColor("#bebebe"));



            }catch (Exception ex){

            }
        }

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView myTextView,tv_item_id;
        ImageView img_back;
        LinearLayout card_view;
        FrameLayout fr_wrap_text;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.info_text);
            tv_item_id = itemView.findViewById(R.id.tv_item_id);
            img_back = itemView.findViewById(R.id.img_back);
            fr_wrap_text = itemView.findViewById(R.id.fr_wrap_text);
            card_view = itemView.findViewById(R.id.card_view);
            if(WapriManager.configuration().showItemCode.get())
                tv_item_id.setVisibility(View.VISIBLE);
            else
                tv_item_id.setVisibility(View.GONE);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(getAdapterPosition(), mItems.get(getAdapterPosition()));

        }

        @Override
        public boolean onLongClick(View view) {
            if (mClickListener != null) mClickListener.onItemLongClick(getAdapterPosition(), mItems.get(getAdapterPosition()));
            return true;
        }
    }

    public interface ItemClickListener {
        void onItemClick(int position, Items item);
        void onItemLongClick(int position, Items item);
    }
}
