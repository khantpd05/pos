package com.example.abc.pos.screen.tab_of_sale.menu;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.view.menu.MenuAdapter;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abc.pos.R;
import com.example.abc.pos.adapter.ItemAdapter;
import com.example.abc.pos.adapter.MenusAdapter;
import com.example.abc.pos.base.BaseFragment;
import com.example.abc.pos.model.Items;
import com.example.abc.pos.model.Menu;
import com.example.abc.pos.screen.order.OrderFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class MenuTabFragment extends BaseFragment implements MenuTabConstract.View,
        MenusAdapter.ItemClickListener,
        ItemAdapter.ItemClickListener {



    public interface OnMenuItemClick{
        void onMenuItemClickListener(Items item);
        void onMenuItemLongClickListener(Items item);
    }

    private OnMenuItemClick mListener;
    private MenuTabPresenter presenter;
    private MenusAdapter adapter_menu;
    private RecyclerView rc_menu;

    private RecyclerView rc_item;
    private ItemAdapter adapter_item;
    public MenuTabFragment() {

        // Required empty public constructor
    }

    public MenuTabFragment(OnMenuItemClick itemClick) {
        mListener = itemClick;
        // Required empty public constructor
    }


    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_menu_tab;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu_tab, container, false);

        //menu
        rc_menu = view.findViewById(R.id.rc_menu);
        rc_menu.setLayoutManager(new GridLayoutManager(getContext(),3));

        adapter_menu = new MenusAdapter(getContext(),new ArrayList<Menu>(), this);
        rc_menu.setAdapter(adapter_menu);

        //item
        rc_item = view.findViewById(R.id.rc_item);
        adapter_item = new ItemAdapter(getContext(),new ArrayList<Items>(), this);
        rc_item.setLayoutManager(new GridLayoutManager(getContext(),3));
        rc_item.setAdapter(adapter_item);


        presenter = new MenuTabPresenter(this);

        if(mListener==null){
            mListener = OrderFragment.getOrderFragmentListener();
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void showListMenus(List<Menu> menus) {
        adapter_menu.setNewDatas(menus);
    }

    @Override
    public void showListItems(List<Items> items) {
        adapter_item.setNewDatas(items);
        rc_item.setVisibility(View.VISIBLE);
    }


    @Override
    public void onItemClick(View view, Menu menu) {
//        mListener.onItemClickListener(menu);
        rc_menu.setVisibility(View.GONE);
        presenter.getItems(menu.getId()+"");
    }

    @Override
    public void onItemClick(int position, Items item) {

        if(position==0){
            rc_item.setVisibility(View.GONE);
            rc_menu.setVisibility(View.VISIBLE);
        }else{
            if(mListener==null){
                Log.d("/////","null");
            }else
            mListener.onMenuItemClickListener(item);
        }

    }

    @Override
    public void onItemLongClick(int position, Items item) {
        mListener.onMenuItemLongClickListener(item);
    }
}
