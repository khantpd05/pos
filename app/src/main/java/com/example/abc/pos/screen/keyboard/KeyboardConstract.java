package com.example.abc.pos.screen.keyboard;

import com.example.abc.pos.model.Connection;

import java.util.List;

public interface KeyboardConstract {
    interface View{
        void getDataInput(String data);

    }

    interface Presenter{
        void getDataInput();

    }
}
